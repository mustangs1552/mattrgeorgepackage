## [4.0.2] - 2/28/20
### [Fixed]
- GUID conficts bug when installing package.

## [4.0.1] - 2/28/20
### [Changed]
- InputControl to include functionality to work alone and not using a main class that it was using in the project it originated from.

## [4.0.0] - 2/28/20
### [Added]
- New Actions that follow the same or similar design patterns as the Zinnia package used by VRTK.
- A script with custom UnityEvents
- A script with helper classes.
### [Changed]
- Location of various classes within folders and namespaces.
- The coding design and practices to remain up to date with current practices.
- All events to use UnityEvents instead of C#'s Action and Func.

## [3.4.8] - 2/21/20
### [Fixed]
- A bug where the items stored was using an improper way of differentiating themselves in the inventory. Now using thier gameObject.name.

## [3.4.7] - 2/21/20
### [Added]
- A modular base inventory system. Inventory manages a list of InventorySlots that each contains an Item and the count of that Item.
- ReflectionUtility with two methods that can get/set the value of a non-public variable.

## [3.4.6] - 2/9/20
### [Fixed]
- Fixed ListUtility.GetObjectWithChanceByChance() when providing a list of objects that has chance values that are 0.
### [Added]
- ListUtility.GetObjectWithChanceByChance() mostly as a helper method for manually selecting an object from a list of ObjectWithChance by the chance values.
### [Changed]
- Name format of tests.

## [3.4.5] - 2/7/20
### [Fixed]
- Fixed bug with ListUtility.GetRandomObjectWithChance() where result would always be null.

## [3.4.4] - 2/6/20
### [Fixed]
- The possiblity of get random objects with chance methods in ListUtility to be messed up if a negative chance value is given.

## [3.4.3] - 2/6/20
### [Added]
- Added ListUtility.GetRandomObjectWithChance().

## [3.4.2] - 2/6/20
### [Fixed]
- Namespace name for previously added classes.

## [3.4.1] - 2/6/20
### [Added]
- New ListUtility class for utilities using Lists.
- New UnityMathUtilities class for utilities using Unity's math.
### [Fixed]
- Door not using local position resulting in endless movement with some orientations.

## [3.4.0] - 1/27/20
### [Fixed]
- Incorrect calculation of RangeStat.SetAmount().
### Added
- Tests for JSONParser.
- Tests for Stats and removed the runtime tests.
### Removed
- Runtime.General.Misc.MathUtility.

## [3.3.1] - 1/26/20
### Fixed
- Array out of range bug with ObjectPool.

## [3.3.0] - 1/4/20
### Added
- A new TransformUtility.GetClosest() that uses Transforms instead of Colliders.
### Changed
- Parameter 2 of TransformUtility.GetClosest() from Collider[] to List<Collider>.

## [3.2.0] - 12/20/19
### Changed
- Name of old Stat and DynamicStat to StatGauge and DynamicStatGauge repectively.
### Added
- New Stat class that represents a single adjustable value that all other stats would inherit from.
### Fixed
- Various bugs with Stats classes.

## [3.1.1] - 12/17/19
### Fixed
- A bug where object pool would return objects that didn't have the requested component due to them being "null" instead of null.

## [3.1.0] - 12/16/19
### Changed
- Now in the form of a [Custom Package](https://docs.unity3d.com/Manual/CustomPackages.html) instead of the old Asset Packages.
- Location of uGUITools to a 3rdParty folder.

## [3.0.3] - 12/15/19
### Added
- Added property to get the percentage of a RangeStat.

## [3.0.2] - 12/15/19
### Added
- New AIBrain.GetTrait() using the T class which doesn't require a cast from the user.
### Deprecated
- Old AIBrain.GetTrait() that doesn't use the T class.

## [3.0.1] - 12/14/19
### Added
- A door class in .Unity.Objects.

## [3.0.0] - 12/9/19
### Changed
- Namespace location of IObjectPoolEvents from .Unity.ObjectPooling.Interfaces to .Unity.ObjectPooling.
### Fixed
- Wrong caps of IObjectPoolEvents.

## [2.0.0] - 12/8/19
### Changed
- Changed MattRGeorge.Unity.Objects to .Unity.Misc.
### Added
- Day Night Cycle
- New misc. classes from Living World.

## [1.0.0] - 12/3/19
- Initial release.