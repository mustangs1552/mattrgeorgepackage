﻿using MattRGeorge.General.Tools;

namespace MattRGeorge.Unity.Tools.PortfolioSite.Models
{
    public class ScoreValue
    {
        public int id = 0;
        public int projectID = 0;
        public string playerName = "";
        public string scoreType = "";
        public string scoreAmount = "";

        public ScoreValue()
        {

        }
        public ScoreValue(JSONObject parsedJSON)
        {
            int.TryParse(parsedJSON.values["id"], out id);
            int.TryParse(parsedJSON.values["projectID"], out projectID);
            playerName = parsedJSON.values["playerName"];
            scoreType = parsedJSON.values["scoreType"];
            scoreAmount = parsedJSON.values["scoreAmount"];
        }
    }
}
