﻿using UnityEngine;
using UnityEngine.Networking;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using MattRGeorge.General.Tools;
using MattRGeorge.Unity.Tools.PortfolioSite.Models;

namespace MattRGeorge.Unity.Tools.PortfolioSiteAccess
{
    /// <summary>
    /// Used to access various functions on the portfolio site at MattRGeorge.com such as score and logging in/out (no logging in/out).
    /// </summary>
    public class ScoreController : MonoBehaviour
    {
        [Tooltip("The ID of the game in the site's database.")]
        [SerializeField] private int serverGameID = 0;

        /// <summary>
        /// Supported score types by the site.
        /// </summary>
        public List<string> SupportedScoreTypes
        {
            get
            {
                return supportedScoreTypes;
            }
        }

        private const string ROOT_DOMAIN_URL = "http://mattrgeorge.com/";
        private const string SCORES_CONTROLLER = "Scores/";
        private const string SAVE_SCORE_CONTROLLER = "SaveScore";
        private const string GET_SCORES_ACTION = "GetScores";
        private const string GET_USER_SCORES_ACTION = "GetUserScores";
        private const string GET_TOP_SCORES_ACTION = "GetTopScores";
        private const string GET_USER_TOP_SCORES_ACTION = "GetUserTopScores";
        private const string GET_SCORE_TYPES_ACTION = "GetScoreTypes";

        private List<string> supportedScoreTypes = new List<string>();

        #region Methods
        /// <summary>
        /// Start uploading the given score to the site.
        /// </summary>
        /// <param name="user">The username for the score.</param>
        /// <param name="type">The type of score (Points, Seconds, etc...).</param>
        /// <param name="score">The score value.</param>
        public void StartUploadScore(string user, string type, int score)
        {
            if (supportedScoreTypes == null || supportedScoreTypes.Count == 0 || user == null || user == "" || type == null || type == "") return;
            if (!supportedScoreTypes.Contains(type))
            {
                Debug.LogError("\"" + type + "\" score type is not supported by site!");
                return;
            }

            ScoreValue data = new ScoreValue()
            {
                playerName = user,
                scoreType = type,
                scoreAmount = score.ToString(),
            };
            StartCoroutine("UploadScore", data);
        }

        /// <summary>
        /// Start getting all scores for this game.
        /// </summary>
        /// <param name="callbackMethod">The callback method that will be called to send the resulting scores.</param>
        public void StartGettingScores(Func<List<ScoreValue>, bool> callbackMethod)
        {
            if (callbackMethod == null) return;

            GetScoreData data = new GetScoreData()
            {
                URLAction = GET_SCORES_ACTION,
                OnGotScores = callbackMethod,
            };
            StartCoroutine("GetScores", data);
        }
        /// <summary>
        /// Start getting all scores for this game for the given user.
        /// </summary>
        /// <param name="user">The user that the desired scores belong to.</param>
        /// <param name="callbackMethod">The callback method that will be called to send the resulting scores.</param>
        public void StartGettingScores(string user, Func<List<ScoreValue>, bool> callbackMethod)
        {
            if (user == null || user == "" || callbackMethod == null) return;

            GetScoreData data = new GetScoreData()
            {
                URLAction = GET_USER_SCORES_ACTION,
                User = user,
                OnGotScores = callbackMethod,
            };
            StartCoroutine("GetScores", data);
        }
        /// <summary>
        /// Start getting the top x scores for this game.
        /// </summary>
        /// <param name="topX">The amount of scores to get.</param>
        /// <param name="callbackMethod">The callback method that will be called to send the resulting scores.</param>
        public void StartGettingTopScores(int topX, Func<List<ScoreValue>, bool> callbackMethod)
        {
            if (topX <= 0 || callbackMethod == null) return;

            GetScoreData data = new GetScoreData()
            {
                URLAction = GET_TOP_SCORES_ACTION,
                TopX = topX,
                OnGotScores = callbackMethod,
            };
            StartCoroutine("GetScores", data);
        }
        /// <summary>
        /// Start getting the top x scores for this game for the given user.
        /// </summary>
        /// <param name="user">The user that the desired scores belong to.</param>
        /// <param name="topX">The amount of scores to get.</param>
        /// <param name="callbackMethod">The callback method that will be called to send the resulting scores.</param>
        public void StartGettingTopScores(string user, int topX, Func<List<ScoreValue>, bool> callbackMethod)
        {
            if (user == null || user == "" || topX <= 0 || callbackMethod == null) return;

            GetScoreData data = new GetScoreData()
            {
                URLAction = GET_USER_TOP_SCORES_ACTION,
                User = user,
                TopX = topX,
                OnGotScores = callbackMethod,
            };
            StartCoroutine("GetScores", data);
        }

        #region Private
        /// <summary>
        /// Gets the list of supported score types from the site.
        /// </summary>
        private IEnumerator GetSupportedScoreTypes()
        {
            using (UnityWebRequest www = UnityWebRequest.Get(ROOT_DOMAIN_URL + SCORES_CONTROLLER + GET_SCORE_TYPES_ACTION))
            {
                //www.certificateHandler = new AcceptAllSelfSignedCerts();
                Debug.Log("Getting score types...");
                yield return www.SendWebRequest();

                if (www.isNetworkError || www.isHttpError) Debug.LogError("Error while getting score types: " + www.error + "\n\tURL: " + www.url);
                else
                {
                    Debug.Log("Received score types! JSON:\n" + www.downloadHandler.text);
                    supportedScoreTypes = JSONParser.ParseJSON(www.downloadHandler.text).valueArrays["ROOT_ARRAY"];
                }
            }

            if (supportedScoreTypes == null || supportedScoreTypes.Count == 0) Debug.LogError("No supported score types found!");
            else
            {
                string debugStr = "Supported score types received (" + supportedScoreTypes.Count + "):";
                foreach (string type in supportedScoreTypes) debugStr += "\n\t- " + type;
                Debug.Log(debugStr);
            }
        }

        /// <summary>
        /// Uploads the given score to the site.
        /// </summary>
        /// <param name="data">The score information to be uploaded.</param>
        private IEnumerator UploadScore(ScoreValue data)
        {
            List<IMultipartFormSection> formData = new List<IMultipartFormSection>();
            formData.Add(new MultipartFormDataSection("projectID", serverGameID.ToString()));
            formData.Add(new MultipartFormDataSection("playerName", data.playerName));
            formData.Add(new MultipartFormDataSection("scoreType", data.scoreType));
            formData.Add(new MultipartFormDataSection("score", data.scoreAmount.ToString()));

            using (UnityWebRequest www = UnityWebRequest.Post(ROOT_DOMAIN_URL + SCORES_CONTROLLER + SAVE_SCORE_CONTROLLER, formData))
            {
                //www.certificateHandler = new AcceptAllSelfSignedCerts();
                Debug.Log("Uploading score...\n\tUsername: " + data.playerName + "\n\tScore Type: " + data.scoreType + "\n\tScore: " + data.scoreAmount);
                yield return www.SendWebRequest();

                if (www.isNetworkError || www.isHttpError) Debug.LogError("Error while uploading score: " + www.error + "\n\tURL: " + www.url);
                else Debug.Log("Uploaded score!");
            }
        }

        /// <summary>
        /// Gets the scores using the data given. Uses a provided callback method to send results.
        /// </summary>
        /// <param name="data">The data to be used when getting results.</param>
        private IEnumerator GetScores(GetScoreData data)
        {
            if ((data != null || data.OnGotScores != null) && (data.URLAction != null || data.URLAction != ""))
            {
                List<IMultipartFormSection> formData = new List<IMultipartFormSection>();
                formData.Add(new MultipartFormDataSection("projectID", serverGameID.ToString()));
                if (data.User != null && data.User != "") formData.Add(new MultipartFormDataSection("username", data.User));
                if (data.TopX > 0) formData.Add(new MultipartFormDataSection("topX", data.TopX.ToString()));

                using (UnityWebRequest www = UnityWebRequest.Post(ROOT_DOMAIN_URL + SCORES_CONTROLLER + data.URLAction, formData))
                {
                    //www.certificateHandler = new AcceptAllSelfSignedCerts();
                    Debug.Log("Getting" + ((data.TopX > 0) ? " Top " + data.TopX : "") + " scores" + ((data.User != null && data.User != "") ? " for " + data.User : "") + "...");
                    yield return www.SendWebRequest();

                    if (www.isNetworkError || www.isHttpError) Debug.LogError("Error while getting scores: " + www.error + "\n\tURL: " + www.url);
                    else
                    {
                        Debug.Log("Received scores! JSON:\n" + www.downloadHandler.text);

                        List<ScoreValue> scores = new List<ScoreValue>();
                        JSONObject parsedJSON = JSONParser.ParseJSON(www.downloadHandler.text);
                        foreach (JSONObject obj in parsedJSON.objArrays["ROOT_ARRAY"]) scores.Add(new ScoreValue(obj));
                        if (!data.OnGotScores(scores)) Debug.LogWarning("OnGetScores callback failed for some reason!");
                    }
                }
            }
        }
        #endregion
        #endregion

        private void OnValidate()
        {
            if (serverGameID < 1) serverGameID = 1;
        }

        private void Awake()
        {
            StartCoroutine("GetSupportedScoreTypes");
        }

        /// <summary>
        /// The options that are passed the single parameter IEnumerators getting scores from server.
        /// </summary>
        private class GetScoreData
        {
            public string URLAction { get; set; }
            public string User { get; set; }
            public int TopX { get; set; }
            public Func<List<ScoreValue>, bool> OnGotScores { get; set; }
        }
    }

    public class AcceptAllSelfSignedCerts : CertificateHandler
    {
        public static string PUBLIC_KEY = "PublicKey";

        protected override bool ValidateCertificate(byte[] certificateData)
        {
            X509Certificate2 cert = new X509Certificate2(certificateData);
            string pk = cert.GetPublicKeyString();
            if (pk.ToLower().Equals(PUBLIC_KEY.ToLower())) return true;

            return false;
        }
    }
}