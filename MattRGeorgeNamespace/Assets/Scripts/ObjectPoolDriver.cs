﻿using System.Collections.Generic;
using UnityEngine;
using MattRGeorge.Unity.Tools.ObjectPooling;

namespace Assets.Scripts
{
    class ObjectPoolDriver : MonoBehaviour
    {
        [SerializeField] private GameObject[] objectsToSpawn = null;
        [SerializeField] private int numOfSpawns = 10;

        private void GetAudioSources()
        {
            List<AudioSource> audioSources = ObjectPoolManager.SINGLETON.GetActiveObjectsComponents<AudioSource>();
            string debugMsg = "Found " + audioSources.Count + " Audio Sources:";
            foreach (AudioSource audioSource in audioSources) debugMsg += "\n    " + audioSource.gameObject.name;
            Debug.Log(debugMsg);
        }

        private void SpawnObjects()
        {
            for (int i = 0; i < numOfSpawns; i++) ObjectPoolManager.SINGLETON.Instantiate(objectsToSpawn[Random.Range(0, objectsToSpawn.Length)]);
        }

        private void Start()
        {
            SpawnObjects();
            GetAudioSources();
        }
    }
}
