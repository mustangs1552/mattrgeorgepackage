﻿using System;
using UnityEngine;
using MattRGeorge.Unity.Utilities.Static;

namespace Assets.Scripts
{
    public class fovDriver : MonoBehaviour
    {
        public Transform origin = null;
        public Transform target = null;
        public float fov = 180;

        protected bool result = false;

        protected virtual void OnDrawGizmos()
        {
            if (origin && target)
            {
                result = TransformUtility.CheckFOV(origin, target, fov);
                Gizmos.color = (result) ? Color.green : Color.red;
                Gizmos.DrawLine(origin.position, target.position);
            }
        }

        protected virtual void Awake()
        {
            if(!origin) origin = transform;

            if (!origin) Debug.LogError("No 'origin' set!");
            if (!target) Debug.LogError("No 'target' set!");
        }
    }
}
