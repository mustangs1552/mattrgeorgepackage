﻿using UnityEngine;
using MattRGeorge.Unity.Utilities.Static;
using MattRGeorge.Unity.Utilities.Enums;

namespace Assets.Scripts
{
    public class LayersTest : MonoBehaviour
    {
        public LayerMask layerMask = (int)LayerOptions.Nothing;

        public void Update()
        {
            if (Input.GetKeyUp(KeyCode.P)) layerMask = (int)LayerOptions.Everything;
            if (Input.GetKeyUp(KeyCode.O)) layerMask = (int)LayerOptions.Nothing;

            if (Input.GetKeyUp(KeyCode.L)) LayerUtility.AddLayerToLayerMask("Default", ref layerMask);
            if (Input.GetKeyUp(KeyCode.K)) LayerUtility.AddLayerToLayerMask("Ignore Raycast", ref layerMask);

            if (Input.GetKeyUp(KeyCode.M)) LayerUtility.RemoveLayerFromLayerMask("Default", ref layerMask);
            if (Input.GetKeyUp(KeyCode.N)) LayerUtility.RemoveLayerFromLayerMask("Ignore Raycast", ref layerMask);
        }
    }
}
