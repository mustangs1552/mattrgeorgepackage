﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using MattRGeorge.Unity.Tools.Stats;

namespace MattRGeorge.UnityTests.Unity.Tools.Stats
{
    public class HealthUnityTests
    {
        private Health healthObj = null;
        private List<HealthPiece> healthPieceObjs = new List<HealthPiece>();

        [UnityTest]
        public IEnumerator TakeDamageMultiple_Tests()
        {
            healthObj.TakeDamage(50);
            CheckHealth(50, 100, 0, 1, 1, true, healthPieceObjs);
            healthObj.TakeDamage(75);
            CheckHealth(50, 100, 0, 1, 1, true, healthPieceObjs);

            yield return new WaitForSeconds(.2f);
            healthObj.TakeDamage(75);
            CheckHealth(0, 100, 0, 1, 1, true, healthPieceObjs);
        }

        [UnityTest]
        public IEnumerator HealMultiple_Tests()
        {
            healthObj.TakeDamage(50);

            healthObj.Heal(25);
            CheckHealth(75, 100, 0, 1, 1, true, healthPieceObjs);
            healthObj.Heal(75);
            CheckHealth(75, 100, 0, 1, 1, true, healthPieceObjs);

            yield return new WaitForSeconds(.2f);
            healthObj.Heal(75);
            CheckHealth(100, 100, 0, 1, 1, true, healthPieceObjs);
        }

        // Note: Takes about 27.3 secs
        [UnityTest]
        public IEnumerator UpdatingHealth_Tests()
        {
            healthObj.CurrHealthUpdateAmount = -5;
            yield return new WaitForSeconds(1.1f);
            CheckHealth(95, 100, -5, 1, 1, true, healthPieceObjs);
            yield return new WaitForSeconds(1);
            CheckHealth(90, 100, -5, 1, 1, true, healthPieceObjs);
            yield return new WaitForSeconds(1);
            CheckHealth(85, 100, -5, 1, 1, true, healthPieceObjs);

            healthObj.CurrHealthUpdateAmount = 0;
            yield return new WaitForSeconds(1);
            CheckHealth(85, 100, 0, 1, 1, true, healthPieceObjs);
            yield return new WaitForSeconds(1);
            CheckHealth(85, 100, 0, 1, 1, true, healthPieceObjs);
            yield return new WaitForSeconds(1);
            CheckHealth(85, 100, 0, 1, 1, true, healthPieceObjs);

            healthObj.CurrHealthUpdateAmount = 1;
            yield return new WaitForSeconds(1);
            CheckHealth(86, 100, 1, 1, 1, true, healthPieceObjs);
            yield return new WaitForSeconds(1);
            CheckHealth(87, 100, 1, 1, 1, true, healthPieceObjs);
            yield return new WaitForSeconds(1);
            CheckHealth(88, 100, 1, 1, 1, true, healthPieceObjs);

            healthObj.UpdatingHealth = false;
            yield return new WaitForSeconds(1);
            CheckHealth(88, 100, 1, 1, 1, false, healthPieceObjs);
            yield return new WaitForSeconds(1);
            CheckHealth(88, 100, 1, 1, 1, false, healthPieceObjs);
            yield return new WaitForSeconds(1);
            CheckHealth(88, 100, 1, 1, 1, false, healthPieceObjs);

            healthObj.PassiveHealRate = 2;
            yield return new WaitForSeconds(2);
            CheckHealth(88, 100, 1, 2, 1, false, healthPieceObjs);
            yield return new WaitForSeconds(2);
            CheckHealth(88, 100, 1, 2, 1, false, healthPieceObjs);
            yield return new WaitForSeconds(2);
            CheckHealth(88, 100, 1, 2, 1, false, healthPieceObjs);

            healthObj.UpdatingHealth = true;
            yield return new WaitForSeconds(2.1f);
            CheckHealth(89, 100, 1, 2, 1, true, healthPieceObjs);
            yield return new WaitForSeconds(2);
            CheckHealth(90, 100, 1, 2, 1, true, healthPieceObjs);
            yield return new WaitForSeconds(2);
            CheckHealth(91, 100, 1, 2, 1, true, healthPieceObjs);

            healthObj.PassiveHealRate = 1;
            yield return new WaitForSeconds(1.1f);
            CheckHealth(92, 100, 1, 1, 1, true, healthPieceObjs);
            yield return new WaitForSeconds(1);
            CheckHealth(93, 100, 1, 1, 1, true, healthPieceObjs);
            yield return new WaitForSeconds(1);
            CheckHealth(94, 100, 1, 1, 1, true, healthPieceObjs);
        }

        private void CheckHealth(float currHealth, float currMaxHealth, float currHealthUpdate, float passiveHealRate, float dmgMulti, bool updatingHealth, List<HealthPiece> healthPieces)
        {
            Assert.AreEqual(currHealth, healthObj.CurrHealth);
            Assert.AreEqual(currMaxHealth, healthObj.CurrMaxHealth);
            Assert.AreEqual(currHealthUpdate, healthObj.CurrHealthUpdateAmount);
            Assert.AreEqual(passiveHealRate, healthObj.PassiveHealRate);
            Assert.AreEqual(dmgMulti, healthObj.damageMultiplier.CurrAmount);
            Assert.AreEqual(updatingHealth, healthObj.UpdatingHealth);

            List<HealthPiece> healthObjHealthPieces = healthObj.HealthPieces;
            Assert.IsNotNull(healthObjHealthPieces);
            if (healthPieces == null) Assert.AreEqual(0, healthObjHealthPieces.Count);
            else
            {
                Assert.AreEqual(healthPieces.Count, healthObjHealthPieces.Count);
                for (int i = 0; i < healthPieces.Count; i++) Assert.AreEqual(healthPieces[i], healthObjHealthPieces[i]);
            }
        }

        [SetUp]
        public void TestSetup()
        {
            GameObject obj = new GameObject("HealthObj");
            healthObj = obj.AddComponent<Health>();
            healthPieceObjs = new List<HealthPiece>();
            healthPieceObjs.Add(obj.AddComponent<HealthPiece>());
            healthObj.HealthPieces = healthPieceObjs;

            healthObj.ResetStats();
        }
        [TearDown]
        public void TearDown()
        {
            GameObject.Destroy(healthObj.gameObject);
            healthObj = null;
        }
    }
}
