﻿using System.Collections;
using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using MattRGeorge.Unity.Utilities.Static;
using MattRGeorge.Unity.Tools.ObjectPooling;

namespace MattRGeorge.UnityTests.Unity.Tools.ObjectPooling
{
    public class ObjectPoolManagerUnityTests
    {
        private ObjectPoolManager objPool = null;
        private bool onObjectMadeActiveRan = false;
        private bool onObjectMadeInactiveRan = false;
        private GameObject obj = null;
        private GameObject obj2 = null;
        private GameObject obj3 = null;
        private List<GameObject> instantiatedObjs = new List<GameObject>();
        private List<GameObject> destroyedObjs = new List<GameObject>();
        private GameObject dontAddObj = null;
        private string tagName = "Player";
        private List<GameObject> returnedObjs = new List<GameObject>();
        private List<Transform> returnedTrans = new List<Transform>();

        [UnityTest]
        public IEnumerator AllGameObjects_Tests()
        {
            yield return new WaitForEndOfFrame();

            InstantiateObjects(1);

            returnedObjs = objPool.AllGameObjects;
            Assert.IsNotNull(returnedObjs);
            Assert.IsNotEmpty(returnedObjs);
            CollectionAssert.AreEquivalent(instantiatedObjs, returnedObjs);

            DestroyObject(1);
            DestroyObject(3);
            returnedObjs = objPool.AllGameObjects;
            Assert.IsNotNull(returnedObjs);
            Assert.IsNotEmpty(returnedObjs);
            CollectionAssert.AreEquivalent(instantiatedObjs, returnedObjs);
        }

        [UnityTest]
        public IEnumerator AllTransforms_Tests()
        {
            yield return new WaitForEndOfFrame();

            InstantiateObjects(1);

            returnedTrans = objPool.AllTransforms;
            Assert.IsNotNull(returnedTrans);
            Assert.IsNotEmpty(returnedTrans);
            CollectionAssert.AreEquivalent(instantiatedObjs, TransformsToGameObjects(returnedTrans));

            DestroyObject(1);
            DestroyObject(3);
            returnedTrans = objPool.AllTransforms;
            Assert.IsNotNull(returnedTrans);
            Assert.IsNotEmpty(returnedTrans);
            CollectionAssert.AreEquivalent(instantiatedObjs, TransformsToGameObjects(returnedTrans));
        }

        #region Instatiate Tests
        [UnityTest]
        public IEnumerator Instantiate_AsActive_PoolManaged_Tests()
        {
            yield return new WaitForEndOfFrame();

            instantiatedObjs.Add(objPool.Instantiate(obj, true, true));
            Assert.IsNotNull(instantiatedObjs[0]);
            Assert.IsTrue(instantiatedObjs[0]);
            Assert.AreNotEqual(obj, instantiatedObjs[0]);
            Assert.IsTrue(onObjectMadeActiveRan);
            Assert.IsFalse(onObjectMadeInactiveRan);
            Assert.IsTrue(instantiatedObjs[0].activeInHierarchy);
            returnedObjs = objPool.AllGameObjects;
            Assert.IsNotNull(returnedObjs);
            Assert.IsNotEmpty(returnedObjs);
            Assert.AreEqual(1, returnedObjs.Count);
            Assert.AreEqual(instantiatedObjs[0], returnedObjs[0]);
            Assert.AreEqual(1, objPool.transform.GetChild(0).childCount);
            Assert.AreEqual($"{GameObjectUtility.GetActualName(obj).ToUpper()}_ANCHOR", objPool.transform.GetChild(0).GetChild(0).name);
            Assert.AreEqual(1, objPool.transform.GetChild(0).GetChild(0).childCount);
            Assert.AreEqual(instantiatedObjs[0], objPool.transform.GetChild(0).GetChild(0).GetChild(0).gameObject);

            ResetFlags();
            instantiatedObjs.Add(objPool.Instantiate(obj, true, true));
            Assert.IsNotNull(instantiatedObjs[1]);
            Assert.IsTrue(instantiatedObjs[1]);
            Assert.AreNotEqual(obj, instantiatedObjs[1]);
            Assert.IsTrue(onObjectMadeActiveRan);
            Assert.IsFalse(onObjectMadeInactiveRan);
            Assert.IsTrue(instantiatedObjs[1].activeInHierarchy);
            returnedObjs = objPool.AllGameObjects;
            Assert.IsNotNull(returnedObjs);
            Assert.IsNotEmpty(returnedObjs);
            Assert.AreEqual(2, returnedObjs.Count);
            Assert.AreEqual(instantiatedObjs[0], returnedObjs[0]);
            Assert.AreEqual(instantiatedObjs[1], returnedObjs[1]);
            Assert.AreEqual(1, objPool.transform.GetChild(0).childCount);
            Assert.AreEqual($"{GameObjectUtility.GetActualName(obj).ToUpper()}_ANCHOR", objPool.transform.GetChild(0).GetChild(0).name);
            Assert.AreEqual(2, objPool.transform.GetChild(0).GetChild(0).childCount);
            Assert.AreEqual(instantiatedObjs[0], objPool.transform.GetChild(0).GetChild(0).GetChild(0).gameObject);
            Assert.AreEqual(instantiatedObjs[1], objPool.transform.GetChild(0).GetChild(0).GetChild(1).gameObject);

            ResetFlags();
            instantiatedObjs.Add(objPool.Instantiate(obj2, true, true));
            Assert.IsNotNull(instantiatedObjs[2]);
            Assert.IsTrue(instantiatedObjs[2]);
            Assert.AreNotEqual(obj2, instantiatedObjs[2]);
            Assert.IsTrue(onObjectMadeActiveRan);
            Assert.IsFalse(onObjectMadeInactiveRan);
            Assert.IsTrue(instantiatedObjs[2].activeInHierarchy);
            returnedObjs = objPool.AllGameObjects;
            Assert.IsNotNull(returnedObjs);
            Assert.IsNotEmpty(returnedObjs);
            Assert.AreEqual(3, returnedObjs.Count);
            Assert.AreEqual(instantiatedObjs[0], returnedObjs[0]);
            Assert.AreEqual(instantiatedObjs[1], returnedObjs[1]);
            Assert.AreEqual(instantiatedObjs[2], returnedObjs[2]);
            Assert.AreEqual(2, objPool.transform.GetChild(0).childCount);
            Assert.AreEqual($"{GameObjectUtility.GetActualName(obj).ToUpper()}_ANCHOR", objPool.transform.GetChild(0).GetChild(0).name);
            Assert.AreEqual(2, objPool.transform.GetChild(0).GetChild(0).childCount);
            Assert.AreEqual(instantiatedObjs[0], objPool.transform.GetChild(0).GetChild(0).GetChild(0).gameObject);
            Assert.AreEqual(instantiatedObjs[1], objPool.transform.GetChild(0).GetChild(0).GetChild(1).gameObject);
            Assert.AreEqual($"{GameObjectUtility.GetActualName(obj2).ToUpper()}_ANCHOR", objPool.transform.GetChild(0).GetChild(1).name);
            Assert.AreEqual(1, objPool.transform.GetChild(0).GetChild(1).childCount);
            Assert.AreEqual(instantiatedObjs[2], objPool.transform.GetChild(0).GetChild(1).GetChild(0).gameObject);

            ResetFlags();
            obj2.SetActive(false);
            instantiatedObjs.Add(objPool.Instantiate(obj2, true, true));
            Assert.IsNotNull(instantiatedObjs[3]);
            Assert.IsTrue(instantiatedObjs[3]);
            Assert.AreNotEqual(obj2, instantiatedObjs[3]);
            Assert.IsTrue(onObjectMadeActiveRan);
            Assert.IsFalse(onObjectMadeInactiveRan);
            Assert.IsFalse(instantiatedObjs[3].activeInHierarchy);
            returnedObjs = objPool.AllGameObjects;
            Assert.IsNotNull(returnedObjs);
            Assert.IsNotEmpty(returnedObjs);
            Assert.AreEqual(4, returnedObjs.Count);
            Assert.AreEqual(instantiatedObjs[0], returnedObjs[0]);
            Assert.AreEqual(instantiatedObjs[1], returnedObjs[1]);
            Assert.AreEqual(instantiatedObjs[2], returnedObjs[2]);
            Assert.AreEqual(instantiatedObjs[3], returnedObjs[3]);
            Assert.AreEqual(2, objPool.transform.GetChild(0).childCount);
            Assert.AreEqual($"{GameObjectUtility.GetActualName(obj).ToUpper()}_ANCHOR", objPool.transform.GetChild(0).GetChild(0).name);
            Assert.AreEqual(2, objPool.transform.GetChild(0).GetChild(0).childCount);
            Assert.AreEqual(instantiatedObjs[0], objPool.transform.GetChild(0).GetChild(0).GetChild(0).gameObject);
            Assert.AreEqual(instantiatedObjs[1], objPool.transform.GetChild(0).GetChild(0).GetChild(1).gameObject);
            Assert.AreEqual($"{GameObjectUtility.GetActualName(obj2).ToUpper()}_ANCHOR", objPool.transform.GetChild(0).GetChild(1).name);
            Assert.AreEqual(2, objPool.transform.GetChild(0).GetChild(1).childCount);
            Assert.AreEqual(instantiatedObjs[2], objPool.transform.GetChild(0).GetChild(1).GetChild(0).gameObject);
            Assert.AreEqual(instantiatedObjs[3], objPool.transform.GetChild(0).GetChild(1).GetChild(1).gameObject);

            ResetFlags();
            objPool.Destroy(instantiatedObjs[0]);
            Assert.IsFalse(instantiatedObjs[0].activeInHierarchy);
            Assert.AreEqual($"{GameObjectUtility.GetActualName(obj).ToUpper()}_ANCHOR", objPool.transform.GetChild(1).GetChild(0).name);
            Assert.AreEqual(1, objPool.transform.GetChild(1).GetChild(0).childCount);
            Assert.AreEqual(instantiatedObjs[0], objPool.transform.GetChild(1).GetChild(0).GetChild(0).gameObject);
            instantiatedObjs.Add(objPool.Instantiate(obj, true, true));
            Assert.IsNotNull(instantiatedObjs[4]);
            Assert.IsTrue(instantiatedObjs[4]);
            Assert.AreNotEqual(obj, instantiatedObjs[4]);
            Assert.IsTrue(onObjectMadeActiveRan);
            Assert.IsTrue(onObjectMadeInactiveRan);
            Assert.IsFalse(instantiatedObjs[4].activeInHierarchy);
            returnedObjs = objPool.AllGameObjects;
            Assert.IsNotNull(returnedObjs);
            Assert.IsNotEmpty(returnedObjs);
            Assert.AreEqual(4, returnedObjs.Count);
            Assert.AreEqual(instantiatedObjs[0], returnedObjs[1]);
            Assert.AreEqual(instantiatedObjs[1], returnedObjs[0]);
            Assert.AreEqual(instantiatedObjs[2], returnedObjs[2]);
            Assert.AreEqual(instantiatedObjs[3], returnedObjs[3]);
            Assert.AreEqual(2, objPool.transform.GetChild(0).childCount);
            Assert.AreEqual($"{GameObjectUtility.GetActualName(obj).ToUpper()}_ANCHOR", objPool.transform.GetChild(0).GetChild(0).name);
            Assert.AreEqual(2, objPool.transform.GetChild(0).GetChild(0).childCount);
            Assert.AreEqual(instantiatedObjs[0], objPool.transform.GetChild(0).GetChild(0).GetChild(1).gameObject);
            Assert.AreEqual(instantiatedObjs[1], objPool.transform.GetChild(0).GetChild(0).GetChild(0).gameObject);
            Assert.AreEqual($"{GameObjectUtility.GetActualName(obj2).ToUpper()}_ANCHOR", objPool.transform.GetChild(0).GetChild(1).name);
            Assert.AreEqual(2, objPool.transform.GetChild(0).GetChild(1).childCount);
            Assert.AreEqual(instantiatedObjs[2], objPool.transform.GetChild(0).GetChild(1).GetChild(0).gameObject);
            Assert.AreEqual(instantiatedObjs[3], objPool.transform.GetChild(0).GetChild(1).GetChild(1).gameObject);
            Assert.AreEqual($"{GameObjectUtility.GetActualName(obj).ToUpper()}_ANCHOR", objPool.transform.GetChild(1).GetChild(0).name);
            Assert.AreEqual(0, objPool.transform.GetChild(1).GetChild(0).childCount);
        }
        [UnityTest]
        public IEnumerator Instantiate_AsInactive_PoolManaged_Tests()
        {
            yield return new WaitForEndOfFrame();

            instantiatedObjs.Add(objPool.Instantiate(obj, false, true));
            Assert.IsNotNull(instantiatedObjs[0]);
            Assert.IsTrue(instantiatedObjs[0]);
            Assert.AreNotEqual(obj, instantiatedObjs[0]);
            Assert.IsFalse(onObjectMadeActiveRan);
            Assert.IsTrue(onObjectMadeInactiveRan);
            Assert.IsFalse(instantiatedObjs[0].activeInHierarchy);
            returnedObjs = objPool.AllGameObjects;
            Assert.IsNotNull(returnedObjs);
            Assert.IsEmpty(returnedObjs);
            Assert.AreEqual(0, objPool.transform.GetChild(0).childCount);
            Assert.AreEqual(1, objPool.transform.GetChild(1).childCount);
            Assert.AreEqual($"{GameObjectUtility.GetActualName(obj).ToUpper()}_ANCHOR", objPool.transform.GetChild(1).GetChild(0).name);
            Assert.AreEqual(1, objPool.transform.GetChild(1).GetChild(0).childCount);
            Assert.AreEqual(instantiatedObjs[0], objPool.transform.GetChild(1).GetChild(0).GetChild(0).gameObject);

            ResetFlags();
            instantiatedObjs.Add(objPool.Instantiate(obj, false, true));
            Assert.IsNotNull(instantiatedObjs[1]);
            Assert.IsTrue(instantiatedObjs[1]);
            Assert.AreNotEqual(obj, instantiatedObjs[1]);
            Assert.IsFalse(onObjectMadeActiveRan);
            Assert.IsTrue(onObjectMadeInactiveRan);
            Assert.IsFalse(instantiatedObjs[1].activeInHierarchy);
            returnedObjs = objPool.AllGameObjects;
            Assert.IsNotNull(returnedObjs);
            Assert.IsEmpty(returnedObjs);
            Assert.AreEqual(0, objPool.transform.GetChild(0).childCount);
            Assert.AreEqual(1, objPool.transform.GetChild(1).childCount);
            Assert.AreEqual($"{GameObjectUtility.GetActualName(obj).ToUpper()}_ANCHOR", objPool.transform.GetChild(1).GetChild(0).name);
            Assert.AreEqual(2, objPool.transform.GetChild(1).GetChild(0).childCount);
            Assert.AreEqual(instantiatedObjs[0], objPool.transform.GetChild(1).GetChild(0).GetChild(0).gameObject);
            Assert.AreEqual(instantiatedObjs[1], objPool.transform.GetChild(1).GetChild(0).GetChild(1).gameObject);

            ResetFlags();
            instantiatedObjs.Add(objPool.Instantiate(obj2, false, true));
            Assert.IsNotNull(instantiatedObjs[2]);
            Assert.IsTrue(instantiatedObjs[2]);
            Assert.AreNotEqual(obj2, instantiatedObjs[2]);
            Assert.IsFalse(onObjectMadeActiveRan);
            Assert.IsTrue(onObjectMadeInactiveRan);
            Assert.IsFalse(instantiatedObjs[2].activeInHierarchy);
            returnedObjs = objPool.AllGameObjects;
            Assert.IsNotNull(returnedObjs);
            Assert.IsEmpty(returnedObjs);
            Assert.AreEqual(0, objPool.transform.GetChild(0).childCount);
            Assert.AreEqual(2, objPool.transform.GetChild(1).childCount);
            Assert.AreEqual($"{GameObjectUtility.GetActualName(obj).ToUpper()}_ANCHOR", objPool.transform.GetChild(1).GetChild(0).name);
            Assert.AreEqual(2, objPool.transform.GetChild(1).GetChild(0).childCount);
            Assert.AreEqual(instantiatedObjs[0], objPool.transform.GetChild(1).GetChild(0).GetChild(0).gameObject);
            Assert.AreEqual(instantiatedObjs[1], objPool.transform.GetChild(1).GetChild(0).GetChild(1).gameObject);
            Assert.AreEqual($"{GameObjectUtility.GetActualName(obj2).ToUpper()}_ANCHOR", objPool.transform.GetChild(1).GetChild(1).name);
            Assert.AreEqual(1, objPool.transform.GetChild(1).GetChild(1).childCount);
            Assert.AreEqual(instantiatedObjs[2], objPool.transform.GetChild(1).GetChild(1).GetChild(0).gameObject);
        }
        [UnityTest]
        public IEnumerator Instantiate_AsActive_NotPoolManaged_Tests()
        {
            yield return new WaitForEndOfFrame();

            instantiatedObjs.Add(objPool.Instantiate(obj, true, false));
            Assert.IsNotNull(instantiatedObjs[0]);
            Assert.IsTrue(instantiatedObjs[0]);
            Assert.AreNotEqual(obj, instantiatedObjs[0]);
            Assert.IsFalse(onObjectMadeActiveRan);
            Assert.IsFalse(onObjectMadeInactiveRan);
            Assert.IsTrue(instantiatedObjs[0].activeInHierarchy);
            returnedObjs = objPool.AllGameObjects;
            Assert.IsNotNull(returnedObjs);
            Assert.IsEmpty(returnedObjs);
            Assert.AreEqual(0, objPool.transform.GetChild(0).childCount);

            instantiatedObjs.Add(objPool.Instantiate(obj, true, false));
            Assert.IsNotNull(instantiatedObjs[1]);
            Assert.IsTrue(instantiatedObjs[1]);
            Assert.AreNotEqual(obj, instantiatedObjs[1]);
            Assert.IsFalse(onObjectMadeActiveRan);
            Assert.IsFalse(onObjectMadeInactiveRan);
            Assert.IsTrue(instantiatedObjs[1].activeInHierarchy);
            returnedObjs = objPool.AllGameObjects;
            Assert.IsNotNull(returnedObjs);
            Assert.IsEmpty(returnedObjs);
            Assert.AreEqual(0, objPool.transform.GetChild(0).childCount);

            instantiatedObjs.Add(objPool.Instantiate(obj2, true, false));
            Assert.IsNotNull(instantiatedObjs[2]);
            Assert.IsTrue(instantiatedObjs[2]);
            Assert.AreNotEqual(obj2, instantiatedObjs[2]);
            Assert.IsFalse(onObjectMadeActiveRan);
            Assert.IsFalse(onObjectMadeInactiveRan);
            Assert.IsTrue(instantiatedObjs[2].activeInHierarchy);
            returnedObjs = objPool.AllGameObjects;
            Assert.IsNotNull(returnedObjs);
            Assert.IsEmpty(returnedObjs);
            Assert.AreEqual(0, objPool.transform.GetChild(0).childCount);

            obj2.SetActive(false);
            instantiatedObjs.Add(objPool.Instantiate(obj2, true, false));
            Assert.IsNotNull(instantiatedObjs[3]);
            Assert.IsTrue(instantiatedObjs[3]);
            Assert.AreNotEqual(obj2, instantiatedObjs[3]);
            Assert.IsFalse(onObjectMadeActiveRan);
            Assert.IsFalse(onObjectMadeInactiveRan);
            Assert.IsFalse(instantiatedObjs[3].activeInHierarchy);
            returnedObjs = objPool.AllGameObjects;
            Assert.IsNotNull(returnedObjs);
            Assert.IsEmpty(returnedObjs);
            Assert.AreEqual(0, objPool.transform.GetChild(0).childCount);

            objPool.Destroy(instantiatedObjs[0]);
            Assert.IsFalse(instantiatedObjs[0].activeInHierarchy);
            Assert.AreEqual($"{GameObjectUtility.GetActualName(obj).ToUpper()}_ANCHOR", objPool.transform.GetChild(1).GetChild(0).name);
            Assert.AreEqual(1, objPool.transform.GetChild(1).GetChild(0).childCount);
            Assert.AreEqual(instantiatedObjs[0], objPool.transform.GetChild(1).GetChild(0).GetChild(0).gameObject);
            instantiatedObjs.Add(objPool.Instantiate(obj, true, false));
            Assert.IsNotNull(instantiatedObjs[4]);
            Assert.IsTrue(instantiatedObjs[4]);
            Assert.AreNotEqual(obj, instantiatedObjs[4]);
            Assert.IsFalse(onObjectMadeActiveRan);
            Assert.IsTrue(onObjectMadeInactiveRan);
            Assert.IsFalse(instantiatedObjs[4].activeInHierarchy);
            returnedObjs = objPool.AllGameObjects;
            Assert.IsNotNull(returnedObjs);
            Assert.IsEmpty(returnedObjs);
            Assert.AreEqual($"{GameObjectUtility.GetActualName(obj).ToUpper()}_ANCHOR", objPool.transform.GetChild(1).GetChild(0).name);
            Assert.AreEqual(0, objPool.transform.GetChild(1).GetChild(0).childCount);
        }
        [UnityTest]
        public IEnumerator Instantiate_AsInactive_NotPoolManaged_Tests()
        {
            yield return new WaitForEndOfFrame();

            instantiatedObjs.Add(objPool.Instantiate(obj, false, false));
            Assert.IsNotNull(instantiatedObjs[0]);
            Assert.IsTrue(instantiatedObjs[0]);
            Assert.AreNotEqual(obj, instantiatedObjs[0]);
            Assert.IsFalse(onObjectMadeActiveRan);
            Assert.IsFalse(onObjectMadeInactiveRan);
            Assert.IsTrue(instantiatedObjs[0].activeInHierarchy);
            returnedObjs = objPool.AllGameObjects;
            Assert.IsNotNull(returnedObjs);
            Assert.IsEmpty(returnedObjs);
            Assert.AreEqual(0, objPool.transform.GetChild(0).childCount);
            Assert.AreEqual(0, objPool.transform.GetChild(1).childCount);

            instantiatedObjs.Add(objPool.Instantiate(obj, false, false));
            Assert.IsNotNull(instantiatedObjs[1]);
            Assert.IsTrue(instantiatedObjs[1]);
            Assert.AreNotEqual(obj, instantiatedObjs[1]);
            Assert.IsFalse(onObjectMadeActiveRan);
            Assert.IsFalse(onObjectMadeInactiveRan);
            Assert.IsTrue(instantiatedObjs[1].activeInHierarchy);
            returnedObjs = objPool.AllGameObjects;
            Assert.IsNotNull(returnedObjs);
            Assert.IsEmpty(returnedObjs);
            Assert.AreEqual(0, objPool.transform.GetChild(0).childCount);
            Assert.AreEqual(0, objPool.transform.GetChild(1).childCount);

            instantiatedObjs.Add(objPool.Instantiate(obj2, false, false));
            Assert.IsNotNull(instantiatedObjs[2]);
            Assert.IsTrue(instantiatedObjs[2]);
            Assert.AreNotEqual(obj2, instantiatedObjs[2]);
            Assert.IsFalse(onObjectMadeActiveRan);
            Assert.IsFalse(onObjectMadeInactiveRan);
            Assert.IsTrue(instantiatedObjs[2].activeInHierarchy);
            returnedObjs = objPool.AllGameObjects;
            Assert.IsNotNull(returnedObjs);
            Assert.IsEmpty(returnedObjs);
            Assert.AreEqual(0, objPool.transform.GetChild(0).childCount);
            Assert.AreEqual(0, objPool.transform.GetChild(1).childCount);

            obj2.SetActive(false);
            instantiatedObjs.Add(objPool.Instantiate(obj2, false, false));
            Assert.IsNotNull(instantiatedObjs[3]);
            Assert.IsTrue(instantiatedObjs[3]);
            Assert.AreNotEqual(obj2, instantiatedObjs[3]);
            Assert.IsFalse(onObjectMadeActiveRan);
            Assert.IsFalse(onObjectMadeInactiveRan);
            Assert.IsFalse(instantiatedObjs[3].activeInHierarchy);
            returnedObjs = objPool.AllGameObjects;
            Assert.IsNotNull(returnedObjs);
            Assert.IsEmpty(returnedObjs);
            Assert.AreEqual(0, objPool.transform.GetChild(0).childCount);
            Assert.AreEqual(0, objPool.transform.GetChild(1).childCount);
        }
        [UnityTest]
        public IEnumerator Instantiate_InvalidInput_Tests()
        {
            yield return new WaitForEndOfFrame();

            instantiatedObjs.Add(objPool.Instantiate(null, true, true));
            Assert.IsNull(instantiatedObjs[0]);
        }

        // Parent
        [UnityTest]
        public IEnumerator Instantiate_Parent_AsActive_NotPoolManaged_Tests()
        {
            yield return new WaitForEndOfFrame();

            instantiatedObjs.Add(objPool.Instantiate(obj, dontAddObj.transform, true, false));
            Assert.IsNotNull(instantiatedObjs[0]);
            Assert.IsTrue(instantiatedObjs[0]);
            Assert.AreNotEqual(obj, instantiatedObjs[0]);
            Assert.AreEqual(dontAddObj, instantiatedObjs[0].transform.parent.gameObject);
            Assert.IsFalse(onObjectMadeActiveRan);
            Assert.IsFalse(onObjectMadeInactiveRan);
            Assert.IsTrue(instantiatedObjs[0].activeInHierarchy);
            returnedObjs = objPool.AllGameObjects;
            Assert.IsNotNull(returnedObjs);
            Assert.IsEmpty(returnedObjs);
            Assert.AreEqual(0, objPool.transform.GetChild(0).childCount);

            instantiatedObjs.Add(objPool.Instantiate(obj, dontAddObj.transform, true, false));
            Assert.IsNotNull(instantiatedObjs[1]);
            Assert.IsTrue(instantiatedObjs[1]);
            Assert.AreNotEqual(obj, instantiatedObjs[1]);
            Assert.AreEqual(dontAddObj, instantiatedObjs[1].transform.parent.gameObject);
            Assert.IsFalse(onObjectMadeActiveRan);
            Assert.IsFalse(onObjectMadeInactiveRan);
            Assert.IsTrue(instantiatedObjs[1].activeInHierarchy);
            returnedObjs = objPool.AllGameObjects;
            Assert.IsNotNull(returnedObjs);
            Assert.IsEmpty(returnedObjs);
            Assert.AreEqual(0, objPool.transform.GetChild(0).childCount);

            instantiatedObjs.Add(objPool.Instantiate(obj2, dontAddObj.transform, true, false));
            Assert.IsNotNull(instantiatedObjs[2]);
            Assert.IsTrue(instantiatedObjs[2]);
            Assert.AreNotEqual(obj2, instantiatedObjs[2]);
            Assert.AreEqual(dontAddObj, instantiatedObjs[2].transform.parent.gameObject);
            Assert.IsFalse(onObjectMadeActiveRan);
            Assert.IsFalse(onObjectMadeInactiveRan);
            Assert.IsTrue(instantiatedObjs[2].activeInHierarchy);
            returnedObjs = objPool.AllGameObjects;
            Assert.IsNotNull(returnedObjs);
            Assert.IsEmpty(returnedObjs);
            Assert.AreEqual(0, objPool.transform.GetChild(0).childCount);

            obj2.SetActive(false);
            instantiatedObjs.Add(objPool.Instantiate(obj2, dontAddObj.transform, true, false));
            Assert.IsNotNull(instantiatedObjs[3]);
            Assert.IsTrue(instantiatedObjs[3]);
            Assert.AreNotEqual(obj2, instantiatedObjs[3]);
            Assert.AreEqual(dontAddObj, instantiatedObjs[3].transform.parent.gameObject);
            Assert.IsFalse(onObjectMadeActiveRan);
            Assert.IsFalse(onObjectMadeInactiveRan);
            Assert.IsFalse(instantiatedObjs[3].activeInHierarchy);
            returnedObjs = objPool.AllGameObjects;
            Assert.IsNotNull(returnedObjs);
            Assert.IsEmpty(returnedObjs);
            Assert.AreEqual(0, objPool.transform.GetChild(0).childCount);

            objPool.Destroy(instantiatedObjs[0]);
            Assert.IsFalse(instantiatedObjs[0].activeInHierarchy);
            Assert.AreEqual($"{GameObjectUtility.GetActualName(obj).ToUpper()}_ANCHOR", objPool.transform.GetChild(1).GetChild(0).name);
            Assert.AreEqual(1, objPool.transform.GetChild(1).GetChild(0).childCount);
            Assert.AreEqual(instantiatedObjs[0], objPool.transform.GetChild(1).GetChild(0).GetChild(0).gameObject);
            instantiatedObjs.Add(objPool.Instantiate(obj, instantiatedObjs[3].transform, true, false));
            Assert.IsNotNull(instantiatedObjs[4]);
            Assert.IsTrue(instantiatedObjs[4]);
            Assert.AreNotEqual(obj, instantiatedObjs[4]);
            Assert.AreEqual(instantiatedObjs[3], instantiatedObjs[4].transform.parent.gameObject);
            Assert.IsFalse(onObjectMadeActiveRan);
            Assert.IsTrue(onObjectMadeInactiveRan);
            Assert.IsFalse(instantiatedObjs[4].activeInHierarchy);
            returnedObjs = objPool.AllGameObjects;
            Assert.IsNotNull(returnedObjs);
            Assert.IsEmpty(returnedObjs);
            Assert.AreEqual(0, objPool.transform.GetChild(0).childCount);
            Assert.AreEqual($"{GameObjectUtility.GetActualName(obj).ToUpper()}_ANCHOR", objPool.transform.GetChild(1).GetChild(0).name);
            Assert.AreEqual(0, objPool.transform.GetChild(1).GetChild(0).childCount);
        }
        [UnityTest]
        public IEnumerator Instantiate_Parent_InvalidInput_Tests()
        {
            yield return new WaitForEndOfFrame();

            instantiatedObjs.Add(objPool.Instantiate(null, null, true, true));
            Assert.IsNull(instantiatedObjs[0]);

            instantiatedObjs.Add(objPool.Instantiate(obj, null, true, true));
            Assert.IsNotNull(instantiatedObjs[1]);
            Assert.AreEqual(instantiatedObjs[1], objPool.AllGameObjects[0]);

            instantiatedObjs.Add(objPool.Instantiate(null, instantiatedObjs[1].transform, true, true));
            Assert.IsNull(instantiatedObjs[2]);
        }

        // Position | Rotation
        [UnityTest]
        public IEnumerator Instantiate_Position_Rotation_AsActive_PoolManaged_Tests()
        {
            yield return new WaitForEndOfFrame();

            instantiatedObjs.Add(objPool.Instantiate(obj, Vector3.one, Quaternion.identity, true, true));
            Assert.IsNotNull(instantiatedObjs[0]);
            Assert.IsTrue(instantiatedObjs[0]);
            Assert.AreNotEqual(obj, instantiatedObjs[0]);
            Assert.IsTrue(onObjectMadeActiveRan);
            Assert.IsFalse(onObjectMadeInactiveRan);
            Assert.IsTrue(instantiatedObjs[0].activeInHierarchy);
            Assert.AreEqual(Vector3.one, instantiatedObjs[0].transform.position);
            Assert.AreEqual(Vector3.zero, instantiatedObjs[0].transform.eulerAngles);
            returnedObjs = objPool.AllGameObjects;
            Assert.IsNotNull(returnedObjs);
            Assert.IsNotEmpty(returnedObjs);
            Assert.AreEqual(1, returnedObjs.Count);
            Assert.AreEqual(instantiatedObjs[0], returnedObjs[0]);
            Assert.AreEqual(1, objPool.transform.GetChild(0).childCount);
            Assert.AreEqual($"{GameObjectUtility.GetActualName(obj).ToUpper()}_ANCHOR", objPool.transform.GetChild(0).GetChild(0).name);
            Assert.AreEqual(1, objPool.transform.GetChild(0).GetChild(0).childCount);
            Assert.AreEqual(instantiatedObjs[0], objPool.transform.GetChild(0).GetChild(0).GetChild(0).gameObject);

            ResetFlags();
            instantiatedObjs.Add(objPool.Instantiate(obj, new Vector3(1, 2, 3), Quaternion.identity, true, true));
            Assert.IsNotNull(instantiatedObjs[1]);
            Assert.IsTrue(instantiatedObjs[1]);
            Assert.AreNotEqual(obj, instantiatedObjs[1]);
            Assert.IsTrue(onObjectMadeActiveRan);
            Assert.IsFalse(onObjectMadeInactiveRan);
            Assert.IsTrue(instantiatedObjs[1].activeInHierarchy);
            Assert.AreEqual(new Vector3(1, 2, 3), instantiatedObjs[1].transform.position);
            Assert.AreEqual(Vector3.zero, instantiatedObjs[1].transform.eulerAngles);
            returnedObjs = objPool.AllGameObjects;
            Assert.IsNotNull(returnedObjs);
            Assert.IsNotEmpty(returnedObjs);
            Assert.AreEqual(2, returnedObjs.Count);
            Assert.AreEqual(instantiatedObjs[0], returnedObjs[0]);
            Assert.AreEqual(instantiatedObjs[1], returnedObjs[1]);
            Assert.AreEqual(1, objPool.transform.GetChild(0).childCount);
            Assert.AreEqual($"{GameObjectUtility.GetActualName(obj).ToUpper()}_ANCHOR", objPool.transform.GetChild(0).GetChild(0).name);
            Assert.AreEqual(2, objPool.transform.GetChild(0).GetChild(0).childCount);
            Assert.AreEqual(instantiatedObjs[0], objPool.transform.GetChild(0).GetChild(0).GetChild(0).gameObject);
            Assert.AreEqual(instantiatedObjs[1], objPool.transform.GetChild(0).GetChild(0).GetChild(1).gameObject);

            ResetFlags();
            instantiatedObjs.Add(objPool.Instantiate(obj2, Vector3.one, new Quaternion(1, 2, 3, 4), true, true));
            Assert.IsNotNull(instantiatedObjs[2]);
            Assert.IsTrue(instantiatedObjs[2]);
            Assert.AreNotEqual(obj2, instantiatedObjs[2]);
            Assert.IsTrue(onObjectMadeActiveRan);
            Assert.IsFalse(onObjectMadeInactiveRan);
            Assert.IsTrue(instantiatedObjs[2].activeInHierarchy);
            Assert.AreEqual(Vector3.one, instantiatedObjs[2].transform.position);
            Assert.AreEqual(new Quaternion(1, 2, 3, 4).eulerAngles, instantiatedObjs[2].transform.rotation.eulerAngles);
            returnedObjs = objPool.AllGameObjects;
            Assert.IsNotNull(returnedObjs);
            Assert.IsNotEmpty(returnedObjs);
            Assert.AreEqual(3, returnedObjs.Count);
            Assert.AreEqual(instantiatedObjs[0], returnedObjs[0]);
            Assert.AreEqual(instantiatedObjs[1], returnedObjs[1]);
            Assert.AreEqual(instantiatedObjs[2], returnedObjs[2]);
            Assert.AreEqual(2, objPool.transform.GetChild(0).childCount);
            Assert.AreEqual($"{GameObjectUtility.GetActualName(obj).ToUpper()}_ANCHOR", objPool.transform.GetChild(0).GetChild(0).name);
            Assert.AreEqual(2, objPool.transform.GetChild(0).GetChild(0).childCount);
            Assert.AreEqual(instantiatedObjs[0], objPool.transform.GetChild(0).GetChild(0).GetChild(0).gameObject);
            Assert.AreEqual(instantiatedObjs[1], objPool.transform.GetChild(0).GetChild(0).GetChild(1).gameObject);
            Assert.AreEqual($"{GameObjectUtility.GetActualName(obj2).ToUpper()}_ANCHOR", objPool.transform.GetChild(0).GetChild(1).name);
            Assert.AreEqual(1, objPool.transform.GetChild(0).GetChild(1).childCount);
            Assert.AreEqual(instantiatedObjs[2], objPool.transform.GetChild(0).GetChild(1).GetChild(0).gameObject);

            ResetFlags();
            obj2.SetActive(false);
            instantiatedObjs.Add(objPool.Instantiate(obj2, new Vector3(1, 2, 3), new Quaternion(1, 2, 3, 4), true, true));
            Assert.IsNotNull(instantiatedObjs[3]);
            Assert.IsTrue(instantiatedObjs[3]);
            Assert.AreNotEqual(obj2, instantiatedObjs[3]);
            Assert.IsTrue(onObjectMadeActiveRan);
            Assert.IsFalse(onObjectMadeInactiveRan);
            Assert.IsFalse(instantiatedObjs[3].activeInHierarchy);
            Assert.AreEqual(new Vector3(1, 2, 3), instantiatedObjs[3].transform.position);
            Assert.AreEqual(new Quaternion(1, 2, 3, 4).eulerAngles, instantiatedObjs[3].transform.rotation.eulerAngles);
            returnedObjs = objPool.AllGameObjects;
            Assert.IsNotNull(returnedObjs);
            Assert.IsNotEmpty(returnedObjs);
            Assert.AreEqual(4, returnedObjs.Count);
            Assert.AreEqual(instantiatedObjs[0], returnedObjs[0]);
            Assert.AreEqual(instantiatedObjs[1], returnedObjs[1]);
            Assert.AreEqual(instantiatedObjs[2], returnedObjs[2]);
            Assert.AreEqual(instantiatedObjs[3], returnedObjs[3]);
            Assert.AreEqual(2, objPool.transform.GetChild(0).childCount);
            Assert.AreEqual($"{GameObjectUtility.GetActualName(obj).ToUpper()}_ANCHOR", objPool.transform.GetChild(0).GetChild(0).name);
            Assert.AreEqual(2, objPool.transform.GetChild(0).GetChild(0).childCount);
            Assert.AreEqual(instantiatedObjs[0], objPool.transform.GetChild(0).GetChild(0).GetChild(0).gameObject);
            Assert.AreEqual(instantiatedObjs[1], objPool.transform.GetChild(0).GetChild(0).GetChild(1).gameObject);
            Assert.AreEqual($"{GameObjectUtility.GetActualName(obj2).ToUpper()}_ANCHOR", objPool.transform.GetChild(0).GetChild(1).name);
            Assert.AreEqual(2, objPool.transform.GetChild(0).GetChild(1).childCount);
            Assert.AreEqual(instantiatedObjs[2], objPool.transform.GetChild(0).GetChild(1).GetChild(0).gameObject);
            Assert.AreEqual(instantiatedObjs[3], objPool.transform.GetChild(0).GetChild(1).GetChild(1).gameObject);

            ResetFlags();
            objPool.Destroy(instantiatedObjs[0]);
            Assert.IsFalse(instantiatedObjs[0].activeInHierarchy);
            Assert.AreEqual($"{GameObjectUtility.GetActualName(obj).ToUpper()}_ANCHOR", objPool.transform.GetChild(1).GetChild(0).name);
            Assert.AreEqual(1, objPool.transform.GetChild(1).GetChild(0).childCount);
            Assert.AreEqual(instantiatedObjs[0], objPool.transform.GetChild(1).GetChild(0).GetChild(0).gameObject);
            instantiatedObjs.Add(objPool.Instantiate(obj, new Vector3(4, 5, 6), new Quaternion(5, 6, 7, 8), true, true));
            Assert.IsNotNull(instantiatedObjs[4]);
            Assert.IsTrue(instantiatedObjs[4]);
            Assert.AreNotEqual(obj, instantiatedObjs[4]);
            Assert.IsTrue(onObjectMadeActiveRan);
            Assert.IsTrue(onObjectMadeInactiveRan);
            Assert.IsFalse(instantiatedObjs[4].activeInHierarchy);
            Assert.AreEqual(new Vector3(4, 5, 6), instantiatedObjs[4].transform.position);
            Assert.AreEqual(new Quaternion(5, 6, 7, 8).eulerAngles, instantiatedObjs[4].transform.rotation.eulerAngles);
            returnedObjs = objPool.AllGameObjects;
            Assert.IsNotNull(returnedObjs);
            Assert.IsNotEmpty(returnedObjs);
            Assert.AreEqual(4, returnedObjs.Count);
            Assert.AreEqual(instantiatedObjs[0], returnedObjs[1]);
            Assert.AreEqual(instantiatedObjs[1], returnedObjs[0]);
            Assert.AreEqual(instantiatedObjs[2], returnedObjs[2]);
            Assert.AreEqual(instantiatedObjs[3], returnedObjs[3]);
            Assert.AreEqual(2, objPool.transform.GetChild(0).childCount);
            Assert.AreEqual($"{GameObjectUtility.GetActualName(obj).ToUpper()}_ANCHOR", objPool.transform.GetChild(0).GetChild(0).name);
            Assert.AreEqual(2, objPool.transform.GetChild(0).GetChild(0).childCount);
            Assert.AreEqual(instantiatedObjs[0], objPool.transform.GetChild(0).GetChild(0).GetChild(1).gameObject);
            Assert.AreEqual(instantiatedObjs[1], objPool.transform.GetChild(0).GetChild(0).GetChild(0).gameObject);
            Assert.AreEqual($"{GameObjectUtility.GetActualName(obj2).ToUpper()}_ANCHOR", objPool.transform.GetChild(0).GetChild(1).name);
            Assert.AreEqual(2, objPool.transform.GetChild(0).GetChild(1).childCount);
            Assert.AreEqual(instantiatedObjs[2], objPool.transform.GetChild(0).GetChild(1).GetChild(0).gameObject);
            Assert.AreEqual(instantiatedObjs[3], objPool.transform.GetChild(0).GetChild(1).GetChild(1).gameObject);
            Assert.AreEqual($"{GameObjectUtility.GetActualName(obj).ToUpper()}_ANCHOR", objPool.transform.GetChild(1).GetChild(0).name);
            Assert.AreEqual(0, objPool.transform.GetChild(1).GetChild(0).childCount);
        }
        [UnityTest]
        public IEnumerator Instantiate_Position_Rotation_AsInactive_PoolManaged_Tests()
        {
            yield return new WaitForEndOfFrame();

            instantiatedObjs.Add(objPool.Instantiate(obj, new Vector3(1, 2, 3), Quaternion.identity, false, true));
            Assert.IsNotNull(instantiatedObjs[0]);
            Assert.IsTrue(instantiatedObjs[0]);
            Assert.AreNotEqual(obj, instantiatedObjs[0]);
            Assert.IsFalse(onObjectMadeActiveRan);
            Assert.IsTrue(onObjectMadeInactiveRan);
            Assert.IsFalse(instantiatedObjs[0].activeInHierarchy);
            Assert.AreEqual(new Vector3(1, 2, 3), instantiatedObjs[0].transform.position);
            Assert.AreEqual(Vector3.zero, instantiatedObjs[0].transform.eulerAngles);
            returnedObjs = objPool.AllGameObjects;
            Assert.IsNotNull(returnedObjs);
            Assert.IsEmpty(returnedObjs);
            Assert.AreEqual(0, objPool.transform.GetChild(0).childCount);
            Assert.AreEqual(1, objPool.transform.GetChild(1).childCount);
            Assert.AreEqual($"{GameObjectUtility.GetActualName(obj).ToUpper()}_ANCHOR", objPool.transform.GetChild(1).GetChild(0).name);
            Assert.AreEqual(1, objPool.transform.GetChild(1).GetChild(0).childCount);
            Assert.AreEqual(instantiatedObjs[0], objPool.transform.GetChild(1).GetChild(0).GetChild(0).gameObject);

            ResetFlags();
            instantiatedObjs.Add(objPool.Instantiate(obj, Vector3.one, new Quaternion(1, 2, 3, 4), false, true));
            Assert.IsNotNull(instantiatedObjs[1]);
            Assert.IsTrue(instantiatedObjs[1]);
            Assert.AreNotEqual(obj, instantiatedObjs[1]);
            Assert.IsFalse(onObjectMadeActiveRan);
            Assert.IsTrue(onObjectMadeInactiveRan);
            Assert.IsFalse(instantiatedObjs[1].activeInHierarchy);
            Assert.AreEqual(Vector3.one, instantiatedObjs[1].transform.position);
            Assert.AreEqual(new Quaternion(1, 2, 3, 4).eulerAngles, instantiatedObjs[1].transform.rotation.eulerAngles);
            returnedObjs = objPool.AllGameObjects;
            Assert.IsNotNull(returnedObjs);
            Assert.IsEmpty(returnedObjs);
            Assert.AreEqual(0, objPool.transform.GetChild(0).childCount);
            Assert.AreEqual(1, objPool.transform.GetChild(1).childCount);
            Assert.AreEqual($"{GameObjectUtility.GetActualName(obj).ToUpper()}_ANCHOR", objPool.transform.GetChild(1).GetChild(0).name);
            Assert.AreEqual(2, objPool.transform.GetChild(1).GetChild(0).childCount);
            Assert.AreEqual(instantiatedObjs[0], objPool.transform.GetChild(1).GetChild(0).GetChild(0).gameObject);
            Assert.AreEqual(instantiatedObjs[1], objPool.transform.GetChild(1).GetChild(0).GetChild(1).gameObject);

            ResetFlags();
            instantiatedObjs.Add(objPool.Instantiate(obj2, new Vector3(4, 5, 6), new Quaternion(5, 6, 7, 8), false, true));
            Assert.IsNotNull(instantiatedObjs[2]);
            Assert.IsTrue(instantiatedObjs[2]);
            Assert.AreNotEqual(obj2, instantiatedObjs[2]);
            Assert.IsFalse(onObjectMadeActiveRan);
            Assert.IsTrue(onObjectMadeInactiveRan);
            Assert.IsFalse(instantiatedObjs[2].activeInHierarchy);
            Assert.AreEqual(new Vector3(4, 5, 6), instantiatedObjs[2].transform.position);
            Assert.AreEqual(new Quaternion(5, 6, 7, 8).eulerAngles, instantiatedObjs[2].transform.rotation.eulerAngles);
            returnedObjs = objPool.AllGameObjects;
            Assert.IsNotNull(returnedObjs);
            Assert.IsEmpty(returnedObjs);
            Assert.AreEqual(0, objPool.transform.GetChild(0).childCount);
            Assert.AreEqual(2, objPool.transform.GetChild(1).childCount);
            Assert.AreEqual($"{GameObjectUtility.GetActualName(obj).ToUpper()}_ANCHOR", objPool.transform.GetChild(1).GetChild(0).name);
            Assert.AreEqual(2, objPool.transform.GetChild(1).GetChild(0).childCount);
            Assert.AreEqual(instantiatedObjs[0], objPool.transform.GetChild(1).GetChild(0).GetChild(0).gameObject);
            Assert.AreEqual(instantiatedObjs[1], objPool.transform.GetChild(1).GetChild(0).GetChild(1).gameObject);
            Assert.AreEqual($"{GameObjectUtility.GetActualName(obj2).ToUpper()}_ANCHOR", objPool.transform.GetChild(1).GetChild(1).name);
            Assert.AreEqual(1, objPool.transform.GetChild(1).GetChild(1).childCount);
            Assert.AreEqual(instantiatedObjs[2], objPool.transform.GetChild(1).GetChild(1).GetChild(0).gameObject);
        }
        [UnityTest]
        public IEnumerator Instantiate_Position_Rotation_AsActive_NotPoolManaged_Tests()
        {
            yield return new WaitForEndOfFrame();

            instantiatedObjs.Add(objPool.Instantiate(obj, Vector3.one, Quaternion.identity, true, false));
            Assert.IsNotNull(instantiatedObjs[0]);
            Assert.IsTrue(instantiatedObjs[0]);
            Assert.AreNotEqual(obj, instantiatedObjs[0]);
            Assert.IsFalse(onObjectMadeActiveRan);
            Assert.IsFalse(onObjectMadeInactiveRan);
            Assert.IsTrue(instantiatedObjs[0].activeInHierarchy);
            Assert.AreEqual(Vector3.one, instantiatedObjs[0].transform.position);
            Assert.AreEqual(Vector3.zero, instantiatedObjs[0].transform.eulerAngles);
            returnedObjs = objPool.AllGameObjects;
            Assert.IsNotNull(returnedObjs);
            Assert.IsEmpty(returnedObjs);
            Assert.AreEqual(0, objPool.transform.GetChild(0).childCount);

            instantiatedObjs.Add(objPool.Instantiate(obj, new Vector3(1, 2, 3), Quaternion.identity, true, false));
            Assert.IsNotNull(instantiatedObjs[1]);
            Assert.IsTrue(instantiatedObjs[1]);
            Assert.AreNotEqual(obj, instantiatedObjs[1]);
            Assert.IsFalse(onObjectMadeActiveRan);
            Assert.IsFalse(onObjectMadeInactiveRan);
            Assert.IsTrue(instantiatedObjs[1].activeInHierarchy);
            Assert.AreEqual(new Vector3(1, 2, 3), instantiatedObjs[1].transform.position);
            Assert.AreEqual(Vector3.zero, instantiatedObjs[1].transform.eulerAngles);
            returnedObjs = objPool.AllGameObjects;
            Assert.IsNotNull(returnedObjs);
            Assert.IsEmpty(returnedObjs);
            Assert.AreEqual(0, objPool.transform.GetChild(0).childCount);

            instantiatedObjs.Add(objPool.Instantiate(obj2, Vector3.one, new Quaternion(1, 2, 3, 4), true, false));
            Assert.IsNotNull(instantiatedObjs[2]);
            Assert.IsTrue(instantiatedObjs[2]);
            Assert.AreNotEqual(obj2, instantiatedObjs[2]);
            Assert.IsFalse(onObjectMadeActiveRan);
            Assert.IsFalse(onObjectMadeInactiveRan);
            Assert.IsTrue(instantiatedObjs[2].activeInHierarchy);
            Assert.AreEqual(Vector3.one, instantiatedObjs[2].transform.position);
            Assert.AreEqual(new Quaternion(1, 2, 3, 4).eulerAngles, instantiatedObjs[2].transform.rotation.eulerAngles);
            returnedObjs = objPool.AllGameObjects;
            Assert.IsNotNull(returnedObjs);
            Assert.IsEmpty(returnedObjs);
            Assert.AreEqual(0, objPool.transform.GetChild(0).childCount);

            obj2.SetActive(false);
            instantiatedObjs.Add(objPool.Instantiate(obj2, new Vector3(1, 2, 3), new Quaternion(1, 2, 3, 4), true, false));
            Assert.IsNotNull(instantiatedObjs[3]);
            Assert.IsTrue(instantiatedObjs[3]);
            Assert.AreNotEqual(obj2, instantiatedObjs[3]);
            Assert.IsFalse(onObjectMadeActiveRan);
            Assert.IsFalse(onObjectMadeInactiveRan);
            Assert.IsFalse(instantiatedObjs[3].activeInHierarchy);
            Assert.AreEqual(new Vector3(1, 2, 3), instantiatedObjs[3].transform.position);
            Assert.AreEqual(new Quaternion(1, 2, 3, 4).eulerAngles, instantiatedObjs[3].transform.rotation.eulerAngles);
            returnedObjs = objPool.AllGameObjects;
            Assert.IsNotNull(returnedObjs);
            Assert.IsEmpty(returnedObjs);
            Assert.AreEqual(0, objPool.transform.GetChild(0).childCount);

            objPool.Destroy(instantiatedObjs[0]);
            Assert.IsFalse(instantiatedObjs[0].activeInHierarchy);
            Assert.AreEqual($"{GameObjectUtility.GetActualName(obj).ToUpper()}_ANCHOR", objPool.transform.GetChild(1).GetChild(0).name);
            Assert.AreEqual(1, objPool.transform.GetChild(1).GetChild(0).childCount);
            Assert.AreEqual(instantiatedObjs[0], objPool.transform.GetChild(1).GetChild(0).GetChild(0).gameObject);
            instantiatedObjs.Add(objPool.Instantiate(obj, new Vector3(4, 5, 6), new Quaternion(5, 6, 7, 8), true, false));
            Assert.IsNotNull(instantiatedObjs[4]);
            Assert.IsTrue(instantiatedObjs[4]);
            Assert.AreNotEqual(obj, instantiatedObjs[4]);
            Assert.IsFalse(onObjectMadeActiveRan);
            Assert.IsTrue(onObjectMadeInactiveRan);
            Assert.IsFalse(instantiatedObjs[4].activeInHierarchy);
            Assert.AreEqual(new Vector3(4, 5, 6), instantiatedObjs[4].transform.position);
            Assert.AreEqual(new Quaternion(5, 6, 7, 8).eulerAngles, instantiatedObjs[4].transform.rotation.eulerAngles);
            returnedObjs = objPool.AllGameObjects;
            Assert.IsNotNull(returnedObjs);
            Assert.IsEmpty(returnedObjs);
            Assert.AreEqual($"{GameObjectUtility.GetActualName(obj).ToUpper()}_ANCHOR", objPool.transform.GetChild(1).GetChild(0).name);
            Assert.AreEqual(0, objPool.transform.GetChild(1).GetChild(0).childCount);
        }
        [UnityTest]
        public IEnumerator Instantiate_Position_Rotation_AsInactive_NotPoolManaged_Tests()
        {
            yield return new WaitForEndOfFrame();

            instantiatedObjs.Add(objPool.Instantiate(obj, Vector3.one, Quaternion.identity, false, false));
            Assert.IsNotNull(instantiatedObjs[0]);
            Assert.IsTrue(instantiatedObjs[0]);
            Assert.AreNotEqual(obj, instantiatedObjs[0]);
            Assert.IsFalse(onObjectMadeActiveRan);
            Assert.IsFalse(onObjectMadeInactiveRan);
            Assert.IsTrue(instantiatedObjs[0].activeInHierarchy);
            Assert.AreEqual(Vector3.one, instantiatedObjs[0].transform.position);
            Assert.AreEqual(Vector3.zero, instantiatedObjs[0].transform.eulerAngles);
            returnedObjs = objPool.AllGameObjects;
            Assert.IsNotNull(returnedObjs);
            Assert.IsEmpty(returnedObjs);
            Assert.AreEqual(0, objPool.transform.GetChild(0).childCount);
            Assert.AreEqual(0, objPool.transform.GetChild(1).childCount);

            instantiatedObjs.Add(objPool.Instantiate(obj, new Vector3(1, 2, 3), Quaternion.identity, false, false));
            Assert.IsNotNull(instantiatedObjs[1]);
            Assert.IsTrue(instantiatedObjs[1]);
            Assert.AreNotEqual(obj, instantiatedObjs[1]);
            Assert.IsFalse(onObjectMadeActiveRan);
            Assert.IsFalse(onObjectMadeInactiveRan);
            Assert.IsTrue(instantiatedObjs[1].activeInHierarchy);
            Assert.AreEqual(new Vector3(1, 2, 3), instantiatedObjs[1].transform.position);
            Assert.AreEqual(Vector3.zero, instantiatedObjs[1].transform.eulerAngles);
            returnedObjs = objPool.AllGameObjects;
            Assert.IsNotNull(returnedObjs);
            Assert.IsEmpty(returnedObjs);
            Assert.AreEqual(0, objPool.transform.GetChild(0).childCount);
            Assert.AreEqual(0, objPool.transform.GetChild(1).childCount);

            instantiatedObjs.Add(objPool.Instantiate(obj2, Vector3.one, new Quaternion(1, 2, 3, 4), false, false));
            Assert.IsNotNull(instantiatedObjs[2]);
            Assert.IsTrue(instantiatedObjs[2]);
            Assert.AreNotEqual(obj2, instantiatedObjs[2]);
            Assert.IsFalse(onObjectMadeActiveRan);
            Assert.IsFalse(onObjectMadeInactiveRan);
            Assert.IsTrue(instantiatedObjs[2].activeInHierarchy);
            Assert.AreEqual(Vector3.one, instantiatedObjs[2].transform.position);
            Assert.AreEqual(new Quaternion(1, 2, 3, 4).eulerAngles, instantiatedObjs[2].transform.rotation.eulerAngles);
            returnedObjs = objPool.AllGameObjects;
            Assert.IsNotNull(returnedObjs);
            Assert.IsEmpty(returnedObjs);
            Assert.AreEqual(0, objPool.transform.GetChild(0).childCount);
            Assert.AreEqual(0, objPool.transform.GetChild(1).childCount);

            obj2.SetActive(false);
            instantiatedObjs.Add(objPool.Instantiate(obj2, new Vector3(1, 2, 3), new Quaternion(1, 2, 3, 4), false, false));
            Assert.IsNotNull(instantiatedObjs[3]);
            Assert.IsTrue(instantiatedObjs[3]);
            Assert.AreNotEqual(obj2, instantiatedObjs[3]);
            Assert.IsFalse(onObjectMadeActiveRan);
            Assert.IsFalse(onObjectMadeInactiveRan);
            Assert.IsFalse(instantiatedObjs[3].activeInHierarchy);
            Assert.AreEqual(new Vector3(1, 2, 3), instantiatedObjs[3].transform.position);
            Assert.AreEqual(new Quaternion(1, 2, 3, 4).eulerAngles, instantiatedObjs[3].transform.rotation.eulerAngles);
            returnedObjs = objPool.AllGameObjects;
            Assert.IsNotNull(returnedObjs);
            Assert.IsEmpty(returnedObjs);
            Assert.AreEqual(0, objPool.transform.GetChild(0).childCount);
            Assert.AreEqual(0, objPool.transform.GetChild(1).childCount);
        }
        [UnityTest]
        public IEnumerator Instantiate_Position_Rotation_InvalidInput_Tests()
        {
            yield return new WaitForEndOfFrame();

            instantiatedObjs.Add(objPool.Instantiate(null, Vector3.zero, Quaternion.identity, true, true));
            Assert.IsNull(instantiatedObjs[0]);
        }

        // Position | Rotation | Parent
        [UnityTest]
        public IEnumerator Instantiate_Position_Rotation_Parent_AsActive_NotPoolManaged_Tests()
        {
            yield return new WaitForEndOfFrame();

            instantiatedObjs.Add(objPool.Instantiate(obj, Vector3.one, Quaternion.identity, dontAddObj.transform, true, false));
            Assert.IsNotNull(instantiatedObjs[0]);
            Assert.IsTrue(instantiatedObjs[0]);
            Assert.AreNotEqual(obj, instantiatedObjs[0]);
            Assert.AreEqual(dontAddObj, instantiatedObjs[0].transform.parent.gameObject);
            Assert.IsFalse(onObjectMadeActiveRan);
            Assert.IsFalse(onObjectMadeInactiveRan);
            Assert.IsTrue(instantiatedObjs[0].activeInHierarchy);
            Assert.AreEqual(Vector3.one, instantiatedObjs[0].transform.position);
            Assert.AreEqual(Vector3.zero, instantiatedObjs[0].transform.eulerAngles);
            returnedObjs = objPool.AllGameObjects;
            Assert.IsNotNull(returnedObjs);
            Assert.IsEmpty(returnedObjs);
            Assert.AreEqual(0, objPool.transform.GetChild(0).childCount);

            instantiatedObjs.Add(objPool.Instantiate(obj, new Vector3(1, 2, 3), Quaternion.identity, dontAddObj.transform, true, false));
            Assert.IsNotNull(instantiatedObjs[1]);
            Assert.IsTrue(instantiatedObjs[1]);
            Assert.AreNotEqual(obj, instantiatedObjs[1]);
            Assert.AreEqual(dontAddObj, instantiatedObjs[0].transform.parent.gameObject);
            Assert.IsFalse(onObjectMadeActiveRan);
            Assert.IsFalse(onObjectMadeInactiveRan);
            Assert.IsTrue(instantiatedObjs[1].activeInHierarchy);
            Assert.AreEqual(new Vector3(1, 2, 3), instantiatedObjs[1].transform.position);
            Assert.AreEqual(Vector3.zero, instantiatedObjs[1].transform.eulerAngles);
            returnedObjs = objPool.AllGameObjects;
            Assert.IsNotNull(returnedObjs);
            Assert.IsEmpty(returnedObjs);
            Assert.AreEqual(0, objPool.transform.GetChild(0).childCount);

            instantiatedObjs.Add(objPool.Instantiate(obj2, Vector3.one, new Quaternion(1, 2, 3, 4), dontAddObj.transform, true, false));
            Assert.IsNotNull(instantiatedObjs[2]);
            Assert.IsTrue(instantiatedObjs[2]);
            Assert.AreNotEqual(obj2, instantiatedObjs[2]);
            Assert.AreEqual(dontAddObj, instantiatedObjs[0].transform.parent.gameObject);
            Assert.IsFalse(onObjectMadeActiveRan);
            Assert.IsFalse(onObjectMadeInactiveRan);
            Assert.IsTrue(instantiatedObjs[2].activeInHierarchy);
            Assert.AreEqual(Vector3.one, instantiatedObjs[2].transform.position);
            Assert.AreEqual(new Quaternion(1, 2, 3, 4).eulerAngles, instantiatedObjs[2].transform.rotation.eulerAngles);
            returnedObjs = objPool.AllGameObjects;
            Assert.IsNotNull(returnedObjs);
            Assert.IsEmpty(returnedObjs);
            Assert.AreEqual(0, objPool.transform.GetChild(0).childCount);

            obj2.SetActive(false);
            instantiatedObjs.Add(objPool.Instantiate(obj2, new Vector3(1, 2, 3), new Quaternion(1, 2, 3, 4), dontAddObj.transform, true, false));
            Assert.IsNotNull(instantiatedObjs[3]);
            Assert.IsTrue(instantiatedObjs[3]);
            Assert.AreNotEqual(obj2, instantiatedObjs[3]);
            Assert.AreEqual(dontAddObj, instantiatedObjs[0].transform.parent.gameObject);
            Assert.IsFalse(onObjectMadeActiveRan);
            Assert.IsFalse(onObjectMadeInactiveRan);
            Assert.IsFalse(instantiatedObjs[3].activeInHierarchy);
            Assert.AreEqual(new Vector3(1, 2, 3), instantiatedObjs[3].transform.position);
            Assert.AreEqual(new Quaternion(1, 2, 3, 4).eulerAngles, instantiatedObjs[3].transform.rotation.eulerAngles);
            returnedObjs = objPool.AllGameObjects;
            Assert.IsNotNull(returnedObjs);
            Assert.IsEmpty(returnedObjs);
            Assert.AreEqual(0, objPool.transform.GetChild(0).childCount);

            objPool.Destroy(instantiatedObjs[0]);
            Assert.IsFalse(instantiatedObjs[0].activeInHierarchy);
            Assert.AreEqual($"{GameObjectUtility.GetActualName(obj).ToUpper()}_ANCHOR", objPool.transform.GetChild(1).GetChild(0).name);
            Assert.AreEqual(1, objPool.transform.GetChild(1).GetChild(0).childCount);
            Assert.AreEqual(instantiatedObjs[0], objPool.transform.GetChild(1).GetChild(0).GetChild(0).gameObject);
            instantiatedObjs.Add(objPool.Instantiate(obj, new Vector3(4, 5, 6), new Quaternion(5, 6, 7, 8), dontAddObj.transform, true, false));
            Assert.IsNotNull(instantiatedObjs[4]);
            Assert.IsTrue(instantiatedObjs[4]);
            Assert.AreNotEqual(obj, instantiatedObjs[4]);
            Assert.AreEqual(dontAddObj, instantiatedObjs[0].transform.parent.gameObject);
            Assert.IsFalse(onObjectMadeActiveRan);
            Assert.IsTrue(onObjectMadeInactiveRan);
            Assert.IsFalse(instantiatedObjs[4].activeInHierarchy);
            Assert.AreEqual(new Vector3(4, 5, 6), instantiatedObjs[4].transform.position);
            Assert.AreEqual(new Quaternion(5, 6, 7, 8).eulerAngles, instantiatedObjs[4].transform.rotation.eulerAngles);
            returnedObjs = objPool.AllGameObjects;
            Assert.IsNotNull(returnedObjs);
            Assert.IsEmpty(returnedObjs);
            Assert.AreEqual($"{GameObjectUtility.GetActualName(obj).ToUpper()}_ANCHOR", objPool.transform.GetChild(1).GetChild(0).name);
            Assert.AreEqual(0, objPool.transform.GetChild(1).GetChild(0).childCount);
        }
        [UnityTest]
        public IEnumerator Instantiate_Position_Rotation_Parent_InvalidInput_Tests()
        {
            yield return new WaitForEndOfFrame();
            objPool.OnObjectMadeActive = null;
            objPool.OnObjectMadeInactive = null;

            instantiatedObjs.Add(objPool.Instantiate(null, null, null, null, true, true));
            Assert.IsNull(instantiatedObjs[0]);

            instantiatedObjs.Add(objPool.Instantiate(obj, null, null, null, true, true));
            Assert.IsNotNull(instantiatedObjs[1]);
            Assert.AreEqual(instantiatedObjs[1], objPool.AllGameObjects[0]);

            instantiatedObjs.Add(objPool.Instantiate(obj, Vector3.one, null, null, true, true)); // Requires both position and rotation
            Assert.IsNull(instantiatedObjs[2]);

            instantiatedObjs.Add(objPool.Instantiate(obj, Vector3.one, new Quaternion(1, 2, 3, 4), null, true, true));
            Assert.IsNotNull(instantiatedObjs[3]);
            Assert.AreEqual(instantiatedObjs[3], objPool.AllGameObjects[1]);
            Assert.AreEqual(Vector3.one, instantiatedObjs[3].transform.position);
            Assert.AreEqual(new Quaternion(1, 2, 3, 4).eulerAngles, instantiatedObjs[3].transform.rotation.eulerAngles);

            instantiatedObjs.Add(objPool.Instantiate(null, Vector3.one, Quaternion.identity, instantiatedObjs[1].transform, true, true));
            Assert.IsNull(instantiatedObjs[4]);

            instantiatedObjs.Add(objPool.Instantiate(null, null, Quaternion.identity, instantiatedObjs[1].transform, true, true));
            Assert.IsNull(instantiatedObjs[5]);

            instantiatedObjs.Add(objPool.Instantiate(null, null, null, instantiatedObjs[1].transform, true, true));
            Assert.IsNull(instantiatedObjs[6]);

            instantiatedObjs.Add(objPool.Instantiate(null, Vector3.one, null, null, true, true));
            Assert.IsNull(instantiatedObjs[7]);

            instantiatedObjs.Add(objPool.Instantiate(null, Vector3.one, Quaternion.identity, null, true, true));
            Assert.IsNull(instantiatedObjs[8]);

            instantiatedObjs.Add(objPool.Instantiate(null, Vector3.one, null, instantiatedObjs[1].transform, true, true));
            Assert.IsNull(instantiatedObjs[9]);

            instantiatedObjs.Add(objPool.Instantiate(obj, null, Quaternion.identity, null, true, true)); // Requires both position and rotation
            Assert.IsNull(instantiatedObjs[10]);

            instantiatedObjs.Add(objPool.Instantiate(obj, null, null, instantiatedObjs[1].transform, true, false));
            Assert.IsNotNull(instantiatedObjs[11]);
            Assert.AreEqual(instantiatedObjs[1], instantiatedObjs[11].transform.parent.gameObject);
        }
        #endregion

        [UnityTest]
        public IEnumerator Destroy_Tests()
        {
            yield return new WaitForEndOfFrame();

            InstantiateObjects(1);

            ResetFlags();
            DestroyObject(1);
            Assert.IsFalse(onObjectMadeActiveRan);
            Assert.IsTrue(onObjectMadeInactiveRan);
            returnedObjs = objPool.AllGameObjects;
            Assert.IsTrue(destroyedObjs[0]);
            Assert.IsFalse(destroyedObjs[0].activeInHierarchy);
            CollectionAssert.DoesNotContain(returnedObjs, destroyedObjs[0]);
            Assert.AreEqual(1, objPool.transform.GetChild(1).childCount);
            Assert.AreEqual($"{GameObjectUtility.GetActualName(destroyedObjs[0]).ToUpper()}_ANCHOR", objPool.transform.GetChild(1).GetChild(0).name);
            Assert.AreEqual(1, objPool.transform.GetChild(1).GetChild(0).childCount);
            Assert.AreEqual(destroyedObjs[0], objPool.transform.GetChild(1).GetChild(0).GetChild(0).gameObject);
            Assert.AreEqual(3, objPool.transform.GetChild(0).childCount);
            Assert.AreEqual($"{GameObjectUtility.GetActualName(destroyedObjs[0]).ToUpper()}_ANCHOR", objPool.transform.GetChild(0).GetChild(1).name);
            Assert.AreEqual(1, objPool.transform.GetChild(0).GetChild(1).childCount);

            ResetFlags();
            objPool.Destroy(destroyedObjs[0]);
            Assert.IsFalse(onObjectMadeActiveRan);
            Assert.IsTrue(onObjectMadeInactiveRan);
            returnedObjs = objPool.AllGameObjects;
            Assert.IsTrue(destroyedObjs[0]);
            Assert.IsFalse(destroyedObjs[0].activeInHierarchy);
            CollectionAssert.DoesNotContain(returnedObjs, destroyedObjs[0]);
            Assert.AreEqual(1, objPool.transform.GetChild(1).childCount);
            Assert.AreEqual($"{GameObjectUtility.GetActualName(destroyedObjs[0]).ToUpper()}_ANCHOR", objPool.transform.GetChild(1).GetChild(0).name);
            Assert.AreEqual(1, objPool.transform.GetChild(1).GetChild(0).childCount);
            Assert.AreEqual(destroyedObjs[0], objPool.transform.GetChild(1).GetChild(0).GetChild(0).gameObject);
            Assert.AreEqual(3, objPool.transform.GetChild(0).childCount);
            Assert.AreEqual($"{GameObjectUtility.GetActualName(destroyedObjs[0]).ToUpper()}_ANCHOR", objPool.transform.GetChild(0).GetChild(1).name);
            Assert.AreEqual(1, objPool.transform.GetChild(0).GetChild(1).childCount);

            ResetFlags();
            objPool.Destroy(dontAddObj);
            Assert.IsFalse(onObjectMadeActiveRan);
            Assert.IsTrue(onObjectMadeInactiveRan);
            returnedObjs = objPool.AllGameObjects;
            Assert.IsTrue(dontAddObj);
            Assert.IsFalse(dontAddObj.activeInHierarchy);
            CollectionAssert.DoesNotContain(returnedObjs, dontAddObj);
            Assert.AreEqual(2, objPool.transform.GetChild(1).childCount);
            Assert.AreEqual($"{GameObjectUtility.GetActualName(dontAddObj).ToUpper()}_ANCHOR", objPool.transform.GetChild(1).GetChild(1).name);
            Assert.AreEqual(1, objPool.transform.GetChild(1).GetChild(1).childCount);
            Assert.AreEqual(dontAddObj, objPool.transform.GetChild(1).GetChild(1).GetChild(0).gameObject);
            Assert.AreEqual(3, objPool.transform.GetChild(0).childCount);
        }
        [UnityTest]
        public IEnumerator Destroy_InvalidInput_Tests()
        {
            yield return new WaitForEndOfFrame();

            objPool.Destroy(null);
        }

        [UnityTest]
        public IEnumerator AddActiveObject_Tests()
        {
            yield return new WaitForEndOfFrame();

            Assert.IsTrue(objPool.AddActiveObject(obj));
            Assert.IsTrue(onObjectMadeActiveRan);
            Assert.IsFalse(onObjectMadeInactiveRan);
            Assert.IsTrue(obj.activeInHierarchy);
            returnedObjs = objPool.AllGameObjects;
            Assert.IsNotNull(returnedObjs);
            Assert.IsNotEmpty(returnedObjs);
            Assert.AreEqual(1, returnedObjs.Count);
            Assert.AreEqual(obj, returnedObjs[0]);
            Assert.AreEqual(1, objPool.transform.GetChild(0).childCount);
            Assert.AreEqual($"{GameObjectUtility.GetActualName(obj).ToUpper()}_ANCHOR", objPool.transform.GetChild(0).GetChild(0).name);
            Assert.AreEqual(1, objPool.transform.GetChild(0).GetChild(0).childCount);
            Assert.AreEqual(obj, objPool.transform.GetChild(0).GetChild(0).GetChild(0).gameObject);

            ResetFlags();
            obj2.SetActive(false);
            Assert.IsTrue(objPool.AddActiveObject(obj2));
            Assert.IsTrue(onObjectMadeActiveRan);
            Assert.IsFalse(onObjectMadeInactiveRan);
            Assert.IsFalse(obj2.activeInHierarchy);
            returnedObjs = objPool.AllGameObjects;
            Assert.IsNotNull(returnedObjs);
            Assert.IsNotEmpty(returnedObjs);
            Assert.AreEqual(2, returnedObjs.Count);
            Assert.AreEqual(obj2, returnedObjs[1]);
            Assert.AreEqual(2, objPool.transform.GetChild(0).childCount);
            Assert.AreEqual($"{GameObjectUtility.GetActualName(obj2).ToUpper()}_ANCHOR", objPool.transform.GetChild(0).GetChild(1).name);
            Assert.AreEqual(1, objPool.transform.GetChild(0).GetChild(1).childCount);
            Assert.AreEqual(obj2, objPool.transform.GetChild(0).GetChild(1).GetChild(0).gameObject);

            objPool.AddInactiveObject(obj);
            Assert.AreEqual(1, objPool.transform.GetChild(1).childCount);
            Assert.AreEqual($"{GameObjectUtility.GetActualName(obj).ToUpper()}_ANCHOR", objPool.transform.GetChild(1).GetChild(0).name);
            Assert.AreEqual(1, objPool.transform.GetChild(1).GetChild(0).childCount);
            Assert.AreEqual(obj, objPool.transform.GetChild(1).GetChild(0).GetChild(0).gameObject);
            ResetFlags();
            Assert.IsTrue(objPool.AddActiveObject(obj));
            Assert.IsTrue(onObjectMadeActiveRan);
            Assert.IsFalse(onObjectMadeInactiveRan);
            Assert.IsFalse(obj.activeInHierarchy);
            returnedObjs = objPool.AllGameObjects;
            Assert.IsNotNull(returnedObjs);
            Assert.IsNotEmpty(returnedObjs);
            Assert.AreEqual(2, returnedObjs.Count);
            Assert.AreEqual(obj, returnedObjs[0]);
            Assert.AreEqual(2, objPool.transform.GetChild(0).childCount);
            Assert.AreEqual($"{GameObjectUtility.GetActualName(obj).ToUpper()}_ANCHOR", objPool.transform.GetChild(0).GetChild(0).name);
            Assert.AreEqual(1, objPool.transform.GetChild(0).GetChild(0).childCount);
            Assert.AreEqual(obj, objPool.transform.GetChild(0).GetChild(0).GetChild(0).gameObject);
            Assert.AreEqual(1, objPool.transform.GetChild(1).childCount);
            Assert.AreEqual($"{GameObjectUtility.GetActualName(obj).ToUpper()}_ANCHOR", objPool.transform.GetChild(1).GetChild(0).name);
            Assert.AreEqual(0, objPool.transform.GetChild(1).GetChild(0).childCount);
        }
        [UnityTest]
        public IEnumerator AddActiveObject_InvalidInput_Tests()
        {
            yield return new WaitForEndOfFrame();

            Assert.IsFalse(objPool.AddActiveObject(null));
        }

        [UnityTest]
        public IEnumerator AddInactiveObject_Tests()
        {
            yield return new WaitForEndOfFrame();

            Assert.IsTrue(objPool.AddInactiveObject(obj));
            Assert.IsFalse(onObjectMadeActiveRan);
            Assert.IsTrue(onObjectMadeInactiveRan);
            Assert.IsFalse(obj.activeInHierarchy);
            returnedObjs = objPool.AllGameObjects;
            Assert.IsNotNull(returnedObjs);
            Assert.IsEmpty(returnedObjs);
            Assert.AreEqual(1, objPool.transform.GetChild(1).childCount);
            Assert.AreEqual($"{GameObjectUtility.GetActualName(obj).ToUpper()}_ANCHOR", objPool.transform.GetChild(1).GetChild(0).name);
            Assert.AreEqual(1, objPool.transform.GetChild(1).GetChild(0).childCount);
            Assert.AreEqual(obj, objPool.transform.GetChild(1).GetChild(0).GetChild(0).gameObject);

            ResetFlags();
            obj2.SetActive(false);
            Assert.IsTrue(objPool.AddInactiveObject(obj2));
            Assert.IsFalse(onObjectMadeActiveRan);
            Assert.IsTrue(onObjectMadeInactiveRan);
            Assert.IsFalse(obj2.activeInHierarchy);
            returnedObjs = objPool.AllGameObjects;
            Assert.IsNotNull(returnedObjs);
            Assert.IsEmpty(returnedObjs);
            Assert.AreEqual(2, objPool.transform.GetChild(1).childCount);
            Assert.AreEqual($"{GameObjectUtility.GetActualName(obj2).ToUpper()}_ANCHOR", objPool.transform.GetChild(1).GetChild(1).name);
            Assert.AreEqual(1, objPool.transform.GetChild(1).GetChild(1).childCount);
            Assert.AreEqual(obj2, objPool.transform.GetChild(1).GetChild(1).GetChild(0).gameObject);

            objPool.AddActiveObject(obj);
            Assert.AreEqual(1, objPool.transform.GetChild(0).childCount);
            Assert.AreEqual($"{GameObjectUtility.GetActualName(obj).ToUpper()}_ANCHOR", objPool.transform.GetChild(0).GetChild(0).name);
            Assert.AreEqual(1, objPool.transform.GetChild(0).GetChild(0).childCount);
            Assert.AreEqual(obj, objPool.transform.GetChild(0).GetChild(0).GetChild(0).gameObject);
            ResetFlags();
            Assert.IsTrue(objPool.AddInactiveObject(obj));
            Assert.IsFalse(onObjectMadeActiveRan);
            Assert.IsTrue(onObjectMadeInactiveRan);
            Assert.IsFalse(obj.activeInHierarchy);
            returnedObjs = objPool.AllGameObjects;
            Assert.IsNotNull(returnedObjs);
            Assert.IsEmpty(returnedObjs);
            Assert.AreEqual(2, objPool.transform.GetChild(1).childCount);
            Assert.AreEqual($"{GameObjectUtility.GetActualName(obj).ToUpper()}_ANCHOR", objPool.transform.GetChild(1).GetChild(0).name);
            Assert.AreEqual(1, objPool.transform.GetChild(1).GetChild(0).childCount);
            Assert.AreEqual(obj, objPool.transform.GetChild(1).GetChild(0).GetChild(0).gameObject);
            Assert.AreEqual(1, objPool.transform.GetChild(0).childCount);
            Assert.AreEqual($"{GameObjectUtility.GetActualName(obj).ToUpper()}_ANCHOR", objPool.transform.GetChild(0).GetChild(0).name);
            Assert.AreEqual(0, objPool.transform.GetChild(0).GetChild(0).childCount);
        }
        [UnityTest]
        public IEnumerator AddInactiveObject_InvalidInput_Tests()
        {
            yield return new WaitForEndOfFrame();

            Assert.IsFalse(objPool.AddInactiveObject(null));
        }

        [UnityTest]
        public IEnumerator CheckIfExists_Tests()
        {
            yield return new WaitForEndOfFrame();

            Assert.IsFalse(objPool.CheckIfExists(obj));
            Assert.IsFalse(objPool.CheckIfExists(obj2));
            Assert.IsFalse(objPool.CheckIfExists(obj3));

            InstantiateObjects();
            Assert.IsFalse(objPool.CheckIfExists(obj));
            Assert.IsFalse(objPool.CheckIfExists(obj2));
            Assert.IsFalse(objPool.CheckIfExists(obj3));
            Assert.IsTrue(objPool.CheckIfExists(instantiatedObjs[0]));
            Assert.IsTrue(objPool.CheckIfExists(instantiatedObjs[1]));
            Assert.IsTrue(objPool.CheckIfExists(instantiatedObjs[2]));

            DestroyObject(1);
            Assert.IsFalse(objPool.CheckIfExists(obj));
            Assert.IsFalse(objPool.CheckIfExists(obj2));
            Assert.IsFalse(objPool.CheckIfExists(obj3));
            Assert.IsTrue(objPool.CheckIfExists(instantiatedObjs[0]));
            Assert.IsTrue(objPool.CheckIfExists(destroyedObjs[0]));
            Assert.IsTrue(objPool.CheckIfExists(instantiatedObjs[1]));
        }
        [UnityTest]
        public IEnumerator CheckIfExists_InvalidInput_Tests()
        {
            yield return new WaitForEndOfFrame();

            Assert.IsFalse(objPool.CheckIfExists(null));
        }

        [UnityTest]
        public IEnumerator GetActiveObjects_Tests()
        {
            yield return new WaitForEndOfFrame();

            returnedObjs = objPool.GetActiveObjects(obj);
            Assert.IsNotNull(returnedObjs);
            CollectionAssert.IsEmpty(returnedObjs);

            InstantiateObjects(1);
            returnedObjs = objPool.GetActiveObjects(obj);
            Assert.IsNotNull(returnedObjs);
            Assert.AreEqual(2, returnedObjs.Count);
            Assert.AreEqual(instantiatedObjs[0], returnedObjs[0]);
            Assert.AreEqual(instantiatedObjs[3], returnedObjs[1]);
            returnedObjs = objPool.GetActiveObjects(obj2);
            Assert.IsNotNull(returnedObjs);
            Assert.AreEqual(2, returnedObjs.Count);
            Assert.AreEqual(instantiatedObjs[1], returnedObjs[0]);
            Assert.AreEqual(instantiatedObjs[4], returnedObjs[1]);
            returnedObjs = objPool.GetActiveObjects(obj3);
            Assert.IsNotNull(returnedObjs);
            Assert.AreEqual(2, returnedObjs.Count);
            Assert.AreEqual(instantiatedObjs[2], returnedObjs[0]);
            Assert.AreEqual(instantiatedObjs[5], returnedObjs[1]);

            returnedObjs = objPool.GetActiveObjects(dontAddObj);
            Assert.IsNotNull(returnedObjs);
            CollectionAssert.IsEmpty(returnedObjs);
        }
        [UnityTest]
        public IEnumerator GetActiveObjects_InvalidInput_Tests()
        {
            yield return new WaitForEndOfFrame();

            returnedObjs = objPool.GetActiveObjects(null);
            Assert.IsNotNull(returnedObjs);
            CollectionAssert.IsEmpty(returnedObjs);
        }

        [UnityTest]
        public IEnumerator GetActiveObjectsComponents_Tests()
        {
            yield return new WaitForEndOfFrame();

            returnedTrans = objPool.GetActiveObjectsComponents<Transform>();
            Assert.IsNotNull(returnedTrans);
            CollectionAssert.IsEmpty(returnedTrans);

            InstantiateObjects(1);
            returnedTrans = objPool.GetActiveObjectsComponents<Transform>();
            Assert.IsNotNull(returnedTrans);
            CollectionAssert.AreEquivalent(instantiatedObjs, TransformsToGameObjects(returnedTrans));

            DestroyObject(1);
            returnedTrans = objPool.GetActiveObjectsComponents<Transform>();
            Assert.IsNotNull(returnedTrans);
            CollectionAssert.AreEquivalent(instantiatedObjs, TransformsToGameObjects(returnedTrans));
        }

        [UnityTest]
        public IEnumerator GetActiveObjectsWithTag_Tests()
        {
            yield return new WaitForEndOfFrame();

            returnedObjs = objPool.GetActiveObjectsWithTag("Untagged");
            Assert.IsNotNull(returnedObjs);
            CollectionAssert.IsEmpty(returnedObjs);

            InstantiateObjects(1);

            returnedObjs = objPool.GetActiveObjectsWithTag(tagName);
            Assert.IsNotNull(returnedObjs);
            Assert.AreEqual(2, returnedObjs.Count);
            Assert.AreEqual(instantiatedObjs[1], returnedObjs[0]);
            Assert.AreEqual(instantiatedObjs[4], returnedObjs[1]);

            returnedObjs = objPool.GetActiveObjectsWithTag("Untagged");
            Assert.IsNotNull(returnedObjs);
            Assert.AreEqual(4, returnedObjs.Count);
            Assert.AreEqual(instantiatedObjs[0], returnedObjs[0]);
            Assert.AreEqual(instantiatedObjs[3], returnedObjs[1]);
            Assert.AreEqual(instantiatedObjs[2], returnedObjs[2]);
            Assert.AreEqual(instantiatedObjs[5], returnedObjs[3]);
        }
        [UnityTest]
        public IEnumerator GetActiveObjectsWithTag_InvalidInput_Tests()
        {
            yield return new WaitForEndOfFrame();

            returnedObjs = objPool.GetActiveObjectsWithTag(null);
            Assert.IsNotNull(returnedObjs);
            CollectionAssert.IsEmpty(returnedObjs);

            returnedObjs = objPool.GetActiveObjectsWithTag("");
            Assert.IsNotNull(returnedObjs);
            CollectionAssert.IsEmpty(returnedObjs);

            returnedObjs = objPool.GetActiveObjectsWithTag("Tag Doesn't Exist");
            Assert.IsNotNull(returnedObjs);
            CollectionAssert.IsEmpty(returnedObjs);
        }

        [UnityTest]
        public IEnumerator GetActiveTransformsWithTag_Tests()
        {
            yield return new WaitForEndOfFrame();

            returnedTrans = objPool.GetActiveTransformsWithTag("Untagged");
            Assert.IsNotNull(returnedTrans);
            CollectionAssert.IsEmpty(returnedTrans);

            InstantiateObjects(1);

            returnedTrans = objPool.GetActiveTransformsWithTag(tagName);
            Assert.IsNotNull(returnedTrans);
            Assert.AreEqual(2, returnedTrans.Count);
            Assert.AreEqual(instantiatedObjs[1].transform, returnedTrans[0]);
            Assert.AreEqual(instantiatedObjs[4].transform, returnedTrans[1]);

            returnedTrans = objPool.GetActiveTransformsWithTag("Untagged");
            Assert.IsNotNull(returnedTrans);
            Assert.AreEqual(4, returnedTrans.Count);
            Assert.AreEqual(instantiatedObjs[0].transform, returnedTrans[0]);
            Assert.AreEqual(instantiatedObjs[3].transform, returnedTrans[1]);
            Assert.AreEqual(instantiatedObjs[2].transform, returnedTrans[2]);
            Assert.AreEqual(instantiatedObjs[5].transform, returnedTrans[3]);
        }
        [UnityTest]
        public IEnumerator GetActiveTransformsWithTag_InvalidInput_Tests()
        {
            yield return new WaitForEndOfFrame();

            returnedTrans = objPool.GetActiveTransformsWithTag(null);
            Assert.IsNotNull(returnedTrans);
            CollectionAssert.IsEmpty(returnedTrans);

            returnedTrans = objPool.GetActiveTransformsWithTag("");
            Assert.IsNotNull(returnedTrans);
            CollectionAssert.IsEmpty(returnedTrans);

            returnedTrans = objPool.GetActiveTransformsWithTag("Tag Doesn't Exist");
            Assert.IsNotNull(returnedTrans);
            CollectionAssert.IsEmpty(returnedTrans);
        }

        [SetUp]
        public void Setup()
        {
            objPool = new GameObject("ObjPool").AddComponent<ObjectPoolManager>();
            objPool.OnObjectMadeActive.AddListener(OnObjectMadeActive);
            objPool.OnObjectMadeInactive.AddListener(OnObjectMadeInactive);

            obj = new GameObject("Obj");
            obj2 = new GameObject("Obj2");
            obj2.tag = tagName;
            obj3 = new GameObject("Obj3");

            dontAddObj = new GameObject("DontAddObj");
        }
        [TearDown]
        public void TearDown()
        {
            if (objPool) GameObject.Destroy(objPool.gameObject);
            objPool = null;

            if (obj) GameObject.Destroy(obj);
            obj = null;
            if (obj2) GameObject.Destroy(obj2);
            obj2 = null;
            if (obj3) GameObject.Destroy(obj3);
            obj3 = null;

            if (dontAddObj) GameObject.Destroy(dontAddObj);
            dontAddObj = null;

            foreach (GameObject obj in instantiatedObjs)
            {
                if (obj) GameObject.Destroy(obj);
            }
            instantiatedObjs = new List<GameObject>();
            foreach (GameObject obj in destroyedObjs)
            {
                if (obj) GameObject.Destroy(obj);
            }
            destroyedObjs = new List<GameObject>();

            returnedObjs = new List<GameObject>();
            returnedTrans = new List<Transform>();

            ResetFlags();
        }

        private void InstantiateObjects(int copies = 0)
        {
            if (copies < 0) return;

            for (int i = -1; i < copies; i++)
            {
                instantiatedObjs.Add(objPool.Instantiate(obj));
                instantiatedObjs.Add(objPool.Instantiate(obj2));
                instantiatedObjs.Add(objPool.Instantiate(obj3));
            }
        }
        private void DestroyObject(int instantiatedObjI)
        {
            objPool.Destroy(instantiatedObjs[instantiatedObjI]);
            destroyedObjs.Add(instantiatedObjs[instantiatedObjI]);
            instantiatedObjs.RemoveAt(instantiatedObjI);
        }

        private List<GameObject> TransformsToGameObjects(List<Transform> trans)
        {
            List<GameObject> transObjs = new List<GameObject>();
            trans.ForEach(x => transObjs.Add(x.gameObject));
            return transObjs;
        }

        private void OnObjectMadeActive(GameObject obj)
        {
            onObjectMadeActiveRan = true;

            Assert.IsNotNull(obj);
            Assert.IsTrue(obj);
        }
        private void OnObjectMadeInactive(GameObject obj)
        {
            onObjectMadeInactiveRan = true;

            Assert.IsNotNull(obj);
            Assert.IsTrue(obj);
        }
        private void ResetFlags()
        {
            onObjectMadeActiveRan = false;
            onObjectMadeInactiveRan = false;
        }
    }
}
