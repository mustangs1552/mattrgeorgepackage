﻿using System.Collections;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;
using MattRGeorge.General.Utilities.Static;
using MattRGeorge.Unity.Tools.ObjectPooling;

namespace MattRGeorge.UnityTests.Unity.Tools.ObjectPooling
{
    public class ObjectPoolObjectUnityTests
    {
        private ObjectPoolManager objPool = null;
        private ObjectPoolObject obj = null;

        [UnityTest]
        public IEnumerator AddToObjectPool_ObjEnabled_AddAsActive_Tests()
        {
            yield return new WaitForEndOfFrame();

            ReflectionUtility.SetInstanceField(typeof(ObjectPoolObject), obj, "addAsActive", true);

            obj.AddToObjectPool();
            CheckObjInPool(true);
            Assert.IsTrue(obj.gameObject.activeInHierarchy);
        }
        [UnityTest]
        public IEnumerator AddToObjectPool_ObjDisabled_AddAsActive_Tests()
        {
            yield return new WaitForEndOfFrame();

            obj.gameObject.SetActive(false);
            ReflectionUtility.SetInstanceField(typeof(ObjectPoolObject), obj, "addAsActive", true);

            obj.AddToObjectPool();
            CheckObjInPool(true);
            Assert.IsFalse(obj.gameObject.activeInHierarchy);
        }

        [UnityTest]
        public IEnumerator AddToObjectPool_ObjEnabled_AddAsInActive_Tests()
        {
            yield return new WaitForEndOfFrame();

            ReflectionUtility.SetInstanceField(typeof(ObjectPoolObject), obj, "addAsActive", false);

            obj.AddToObjectPool();
            CheckObjInPool(false);
            Assert.IsFalse(obj.gameObject.activeInHierarchy);
        }
        [UnityTest]
        public IEnumerator AddToObjectPool_ObjDisabled_AddAsInActive_Tests()
        {
            yield return new WaitForEndOfFrame();

            obj.gameObject.SetActive(false);
            ReflectionUtility.SetInstanceField(typeof(ObjectPoolObject), obj, "addAsActive", false);

            obj.AddToObjectPool();
            CheckObjInPool(false);
            Assert.IsFalse(obj.gameObject.activeInHierarchy);
        }

        [SetUp]
        public void Setup()
        {
            objPool = new GameObject("ObjPool").AddComponent<ObjectPoolManager>();
            obj = new GameObject("Obj").AddComponent<ObjectPoolObject>();
        }
        [TearDown]
        public void TearDown()
        {
            if (objPool) GameObject.DestroyImmediate(objPool.gameObject);
            objPool = null;
            if (obj) GameObject.DestroyImmediate(obj.gameObject);
            obj = null;
        }

        private void CheckObjInPool(bool asActive)
        {
            Transform currChild = null;

            if (asActive)
            {
                currChild = objPool.transform.GetChild(0);
                Assert.AreEqual(1, currChild.childCount);
                currChild = currChild.GetChild(0);
                Assert.AreEqual(1, currChild.childCount);
                Assert.AreEqual(obj.transform, currChild.GetChild(0));
            }
            else
            {
                currChild = objPool.transform.GetChild(1);
                Assert.AreEqual(1, currChild.childCount);
                currChild = currChild.GetChild(0);
                Assert.AreEqual(1, currChild.childCount);
                Assert.AreEqual(obj.transform, currChild.GetChild(0));
            }
        }
    }
}
