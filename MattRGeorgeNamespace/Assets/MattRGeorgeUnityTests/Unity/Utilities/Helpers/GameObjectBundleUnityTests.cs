﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using MattRGeorge.Unity.Utilities.Helpers;

namespace MattRGeorge.UnityTests.Unity.Utilities.Helpers
{
    public class GameObjectBundleUnityTests
    {
        private GameObjectBundle objBundle = new GameObjectBundle();
        private List<GameObject> spawnedObjs = new List<GameObject>();
        private List<GameObject> testObjs = new List<GameObject>();

        [UnityTest]
        public IEnumerator DestroyObjs_NotImmediate_Tests()
        {
            objBundle.Objs = testObjs;
            objBundle.DestroyObjs(false);
            Assert.IsNotNull(objBundle.Objs);
            CollectionAssert.IsEmpty(objBundle.Objs);
            testObjs.ForEach(x => Assert.IsTrue(x));
            yield return new WaitForEndOfFrame();
            Assert.IsNotNull(objBundle.Objs);
            CollectionAssert.IsEmpty(objBundle.Objs);
            testObjs.ForEach(x => Assert.IsFalse(x));

            GameObject newObjA = objBundle.CreateNewGameObject("ObjA");
            spawnedObjs.Add(newObjA);
            objBundle.DestroyObjs(false);
            Assert.IsNotNull(objBundle.Objs);
            CollectionAssert.IsEmpty(objBundle.Objs);
            Assert.IsTrue(newObjA);
            yield return new WaitForEndOfFrame();
            Assert.IsNotNull(objBundle.Objs);
            CollectionAssert.IsEmpty(objBundle.Objs);
            Assert.IsFalse(newObjA);
        }

        [SetUp]
        public void Setup()
        {
            for (int i = 0; i < 5; i++)
            {
                testObjs.Add(new GameObject($"Obj-{i + 1}"));
                spawnedObjs.Add(testObjs[i]);
            }
        }
        [TearDown]
        public void TearDown()
        {
            objBundle = new GameObjectBundle();

            foreach (Object obj in spawnedObjs)
            {
                if (obj) GameObject.DestroyImmediate(obj);
            }
            spawnedObjs = new List<GameObject>();
            testObjs = new List<GameObject>();
        }
    }
}
