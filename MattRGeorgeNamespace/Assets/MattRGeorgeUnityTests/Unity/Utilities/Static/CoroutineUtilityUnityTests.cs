﻿using System.Collections;
using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using MattRGeorge.Unity.Utilities.Static;
using MattRGeorge.Unity.Utilities.Components;

namespace MattRGeorge.UnityTests.Unity.Utilities.Static
{
    public class CoroutineUtilityUnityTests
    {
        private MonoBehaviour obj = null;
        private bool methodRan = false;
        private bool whileCond = true;
        private int whileCondWaitI = 0;

        [UnityTest]
        public IEnumerator WhileCoroutine_Tests()
        {
            obj.StartCoroutine(CoroutineUtility.WhileCoroutine(WhileCondWait));
            Assert.IsFalse(methodRan);
            yield return new WaitForEndOfFrame();
            Assert.IsFalse(methodRan);
            yield return new WaitForEndOfFrame();
            Assert.IsFalse(methodRan);
            yield return new WaitForEndOfFrame();
            Assert.IsFalse(methodRan);
            yield return new WaitForEndOfFrame();
            Assert.IsFalse(methodRan);
            yield return new WaitForEndOfFrame();
            Assert.IsFalse(methodRan);
            yield return new WaitForEndOfFrame();
            Assert.IsTrue(methodRan);
            yield return new WaitForEndOfFrame();
            ResetVariables();

            obj.StartCoroutine(CoroutineUtility.WhileCoroutine(WhileCond, Method));
            Assert.IsFalse(methodRan);
            yield return new WaitForSeconds(3);
            Assert.IsFalse(methodRan);
            whileCond = false;
            yield return new WaitForEndOfFrame();
            Assert.IsTrue(methodRan);
        }
        [UnityTest]
        public IEnumerator WhileCoroutine_UnityTime_Tests()
        {
            obj.StartCoroutine(CoroutineUtility.WhileCoroutine(WhileCond, Method, 3));
            whileCond = false;
            Assert.IsFalse(methodRan);
            yield return new WaitForSeconds(2.9f);
            Assert.IsFalse(methodRan);
            yield return new WaitForSeconds(.2f);
            Assert.IsTrue(methodRan);
            ResetVariables();

            obj.StartCoroutine(CoroutineUtility.WhileCoroutine(WhileCondWait, Method, 0, 1));
            Assert.IsFalse(methodRan);
            yield return new WaitForSeconds(.9f);
            Assert.IsFalse(methodRan);
            Assert.AreEqual(1, whileCondWaitI);
            yield return new WaitForSeconds(1);
            Assert.IsFalse(methodRan);
            Assert.AreEqual(2, whileCondWaitI);
            yield return new WaitForSeconds(1);
            Assert.IsFalse(methodRan);
            Assert.AreEqual(3, whileCondWaitI);
            yield return new WaitForSeconds(1);
            Assert.IsFalse(methodRan);
            Assert.AreEqual(4, whileCondWaitI);
            yield return new WaitForSeconds(1);
            Assert.IsFalse(methodRan);
            Assert.AreEqual(5, whileCondWaitI);
            yield return new WaitForSeconds(1);
            Assert.IsFalse(methodRan);
            Assert.AreEqual(6, whileCondWaitI);
            yield return new WaitForSeconds(1);
            Assert.IsTrue(methodRan);
        }
        [UnityTest]
        public IEnumerator WhileCoroutine_Realtime_Tests()
        {
            Time.timeScale = .5f;

            obj.StartCoroutine(CoroutineUtility.WhileCoroutine(WhileCond, Method, 3, 0, true));
            whileCond = false;
            Assert.IsFalse(methodRan);
            yield return new WaitForSeconds(1.4f);
            Assert.IsFalse(methodRan);
            yield return new WaitForSeconds(.1f);
            Assert.IsTrue(methodRan);
            ResetVariables();

            obj.StartCoroutine(CoroutineUtility.WhileCoroutine(WhileCondWait, Method, 0, 1, true));
            Assert.IsFalse(methodRan);
            yield return new WaitForSeconds(.4f);
            Assert.IsFalse(methodRan);
            Assert.AreEqual(1, whileCondWaitI);
            yield return new WaitForSeconds(.5f);
            Assert.IsFalse(methodRan);
            Assert.AreEqual(2, whileCondWaitI);
            yield return new WaitForSeconds(.5f);
            Assert.IsFalse(methodRan);
            Assert.AreEqual(3, whileCondWaitI);
            yield return new WaitForSeconds(.5f);
            Assert.IsFalse(methodRan);
            Assert.AreEqual(4, whileCondWaitI);
            yield return new WaitForSeconds(.5f);
            Assert.IsFalse(methodRan);
            Assert.AreEqual(5, whileCondWaitI);
            yield return new WaitForSeconds(.5f);
            Assert.IsFalse(methodRan);
            Assert.AreEqual(6, whileCondWaitI);
            yield return new WaitForSeconds(.5f);
            Assert.IsTrue(methodRan);
        }
        [UnityTest]
        public IEnumerator WhileCoroutine_InvalidInput_Tests()
        {
            whileCond = false;
            obj.StartCoroutine(CoroutineUtility.WhileCoroutine(null, Method));
            yield return new WaitForEndOfFrame();
            Assert.IsFalse(methodRan);

            obj.StartCoroutine(CoroutineUtility.WhileCoroutine(WhileCondWait, null));
            yield return new WaitForEndOfFrame();
            Assert.GreaterOrEqual(whileCondWaitI, 1);
            yield return new WaitForSeconds(1);
            ResetVariables();

            obj.StartCoroutine(CoroutineUtility.WhileCoroutine(null, Method, -1));
            yield return new WaitForEndOfFrame();
            Assert.IsFalse(methodRan);

            obj.StartCoroutine(CoroutineUtility.WhileCoroutine(null, Method, -1, -1));
            yield return new WaitForEndOfFrame();
            Assert.IsFalse(methodRan);

            whileCond = false;
            obj.StartCoroutine(CoroutineUtility.WhileCoroutine(WhileCond, Method, -1, 0));
            yield return new WaitForEndOfFrame();
            Assert.IsTrue(methodRan);
            ResetVariables();

            whileCond = false;
            obj.StartCoroutine(CoroutineUtility.WhileCoroutine(WhileCond, Method, -1, -1));
            yield return new WaitForEndOfFrame();
            Assert.IsTrue(methodRan);
            ResetVariables();

            whileCond = false;
            obj.StartCoroutine(CoroutineUtility.WhileCoroutine(WhileCond, Method, 0, -1));
            yield return new WaitForEndOfFrame();
            Assert.IsTrue(methodRan);
            ResetVariables();
        }

        [UnityTest]
        public IEnumerator DelayedMethodCoroutine_UnityTime_Tests()
        {
            obj.StartCoroutine(CoroutineUtility.DelayedMethodCoroutine(Method, 0));
            yield return new WaitForEndOfFrame();
            Assert.IsTrue(methodRan);
            ResetVariables();

            obj.StartCoroutine(CoroutineUtility.DelayedMethodCoroutine(Method, 3));
            yield return new WaitForSeconds(2.9f);
            Assert.IsFalse(methodRan);
            yield return new WaitForSeconds(.2f);
            Assert.IsTrue(methodRan);
        }
        [UnityTest]
        public IEnumerator DelayedMethodCoroutine_Realtime_Tests()
        {
            Time.timeScale = .5f;
            obj.StartCoroutine(CoroutineUtility.DelayedMethodCoroutine(Method, 0, true));
            yield return new WaitForEndOfFrame();
            Assert.IsTrue(methodRan);
            ResetVariables();

            obj.StartCoroutine(CoroutineUtility.DelayedMethodCoroutine(Method, 3, true));
            yield return new WaitForSeconds(1.4f);
            Assert.IsFalse(methodRan);
            yield return new WaitForSeconds(.15f);
            Assert.IsTrue(methodRan);
        }
        [UnityTest]
        public IEnumerator DelayedMethodCoroutine_InvalidInput_Tests()
        {
            obj.StartCoroutine(CoroutineUtility.DelayedMethodCoroutine(null, -1));
            yield return new WaitForEndOfFrame();
            Assert.IsFalse(methodRan);

            obj.StartCoroutine(CoroutineUtility.DelayedMethodCoroutine(null, 0));
            yield return new WaitForEndOfFrame();
            Assert.IsFalse(methodRan);

            obj.StartCoroutine(CoroutineUtility.DelayedMethodCoroutine(Method, -1));
            yield return new WaitForEndOfFrame();
            Assert.IsFalse(methodRan);
        }

        [SetUp]
        public void Setup()
        {
            obj = new GameObject("Obj").AddComponent<Versioning>();
            ResetVariables();
        }
        [TearDown]
        public void TearDown()
        {
            GameObject.Destroy(obj.gameObject);
            obj = null;
            Time.timeScale = 1;
        }

        private void ResetVariables()
        {
            methodRan = false;
            whileCond = true;
            whileCondWaitI = 0;
        }

        private void Method()
        {
            methodRan = true;
        }
        private bool WhileCond()
        {
            return whileCond;
        }
        private bool WhileCondWait()
        {
            if (whileCondWaitI > 5)
            {
                Method();
                return false;
            }

            whileCondWaitI++;
            return true;
        }
    }
}
