﻿using System.Collections;
using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using MattRGeorge.General.Utilities.Static;
using MattRGeorge.Unity.Utilities.Enums;
using MattRGeorge.Unity.Utilities.Components;

namespace MattRGeorge.UnityTests.Unity.Utilities.Components
{
    public class RotateObjectUnityTests
    {
        private RotateObject obj = null;

        [UnityTest]
        public IEnumerator StartStopRotating_Tests()
        {
            obj.StartRotating();
            yield return new WaitForSeconds(1);
            CheckRotation(new Vector3(0, 0, 0));

            obj.rotations = Vector3.up * 1;
            yield return new WaitForSeconds(1);
            CheckRotation(new Vector3(0, 1, 0));

            obj.rotations = Vector3.forward * 1;
            yield return new WaitForSeconds(1);
            CheckRotation(new Vector3(0, 1, 1));

            obj.rotations = Vector3.right * 1;
            yield return new WaitForSeconds(1);
            CheckRotation(new Vector3(1, 1, 1));

            obj.rotations = Vector3.one;
            yield return new WaitForSeconds(1);
            CheckRotation(new Vector3(2, 2, 2));

            obj.rotations = -Vector3.one * 4;
            yield return new WaitForSeconds(1);
            CheckRotation(new Vector3(358, 358, 358));
        }

        [UnityTest]
        public IEnumerator StartRotating_RotateOn_Tests()
        {
            GameObject testObj = new GameObject("TestObj");
            RotateObject rotTestObj = testObj.AddComponent<RotateObject>();
            rotTestObj.rotations = Vector3.one;
            yield return new WaitForSeconds(1);
            Assert.IsTrue(MathUtility.AreAlmostEqual(testObj.transform.eulerAngles.x, 0));
            Assert.IsTrue(MathUtility.AreAlmostEqual(testObj.transform.eulerAngles.y, 0));
            Assert.IsTrue(MathUtility.AreAlmostEqual(testObj.transform.eulerAngles.z, 0));

            rotTestObj.rotateOn = UnityStartMethods.Awake;
            testObj.transform.eulerAngles = Vector3.zero;
            GameObject testObj2 = GameObject.Instantiate(testObj);
            yield return new WaitForSeconds(1);
            Assert.IsTrue(MathUtility.AreAlmostEqual(testObj2.transform.eulerAngles.x, 1));
            Assert.IsTrue(MathUtility.AreAlmostEqual(testObj2.transform.eulerAngles.y, 1));
            Assert.IsTrue(MathUtility.AreAlmostEqual(testObj2.transform.eulerAngles.z, 1));

            rotTestObj.rotateOn = UnityStartMethods.Start;
            testObj.transform.eulerAngles = Vector3.zero;
            GameObject testObj3 = GameObject.Instantiate(testObj);
            yield return new WaitForSeconds(1);
            Assert.IsTrue(MathUtility.AreAlmostEqual(testObj3.transform.eulerAngles.x, 1));
            Assert.IsTrue(MathUtility.AreAlmostEqual(testObj3.transform.eulerAngles.y, 1));
            Assert.IsTrue(MathUtility.AreAlmostEqual(testObj3.transform.eulerAngles.z, 1));

            GameObject.Destroy(testObj);
            GameObject.Destroy(testObj2);
            GameObject.Destroy(testObj3);
        }

        [SetUp]
        public void Setup()
        {
            obj = new GameObject("Object").AddComponent<RotateObject>();
        }
        [TearDown]
        public void Teardown()
        {
            if (obj) GameObject.Destroy(obj.gameObject);
            obj = null;
        }

        private void CheckRotation(Vector3 expectedRot)
        {
            Assert.IsTrue(MathUtility.AreAlmostEqual(obj.transform.eulerAngles.x, expectedRot.x));
            Assert.IsTrue(MathUtility.AreAlmostEqual(obj.transform.eulerAngles.y, expectedRot.y));
            Assert.IsTrue(MathUtility.AreAlmostEqual(obj.transform.eulerAngles.z, expectedRot.z));
        }
    }
}
