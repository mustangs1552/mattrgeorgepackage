﻿using System.Collections;
using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using MattRGeorge.General.Utilities.Static;
using MattRGeorge.Unity.Utilities.Components;

namespace MattRGeorge.UnityTests.Unity.Utilities.Components
{
    public class TimerUnityTests
    {
        private Timer timer = null;
        private bool onTimeUpRan = false;

        [UnityTest]
        public IEnumerator TimeRemaining_TimePassed_Countdown_Tests()
        {
            Assert.AreEqual(0, timer.TimeRemaining);
            Assert.AreEqual(0, timer.TimePassed);

            timer.StartCountdown();
            Assert.IsTrue(MathUtility.AreAlmostEqual(timer.TimeRemaining, 10));
            Assert.IsTrue(MathUtility.AreAlmostEqual(timer.TimePassed, 0));

            yield return new WaitForSeconds(1);
            Assert.IsTrue(MathUtility.AreAlmostEqual(timer.TimeRemaining, 9));
            Assert.IsTrue(MathUtility.AreAlmostEqual(timer.TimePassed, 1));

            yield return new WaitForSeconds(4);
            Assert.IsTrue(MathUtility.AreAlmostEqual(timer.TimeRemaining, 5));
            Assert.IsTrue(MathUtility.AreAlmostEqual(timer.TimePassed, 5));

            yield return new WaitForSeconds(4);
            Assert.IsTrue(MathUtility.AreAlmostEqual(timer.TimeRemaining, 1));
            Assert.IsTrue(MathUtility.AreAlmostEqual(timer.TimePassed, 9));

            yield return new WaitForSeconds(1.1f);
            Assert.AreEqual(0, timer.TimeRemaining);
            Assert.AreEqual(0, timer.TimePassed);
        }
        [UnityTest]
        public IEnumerator TimeRemaining_TimePassed_CountingUp_Tests()
        {
            Assert.AreEqual(0, timer.TimeRemaining);
            Assert.AreEqual(0, timer.TimePassed);

            timer.StartCountingUp();
            Assert.AreEqual(0, timer.TimeRemaining);
            Assert.IsTrue(MathUtility.AreAlmostEqual(timer.TimePassed, 0));

            yield return new WaitForSeconds(1);
            Assert.AreEqual(0, timer.TimeRemaining);
            Assert.IsTrue(MathUtility.AreAlmostEqual(timer.TimePassed, 1));

            yield return new WaitForSeconds(4);
            Assert.AreEqual(0, timer.TimeRemaining);
            Assert.IsTrue(MathUtility.AreAlmostEqual(timer.TimePassed, 5));
        }

        [UnityTest]
        public IEnumerator StartCountdown_CountdownTimer_Tests()
        {
            Assert.IsFalse(onTimeUpRan);
            Assert.AreEqual(0, timer.TimeRemaining);
            Assert.AreEqual(0, timer.TimePassed);

            timer.StartCountdown();
            Assert.IsFalse(onTimeUpRan);
            Assert.IsTrue(MathUtility.AreAlmostEqual(timer.TimeRemaining, 10));
            Assert.IsTrue(MathUtility.AreAlmostEqual(timer.TimePassed, 0));

            yield return new WaitForSeconds(1);
            Assert.IsFalse(onTimeUpRan);
            Assert.IsTrue(MathUtility.AreAlmostEqual(timer.TimeRemaining, 9));
            Assert.IsTrue(MathUtility.AreAlmostEqual(timer.TimePassed, 1));

            yield return new WaitForSeconds(7);
            Assert.IsFalse(onTimeUpRan);
            Assert.IsTrue(MathUtility.AreAlmostEqual(timer.TimeRemaining, 2));
            Assert.IsTrue(MathUtility.AreAlmostEqual(timer.TimePassed, 8));

            yield return new WaitForSeconds(1.9f);
            Assert.IsFalse(onTimeUpRan);
            Assert.IsTrue(MathUtility.AreAlmostEqual(timer.TimeRemaining, .1f));
            Assert.IsTrue(MathUtility.AreAlmostEqual(timer.TimePassed, 9.9f));

            yield return new WaitForSeconds(.2f);
            Assert.IsTrue(onTimeUpRan);
            Assert.AreEqual(0, timer.TimeRemaining);
            Assert.AreEqual(0, timer.TimePassed);
        }
        [UnityTest]
        public IEnumerator StartCountdown_CustomTimer_Tests()
        {
            Assert.IsFalse(onTimeUpRan);
            Assert.AreEqual(0, timer.TimeRemaining);
            Assert.AreEqual(0, timer.TimePassed);

            timer.StartCountdown(5);
            Assert.IsFalse(onTimeUpRan);
            Assert.IsTrue(MathUtility.AreAlmostEqual(timer.TimeRemaining, 5));
            Assert.IsTrue(MathUtility.AreAlmostEqual(timer.TimePassed, 0));

            yield return new WaitForSeconds(1);
            Assert.IsFalse(onTimeUpRan);
            Assert.IsTrue(MathUtility.AreAlmostEqual(timer.TimeRemaining, 4));
            Assert.IsTrue(MathUtility.AreAlmostEqual(timer.TimePassed, 1));

            yield return new WaitForSeconds(3);
            Assert.IsFalse(onTimeUpRan);
            Assert.IsTrue(MathUtility.AreAlmostEqual(timer.TimeRemaining, 1));
            Assert.IsTrue(MathUtility.AreAlmostEqual(timer.TimePassed, 4));

            yield return new WaitForSeconds(.9f);
            Assert.IsFalse(onTimeUpRan);
            Assert.IsTrue(MathUtility.AreAlmostEqual(timer.TimeRemaining, .1f));
            Assert.IsTrue(MathUtility.AreAlmostEqual(timer.TimePassed, 4.9f));

            yield return new WaitForSeconds(.2f);
            Assert.IsTrue(onTimeUpRan);
            Assert.AreEqual(0, timer.TimeRemaining);
            Assert.AreEqual(0, timer.TimePassed);
        }

        [UnityTest]
        public IEnumerator StartCountingUp_CancelCounting_Tests()
        {
            Assert.IsFalse(onTimeUpRan);
            Assert.AreEqual(0, timer.TimePassed);
            Assert.AreEqual(0, timer.TimeRemaining);
            timer.StartCountingUp();

            yield return new WaitForSeconds(1);
            Assert.IsFalse(onTimeUpRan);
            Assert.IsTrue(MathUtility.AreAlmostEqual(timer.TimePassed, 1));
            Assert.AreEqual(0, timer.TimeRemaining);

            yield return new WaitForSeconds(4);
            Assert.IsFalse(onTimeUpRan);
            Assert.IsTrue(MathUtility.AreAlmostEqual(timer.TimePassed, 5));
            Assert.AreEqual(0, timer.TimeRemaining);

            timer.CancelCounting();
            yield return new WaitForSeconds(1);
            Assert.IsFalse(onTimeUpRan);
            Assert.AreEqual(0, timer.TimePassed);
            Assert.AreEqual(0, timer.TimeRemaining);

            yield return new WaitForSeconds(4);
            Assert.IsFalse(onTimeUpRan);
            Assert.AreEqual(0, timer.TimePassed);
            Assert.AreEqual(0, timer.TimeRemaining);
        }

        [UnityTest]
        public IEnumerator CancelCounting_Countdown_Tests()
        {
            Assert.IsFalse(onTimeUpRan);
            Assert.AreEqual(0, timer.TimeRemaining);
            Assert.AreEqual(0, timer.TimePassed);

            timer.StartCountdown();
            yield return new WaitForSeconds(3);
            Assert.IsFalse(onTimeUpRan);
            Assert.AreNotEqual(0, timer.TimeRemaining);
            Assert.AreNotEqual(0, timer.TimePassed);

            timer.CancelCounting();
            Assert.IsFalse(onTimeUpRan);
            Assert.AreEqual(0, timer.TimeRemaining);
            Assert.AreEqual(0, timer.TimePassed);

            yield return new WaitForSeconds(2);
            Assert.IsFalse(onTimeUpRan);
            Assert.AreEqual(0, timer.TimeRemaining);
            Assert.AreEqual(0, timer.TimePassed);
        }

        [SetUp]
        public void Setup()
        {
            timer = new GameObject("Timer").AddComponent<Timer>();
            timer.OnTimeUp.AddListener(OnTimeUp);
        }
        [TearDown]
        public void Teardown()
        {
            if (timer) GameObject.Destroy(timer.gameObject);
            timer = null;

            onTimeUpRan = false;
        }

        private void OnTimeUp()
        {
            onTimeUpRan = true;
        }            
    }
}
