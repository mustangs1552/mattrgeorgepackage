﻿using System.Collections;
using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using MattRGeorge.General.Utilities.Static;
using MattRGeorge.Unity.Utilities.Components;

namespace MattRGeorge.UnityTests.Unity.Utilities.Components
{
    public class CollisionDetectorUnityTests
    {
        private GameObject floor = null;
        private CollisionDetector detector = null;
        private bool onEnterEventRan = false;
        private bool onStayEventRan = false;
        private bool onExitEventRan = false;
        private bool onBacktraceHitEventRan = false;

        [UnityTest]
        public IEnumerator Collision_Tests()
        {
            Assert.IsFalse(onEnterEventRan);
            Assert.IsFalse(onStayEventRan);
            Assert.IsFalse(onExitEventRan);
            Assert.IsFalse(onBacktraceHitEventRan);
            
            yield return new WaitForSeconds(1.5f);
            Assert.IsTrue(onEnterEventRan);
            Assert.IsTrue(onStayEventRan);
            Assert.IsTrue(onExitEventRan);
            Assert.IsFalse(onBacktraceHitEventRan);
        }
        [UnityTest]
        public IEnumerator Collision_WithBacktracing_Tests()
        {
            ReflectionUtility.SetInstanceField(typeof(CollisionDetector), detector, "enableAutoBacktracing", true);

            Assert.IsFalse(onEnterEventRan);
            Assert.IsFalse(onStayEventRan);
            Assert.IsFalse(onExitEventRan);
            Assert.IsFalse(onBacktraceHitEventRan);

            detector.GetComponent<Rigidbody>().AddForce(Vector3.down * 20000);
            yield return new WaitForSeconds(.5f);
            Assert.IsFalse(onEnterEventRan);
            Assert.IsFalse(onStayEventRan);
            Assert.IsFalse(onExitEventRan);
            Assert.IsTrue(onBacktraceHitEventRan);
        }

        [SetUp]
        public void Setup()
        {
            floor = GameObject.CreatePrimitive(PrimitiveType.Cube);
            floor.transform.position = Vector3.down * 5;
            floor.transform.localScale = new Vector3(10, .1f, 10);
            floor.transform.Rotate(Vector3.right * 45);

            detector = GameObject.CreatePrimitive(PrimitiveType.Cube).AddComponent<CollisionDetector>();
            detector.name = "Detector";
            detector.OnEnter.AddListener(OnEnterEvent);
            detector.OnStay.AddListener(OnStayEvent);
            detector.OnExit.AddListener(OnExitEvent);
            detector.OnBacktraceHit.AddListener(OnBacktraceHitEvent);
            detector.gameObject.AddComponent<Rigidbody>();

            ResetEventFlags();
        }
        [TearDown]
        public void Teardown()
        {
            GameObject.Destroy(floor);
            GameObject.Destroy(detector.gameObject);
        }

        private void OnEnterEvent(GameObject thisObj, Collision collision)
        {
            onEnterEventRan = true;

            Assert.IsNotNull(thisObj);
            Assert.AreEqual(detector.gameObject, thisObj);
            Assert.IsNotNull(collision);
            Assert.IsNotNull(collision.gameObject);
            Assert.AreEqual(floor, collision.gameObject);
        }
        private void OnStayEvent(GameObject thisObj, Collision collision)
        {
            onStayEventRan = true;

            Assert.IsNotNull(thisObj);
            Assert.AreEqual(detector.gameObject, thisObj);
            Assert.IsNotNull(collision);
            Assert.IsNotNull(collision.gameObject);
            Assert.AreEqual(floor, collision.gameObject);
        }
        private void OnExitEvent(GameObject thisObj, Collision collision)
        {
            onExitEventRan = true;

            Assert.IsNotNull(thisObj);
            Assert.AreEqual(detector.gameObject, thisObj);
            Assert.IsNotNull(collision);
            Assert.IsNotNull(collision.gameObject);
            Assert.AreEqual(floor, collision.gameObject);
        }
        private void OnBacktraceHitEvent(GameObject obj)
        {
            onBacktraceHitEventRan = true;

            Assert.IsNotNull(obj);
            Assert.AreEqual(floor, obj);
        }
        private void ResetEventFlags()
        {
            onEnterEventRan = false;
            onStayEventRan = false;
            onExitEventRan = false;
            onBacktraceHitEventRan = false;
        }
    }
}
