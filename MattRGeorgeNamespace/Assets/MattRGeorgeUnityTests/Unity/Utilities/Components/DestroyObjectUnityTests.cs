﻿using System.Collections;
using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using MattRGeorge.General.Utilities.Static;
using MattRGeorge.Unity.Tools.ObjectPooling;
using MattRGeorge.Unity.Utilities.Components;

namespace MattRGeorge.UnityTests.Unity.Utilities.Components
{
    public class DestroyObjectUnityTests
    {
        private ObjectPoolManager objPool = null;
        private Transform destroyObjPool = null;
        private GameObject obj = null;
        private DestroyObject destroyObj = null;

        [UnityTest]
        public IEnumerator Destroy_UnityDestroy_Tests()
        {
            ReflectionUtility.SetInstanceField(typeof(DestroyObject), destroyObj, "useObjectPoolSystem", false);

            destroyObj.Destroy(false);
            Assert.IsTrue(obj);
            yield return new WaitForEndOfFrame();
            Assert.IsFalse(obj);
        }
        [UnityTest]
        public IEnumerator Destroy_UnityDestroyImmediate_Tests()
        {
            ReflectionUtility.SetInstanceField(typeof(DestroyObject), destroyObj, "useObjectPoolSystem", false);

            destroyObj.Destroy(true);
            Assert.IsFalse(obj);
            yield break;
        }
        [UnityTest]
        public IEnumerator Destroy_ObjectPoolDestroy_Tests()
        {
            destroyObj.Destroy();
            yield return new WaitForEndOfFrame();
            Assert.IsTrue(obj);
            Assert.IsFalse(obj.activeInHierarchy);
            destroyObjPool = objPool.transform.GetChild(1).GetChild(0);
            Assert.AreEqual("OBJ_ANCHOR", destroyObjPool.name);
            Assert.AreEqual(1, destroyObjPool.transform.childCount);
        }

        [SetUp]
        public void Setup()
        {
            objPool = new GameObject("ObjectPool").AddComponent<ObjectPoolManager>();
            destroyObj = new GameObject("Obj").AddComponent<DestroyObject>();
            obj = destroyObj.gameObject;
        }
        [TearDown]
        public void TearDown()
        {
            GameObject.DestroyImmediate(objPool.gameObject);
            objPool = null;
            destroyObjPool = null;
            GameObject.DestroyImmediate(obj.gameObject);
            obj = null;
            destroyObj = null;
        }
    }
}
