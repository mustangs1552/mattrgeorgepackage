﻿using MattRGeorge.Unity.Utilities.Components;
using NUnit.Framework;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.TestTools;

namespace MattRGeorge.UnityTests.Unity.Utilities.Components
{
    public class LineRendererCasterUnityTests
    {
        private LineRendererCaster caster = null;
        private LineRenderer casterLineRenderer = null;
        private List<Transform> points = new List<Transform>();
        private List<Transform> extraPoints = new List<Transform>();

        [UnityTest]
        public IEnumerator UpdatePoints_MovePoints_Auto_Tests()
        {
            caster.auto = true;

            caster.Points = points;
            yield return new WaitForSeconds(.1f);
            CheckLineRendererPoints();

            points[0].position = Vector3.right * 5;
            yield return new WaitForSeconds(.1f);
            CheckLineRendererPoints();

            points[1].position = Vector3.up * 5;
            yield return new WaitForSeconds(.1f);
            CheckLineRendererPoints();

            points[2].position = new Vector3(-5, -5, 5);
            yield return new WaitForSeconds(.1f);
            CheckLineRendererPoints();
        }
        [UnityTest]
        public IEnumerator UpdatePoints_ChangePoints_Auto_Tests()
        {
            caster.auto = true;

            caster.Points = points;
            points[0].position = Vector3.right * 5;
            points[1].position = Vector3.up * 5;
            points[2].position = new Vector3(-5, -5, 5);
            yield return new WaitForSeconds(.1f);
            CheckLineRendererPoints();

            extraPoints[0].position = Vector3.down * 5;
            extraPoints[1].position = Vector3.forward * 5;
            extraPoints[2].position = Vector3.back * 5;
            caster.AddPoints(extraPoints);
            yield return new WaitForSeconds(.1f);
            CheckLineRendererPoints(true);
        }

        [UnityTest]
        public IEnumerator UpdatePoints_MovePoints_Tests()
        {
            caster.Points = points;
            caster.UpdatePoints();
            yield return new WaitForEndOfFrame();
            CheckLineRendererPoints();

            points[0].position = Vector3.right * 5;
            caster.UpdatePoints();
            yield return new WaitForEndOfFrame();
            CheckLineRendererPoints();

            points[1].position = Vector3.up * 5;
            caster.UpdatePoints();
            yield return new WaitForEndOfFrame();
            CheckLineRendererPoints();

            points[2].position = new Vector3(-5, -5, 5);
            caster.UpdatePoints();
            yield return new WaitForEndOfFrame();
            CheckLineRendererPoints();
        }
        [UnityTest]
        public IEnumerator UpdatePoints_ChangePoints_Tests()
        {
            caster.Points = points;
            points[0].position = Vector3.right * 5;
            points[1].position = Vector3.up * 5;
            points[2].position = new Vector3(-5, -5, 5);
            caster.UpdatePoints();
            yield return new WaitForEndOfFrame();
            CheckLineRendererPoints();

            extraPoints[0].position = Vector3.down * 5;
            extraPoints[1].position = Vector3.forward * 5;
            extraPoints[2].position = Vector3.back * 5;
            caster.AddPoints(extraPoints);
            caster.UpdatePoints();
            yield return new WaitForEndOfFrame();
            CheckLineRendererPoints(true);
        }

        [SetUp]
        public void Setup()
        {
            GameObject temp = new GameObject("Caster");
            casterLineRenderer = temp.AddComponent<LineRenderer>();
            caster = temp.AddComponent<LineRendererCaster>();

            for (int i = 0; i < 3; i++) points.Add(new GameObject($"Point-{i + 1}").transform);
            for (int i = 3; i < 6; i++) extraPoints.Add(new GameObject($"ExtraPoint-{i + 1}").transform);
        }
        [TearDown]
        public void TearDown()
        {
            if (caster) GameObject.Destroy(caster.gameObject);
            caster = null;
            casterLineRenderer = null;

            foreach (Transform trans in points)
            {
                if (trans) GameObject.Destroy(trans.gameObject);
            }
            points = new List<Transform>();

            foreach (Transform trans in extraPoints)
            {
                if (trans) GameObject.Destroy(trans.gameObject);
            }
            extraPoints = new List<Transform>();
        }

        private void CheckLineRendererPoints(bool includeExtraPoints = false)
        {
            Vector3[] vertices = new Vector3[casterLineRenderer.positionCount];
            casterLineRenderer.GetPositions(vertices);

            Assert.IsNotNull(vertices);
            Assert.AreEqual((!includeExtraPoints) ? points.Count : points.Count + extraPoints.Count, vertices.Length);
            for (int i = 0; i < points.Count; i++) Assert.AreEqual(points[i].position, vertices[i]);
            if(includeExtraPoints)
            {
                for (int i = 0; i < extraPoints.Count; i++) Assert.AreEqual(extraPoints[i].position, vertices[points.Count + i]);
            }
        }
    }
}
