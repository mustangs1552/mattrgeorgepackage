﻿using MattRGeorge.Unity.Utilities.Components;
using NUnit.Framework;
using System.Collections;
using UnityEngine;
using UnityEngine.TestTools;

namespace MattRGeorge.UnityTests.Unity.Utilities.Components
{
    public class DelayedUnityEventCallUnityTests
    {
        private DelayedUnityEventCall obj = null;
        private bool onCallStartRan = false;
        private bool onCallRan = false;

        [UnityTest]
        public IEnumerator StartDelayedCall_Tests()
        {
            obj.StartDelayedCall();
            Assert.IsTrue(onCallStartRan);
            Assert.IsFalse(onCallRan);
            yield return new WaitForSeconds(.9f);
            Assert.IsTrue(onCallStartRan);
            Assert.IsFalse(onCallRan);
            yield return new WaitForSeconds(.2f);
            Assert.IsTrue(onCallStartRan);
            Assert.IsTrue(onCallRan);

            ResetFlags();
            obj.Delay = 3;
            obj.StartDelayedCall();
            Assert.IsTrue(onCallStartRan);
            Assert.IsFalse(onCallRan);
            yield return new WaitForSeconds(2.9f);
            Assert.IsTrue(onCallStartRan);
            Assert.IsFalse(onCallRan);
            yield return new WaitForSeconds(.2f);
            Assert.IsTrue(onCallStartRan);
            Assert.IsTrue(onCallRan);
        }

        [UnityTest]
        public IEnumerator ClearPendingCalls_Tests()
        {
            obj.Delay = 1;
            obj.StartDelayedCall();
            Assert.IsTrue(onCallStartRan);
            Assert.IsFalse(onCallRan);
            ResetFlags();
            obj.StartDelayedCall();
            Assert.IsTrue(onCallStartRan);
            Assert.IsFalse(onCallRan);
            ResetFlags();
            obj.StartDelayedCall();
            Assert.IsTrue(onCallStartRan);
            Assert.IsFalse(onCallRan);

            yield return new WaitForSeconds(.5f);
            Assert.IsTrue(onCallStartRan);
            Assert.IsFalse(onCallRan);
            obj.ClearPendingCalls();
            yield return new WaitForSeconds(.6f);
            Assert.IsTrue(onCallStartRan);
            Assert.IsFalse(onCallRan);
            yield return new WaitForSeconds(.5f);
            Assert.IsTrue(onCallStartRan);
            Assert.IsFalse(onCallRan);
        }

        [SetUp]
        public void Setup()
        {
            obj = new GameObject("Obj").AddComponent<DelayedUnityEventCall>();
            obj.OnDelayedCallStart.AddListener(OnCallStart);
            obj.OnDelayedCall.AddListener(OnCall);

            ResetFlags();
        }
        [TearDown]
        public void Teardown()
        {
            if (obj.gameObject) GameObject.DestroyImmediate(obj.gameObject);
        }

        private void OnCallStart()
        {
            onCallStartRan = true;
        }
        private void OnCall()
        {
            onCallRan = true;
        }
        private void ResetFlags()
        {
            onCallStartRan = false;
            onCallRan = false;
        }
    }
}
