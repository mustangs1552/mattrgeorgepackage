﻿using System.Collections;
using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using MattRGeorge.General.Utilities.Static;
using MattRGeorge.Unity.Utilities.Components;
using MattRGeorge.Unity.Utilities.Components.Enums;
using MattRGeorge.Unity.Tools.ObjectPooling;

namespace MattRGeorge.UnityTests.Unity.Utilities.Components
{
    public class TranslateObjectUnityTests
    {
        private TranslateObject translateObj = null;
        private bool onStartRan = false;
        private bool onMovedRan = false;
        private bool onEndRan = false;

        [UnityTest]
        public IEnumerator Moving_Tests()
        {
            CheckPosition(Vector3.zero);
            Assert.IsTrue(translateObj.gameObject.activeInHierarchy);
            Assert.IsFalse(onStartRan);
            Assert.IsFalse(onMovedRan);
            Assert.IsFalse(onEndRan);

            translateObj.StartMoving();
            CheckPosition(Vector3.zero);
            Assert.IsTrue(translateObj.gameObject.activeInHierarchy);
            Assert.IsTrue(onStartRan);
            Assert.IsFalse(onMovedRan);
            Assert.IsFalse(onEndRan);

            ResetFlags();
            yield return new WaitForSeconds(1);
            CheckPosition(Vector3.forward * 1);
            Assert.IsTrue(translateObj.gameObject.activeInHierarchy);
            Assert.IsFalse(onStartRan);
            Assert.IsTrue(onMovedRan);
            Assert.IsFalse(onEndRan);

            ResetFlags();
            yield return new WaitForSeconds(4);
            CheckPosition(Vector3.forward * 5);
            Assert.IsTrue(translateObj.gameObject.activeInHierarchy);
            Assert.IsFalse(onStartRan);
            Assert.IsTrue(onMovedRan);
            Assert.IsFalse(onEndRan);
        }

        [UnityTest]
        public IEnumerator Moving_RestartEOLTimer_Tests()
        {
            ReflectionUtility.SetInstanceField(typeof(TranslateObject), translateObj, "endMode", EOLMode.Disable);

            CheckPosition(Vector3.zero);
            Assert.IsTrue(translateObj.gameObject.activeInHierarchy);
            Assert.IsFalse(onStartRan);
            Assert.IsFalse(onMovedRan);
            Assert.IsFalse(onEndRan);

            translateObj.StartMoving();
            CheckPosition(Vector3.zero);
            Assert.IsTrue(translateObj.gameObject.activeInHierarchy);
            Assert.IsTrue(onStartRan);
            Assert.IsFalse(onMovedRan);
            Assert.IsFalse(onEndRan);

            ResetFlags();
            yield return new WaitForSeconds(5);
            CheckPosition(Vector3.forward * 5);
            Assert.IsTrue(translateObj.gameObject.activeInHierarchy);
            Assert.IsFalse(onStartRan);
            Assert.IsTrue(onMovedRan);
            Assert.IsFalse(onEndRan);

            ResetFlags();
            ReflectionUtility.SetInstanceField(typeof(TranslateObject), translateObj, "direction", Vector3.back);
            translateObj.StartMoving(true);
            yield return new WaitForSeconds(5.1f);
            CheckPosition(Vector3.forward * -.1f);
            Assert.IsTrue(translateObj.gameObject.activeInHierarchy);
            Assert.IsTrue(onStartRan);
            Assert.IsTrue(onMovedRan);
            Assert.IsFalse(onEndRan);

            ResetFlags();
            yield return new WaitForSeconds(4.8f);
            CheckPosition(Vector3.forward * -4.9f);
            Assert.IsTrue(translateObj.gameObject.activeInHierarchy);
            Assert.IsFalse(onStartRan);
            Assert.IsTrue(onMovedRan);
            Assert.IsFalse(onEndRan);

            ResetFlags();
            yield return new WaitForSeconds(.2f);
            CheckPosition(Vector3.forward * -5);
            Assert.IsFalse(translateObj.gameObject.activeInHierarchy);
            Assert.IsFalse(onStartRan);
            Assert.IsTrue(onMovedRan);
            Assert.IsTrue(onEndRan);
        }
        [UnityTest]
        public IEnumerator Moving_ContinueEOLTimer_Tests()
        {
            ReflectionUtility.SetInstanceField(typeof(TranslateObject), translateObj, "endMode", EOLMode.Disable);

            CheckPosition(Vector3.zero);
            Assert.IsTrue(translateObj.gameObject.activeInHierarchy);
            Assert.IsFalse(onStartRan);
            Assert.IsFalse(onMovedRan);
            Assert.IsFalse(onEndRan);

            translateObj.StartMoving();
            CheckPosition(Vector3.zero);
            Assert.IsTrue(translateObj.gameObject.activeInHierarchy);
            Assert.IsTrue(onStartRan);
            Assert.IsFalse(onMovedRan);
            Assert.IsFalse(onEndRan);

            ResetFlags();
            yield return new WaitForSeconds(5);
            CheckPosition(Vector3.forward * 5);
            Assert.IsTrue(translateObj.gameObject.activeInHierarchy);
            Assert.IsFalse(onStartRan);
            Assert.IsTrue(onMovedRan);
            Assert.IsFalse(onEndRan);

            ResetFlags();
            ReflectionUtility.SetInstanceField(typeof(TranslateObject), translateObj, "direction", Vector3.back);
            translateObj.StartMoving(false);
            yield return new WaitForSeconds(4.9f);
            CheckPosition(Vector3.forward * .1f);
            Assert.IsTrue(translateObj.gameObject.activeInHierarchy);
            Assert.IsTrue(onStartRan);
            Assert.IsTrue(onMovedRan);
            Assert.IsFalse(onEndRan);

            ResetFlags();
            yield return new WaitForSeconds(.2f);
            CheckPosition(Vector3.forward * 0);
            Assert.IsFalse(translateObj.gameObject.activeInHierarchy);
            Assert.IsFalse(onStartRan);
            Assert.IsTrue(onMovedRan);
            Assert.IsTrue(onEndRan);
        }

        [UnityTest]
        public IEnumerator Moving_EOL_Nothing_Tests()
        {
            ReflectionUtility.SetInstanceField(typeof(TranslateObject), translateObj, "endMode", EOLMode.Nothing);

            CheckPosition(Vector3.zero);
            Assert.IsTrue(translateObj.gameObject.activeInHierarchy);
            Assert.IsFalse(onStartRan);
            Assert.IsFalse(onMovedRan);
            Assert.IsFalse(onEndRan);

            translateObj.StartMoving();
            CheckPosition(Vector3.zero);
            Assert.IsTrue(translateObj.gameObject.activeInHierarchy);
            Assert.IsTrue(onStartRan);
            Assert.IsFalse(onMovedRan);
            Assert.IsFalse(onEndRan);

            ResetFlags();
            yield return new WaitForSeconds(9.9f);
            CheckPosition(Vector3.forward * 9.9f);
            Assert.IsTrue(translateObj.gameObject.activeInHierarchy);
            Assert.IsFalse(onStartRan);
            Assert.IsTrue(onMovedRan);
            Assert.IsFalse(onEndRan);

            ResetFlags();
            yield return new WaitForSeconds(.2f);
            CheckPosition(Vector3.forward * 10.1f);
            Assert.IsTrue(translateObj.gameObject.activeInHierarchy);
            Assert.IsFalse(onStartRan);
            Assert.IsTrue(onMovedRan);
            Assert.IsTrue(onEndRan);

            ResetFlags();
            yield return new WaitForSeconds(1.9f);
            CheckPosition(Vector3.forward * 12);
            Assert.IsTrue(translateObj.gameObject.activeInHierarchy);
            Assert.IsFalse(onStartRan);
            Assert.IsTrue(onMovedRan);
            Assert.IsFalse(onEndRan);
        }
        [UnityTest]
        public IEnumerator Moving_EOL_Stop_Tests()
        {
            ReflectionUtility.SetInstanceField(typeof(TranslateObject), translateObj, "endMode", EOLMode.Stop);

            CheckPosition(Vector3.zero);
            Assert.IsTrue(translateObj.gameObject.activeInHierarchy);
            Assert.IsFalse(onStartRan);
            Assert.IsFalse(onMovedRan);
            Assert.IsFalse(onEndRan);

            translateObj.StartMoving();
            CheckPosition(Vector3.zero);
            Assert.IsTrue(translateObj.gameObject.activeInHierarchy);
            Assert.IsTrue(onStartRan);
            Assert.IsFalse(onMovedRan);
            Assert.IsFalse(onEndRan);

            ResetFlags();
            yield return new WaitForSeconds(9.9f);
            CheckPosition(Vector3.forward * 9.9f);
            Assert.IsTrue(translateObj.gameObject.activeInHierarchy);
            Assert.IsFalse(onStartRan);
            Assert.IsTrue(onMovedRan);
            Assert.IsFalse(onEndRan);

            ResetFlags();
            yield return new WaitForSeconds(.2f);
            CheckPosition(Vector3.forward * 10);
            Assert.IsTrue(translateObj.gameObject.activeInHierarchy);
            Assert.IsFalse(onStartRan);
            Assert.IsTrue(onMovedRan);
            Assert.IsTrue(onEndRan);

            ResetFlags();
            yield return new WaitForSeconds(1.9f);
            CheckPosition(Vector3.forward * 10);
            Assert.IsTrue(translateObj.gameObject.activeInHierarchy);
            Assert.IsFalse(onStartRan);
            Assert.IsFalse(onMovedRan);
            Assert.IsFalse(onEndRan);
        }
        [UnityTest]
        public IEnumerator Moving_EOL_Disable_Tests()
        {
            ReflectionUtility.SetInstanceField(typeof(TranslateObject), translateObj, "endMode", EOLMode.Disable);

            CheckPosition(Vector3.zero);
            Assert.IsTrue(translateObj.gameObject.activeInHierarchy);
            Assert.IsFalse(onStartRan);
            Assert.IsFalse(onMovedRan);
            Assert.IsFalse(onEndRan);

            translateObj.StartMoving();
            CheckPosition(Vector3.zero);
            Assert.IsTrue(translateObj.gameObject.activeInHierarchy);
            Assert.IsTrue(onStartRan);
            Assert.IsFalse(onMovedRan);
            Assert.IsFalse(onEndRan);

            ResetFlags();
            yield return new WaitForSeconds(9.9f);
            CheckPosition(Vector3.forward * 9.9f);
            Assert.IsTrue(translateObj.gameObject.activeInHierarchy);
            Assert.IsFalse(onStartRan);
            Assert.IsTrue(onMovedRan);
            Assert.IsFalse(onEndRan);

            ResetFlags();
            yield return new WaitForSeconds(.2f);
            CheckPosition(Vector3.forward * 10);
            Assert.IsFalse(translateObj.gameObject.activeInHierarchy);
            Assert.IsFalse(onStartRan);
            Assert.IsTrue(onMovedRan);
            Assert.IsTrue(onEndRan);

            ResetFlags();
            yield return new WaitForSeconds(1.9f);
            CheckPosition(Vector3.forward * 10);
            Assert.IsFalse(translateObj.gameObject.activeInHierarchy);
            Assert.IsFalse(onStartRan);
            Assert.IsFalse(onMovedRan);
            Assert.IsFalse(onEndRan);
        }
        [UnityTest]
        public IEnumerator Moving_EOL_Destroy_UnityDestroy_Tests()
        {
            ReflectionUtility.SetInstanceField(typeof(TranslateObject), translateObj, "endMode", EOLMode.Destroy);

            CheckPosition(Vector3.zero);
            Assert.IsTrue(translateObj.gameObject.activeInHierarchy);
            Assert.IsFalse(onStartRan);
            Assert.IsFalse(onMovedRan);
            Assert.IsFalse(onEndRan);

            translateObj.StartMoving();
            CheckPosition(Vector3.zero);
            Assert.IsTrue(translateObj.gameObject.activeInHierarchy);
            Assert.IsTrue(onStartRan);
            Assert.IsFalse(onMovedRan);
            Assert.IsFalse(onEndRan);

            ResetFlags();
            yield return new WaitForSeconds(9.9f);
            CheckPosition(Vector3.forward * 9.9f);
            Assert.IsTrue(translateObj.gameObject.activeInHierarchy);
            Assert.IsFalse(onStartRan);
            Assert.IsTrue(onMovedRan);
            Assert.IsFalse(onEndRan);

            ResetFlags();
            yield return new WaitForSeconds(.2f);
            Assert.IsFalse(translateObj);
            Assert.IsFalse(onStartRan);
            Assert.IsTrue(onMovedRan);
            Assert.IsTrue(onEndRan);

            ResetFlags();
            yield return new WaitForSeconds(1.9f);
            Assert.IsFalse(translateObj);
            Assert.IsFalse(onStartRan);
            Assert.IsFalse(onMovedRan);
            Assert.IsFalse(onEndRan);
        }
        [UnityTest]
        public IEnumerator Moving_EOL_Destroy_ObjPoolDestroy_Tests()
        {
            ReflectionUtility.SetInstanceField(typeof(TranslateObject), translateObj, "endMode", EOLMode.Destroy);
            translateObj.useObjPoolDestroy = true;
            ObjectPoolManager objPool = new GameObject("ObjPool").AddComponent<ObjectPoolManager>();

            CheckPosition(Vector3.zero);
            Assert.IsTrue(translateObj.gameObject.activeInHierarchy);
            Assert.IsFalse(onStartRan);
            Assert.IsFalse(onMovedRan);
            Assert.IsFalse(onEndRan);

            translateObj.StartMoving();
            CheckPosition(Vector3.zero);
            Assert.IsTrue(translateObj.gameObject.activeInHierarchy);
            Assert.IsTrue(onStartRan);
            Assert.IsFalse(onMovedRan);
            Assert.IsFalse(onEndRan);

            ResetFlags();
            yield return new WaitForSeconds(9.9f);
            CheckPosition(Vector3.forward * 9.9f);
            Assert.IsTrue(translateObj.gameObject.activeInHierarchy);
            Assert.IsFalse(onStartRan);
            Assert.IsTrue(onMovedRan);
            Assert.IsFalse(onEndRan);

            ResetFlags();
            yield return new WaitForSeconds(.2f);
            CheckPosition(Vector3.forward * 10);
            Assert.IsFalse(translateObj.gameObject.activeInHierarchy);
            Transform destroyedObj = objPool.transform.GetChild(1).GetChild(0).GetChild(0);
            Assert.AreEqual(translateObj.transform, destroyedObj);
            Assert.IsFalse(onStartRan);
            Assert.IsTrue(onMovedRan);
            Assert.IsTrue(onEndRan);

            ResetFlags();
            yield return new WaitForSeconds(1.9f);
            CheckPosition(Vector3.forward * 10);
            Assert.IsFalse(translateObj.gameObject.activeInHierarchy);
            destroyedObj = objPool.transform.GetChild(1).GetChild(0).GetChild(0);
            Assert.AreEqual(translateObj.transform, destroyedObj);
            Assert.IsFalse(onStartRan);
            Assert.IsFalse(onMovedRan);
            Assert.IsFalse(onEndRan);

            GameObject.Destroy(objPool.gameObject);
            objPool = null;
            destroyedObj = null;
        }

        [UnityTest]
        public IEnumerator Stop_CancelEOL_Tests()
        {
            CheckPosition(Vector3.zero);
            Assert.IsTrue(translateObj.gameObject.activeInHierarchy);
            Assert.IsFalse(onStartRan);
            Assert.IsFalse(onMovedRan);
            Assert.IsFalse(onEndRan);

            translateObj.StartMoving();
            CheckPosition(Vector3.zero);
            Assert.IsTrue(translateObj.gameObject.activeInHierarchy);
            Assert.IsTrue(onStartRan);
            Assert.IsFalse(onMovedRan);
            Assert.IsFalse(onEndRan);

            ResetFlags();
            yield return new WaitForSeconds(2);
            CheckPosition(Vector3.forward * 2);
            Assert.IsTrue(translateObj.gameObject.activeInHierarchy);
            Assert.IsFalse(onStartRan);
            Assert.IsTrue(onMovedRan);
            Assert.IsFalse(onEndRan);

            ResetFlags();
            translateObj.Stop();
            yield return new WaitForSeconds(2);
            CheckPosition(Vector3.forward * 2);
            Assert.IsTrue(translateObj.gameObject.activeInHierarchy);
            Assert.IsFalse(onStartRan);
            Assert.IsFalse(onMovedRan);
            Assert.IsFalse(onEndRan);

            ResetFlags();
            yield return new WaitForSeconds(8);
            CheckPosition(Vector3.forward * 2);
            Assert.IsTrue(translateObj.gameObject.activeInHierarchy);
            Assert.IsFalse(onStartRan);
            Assert.IsFalse(onMovedRan);
            Assert.IsFalse(onEndRan);
        }
        [UnityTest]
        public IEnumerator Stop_ContinueEOL_Tests()
        {
            ReflectionUtility.SetInstanceField(typeof(TranslateObject), translateObj, "endMode", EOLMode.Disable);

            CheckPosition(Vector3.zero);
            Assert.IsTrue(translateObj.gameObject.activeInHierarchy);
            Assert.IsFalse(onStartRan);
            Assert.IsFalse(onMovedRan);
            Assert.IsFalse(onEndRan);

            translateObj.StartMoving();
            CheckPosition(Vector3.zero);
            Assert.IsTrue(translateObj.gameObject.activeInHierarchy);
            Assert.IsTrue(onStartRan);
            Assert.IsFalse(onMovedRan);
            Assert.IsFalse(onEndRan);

            ResetFlags();
            yield return new WaitForSeconds(2);
            CheckPosition(Vector3.forward * 2);
            Assert.IsTrue(translateObj.gameObject.activeInHierarchy);
            Assert.IsFalse(onStartRan);
            Assert.IsTrue(onMovedRan);
            Assert.IsFalse(onEndRan);

            ResetFlags();
            translateObj.Stop(false);
            yield return new WaitForSeconds(2);
            CheckPosition(Vector3.forward * 2);
            Assert.IsTrue(translateObj.gameObject.activeInHierarchy);
            Assert.IsFalse(onStartRan);
            Assert.IsFalse(onMovedRan);
            Assert.IsFalse(onEndRan);

            ResetFlags();
            yield return new WaitForSeconds(8);
            CheckPosition(Vector3.forward * 2);
            Assert.IsFalse(translateObj.gameObject.activeInHierarchy);
            Assert.IsFalse(onStartRan);
            Assert.IsFalse(onMovedRan);
            Assert.IsTrue(onEndRan);
        }

        [SetUp]
        public void Setup()
        {
            translateObj = new GameObject("TransalateObject").AddComponent<TranslateObject>();
            translateObj.OnStart.AddListener(OnStart);
            translateObj.OnMoved.AddListener(OnMoved);
            translateObj.OnEnd.AddListener(OnEnd);
        }
        [TearDown]
        public void Teardown()
        {
            if (translateObj) GameObject.Destroy(translateObj.gameObject);
            translateObj = null;

            ResetFlags();
        }

        private void OnStart(GameObject obj)
        {
            onStartRan = true;

            Assert.IsTrue(obj);
            Assert.AreEqual(translateObj.gameObject, obj);
        }
        private void OnMoved(GameObject obj)
        {
            onMovedRan = true;

            Assert.IsTrue(obj);
            Assert.AreEqual(translateObj.gameObject, obj);
        }
        private void OnEnd(GameObject obj)
        {
            onEndRan = true;

            Assert.IsTrue(obj);
            Assert.AreEqual(translateObj.gameObject, obj);
        }
        private void ResetFlags()
        {
            onStartRan = false;
            onMovedRan = false;
            onEndRan = false;
        }

        private void CheckPosition(Vector3 expected)
        {
            Assert.IsTrue(MathUtility.AreAlmostEqual(expected.x, translateObj.transform.position.x));
            Assert.IsTrue(MathUtility.AreAlmostEqual(expected.y, translateObj.transform.position.y));
            Assert.IsTrue(MathUtility.AreAlmostEqual(expected.z, translateObj.transform.position.z));
        }
    }
}
