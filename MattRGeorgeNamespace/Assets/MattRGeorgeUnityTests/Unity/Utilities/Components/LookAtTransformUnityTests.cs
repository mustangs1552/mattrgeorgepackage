﻿using System.Collections;
using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using MattRGeorge.Unity.Utilities.Components;

namespace MattRGeorge.UnityTests.Unity.Utilities.Components
{
    public class LookAtTransformUnityTests
    {
        private LookAtTransform lookAtTrans = null;
        private Transform target = null;

        [UnityTest]
        public IEnumerator UpdateLookAt_SameLevel_Tests()
        {
            target.position += Vector3.forward * 5;
            lookAtTrans.UpdateLookAt();
            yield return new WaitForFixedUpdate();
            Assert.IsTrue(Physics.Raycast(lookAtTrans.transform.position, lookAtTrans.transform.forward, 10));

            target.position += Vector3.right * 5;
            lookAtTrans.UpdateLookAt();
            yield return new WaitForFixedUpdate();
            Assert.IsTrue(Physics.Raycast(lookAtTrans.transform.position, lookAtTrans.transform.forward, 10));

            target.position += Vector3.back * 5;
            lookAtTrans.UpdateLookAt();
            yield return new WaitForFixedUpdate();
            Assert.IsTrue(Physics.Raycast(lookAtTrans.transform.position, lookAtTrans.transform.forward, 10));

            target.position += Vector3.back * 5;
            lookAtTrans.UpdateLookAt();
            yield return new WaitForFixedUpdate();
            Assert.IsTrue(Physics.Raycast(lookAtTrans.transform.position, lookAtTrans.transform.forward, 10));

            target.position += Vector3.left * 5;
            lookAtTrans.UpdateLookAt();
            yield return new WaitForFixedUpdate();
            Assert.IsTrue(Physics.Raycast(lookAtTrans.transform.position, lookAtTrans.transform.forward, 10));

            target.position += Vector3.left * 5;
            lookAtTrans.UpdateLookAt();
            yield return new WaitForFixedUpdate();
            Assert.IsTrue(Physics.Raycast(lookAtTrans.transform.position, lookAtTrans.transform.forward, 10));

            target.position += Vector3.forward * 5;
            lookAtTrans.UpdateLookAt();
            yield return new WaitForFixedUpdate();
            Assert.IsTrue(Physics.Raycast(lookAtTrans.transform.position, lookAtTrans.transform.forward, 10));

            target.position += Vector3.forward * 5;
            lookAtTrans.UpdateLookAt();
            yield return new WaitForFixedUpdate();
            Assert.IsTrue(Physics.Raycast(lookAtTrans.transform.position, lookAtTrans.transform.forward, 10));
        }
        [UnityTest]
        public IEnumerator UpdateLookAt_Higher_Tests()
        {
            target.position += new Vector3(0, 1, 1) * 5;
            lookAtTrans.UpdateLookAt();
            yield return new WaitForFixedUpdate();
            Assert.IsTrue(Physics.Raycast(lookAtTrans.transform.position, lookAtTrans.transform.forward, 10));

            target.position += Vector3.right * 5;
            lookAtTrans.UpdateLookAt();
            yield return new WaitForFixedUpdate();
            Assert.IsTrue(Physics.Raycast(lookAtTrans.transform.position, lookAtTrans.transform.forward, 10));

            target.position += Vector3.back * 5;
            lookAtTrans.UpdateLookAt();
            yield return new WaitForFixedUpdate();
            Assert.IsTrue(Physics.Raycast(lookAtTrans.transform.position, lookAtTrans.transform.forward, 10));

            target.position += Vector3.back * 5;
            lookAtTrans.UpdateLookAt();
            yield return new WaitForFixedUpdate();
            Assert.IsTrue(Physics.Raycast(lookAtTrans.transform.position, lookAtTrans.transform.forward, 10));

            target.position += Vector3.left * 5;
            lookAtTrans.UpdateLookAt();
            yield return new WaitForFixedUpdate();
            Assert.IsTrue(Physics.Raycast(lookAtTrans.transform.position, lookAtTrans.transform.forward, 10));

            target.position += Vector3.left * 5;
            lookAtTrans.UpdateLookAt();
            yield return new WaitForFixedUpdate();
            Assert.IsTrue(Physics.Raycast(lookAtTrans.transform.position, lookAtTrans.transform.forward, 10));

            target.position += Vector3.forward * 5;
            lookAtTrans.UpdateLookAt();
            yield return new WaitForFixedUpdate();
            Assert.IsTrue(Physics.Raycast(lookAtTrans.transform.position, lookAtTrans.transform.forward, 10));

            target.position += Vector3.forward * 5;
            lookAtTrans.UpdateLookAt();
            yield return new WaitForFixedUpdate();
            Assert.IsTrue(Physics.Raycast(lookAtTrans.transform.position, lookAtTrans.transform.forward, 10));
        }
        [UnityTest]
        public IEnumerator UpdateLookAt_Lower_Tests()
        {
            target.position += new Vector3(0, -1, 1) * 5;
            lookAtTrans.UpdateLookAt();
            yield return new WaitForFixedUpdate();
            Assert.IsTrue(Physics.Raycast(lookAtTrans.transform.position, lookAtTrans.transform.forward, 10));

            target.position += Vector3.right * 5;
            lookAtTrans.UpdateLookAt();
            yield return new WaitForFixedUpdate();
            Assert.IsTrue(Physics.Raycast(lookAtTrans.transform.position, lookAtTrans.transform.forward, 10));

            target.position += Vector3.back * 5;
            lookAtTrans.UpdateLookAt();
            yield return new WaitForFixedUpdate();
            Assert.IsTrue(Physics.Raycast(lookAtTrans.transform.position, lookAtTrans.transform.forward, 10));

            target.position += Vector3.back * 5;
            lookAtTrans.UpdateLookAt();
            yield return new WaitForFixedUpdate();
            Assert.IsTrue(Physics.Raycast(lookAtTrans.transform.position, lookAtTrans.transform.forward, 10));

            target.position += Vector3.left * 5;
            lookAtTrans.UpdateLookAt();
            yield return new WaitForFixedUpdate();
            Assert.IsTrue(Physics.Raycast(lookAtTrans.transform.position, lookAtTrans.transform.forward, 10));

            target.position += Vector3.left * 5;
            lookAtTrans.UpdateLookAt();
            yield return new WaitForFixedUpdate();
            Assert.IsTrue(Physics.Raycast(lookAtTrans.transform.position, lookAtTrans.transform.forward, 10));

            target.position += Vector3.forward * 5;
            lookAtTrans.UpdateLookAt();
            yield return new WaitForFixedUpdate();
            Assert.IsTrue(Physics.Raycast(lookAtTrans.transform.position, lookAtTrans.transform.forward, 10));

            target.position += Vector3.forward * 5;
            lookAtTrans.UpdateLookAt();
            yield return new WaitForFixedUpdate();
            Assert.IsTrue(Physics.Raycast(lookAtTrans.transform.position, lookAtTrans.transform.forward, 10));
        }

        [UnityTest]
        public IEnumerator UpdateLookAt_SameLevel_Auto_Tests()
        {
            lookAtTrans.auto = true;

            target.position += Vector3.forward * 5;
            yield return new WaitForFixedUpdate();
            yield return new WaitForFixedUpdate();
            Assert.IsTrue(Physics.Raycast(lookAtTrans.transform.position, lookAtTrans.transform.forward, 10));

            target.position += Vector3.right * 5;
            yield return new WaitForFixedUpdate();
            yield return new WaitForFixedUpdate();
            Assert.IsTrue(Physics.Raycast(lookAtTrans.transform.position, lookAtTrans.transform.forward, 10));

            target.position += Vector3.back * 5;
            yield return new WaitForFixedUpdate();
            yield return new WaitForFixedUpdate();
            Assert.IsTrue(Physics.Raycast(lookAtTrans.transform.position, lookAtTrans.transform.forward, 10));

            target.position += Vector3.back * 5;
            yield return new WaitForFixedUpdate();
            yield return new WaitForFixedUpdate();
            Assert.IsTrue(Physics.Raycast(lookAtTrans.transform.position, lookAtTrans.transform.forward, 10));

            target.position += Vector3.left * 5;
            yield return new WaitForFixedUpdate();
            yield return new WaitForFixedUpdate();
            Assert.IsTrue(Physics.Raycast(lookAtTrans.transform.position, lookAtTrans.transform.forward, 10));

            target.position += Vector3.left * 5;
            yield return new WaitForFixedUpdate();
            yield return new WaitForFixedUpdate();
            Assert.IsTrue(Physics.Raycast(lookAtTrans.transform.position, lookAtTrans.transform.forward, 10));

            target.position += Vector3.forward * 5;
            yield return new WaitForFixedUpdate();
            yield return new WaitForFixedUpdate();
            Assert.IsTrue(Physics.Raycast(lookAtTrans.transform.position, lookAtTrans.transform.forward, 10));

            target.position += Vector3.forward * 5;
            yield return new WaitForFixedUpdate();
            yield return new WaitForFixedUpdate();
            Assert.IsTrue(Physics.Raycast(lookAtTrans.transform.position, lookAtTrans.transform.forward, 10));
        }
        [UnityTest]
        public IEnumerator UpdateLookAt_Higher_Auto_Tests()
        {
            lookAtTrans.auto = true;

            target.position += new Vector3(0, 1, 1) * 5;
            yield return new WaitForFixedUpdate();
            yield return new WaitForFixedUpdate();
            Assert.IsTrue(Physics.Raycast(lookAtTrans.transform.position, lookAtTrans.transform.forward, 10));

            target.position += Vector3.right * 5;
            yield return new WaitForFixedUpdate();
            yield return new WaitForFixedUpdate();
            Assert.IsTrue(Physics.Raycast(lookAtTrans.transform.position, lookAtTrans.transform.forward, 10));

            target.position += Vector3.back * 5;
            yield return new WaitForFixedUpdate();
            yield return new WaitForFixedUpdate();
            Assert.IsTrue(Physics.Raycast(lookAtTrans.transform.position, lookAtTrans.transform.forward, 10));

            target.position += Vector3.back * 5;
            yield return new WaitForFixedUpdate();
            yield return new WaitForFixedUpdate();
            Assert.IsTrue(Physics.Raycast(lookAtTrans.transform.position, lookAtTrans.transform.forward, 10));

            target.position += Vector3.left * 5;
            yield return new WaitForFixedUpdate();
            yield return new WaitForFixedUpdate();
            Assert.IsTrue(Physics.Raycast(lookAtTrans.transform.position, lookAtTrans.transform.forward, 10));

            target.position += Vector3.left * 5;
            yield return new WaitForFixedUpdate();
            yield return new WaitForFixedUpdate();
            Assert.IsTrue(Physics.Raycast(lookAtTrans.transform.position, lookAtTrans.transform.forward, 10));

            target.position += Vector3.forward * 5;
            yield return new WaitForFixedUpdate();
            yield return new WaitForFixedUpdate();
            Assert.IsTrue(Physics.Raycast(lookAtTrans.transform.position, lookAtTrans.transform.forward, 10));

            target.position += Vector3.forward * 5;
            yield return new WaitForFixedUpdate();
            yield return new WaitForFixedUpdate();
            Assert.IsTrue(Physics.Raycast(lookAtTrans.transform.position, lookAtTrans.transform.forward, 10));
        }
        [UnityTest]
        public IEnumerator UpdateLookAt_Lower_Auto_Tests()
        {
            lookAtTrans.auto = true;

            target.position += new Vector3(0, -1, 1) * 5;
            yield return new WaitForFixedUpdate();
            yield return new WaitForFixedUpdate();
            Assert.IsTrue(Physics.Raycast(lookAtTrans.transform.position, lookAtTrans.transform.forward, 10));

            target.position += Vector3.right * 5;
            yield return new WaitForFixedUpdate();
            yield return new WaitForFixedUpdate();
            Assert.IsTrue(Physics.Raycast(lookAtTrans.transform.position, lookAtTrans.transform.forward, 10));

            target.position += Vector3.back * 5;
            yield return new WaitForFixedUpdate();
            yield return new WaitForFixedUpdate();
            Assert.IsTrue(Physics.Raycast(lookAtTrans.transform.position, lookAtTrans.transform.forward, 10));

            target.position += Vector3.back * 5;
            yield return new WaitForFixedUpdate();
            yield return new WaitForFixedUpdate();
            Assert.IsTrue(Physics.Raycast(lookAtTrans.transform.position, lookAtTrans.transform.forward, 10));

            target.position += Vector3.left * 5;
            yield return new WaitForFixedUpdate();
            yield return new WaitForFixedUpdate();
            Assert.IsTrue(Physics.Raycast(lookAtTrans.transform.position, lookAtTrans.transform.forward, 10));

            target.position += Vector3.left * 5;
            yield return new WaitForFixedUpdate();
            yield return new WaitForFixedUpdate();
            Assert.IsTrue(Physics.Raycast(lookAtTrans.transform.position, lookAtTrans.transform.forward, 10));

            target.position += Vector3.forward * 5;
            yield return new WaitForFixedUpdate();
            yield return new WaitForFixedUpdate();
            Assert.IsTrue(Physics.Raycast(lookAtTrans.transform.position, lookAtTrans.transform.forward, 10));

            target.position += Vector3.forward * 5;
            yield return new WaitForFixedUpdate();
            yield return new WaitForFixedUpdate();
            Assert.IsTrue(Physics.Raycast(lookAtTrans.transform.position, lookAtTrans.transform.forward, 10));
        }

        [SetUp]
        public void Setup()
        {
            target = GameObject.CreatePrimitive(PrimitiveType.Cube).transform;
            target.name = "Target";
            GameObject temp = new GameObject("LookAtTrans");
            lookAtTrans = temp.AddComponent<LookAtTransform>();
            lookAtTrans.target = target;
        }
        [TearDown]
        public void Teardown()
        {
            if (target) GameObject.Destroy(target.gameObject);
            target = null;
            if (lookAtTrans) GameObject.Destroy(lookAtTrans.gameObject);
            lookAtTrans = null;
        }
    }
}
