﻿using MattRGeorge.General.Utilities.Static;
using MattRGeorge.Unity.Utilities.Components;
using NUnit.Framework;
using System.Collections;
using UnityEngine;
using UnityEngine.TestTools;

namespace MattRGeorge.UnityTests.Unity.Utilities.Components
{
    public class FollowTransformUnityTests
    {
        private FollowTransform followTrans = null;
        private Transform target = null;

        [UnityTest]
        public IEnumerator UpdatePosition_XYZ_Auto_Tests()
        {
            ReflectionUtility.SetInstanceField(typeof(FollowTransform), followTrans, "auto", true);

            Assert.AreEqual(target.position, followTrans.transform.position);

            target.position += new Vector3(5, 5, 5);
            yield return new WaitForSeconds(.1f);
            Assert.AreEqual(target.position, followTrans.transform.position);

            target.position -= new Vector3(10, 10, 10);
            yield return new WaitForSeconds(.1f);
            Assert.AreEqual(target.position, followTrans.transform.position);
        }

        [UnityTest]
        public IEnumerator UpdateRotation_XYZ_Auto_Tests()
        {
            ReflectionUtility.SetInstanceField(typeof(FollowTransform), followTrans, "auto", true);

            CheckRotation();

            target.Rotate(new Vector3(5, 5, 5));
            yield return new WaitForSeconds(.1f);
            CheckRotation();

            target.Rotate(new Vector3(-10, -10, -10));
            yield return new WaitForSeconds(.1f);
            CheckRotation();
        }

        [UnityTest]
        public IEnumerator UpdateTranform_XYZ_Auto_Tests()
        {
            ReflectionUtility.SetInstanceField(typeof(FollowTransform), followTrans, "auto", true);

            Assert.AreEqual(target.position, followTrans.transform.position);
            CheckRotation();

            target.position += new Vector3(5, 5, 5);
            target.Rotate(new Vector3(5, 5, 5));
            yield return new WaitForSeconds(.1f);
            Assert.AreEqual(target.position, followTrans.transform.position);
            CheckRotation();

            target.position += new Vector3(-10, -10, -10);
            target.Rotate(new Vector3(-10, -10, -10));
            yield return new WaitForSeconds(.1f);
            Assert.AreEqual(target.position, followTrans.transform.position);
            CheckRotation();
        }

        [SetUp]
        public void Setup()
        {
            target = new GameObject("Target").transform;
            GameObject temp = new GameObject("FollowTrans");
            followTrans = temp.AddComponent<FollowTransform>();
            followTrans.target = target;
        }
        [TearDown]
        public void Teardown()
        {
            if (target) GameObject.Destroy(target.gameObject);
            target = null;
            if (followTrans) GameObject.Destroy(followTrans.gameObject);
            followTrans = null;
        }

        private void CheckRotation()
        {
            Assert.IsTrue(MathUtility.AreAlmostEqual(followTrans.transform.eulerAngles.x, target.eulerAngles.x, .05f));
            Assert.IsTrue(MathUtility.AreAlmostEqual(followTrans.transform.eulerAngles.y, target.eulerAngles.y, .05f));
            Assert.IsTrue(MathUtility.AreAlmostEqual(followTrans.transform.eulerAngles.z, target.eulerAngles.z, .05f));
        }
    }
}
