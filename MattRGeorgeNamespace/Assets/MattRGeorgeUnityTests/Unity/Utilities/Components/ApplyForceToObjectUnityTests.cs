﻿using System.Collections;
using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using MattRGeorge.General.Utilities.Static;
using MattRGeorge.Unity.Utilities.Components;

namespace MattRGeorge.UnityTests.Unity.Utilities.Components
{
    public class ApplyForceToObjectUnityTests
    {
        private GameObject floor = null;
        private ApplyForceToObject forceObj = null;
        private Rigidbody rBody = null;

        [UnityTest]
        public IEnumerator ApplyForce_Tests()
        {
            yield return new WaitForSeconds(.5f);            
            ReflectionUtility.SetInstanceField(typeof(ApplyForceToObject), forceObj, "force", Vector3.up * 10);
            forceObj.ApplyForce();
            yield return new WaitForFixedUpdate();
            Assert.IsTrue(MathUtility.AreAlmostEqual(rBody.velocity.x, 0, .5f));
            Assert.IsTrue(MathUtility.AreAlmostEqual(rBody.velocity.z, 0, .5f));
            Assert.IsTrue(MathUtility.AreAlmostEqual(rBody.velocity.y, 10, .5f));
            yield return new WaitForSeconds(.5f);
            Assert.Greater(forceObj.transform.position.y, 3);
            Assert.IsTrue(MathUtility.AreAlmostEqual(rBody.velocity.x, 0, .5f));
            Assert.IsTrue(MathUtility.AreAlmostEqual(rBody.velocity.z, 0, .5f));
            Assert.Greater(rBody.velocity.y, 0);
        }
        [UnityTest]
        public IEnumerator ApplyForce_Multiplier_Tests()
        {
            yield return new WaitForSeconds(.5f);
            ReflectionUtility.SetInstanceField(typeof(ApplyForceToObject), forceObj, "force", Vector3.up * 10);
            forceObj.ApplyForce(3);
            yield return new WaitForFixedUpdate();
            Assert.IsTrue(MathUtility.AreAlmostEqual(rBody.velocity.x, 0, .5f));
            Assert.IsTrue(MathUtility.AreAlmostEqual(rBody.velocity.z, 0, .5f));
            Assert.IsTrue(MathUtility.AreAlmostEqual(rBody.velocity.y, 30, .5f));
            yield return new WaitForSeconds(.4f);
            Assert.Greater(forceObj.transform.position.y, 10);
            Assert.IsTrue(MathUtility.AreAlmostEqual(rBody.velocity.x, 0, .5f));
            Assert.IsTrue(MathUtility.AreAlmostEqual(rBody.velocity.z, 0, .5f));
            Assert.Greater(rBody.velocity.y, 20);
        }
        [UnityTest]
        public IEnumerator ApplyForce_ForceModes_Tests()
        {
            yield return new WaitForSeconds(.5f);
            ReflectionUtility.SetInstanceField(typeof(ApplyForceToObject), forceObj, "force", Vector3.up * 10);

            // Force
            ReflectionUtility.SetInstanceField(typeof(ApplyForceToObject), forceObj, "forceMode", ForceMode.Force);
            forceObj.ApplyForce(50);
            yield return new WaitForFixedUpdate();
            Assert.IsTrue(MathUtility.AreAlmostEqual(rBody.velocity.x, 0, .5f));
            Assert.IsTrue(MathUtility.AreAlmostEqual(rBody.velocity.z, 0, .5f));
            Assert.IsTrue(MathUtility.AreAlmostEqual(rBody.velocity.y, 10, .5f));
            yield return new WaitForSeconds(.5f);
            Assert.Greater(forceObj.transform.position.y, 3);
            Assert.IsTrue(MathUtility.AreAlmostEqual(rBody.velocity.x, 0, .5f));
            Assert.IsTrue(MathUtility.AreAlmostEqual(rBody.velocity.z, 0, .5f));
            Assert.Greater(rBody.velocity.y, 0);
            yield return new WaitForSeconds(3);
            Assert.IsTrue(MathUtility.AreAlmostEqual(rBody.velocity.y, 0, .5f));

            // Impulse
            ReflectionUtility.SetInstanceField(typeof(ApplyForceToObject), forceObj, "forceMode", ForceMode.Impulse);
            forceObj.ApplyForce();
            yield return new WaitForFixedUpdate();
            Assert.IsTrue(MathUtility.AreAlmostEqual(rBody.velocity.x, 0, .5f));
            Assert.IsTrue(MathUtility.AreAlmostEqual(rBody.velocity.z, 0, .5f));
            Assert.IsTrue(MathUtility.AreAlmostEqual(rBody.velocity.y, 10, .5f));
            yield return new WaitForSeconds(.5f);
            Assert.Greater(forceObj.transform.position.y, 3);
            Assert.IsTrue(MathUtility.AreAlmostEqual(rBody.velocity.x, 0, .5f));
            Assert.IsTrue(MathUtility.AreAlmostEqual(rBody.velocity.z, 0, .5f));
            Assert.Greater(rBody.velocity.y, 0);
            yield return new WaitForSeconds(3);
            Assert.IsTrue(MathUtility.AreAlmostEqual(rBody.velocity.y, 0, .5f));

            // Velocity Change
            ReflectionUtility.SetInstanceField(typeof(ApplyForceToObject), forceObj, "forceMode", ForceMode.VelocityChange);
            forceObj.ApplyForce();
            yield return new WaitForFixedUpdate();
            Assert.IsTrue(MathUtility.AreAlmostEqual(rBody.velocity.x, 0, .5f));
            Assert.IsTrue(MathUtility.AreAlmostEqual(rBody.velocity.z, 0, .5f));
            Assert.IsTrue(MathUtility.AreAlmostEqual(rBody.velocity.y, 10, .5f));
            yield return new WaitForSeconds(.5f);
            Assert.Greater(forceObj.transform.position.y, 3);
            Assert.IsTrue(MathUtility.AreAlmostEqual(rBody.velocity.x, 0, .5f));
            Assert.IsTrue(MathUtility.AreAlmostEqual(rBody.velocity.z, 0, .5f));
            Assert.Greater(rBody.velocity.y, 0);
            yield return new WaitForSeconds(3);
            Assert.IsTrue(MathUtility.AreAlmostEqual(rBody.velocity.y, 0, .5f));

            // Acceleration
            ReflectionUtility.SetInstanceField(typeof(ApplyForceToObject), forceObj, "forceMode", ForceMode.Acceleration);
            forceObj.ApplyForce(50);
            yield return new WaitForFixedUpdate();
            Assert.IsTrue(MathUtility.AreAlmostEqual(rBody.velocity.x, 0, .5f));
            Assert.IsTrue(MathUtility.AreAlmostEqual(rBody.velocity.z, 0, .5f));
            Assert.IsTrue(MathUtility.AreAlmostEqual(rBody.velocity.y, 10, .5f));
            yield return new WaitForSeconds(.5f);
            Assert.Greater(forceObj.transform.position.y, 3);
            Assert.IsTrue(MathUtility.AreAlmostEqual(rBody.velocity.x, 0, .5f));
            Assert.IsTrue(MathUtility.AreAlmostEqual(rBody.velocity.z, 0, .5f));
            Assert.Greater(rBody.velocity.y, 0);
            yield return new WaitForSeconds(3);
            Assert.IsTrue(MathUtility.AreAlmostEqual(rBody.velocity.y, 0, .5f));
        }
        [UnityTest]
        public IEnumerator ApplyForce_ApplyContinuously_Tests()
        {
            yield return new WaitForSeconds(.5f);
            ReflectionUtility.SetInstanceField(typeof(ApplyForceToObject), forceObj, "force", Vector3.up * 10);
            ReflectionUtility.SetInstanceField(typeof(ApplyForceToObject), forceObj, "applyContinuously", true);
            ReflectionUtility.SetInstanceField(typeof(ApplyForceToObject), forceObj, "continuouslyFreqeuncy", 3);
            forceObj.ApplyForce();
            yield return new WaitForFixedUpdate();
            Assert.IsTrue(MathUtility.AreAlmostEqual(rBody.velocity.x, 0, .5f));
            Assert.IsTrue(MathUtility.AreAlmostEqual(rBody.velocity.z, 0, .5f));
            Assert.IsTrue(MathUtility.AreAlmostEqual(rBody.velocity.y, 10, .5f));
            yield return new WaitForSeconds(.5f);
            Assert.Greater(forceObj.transform.position.y, 3);
            Assert.IsTrue(MathUtility.AreAlmostEqual(rBody.velocity.x, 0, .5f));
            Assert.IsTrue(MathUtility.AreAlmostEqual(rBody.velocity.z, 0, .5f));
            Assert.Greater(rBody.velocity.y, 0);

            yield return new WaitForSeconds(2.4f);
            Assert.IsTrue(MathUtility.AreAlmostEqual(rBody.velocity.x, 0, .5f));
            Assert.IsTrue(MathUtility.AreAlmostEqual(rBody.velocity.z, 0, .5f));
            Assert.IsTrue(MathUtility.AreAlmostEqual(rBody.velocity.y, 0, .5f));
            yield return new WaitForSeconds(.1f);
            Assert.IsTrue(MathUtility.AreAlmostEqual(rBody.velocity.x, 0, .5f));
            Assert.IsTrue(MathUtility.AreAlmostEqual(rBody.velocity.z, 0, .5f));
            Assert.IsTrue(MathUtility.AreAlmostEqual(rBody.velocity.y, 10, .5f));
            yield return new WaitForSeconds(.5f);
            Assert.Greater(forceObj.transform.position.y, 3);
            Assert.IsTrue(MathUtility.AreAlmostEqual(rBody.velocity.x, 0, .5f));
            Assert.IsTrue(MathUtility.AreAlmostEqual(rBody.velocity.z, 0, .5f));
            Assert.Greater(rBody.velocity.y, 0);

            yield return new WaitForSeconds(2.4f);
            Assert.IsTrue(MathUtility.AreAlmostEqual(rBody.velocity.x, 0, .5f));
            Assert.IsTrue(MathUtility.AreAlmostEqual(rBody.velocity.z, 0, .5f));
            Assert.IsTrue(MathUtility.AreAlmostEqual(rBody.velocity.y, 0, .5f));
            yield return new WaitForSeconds(.1f);
            Assert.IsTrue(MathUtility.AreAlmostEqual(rBody.velocity.x, 0, .5f));
            Assert.IsTrue(MathUtility.AreAlmostEqual(rBody.velocity.z, 0, .5f));
            Assert.IsTrue(MathUtility.AreAlmostEqual(rBody.velocity.y, 10, 1));
            yield return new WaitForSeconds(.5f);
            Assert.Greater(forceObj.transform.position.y, 3);
            Assert.IsTrue(MathUtility.AreAlmostEqual(rBody.velocity.x, 0, .5f));
            Assert.IsTrue(MathUtility.AreAlmostEqual(rBody.velocity.z, 0, .5f));
            Assert.Greater(rBody.velocity.y, 0);
        }

        [SetUp]
        public void Setup()
        {
            floor = GameObject.CreatePrimitive(PrimitiveType.Cube);
            floor.transform.localScale = new Vector3(10, .1f, 10);

            GameObject obj = GameObject.CreatePrimitive(PrimitiveType.Cube);
            obj.transform.Translate(Vector3.up * 1);
            obj.name = "ForceObj";
            rBody = obj.AddComponent<Rigidbody>();
            forceObj = obj.AddComponent<ApplyForceToObject>();
        }
        [TearDown]
        public void Teardown()
        {
            GameObject.Destroy(floor);
            floor = null;
            GameObject.Destroy(forceObj.gameObject);
            forceObj = null;
            rBody = null;
        }
    }
}
