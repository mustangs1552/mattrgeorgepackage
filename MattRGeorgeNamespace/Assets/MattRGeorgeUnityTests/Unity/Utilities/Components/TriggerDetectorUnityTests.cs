﻿using System.Collections;
using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using MattRGeorge.Unity.Utilities.Components;

namespace MattRGeorge.UnityTests.Unity.Utilities.Components
{
    public class TriggerDetectorUnityTests
    {
        private TriggerDetector detector = null;
        private Transform obj = null;
        private bool onEnterRan = false;
        private bool onStayRan = false;
        private bool onExitRan = false;

        [UnityTest]
        public IEnumerator Trigger_Tests()
        {
            detector.transform.localScale = new Vector3(1, 5, 1);
            obj.position += Vector3.up * 5;
            yield return new WaitForFixedUpdate();
            ResetFlags();

            yield return new WaitForSeconds(.75f);
            Assert.IsTrue(onEnterRan);
            Assert.IsTrue(onStayRan);
            Assert.IsFalse(onExitRan);
            ResetFlags();

            yield return new WaitForSeconds(.75f);
            Assert.IsFalse(onEnterRan);
            Assert.IsTrue(onStayRan);
            Assert.IsTrue(onExitRan);
        }

        [SetUp]
        public void Setup()
        {
            detector = GameObject.CreatePrimitive(PrimitiveType.Cube).AddComponent<TriggerDetector>();
            detector.GetComponent<Collider>().isTrigger = true;
            detector.OnEnter.AddListener(OnEnter);
            detector.OnStay.AddListener(OnStay);
            detector.OnExit.AddListener(OnExit);

            obj = GameObject.CreatePrimitive(PrimitiveType.Cube).transform;
            obj.gameObject.AddComponent<Rigidbody>();
        }
        [TearDown]
        public void Teardown()
        {
            if (detector) GameObject.Destroy(detector.gameObject);
            detector = null;
            if (obj) GameObject.Destroy(obj.gameObject);
            obj = null;

            ResetFlags();
        }

        private void OnEnter(GameObject thisObj, Collider otherCol)
        {
            onEnterRan = true;

            Assert.IsTrue(thisObj);
            Assert.AreEqual(detector.gameObject, thisObj);
            Assert.IsTrue(otherCol);
            Assert.IsTrue(otherCol.gameObject);
            Assert.AreEqual(obj.gameObject, otherCol.gameObject);
        }
        private void OnStay(GameObject thisObj, Collider otherCol)
        {
            onStayRan = true;

            Assert.IsTrue(thisObj);
            Assert.AreEqual(detector.gameObject, thisObj);
            Assert.IsTrue(otherCol);
            Assert.IsTrue(otherCol.gameObject);
            Assert.AreEqual(obj.gameObject, otherCol.gameObject);
        }
        private void OnExit(GameObject thisObj, Collider otherCol)
        {
            onExitRan = true;

            Assert.IsTrue(thisObj);
            Assert.AreEqual(detector.gameObject, thisObj);
            Assert.IsTrue(otherCol);
            Assert.IsTrue(otherCol.gameObject);
            Assert.AreEqual(obj.gameObject, otherCol.gameObject);
        }
        private void ResetFlags()
        {
            onEnterRan = false;
            onStayRan = false;
            onExitRan = false;
        }
    }
}
