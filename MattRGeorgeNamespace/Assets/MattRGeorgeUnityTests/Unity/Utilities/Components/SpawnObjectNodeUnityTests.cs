﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using MattRGeorge.Unity.Tools.ObjectPooling;
using MattRGeorge.Unity.Utilities.Components;
using MattRGeorge.Unity.Utilities.Components.Helpers;

namespace MattRGeorge.UnityTests.Unity.Utilities.Components
{
    public class SpawnObjectNodeUnityTests
    {
        private SpawnObjectNode spawnObj = null;
        private GameObject obj = null;
        private SpawnRequest spawnRequest = new SpawnRequest();
        private ObjectPoolManager objPool = null;
        private bool onSpawnRan = false;
        private bool onPostSpawnRan = false;
        private bool requestOnSpawnRan = false;
        private bool requestOnPostSpawnRan = false;
        private bool requestOnCompletedRan = false;
        private bool currAsActive = false;
        private bool currUseObjectPool = false;
        private bool currIsObjectPoolManaged = false;
        private List<GameObject> spawnedObjs = new List<GameObject>();

        [UnityTest]
        public IEnumerator SpawnObj_Tests()
        {
            spawnObj.spawnAsActive = true;
            currAsActive = true;
            spawnObj.useObjectPool = false;
            currUseObjectPool = false;
            spawnObj.isObjectPoolManaged = false;
            currIsObjectPoolManaged = false;
            spawnObj.SpawnDelay = 0;
            spawnObj.PostSpawnDelay = 0;
            spawnObj.SpawnObj(obj);
            yield return new WaitForEndOfFrame();
            Assert.IsTrue(onSpawnRan);
            Assert.IsTrue(onPostSpawnRan);

            ResetFlags();
            spawnObj.spawnAsActive = false;
            currAsActive = false;
            spawnObj.SpawnDelay = 5;
            spawnObj.PostSpawnDelay = 3;
            spawnObj.SpawnObj(obj);
            yield return new WaitForSeconds(5.1f);
            Assert.IsTrue(onSpawnRan);
            Assert.IsFalse(onPostSpawnRan);
            yield return new WaitForSeconds(3);
            Assert.IsTrue(onSpawnRan);
            Assert.IsTrue(onPostSpawnRan);

            ResetFlags();
            spawnObj.spawnAsActive = true;
            currAsActive = true;
            spawnObj.SpawnDelay = 3;
            spawnObj.PostSpawnDelay = 5;
            spawnObj.SpawnObj(obj);
            yield return new WaitForSeconds(3.1f);
            Assert.IsTrue(onSpawnRan);
            Assert.IsFalse(onPostSpawnRan);
            yield return new WaitForSeconds(5);
            Assert.IsTrue(onSpawnRan);
            Assert.IsTrue(onPostSpawnRan);
        }
        [UnityTest]
        public IEnumerator SpawnObj_InvalidInput_Tests()
        {
            spawnObj.spawnAsActive = true;
            currAsActive = true;
            spawnObj.useObjectPool = false;
            currUseObjectPool = false;
            spawnObj.isObjectPoolManaged = false;
            currIsObjectPoolManaged = false;
            spawnObj.SpawnDelay = 0;
            spawnObj.PostSpawnDelay = 0;
            spawnObj.SpawnObj(null);
            yield return new WaitForEndOfFrame();
            Assert.IsFalse(onSpawnRan);
            Assert.IsFalse(onPostSpawnRan);

            ResetFlags();
            spawnObj.SpawnDelay = 5;
            spawnObj.PostSpawnDelay = 3;
            spawnObj.SpawnObj(null);
            yield return new WaitForSeconds(5.1f);
            Assert.IsFalse(onSpawnRan);
            Assert.IsFalse(onPostSpawnRan);
            yield return new WaitForSeconds(3);
            Assert.IsFalse(onSpawnRan);
            Assert.IsFalse(onPostSpawnRan);
        }
        [UnityTest]
        public IEnumerator SpawnObj_ObjPool_Tests()
        {
            spawnObj.spawnAsActive = true;
            currAsActive = true;
            spawnObj.useObjectPool = true;
            currUseObjectPool = true;
            spawnObj.isObjectPoolManaged = false;
            currIsObjectPoolManaged = false;
            spawnObj.SpawnDelay = 0;
            spawnObj.PostSpawnDelay = 0;
            spawnObj.SpawnObj(obj);
            yield return new WaitForEndOfFrame();
            Assert.IsTrue(onSpawnRan);
            Assert.IsTrue(onPostSpawnRan);

            ResetFlags();
            spawnObj.spawnAsActive = false;
            currAsActive = false;
            spawnObj.SpawnDelay = 5;
            spawnObj.PostSpawnDelay = 3;
            spawnObj.SpawnObj(obj);
            yield return new WaitForSeconds(5.1f);
            Assert.IsTrue(onSpawnRan);
            Assert.IsFalse(onPostSpawnRan);
            yield return new WaitForSeconds(3);
            Assert.IsTrue(onSpawnRan);
            Assert.IsTrue(onPostSpawnRan);

            ResetFlags();
            spawnObj.spawnAsActive = true;
            currAsActive = true;
            spawnObj.isObjectPoolManaged = true;
            currIsObjectPoolManaged = true;
            spawnObj.SpawnDelay = 3;
            spawnObj.PostSpawnDelay = 5;
            spawnObj.SpawnObj(obj);
            yield return new WaitForSeconds(3.1f);
            Assert.IsTrue(onSpawnRan);
            Assert.IsFalse(onPostSpawnRan);
            yield return new WaitForSeconds(5);
            Assert.IsTrue(onSpawnRan);
            Assert.IsTrue(onPostSpawnRan);

            ResetFlags();
            spawnObj.spawnAsActive = false;
            currAsActive = false;
            spawnObj.isObjectPoolManaged = false;
            currIsObjectPoolManaged = false;
            spawnObj.SpawnDelay = 0;
            spawnObj.PostSpawnDelay = 0;
            spawnObj.SpawnObj(obj);
            yield return new WaitForEndOfFrame();
            Assert.IsTrue(onSpawnRan);
            Assert.IsTrue(onPostSpawnRan);
        }
        [UnityTest]
        public IEnumerator SpawnObj_ObjPool_InvalidInput_Tests()
        {
            spawnObj.spawnAsActive = true;
            currAsActive = true;
            spawnObj.useObjectPool = true;
            currUseObjectPool = true;
            spawnObj.isObjectPoolManaged = false;
            currIsObjectPoolManaged = false;
            spawnObj.SpawnDelay = 0;
            spawnObj.PostSpawnDelay = 0;
            spawnObj.SpawnObj(null);
            yield return new WaitForEndOfFrame();
            Assert.IsFalse(onSpawnRan);
            Assert.IsFalse(onPostSpawnRan);

            ResetFlags();
            spawnObj.isObjectPoolManaged = true;
            currIsObjectPoolManaged = true;
            spawnObj.SpawnDelay = 5;
            spawnObj.PostSpawnDelay = 3;
            spawnObj.SpawnObj(null);
            yield return new WaitForSeconds(5.1f);
            Assert.IsFalse(onSpawnRan);
            Assert.IsFalse(onPostSpawnRan);
            yield return new WaitForSeconds(3);
            Assert.IsFalse(onSpawnRan);
            Assert.IsFalse(onPostSpawnRan);

            ResetFlags();
            GameObject.DestroyImmediate(objPool.gameObject);
            objPool = null;
            spawnObj.SpawnDelay = 5;
            spawnObj.PostSpawnDelay = 3;
            spawnObj.SpawnObj(obj);
            yield return new WaitForSeconds(5.1f);
            Assert.IsFalse(onSpawnRan);
            Assert.IsFalse(onPostSpawnRan);
            yield return new WaitForSeconds(3);
            Assert.IsFalse(onSpawnRan);
            Assert.IsFalse(onPostSpawnRan);

            ResetFlags();
            spawnObj.isObjectPoolManaged = false;
            currIsObjectPoolManaged = false;
            spawnObj.SpawnDelay = 5;
            spawnObj.PostSpawnDelay = 3;
            spawnObj.SpawnObj(obj);
            yield return new WaitForSeconds(5.1f);
            Assert.IsFalse(onSpawnRan);
            Assert.IsFalse(onPostSpawnRan);
            yield return new WaitForSeconds(3);
            Assert.IsFalse(onSpawnRan);
            Assert.IsFalse(onPostSpawnRan);
        }
        [UnityTest]
        public IEnumerator SpawnObj_SpawnRequest_Tests()
        {
            spawnObj.spawnAsActive = true;
            currAsActive = true;
            spawnObj.useObjectPool = false;
            currUseObjectPool = false;
            spawnObj.isObjectPoolManaged = false;
            currIsObjectPoolManaged = false;
            spawnObj.SpawnDelay = 0;
            spawnObj.PostSpawnDelay = 0;
            spawnRequest.OnSpawnCallback += RequestOnSpawn;
            spawnRequest.OnPostSpawnCallback += RequestOnPostSpawn;
            spawnRequest.OnCompletedCallback += RequestOnCompleted;
            spawnObj.SpawnObj(obj, ref spawnRequest);
            yield return new WaitForEndOfFrame();
            Assert.IsTrue(spawnRequest.spawned);
            Assert.IsTrue(spawnRequest.postSpawned);
            Assert.IsFalse(spawnRequest.failed);
            Assert.IsTrue(spawnRequest.completed);
            Assert.IsTrue(onSpawnRan);
            Assert.IsTrue(onPostSpawnRan);
            Assert.IsTrue(requestOnSpawnRan);
            Assert.IsTrue(requestOnPostSpawnRan);
            Assert.IsTrue(requestOnCompletedRan);

            ResetFlags();
            spawnObj.spawnAsActive = false;
            currAsActive = false;
            spawnObj.SpawnDelay = 5;
            spawnObj.PostSpawnDelay = 3;
            spawnRequest = new SpawnRequest();
            spawnRequest.OnSpawnCallback += RequestOnSpawn;
            spawnRequest.OnPostSpawnCallback += RequestOnPostSpawn;
            spawnRequest.OnCompletedCallback += RequestOnCompleted;
            spawnObj.SpawnObj(obj, ref spawnRequest);
            yield return new WaitForSeconds(5.1f);
            Assert.IsTrue(spawnRequest.spawned);
            Assert.IsFalse(spawnRequest.postSpawned);
            Assert.IsFalse(spawnRequest.failed);
            Assert.IsFalse(spawnRequest.completed);
            Assert.IsTrue(onSpawnRan);
            Assert.IsTrue(spawnRequest.spawned);
            Assert.IsFalse(onPostSpawnRan);
            Assert.IsFalse(spawnRequest.postSpawned);
            Assert.IsTrue(requestOnSpawnRan);
            Assert.IsFalse(requestOnPostSpawnRan);
            Assert.IsFalse(requestOnCompletedRan);
            yield return new WaitForSeconds(3);
            Assert.IsTrue(spawnRequest.spawned);
            Assert.IsTrue(spawnRequest.postSpawned);
            Assert.IsFalse(spawnRequest.failed);
            Assert.IsTrue(spawnRequest.completed);
            Assert.IsTrue(onSpawnRan);
            Assert.IsTrue(onPostSpawnRan);
            Assert.IsTrue(requestOnSpawnRan);
            Assert.IsTrue(requestOnPostSpawnRan);
            Assert.IsTrue(requestOnCompletedRan);

            ResetFlags();
            spawnObj.spawnAsActive = true;
            currAsActive = true;
            spawnObj.SpawnDelay = 3;
            spawnObj.PostSpawnDelay = 5;
            spawnRequest = new SpawnRequest();
            spawnRequest.OnSpawnCallback += RequestOnSpawn;
            spawnRequest.OnPostSpawnCallback += RequestOnPostSpawn;
            spawnRequest.OnCompletedCallback += RequestOnCompleted;
            spawnObj.SpawnObj(obj, ref spawnRequest);
            yield return new WaitForSeconds(3.1f);
            Assert.IsTrue(spawnRequest.spawned);
            Assert.IsFalse(spawnRequest.postSpawned);
            Assert.IsFalse(spawnRequest.failed);
            Assert.IsFalse(spawnRequest.completed);
            Assert.IsTrue(onSpawnRan);
            Assert.IsFalse(onPostSpawnRan);
            Assert.IsTrue(requestOnSpawnRan);
            Assert.IsFalse(requestOnPostSpawnRan);
            Assert.IsFalse(requestOnCompletedRan);
            yield return new WaitForSeconds(5);
            Assert.IsTrue(spawnRequest.spawned);
            Assert.IsTrue(spawnRequest.postSpawned);
            Assert.IsFalse(spawnRequest.failed);
            Assert.IsTrue(spawnRequest.completed);
            Assert.IsTrue(onSpawnRan);
            Assert.IsTrue(onPostSpawnRan);
            Assert.IsTrue(requestOnSpawnRan);
            Assert.IsTrue(requestOnPostSpawnRan);
            Assert.IsTrue(requestOnCompletedRan);

            ResetFlags();
            spawnObj.SpawnDelay = 1;
            spawnObj.PostSpawnDelay = 3;
            spawnRequest = new SpawnRequest();
            spawnRequest.OnSpawnCallback += RequestOnSpawn;
            spawnRequest.OnPostSpawnCallback += RequestOnPostSpawn;
            spawnRequest.OnCompletedCallback += RequestOnCompleted;
            spawnObj.SpawnObj(obj, ref spawnRequest);
            yield return new WaitForSeconds(1.1f);
            Assert.IsTrue(spawnRequest.spawned);
            Assert.IsFalse(spawnRequest.postSpawned);
            Assert.IsFalse(spawnRequest.failed);
            Assert.IsFalse(spawnRequest.completed);
            Assert.IsTrue(onSpawnRan);
            Assert.IsFalse(onPostSpawnRan);
            Assert.IsTrue(requestOnSpawnRan);
            Assert.IsFalse(requestOnPostSpawnRan);
            Assert.IsFalse(requestOnCompletedRan);
            spawnObj.StopCoroutine(spawnRequest.calledCoroutine);
            yield return new WaitForSeconds(4);
            Assert.IsTrue(spawnRequest.spawned);
            Assert.IsFalse(spawnRequest.postSpawned);
            Assert.IsFalse(spawnRequest.failed);
            Assert.IsFalse(spawnRequest.completed);
            Assert.IsTrue(onSpawnRan);
            Assert.IsFalse(onPostSpawnRan);
            Assert.IsTrue(requestOnSpawnRan);
            Assert.IsFalse(requestOnPostSpawnRan);
            Assert.IsFalse(requestOnCompletedRan);

            ResetFlags();
            spawnRequest = new SpawnRequest();
            spawnRequest.OnSpawnCallback += RequestOnSpawn;
            spawnRequest.OnPostSpawnCallback += RequestOnPostSpawn;
            spawnRequest.OnCompletedCallback += RequestOnCompleted;
            spawnObj.SpawnObj(obj, ref spawnRequest);
            spawnObj.StopCoroutine(spawnRequest.calledCoroutine);
            yield return new WaitForSeconds(1.1f);
            Assert.IsFalse(spawnRequest.spawned);
            Assert.IsFalse(spawnRequest.postSpawned);
            Assert.IsFalse(spawnRequest.failed);
            Assert.IsFalse(spawnRequest.completed);
            Assert.IsFalse(onSpawnRan);
            Assert.IsFalse(onPostSpawnRan);
            Assert.IsFalse(requestOnSpawnRan);
            Assert.IsFalse(requestOnPostSpawnRan);
            Assert.IsFalse(requestOnCompletedRan);
            yield return new WaitForSeconds(4);
            Assert.IsFalse(spawnRequest.spawned);
            Assert.IsFalse(spawnRequest.postSpawned);
            Assert.IsFalse(spawnRequest.failed);
            Assert.IsFalse(spawnRequest.completed);
            Assert.IsFalse(onSpawnRan);
            Assert.IsFalse(onPostSpawnRan);
            Assert.IsFalse(requestOnSpawnRan);
            Assert.IsFalse(requestOnPostSpawnRan);
            Assert.IsFalse(requestOnCompletedRan);
        }
        [UnityTest]
        public IEnumerator SpawnObj_SpawnRequest_InvalidInput_Tests()
        {
            spawnObj.spawnAsActive = true;
            currAsActive = true;
            spawnObj.useObjectPool = false;
            currUseObjectPool = false;
            spawnObj.isObjectPoolManaged = false;
            currIsObjectPoolManaged = false;
            spawnObj.SpawnDelay = 0;
            spawnObj.PostSpawnDelay = 0;
            spawnRequest.OnSpawnCallback += RequestOnSpawn;
            spawnRequest.OnPostSpawnCallback += RequestOnPostSpawn;
            spawnRequest.OnCompletedCallback += RequestFailedOnCompleted;
            spawnObj.SpawnObj(null, ref spawnRequest);
            yield return new WaitForEndOfFrame();
            Assert.IsFalse(onSpawnRan);
            Assert.IsFalse(onPostSpawnRan);
            Assert.IsFalse(requestOnSpawnRan);
            Assert.IsFalse(requestOnPostSpawnRan);
            Assert.IsTrue(requestOnCompletedRan);

            ResetFlags();
            spawnRequest = new SpawnRequest();
            spawnRequest.OnSpawnCallback += RequestOnSpawn;
            spawnRequest.OnPostSpawnCallback += RequestOnPostSpawn;
            spawnRequest.OnCompletedCallback += RequestFailedOnCompleted;
            spawnObj.SpawnObj(null, ref spawnRequest);
            yield return new WaitForEndOfFrame();
            Assert.IsFalse(onSpawnRan);
            Assert.IsFalse(onPostSpawnRan);
            Assert.IsFalse(requestOnSpawnRan);
            Assert.IsFalse(requestOnPostSpawnRan);
            Assert.IsTrue(requestOnCompletedRan);

            ResetFlags();
            spawnRequest = null;
            spawnObj.SpawnObj(null, ref spawnRequest);
            yield return new WaitForEndOfFrame();
            Assert.IsFalse(onSpawnRan);
            Assert.IsFalse(onPostSpawnRan);
            Assert.IsFalse(requestOnSpawnRan);
            Assert.IsFalse(requestOnPostSpawnRan);
            Assert.IsFalse(requestOnCompletedRan);

            ResetFlags();
            spawnRequest = null;
            spawnObj.SpawnObj(obj, ref spawnRequest);
            yield return new WaitForEndOfFrame();
            Assert.IsTrue(onSpawnRan);
            Assert.IsTrue(onPostSpawnRan);
            Assert.IsFalse(requestOnSpawnRan);
            Assert.IsFalse(requestOnPostSpawnRan);
            Assert.IsFalse(requestOnCompletedRan);
        }

        [SetUp]
        public void Setup()
        {
            spawnObj = new GameObject("SpawnObject").AddComponent<SpawnObjectNode>();
            spawnObj.OnSpawn.AddListener(OnSpawn);
            spawnObj.OnPostSpawn.AddListener(OnPostSpawn);

            spawnRequest = new SpawnRequest();

            obj = new GameObject("Obj");

            objPool = new GameObject("ObjPool").AddComponent<ObjectPoolManager>();
        }
        [TearDown]
        public void TearDown()
        {
            if (spawnObj) GameObject.DestroyImmediate(spawnObj.gameObject);
            spawnObj = null;

            if (obj) GameObject.DestroyImmediate(obj);
            obj = null;

            if (objPool) GameObject.DestroyImmediate(objPool.gameObject);
            objPool = null;

            foreach(GameObject obj in spawnedObjs)
            {
                if (obj) GameObject.DestroyImmediate(obj);
            }
            spawnedObjs = new List<GameObject>();

            currAsActive = false;
            currUseObjectPool = false;
            currIsObjectPoolManaged = false;
            ResetFlags();
        }

        private void OnSpawn(GameObject spawnedObj)
        {
            onSpawnRan = true;
            Assert.IsNotNull(spawnedObj);
            Assert.AreNotEqual(obj, spawnedObj);
            Assert.IsTrue(spawnedObj.activeInHierarchy == currAsActive);
            if(!currUseObjectPool) Assert.IsNull(spawnedObj.transform.parent);
            else
            {
                if (currIsObjectPoolManaged)
                {
                    Assert.IsNotNull(spawnedObj.transform.parent);
                    Assert.AreEqual("OBJ_ANCHOR", spawnedObj.transform.parent.name);
                    if(currAsActive) Assert.AreEqual("ACTIVE_OBJECTS_POOL", spawnedObj.transform.parent.parent.name);
                    else Assert.AreEqual("INACTIVE_OBJECTS_POOL", spawnedObj.transform.parent.parent.name);
                }
                else Assert.IsNull(spawnedObj.transform.parent);
            }

            spawnedObjs.Add(obj);
        }
        private void OnPostSpawn(GameObject spawnedObj)
        {
            onPostSpawnRan = true;
            Assert.IsNotNull(spawnedObj);
            Assert.AreNotEqual(obj, spawnedObj);
            Assert.IsTrue(spawnedObj.activeInHierarchy == currAsActive);
            if (!currUseObjectPool) Assert.IsNull(spawnedObj.transform.parent);
            else
            {
                if (currIsObjectPoolManaged)
                {
                    Assert.IsNotNull(spawnedObj.transform.parent);
                    Assert.AreEqual("OBJ_ANCHOR", spawnedObj.transform.parent.name);
                    if (currAsActive) Assert.AreEqual("ACTIVE_OBJECTS_POOL", spawnedObj.transform.parent.parent.name);
                    else Assert.AreEqual("INACTIVE_OBJECTS_POOL", spawnedObj.transform.parent.parent.name);
                }
                else Assert.IsNull(spawnedObj.transform.parent);
            }
        }

        private void RequestOnSpawn(SpawnRequest request)
        {
            requestOnSpawnRan = true;
            Assert.IsNotNull(request);
            Assert.IsNotNull(request.spawnedObj);
            Assert.IsTrue(request.spawnedObj.activeInHierarchy == currAsActive);
            if (!currUseObjectPool) Assert.IsNull(request.spawnedObj.transform.parent);
            else
            {
                if (currIsObjectPoolManaged)
                {
                    Assert.IsNotNull(request.spawnedObj.transform.parent);
                    Assert.AreEqual("OBJ_ANCHOR", request.spawnedObj.transform.parent.name);
                    if (currAsActive) Assert.AreEqual("ACTIVE_OBJECTS_POOL", request.spawnedObj.transform.parent.parent.name);
                    else Assert.AreEqual("INACTIVE_OBJECTS_POOL", request.spawnedObj.transform.parent.parent.name);
                }
                else Assert.IsNull(request.spawnedObj.transform.parent);
            }
            Assert.AreNotSame(obj, request.spawnedObj);
            Assert.IsTrue(request.spawned);
            Assert.IsFalse(request.postSpawned);
            Assert.IsFalse(request.failed);
            Assert.IsFalse(request.completed);
        }
        private void RequestOnPostSpawn(SpawnRequest request)
        {
            requestOnPostSpawnRan = true;
            Assert.IsNotNull(request);
            Assert.IsNotNull(request.spawnedObj);
            Assert.IsTrue(request.spawnedObj.activeInHierarchy == currAsActive);
            if (!currUseObjectPool) Assert.IsNull(request.spawnedObj.transform.parent);
            else
            {
                if (currIsObjectPoolManaged)
                {
                    Assert.IsNotNull(request.spawnedObj.transform.parent);
                    Assert.AreEqual("OBJ_ANCHOR", request.spawnedObj.transform.parent.name);
                    if (currAsActive) Assert.AreEqual("ACTIVE_OBJECTS_POOL", request.spawnedObj.transform.parent.parent.name);
                    else Assert.AreEqual("INACTIVE_OBJECTS_POOL", request.spawnedObj.transform.parent.parent.name);
                }
                else Assert.IsNull(request.spawnedObj.transform.parent);
            }
            Assert.AreNotSame(obj, request.spawnedObj);
            Assert.IsTrue(request.spawned);
            Assert.IsTrue(request.postSpawned);
            Assert.IsFalse(request.failed);
            Assert.IsFalse(request.completed);
        }
        private void RequestOnCompleted(SpawnRequest request)
        {
            requestOnCompletedRan = true;
            Assert.IsNotNull(request);
            Assert.IsNotNull(request.spawnedObj);
            Assert.IsTrue(request.spawnedObj.activeInHierarchy == currAsActive);
            if (!currUseObjectPool) Assert.IsNull(request.spawnedObj.transform.parent);
            else
            {
                if (currIsObjectPoolManaged)
                {
                    Assert.IsNotNull(request.spawnedObj.transform.parent);
                    Assert.AreEqual("OBJ_ANCHOR", request.spawnedObj.transform.parent.name);
                    if (currAsActive) Assert.AreEqual("ACTIVE_OBJECTS_POOL", request.spawnedObj.transform.parent.parent.name);
                    else Assert.AreEqual("INACTIVE_OBJECTS_POOL", request.spawnedObj.transform.parent.parent.name);
                }
                else Assert.IsNull(request.spawnedObj.transform.parent);
            }
            Assert.AreNotSame(obj, request.spawnedObj);
            Assert.IsTrue(request.spawned);
            Assert.IsTrue(request.postSpawned);
            Assert.IsFalse(request.failed);
            Assert.IsTrue(request.completed);
        }
        private void RequestFailedOnCompleted(SpawnRequest request)
        {
            requestOnCompletedRan = true;
            Assert.IsNotNull(request);
            Assert.IsNull(request.spawnedObj);
            Assert.IsFalse(request.spawned);
            Assert.IsFalse(request.postSpawned);
            Assert.IsTrue(request.failed);
            Assert.IsTrue(request.completed);
        }

        private void ResetFlags()
        {
            onSpawnRan = false;
            onPostSpawnRan = false;
            requestOnSpawnRan = false;
            requestOnPostSpawnRan = false;
            requestOnCompletedRan = false;
        }
    }
}
