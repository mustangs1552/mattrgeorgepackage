﻿using System.Collections;
using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using MattRGeorge.General.Utilities.Static;
using MattRGeorge.Unity.Utilities.Components;

namespace MattRGeorge.UnityTests.Unity.Utilities.Components
{
    public class RaycasterDetectorUnityTests
    {
        private RaycasterDetector detector = null;
        private Transform target = null;
        private bool onHitStartRan = false;
        private bool onHitRan = false;
        private bool onHitEndRan = false;
        private bool result = false;
        private RaycastHit hitResult = new RaycastHit();

        [UnityTest]
        public IEnumerator Auto_Tests()
        {
            target.transform.position += Vector3.forward * 5;
            Assert.IsFalse((bool)ReflectionUtility.GetInstanceField(typeof(RaycasterDetector), detector, "auto"));

            detector.Auto = true;
            Assert.IsTrue((bool)ReflectionUtility.GetInstanceField(typeof(RaycasterDetector), detector, "auto"));
            yield return new WaitForSeconds(1.1f);
            Assert.IsTrue(onHitStartRan);

            detector.Auto = false;
            Assert.IsFalse((bool)ReflectionUtility.GetInstanceField(typeof(RaycasterDetector), detector, "auto"));
            ResetFlags();
            yield return new WaitForSeconds(1.5f);
            Assert.IsFalse(onHitStartRan);
        }

        [UnityTest]
        public IEnumerator Frequency_Tests()
        {
            target.transform.position += Vector3.forward * 5;
            Assert.AreEqual(1, (float)ReflectionUtility.GetInstanceField(typeof(RaycasterDetector), detector, "frequency"));

            detector.Auto = true;
            yield return new WaitForSeconds(1.1f);
            Assert.IsTrue(onHitStartRan);

            target.transform.position += Vector3.up * 5;
            yield return new WaitForSeconds(1.1f);
            ResetFlags();
            detector.Frequency = 3;
            Assert.AreEqual(3, (float)ReflectionUtility.GetInstanceField(typeof(RaycasterDetector), detector, "frequency"));
            target.transform.position += Vector3.down * 5;
            yield return new WaitForSeconds(3.1f);
            Assert.IsTrue(onHitStartRan);

            target.transform.position += Vector3.up * 5;
            yield return new WaitForSeconds(3.1f);
            ResetFlags();
            detector.Auto = false;
            Assert.AreEqual(3, (float)ReflectionUtility.GetInstanceField(typeof(RaycasterDetector), detector, "frequency"));
            target.transform.position += Vector3.down * 5;
            yield return new WaitForSeconds(3.1f);
            Assert.IsFalse(onHitStartRan);

            detector.Auto = true;
            Assert.AreEqual(3, (float)ReflectionUtility.GetInstanceField(typeof(RaycasterDetector), detector, "frequency"));
            yield return new WaitForSeconds(3.1f);
            Assert.IsTrue(onHitStartRan);
        }

        [UnityTest]
        public IEnumerator CheckRaycast_Tests()
        {
            target.transform.position += Vector3.forward * 5;
            yield return new WaitForFixedUpdate();
            result = detector.CheckRaycast();
            Assert.IsTrue(result);
            Assert.IsFalse(onHitStartRan);
            Assert.IsFalse(onHitRan);
            Assert.IsFalse(onHitEndRan);

            target.transform.position += new Vector3(5, 5, -10);
            detector.transform.LookAt(target.transform);
            yield return new WaitForFixedUpdate();
            result = detector.CheckRaycast();
            Assert.IsTrue(result);
            Assert.IsFalse(onHitStartRan);
            Assert.IsFalse(onHitRan);
            Assert.IsFalse(onHitEndRan);
        }
        [UnityTest]
        public IEnumerator CheckRaycast_RangeOffset_Tests()
        {
            // 10.5 away
            target.transform.position += Vector3.forward * 10.5f;
            yield return new WaitForFixedUpdate();
            result = detector.CheckRaycast();
            Assert.IsTrue(result);
            Assert.IsFalse(onHitStartRan);
            Assert.IsFalse(onHitRan);
            Assert.IsFalse(onHitEndRan);

            // 10.6 away
            target.transform.position += Vector3.forward * .1f;
            yield return new WaitForFixedUpdate();
            result = detector.CheckRaycast();
            Assert.IsFalse(result);
            Assert.IsFalse(onHitStartRan);
            Assert.IsFalse(onHitRan);
            Assert.IsFalse(onHitEndRan);

            ReflectionUtility.SetInstanceField(typeof(RaycasterDetector), detector, "range", 20);
            // 20.5 away
            target.transform.position += Vector3.forward * 9.9f;
            yield return new WaitForFixedUpdate();
            result = detector.CheckRaycast();
            Assert.IsTrue(result);
            Assert.IsFalse(onHitStartRan);
            Assert.IsFalse(onHitRan);
            Assert.IsFalse(onHitEndRan);

            // 20.6 away
            target.transform.position += Vector3.forward * .1f;
            yield return new WaitForFixedUpdate();
            result = detector.CheckRaycast();
            Assert.IsFalse(result);
            Assert.IsFalse(onHitStartRan);
            Assert.IsFalse(onHitRan);
            Assert.IsFalse(onHitEndRan);

            ReflectionUtility.SetInstanceField(typeof(RaycasterDetector), detector, "startOffset", 10);
            // 10.4 away
            target.transform.position += Vector3.back * 10.2f;
            yield return new WaitForFixedUpdate();
            result = detector.CheckRaycast();
            Assert.IsFalse(result);
            Assert.IsFalse(onHitStartRan);
            Assert.IsFalse(onHitRan);
            Assert.IsFalse(onHitEndRan);

            // 10.5 away
            target.transform.position += Vector3.forward * .1f;
            yield return new WaitForFixedUpdate();
            result = detector.CheckRaycast();
            Assert.IsTrue(result);
            Assert.IsFalse(onHitStartRan);
            Assert.IsFalse(onHitRan);
            Assert.IsFalse(onHitEndRan);

            // 30.5 away
            target.transform.position += Vector3.forward * 20;
            yield return new WaitForFixedUpdate();
            result = detector.CheckRaycast();
            Assert.IsTrue(result);
            Assert.IsFalse(onHitStartRan);
            Assert.IsFalse(onHitRan);
            Assert.IsFalse(onHitEndRan);

            // 30.6 away
            target.transform.position += Vector3.forward * .1f;
            yield return new WaitForFixedUpdate();
            result = detector.CheckRaycast();
            Assert.IsFalse(result);
            Assert.IsFalse(onHitStartRan);
            Assert.IsFalse(onHitRan);
            Assert.IsFalse(onHitEndRan);
        }
        [UnityTest]
        public IEnumerator CheckRaycast_OutHit_Tests()
        {
            target.transform.position += Vector3.forward * 5;
            yield return new WaitForFixedUpdate();
            result = detector.CheckRaycast(out hitResult);
            Assert.IsTrue(result);
            Assert.IsNotNull(hitResult);
            Assert.AreEqual(target, hitResult.collider.transform);
            Assert.IsFalse(onHitStartRan);
            Assert.IsFalse(onHitRan);
            Assert.IsFalse(onHitEndRan);

            target.transform.position += new Vector3(5, 5, -10);
            detector.transform.LookAt(target.transform);
            yield return new WaitForFixedUpdate();
            result = detector.CheckRaycast(out hitResult);
            Assert.IsTrue(result);
            Assert.IsNotNull(hitResult);
            Assert.AreEqual(target, hitResult.collider.transform);
            Assert.IsFalse(onHitStartRan);
            Assert.IsFalse(onHitRan);
            Assert.IsFalse(onHitEndRan);
        }

        [UnityTest]
        public IEnumerator CheckRaycast_Auto_Tests()
        {
            target.transform.position += Vector3.forward * 5;
            detector.Auto = true;
            yield return new WaitForSeconds(.9f);
            Assert.IsFalse(onHitStartRan);
            yield return new WaitForSeconds(.2f);
            Assert.IsTrue(onHitStartRan);

            target.transform.position += Vector3.up * 5;
            yield return new WaitForSeconds(1);
            ResetFlags();
            target.transform.position += Vector3.down * 5;
            yield return new WaitForSeconds(.8f);
            Assert.IsFalse(onHitStartRan);
            yield return new WaitForSeconds(.3f);
            Assert.IsTrue(onHitStartRan);

            target.transform.position += Vector3.up * 5;
            yield return new WaitForSeconds(1);
            ResetFlags();
            target.transform.position += Vector3.down * 5;
            detector.Auto = false;
            yield return new WaitForSeconds(2);
            Assert.IsFalse(onHitStartRan);
        }

        [SetUp]
        public void Setup()
        {
            detector = new GameObject("Detector").AddComponent<RaycasterDetector>();
            detector.OnHitStart.AddListener(OnHitStart);
            detector.OnHit.AddListener(OnHit);
            detector.OnHitEnd.AddListener(OnHitEnd);

            target = GameObject.CreatePrimitive(PrimitiveType.Cube).transform;
            target.name = "Target";
        }
        [TearDown]
        public void Teardown()
        {
            if (detector) GameObject.Destroy(detector.gameObject);
            detector = null;
            if (target) GameObject.Destroy(target.gameObject);
            target = null;

            ResetFlags();
            result = false;
            hitResult = new RaycastHit();
        }

        private void ResetFlags()
        {
            onHitStartRan = false;
            onHitRan = false;
            onHitEndRan = false;
        }
        private void OnHitStart(RaycastHit hit)
        {
            onHitStartRan = true;
            Assert.IsNotNull(hit);
            Assert.AreEqual(target, hit.collider.transform);
        }
        private void OnHit(RaycastHit hit)
        {
            onHitRan = true;
            Assert.IsNotNull(hit);
            Assert.AreEqual(target, hit.collider.transform);
        }
        private void OnHitEnd(GameObject obj)
        {
            onHitEndRan = true;
            Assert.IsNotNull(obj);
            Assert.AreEqual(target, obj.transform);
        }
    }
}
