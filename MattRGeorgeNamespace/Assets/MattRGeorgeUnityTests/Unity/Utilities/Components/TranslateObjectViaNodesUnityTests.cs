﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using MattRGeorge.General.Utilities.Enums;
using MattRGeorge.General.Utilities.Static;
using MattRGeorge.Unity.Utilities.Components;
using MattRGeorge.Unity.Utilities.Components.Enums;

namespace MattRGeorge.UnityTests.Unity.Utilities.Components
{
    public class TranslateObjectViaNodesUnityTests
    {
        private TranslateObjectViaNodes translateObj = null;
        private List<Transform> nodes = new List<Transform>();
        private List<Transform> usedNodes = new List<Transform>();
        private Transform currUsedNode = null;
        private bool onStartRan = false;
        private bool onMovedRan = false;
        private bool onNodeReachedRan = false;
        private bool onEndRan = false;
        private float maxWait = 10;
        private float lastTime = 0;

        [UnityTest]
        public IEnumerator Moving_AllRandomly_Tests()
        {
            ReflectionUtility.SetInstanceField(typeof(TranslateObjectViaNodes), translateObj, "nodeChoiceMode", ListRandomChoiceMode.AllRandomly);
            ReflectionUtility.SetInstanceField(typeof(TranslateObjectViaNodes), translateObj, "endMode", EOLMode.Nothing);

            translateObj.StartMoving();
            currUsedNode = (Transform)ReflectionUtility.GetInstanceField(typeof(TranslateObjectViaNodes), translateObj, "nextNode");
            Assert.IsTrue(currUsedNode);
            Assert.IsFalse(usedNodes.Contains(currUsedNode));
            Assert.IsTrue(onStartRan);
            Assert.IsFalse(onMovedRan);
            Assert.IsFalse(onNodeReachedRan);
            Assert.IsFalse(onEndRan);
            usedNodes.Add(currUsedNode);
            lastTime = Time.time;

            ResetFlags();
            while (!onNodeReachedRan)
            {
                if (Time.time - lastTime >= maxWait) Assert.Fail("Took too long to reach node or `OnNodeReached()` never ran!");
                else yield return new WaitForSeconds(1);
            }
            yield return new WaitForSeconds(.1f);
            currUsedNode = (Transform)ReflectionUtility.GetInstanceField(typeof(TranslateObjectViaNodes), translateObj, "nextNode");
            Assert.IsTrue(currUsedNode);
            Assert.IsFalse(usedNodes.Contains(currUsedNode));
            Assert.IsFalse(onStartRan);
            Assert.IsTrue(onMovedRan);
            Assert.IsTrue(onNodeReachedRan);
            Assert.IsFalse(onEndRan);
            usedNodes.Add(currUsedNode);
            lastTime = Time.time;

            ResetFlags();
            while (!onNodeReachedRan)
            {
                if (Time.time - lastTime >= maxWait) Assert.Fail("Took too long to reach node or `OnNodeReached()` never ran!");
                else yield return new WaitForSeconds(1);
            }
            yield return new WaitForSeconds(.5f);
            currUsedNode = (Transform)ReflectionUtility.GetInstanceField(typeof(TranslateObjectViaNodes), translateObj, "nextNode");
            Assert.IsTrue(currUsedNode);
            Assert.IsFalse(usedNodes.Contains(currUsedNode));
            Assert.IsFalse(onStartRan);
            Assert.IsTrue(onMovedRan);
            Assert.IsTrue(onNodeReachedRan);
            Assert.IsTrue(onEndRan);
            usedNodes.Add(currUsedNode);
            lastTime = Time.time;

            ResetFlags();
            while (!onNodeReachedRan)
            {
                if (Time.time - lastTime >= maxWait) Assert.Fail("Took too long to reach node or `OnNodeReached()` never ran!");
                else yield return new WaitForSeconds(1);
            }
            yield return new WaitForSeconds(.1f);
            currUsedNode = (Transform)ReflectionUtility.GetInstanceField(typeof(TranslateObjectViaNodes), translateObj, "nextNode");
            Assert.IsTrue(currUsedNode);
            Assert.IsTrue(usedNodes.Contains(currUsedNode));
            Assert.IsFalse(onStartRan);
            Assert.IsTrue(onMovedRan);
            Assert.IsTrue(onNodeReachedRan);
            Assert.IsFalse(onEndRan);
        }
        [UnityTest]
        public IEnumerator Moving_AllSequentially_Tests()
        {
            ReflectionUtility.SetInstanceField(typeof(TranslateObjectViaNodes), translateObj, "nodeChoiceMode", ListRandomChoiceMode.AllSequentially);
            ReflectionUtility.SetInstanceField(typeof(TranslateObjectViaNodes), translateObj, "endMode", EOLMode.Nothing);

            translateObj.StartMoving();
            currUsedNode = (Transform)ReflectionUtility.GetInstanceField(typeof(TranslateObjectViaNodes), translateObj, "nextNode");
            Assert.IsTrue(currUsedNode);
            Assert.AreEqual(nodes[0], currUsedNode);
            Assert.IsTrue(onStartRan);
            Assert.IsFalse(onMovedRan);
            Assert.IsFalse(onNodeReachedRan);
            Assert.IsFalse(onEndRan);
            lastTime = Time.time;

            ResetFlags();
            while (!onNodeReachedRan)
            {
                if (Time.time - lastTime >= maxWait) Assert.Fail("Took too long to reach node or `OnNodeReached()` never ran!");
                else yield return new WaitForSeconds(1);
            }
            yield return new WaitForSeconds(.1f);
            currUsedNode = (Transform)ReflectionUtility.GetInstanceField(typeof(TranslateObjectViaNodes), translateObj, "nextNode");
            Assert.IsTrue(currUsedNode);
            Assert.AreEqual(nodes[1], currUsedNode);
            Assert.IsFalse(onStartRan);
            Assert.IsTrue(onMovedRan);
            Assert.IsTrue(onNodeReachedRan);
            Assert.IsFalse(onEndRan);
            lastTime = Time.time;

            ResetFlags();
            while (!onNodeReachedRan)
            {
                if (Time.time - lastTime >= maxWait) Assert.Fail("Took too long to reach node or `OnNodeReached()` never ran!");
                else yield return new WaitForSeconds(1);
            }
            yield return new WaitForSeconds(.1f);
            currUsedNode = (Transform)ReflectionUtility.GetInstanceField(typeof(TranslateObjectViaNodes), translateObj, "nextNode");
            Assert.IsTrue(currUsedNode);
            Assert.AreEqual(nodes[2], currUsedNode);
            Assert.IsFalse(onStartRan);
            Assert.IsTrue(onMovedRan);
            Assert.IsTrue(onNodeReachedRan);
            Assert.IsTrue(onEndRan);
            lastTime = Time.time;

            ResetFlags();
            while (!onNodeReachedRan)
            {
                if (Time.time - lastTime >= maxWait) Assert.Fail("Took too long to reach node or `OnNodeReached()` never ran!");
                else yield return new WaitForSeconds(1);
            }
            yield return new WaitForSeconds(.1f);
            currUsedNode = (Transform)ReflectionUtility.GetInstanceField(typeof(TranslateObjectViaNodes), translateObj, "nextNode");
            Assert.IsTrue(currUsedNode);
            Assert.AreEqual(nodes[0], currUsedNode);
            Assert.IsFalse(onStartRan);
            Assert.IsTrue(onMovedRan);
            Assert.IsTrue(onNodeReachedRan);
            Assert.IsFalse(onEndRan);
        }

        [SetUp]
        public void Setup()
        {
            translateObj = new GameObject("TransalateObject").AddComponent<TranslateObjectViaNodes>();
            translateObj.OnStart.AddListener(OnStart);
            translateObj.OnMoved.AddListener(OnMoved);
            translateObj.OnNodeReached.AddListener(OnNodeReached);
            translateObj.OnEnd.AddListener(OnEnd);

            nodes.Add(new GameObject("NodeA").transform);
            nodes[0].position += Vector3.forward * 5;
            nodes.Add(new GameObject("NodeB").transform);
            nodes[1].position += Vector3.right * 5;
            nodes.Add(new GameObject("NodeC").transform);
            nodes[2].position += new Vector3(0, 2, -2.5f);
            ReflectionUtility.SetInstanceField(typeof(TranslateObjectViaNodes), translateObj, "nodes", nodes);
        }
        [TearDown]
        public void Teardown()
        {
            if (translateObj) GameObject.Destroy(translateObj.gameObject);
            translateObj = null;

            if (nodes[0]) GameObject.Destroy(nodes[0].gameObject);
            if (nodes[1]) GameObject.Destroy(nodes[1].gameObject);
            if (nodes[2]) GameObject.Destroy(nodes[2].gameObject);
            nodes = new List<Transform>();

            ResetFlags();
            usedNodes = new List<Transform>();
            currUsedNode = null;
        }

        private void OnStart(GameObject obj)
        {
            onStartRan = true;

            Assert.IsTrue(obj);
            Assert.AreEqual(translateObj.gameObject, obj);
        }
        private void OnMoved(GameObject obj)
        {
            onMovedRan = true;

            Assert.IsTrue(obj);
            Assert.AreEqual(translateObj.gameObject, obj);
        }
        private void OnNodeReached(GameObject obj, Transform node)
        {
            onNodeReachedRan = true;

            Assert.IsTrue(obj);
            Assert.AreEqual(translateObj.gameObject, obj);
            Assert.IsTrue(node);
            Assert.IsTrue(nodes.Contains(node));
        }
        private void OnEnd(GameObject obj)
        {
            onEndRan = true;

            Assert.IsTrue(obj);
            Assert.AreEqual(translateObj.gameObject, obj);
        }
        private void ResetFlags()
        {
            onStartRan = false;
            onMovedRan = false;
            onNodeReachedRan = false;
            onEndRan = false;
        }
    }
}
