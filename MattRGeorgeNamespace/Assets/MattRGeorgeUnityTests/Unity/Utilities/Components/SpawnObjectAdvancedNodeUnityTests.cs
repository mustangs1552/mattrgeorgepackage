﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.TestTools;
using NUnit.Framework;
using MattRGeorge.General.Utilities.Enums;
using MattRGeorge.General.Utilities.Static;
using MattRGeorge.Unity.Tools.ObjectPooling;
using MattRGeorge.Unity.Utilities.Helpers;
using MattRGeorge.Unity.Utilities.Static;
using MattRGeorge.Unity.Utilities.Components;
using MattRGeorge.Unity.Utilities.Components.Helpers;

namespace MattRGeorge.UnityTests.Unity.Utilities.Components
{
    public class SpawnObjectAdvancedNodeUnityTests
    {
        private SpawnObjectAdvancedNode spawnObj = null;
        private GameObject objA = null;
        private GameObject objB = null;
        private string objAActualName = "ObjA";
        private string objBActualName = "ObjB";
        private SpawnRequest spawnRequest = new SpawnRequest();
        private ObjectPoolManager objPool = null;
        private bool onSpawnRan = false;
        private bool onPostSpawnRan = false;
        private bool requestOnSpawnRan = false;
        private bool requestOnPostSpawnRan = false;
        private bool requestOnCompletedRan = false;
        private bool currAsActive = false;
        private bool currUseObjectPool = false;
        private bool currIsObjectPoolManaged = false;
        private List<GameObject> spawnedObjs = new List<GameObject>();

        [UnityTest]
        public IEnumerator SpawnObj_Tests()
        {
            spawnObj.spawnAsActive = true;
            currAsActive = true;
            spawnObj.useObjectPool = false;
            currUseObjectPool = false;
            spawnObj.isObjectPoolManaged = false;
            currIsObjectPoolManaged = false;
            spawnObj.SpawnDelay = 0;
            spawnObj.PostSpawnDelay = 0;
            spawnObj.MaxCount = -1;
            spawnObj.ObjsWithMaxCounts = new List<DictionaryGameObjectCount>();
            spawnObj.destroyObjsOnMaxesChanged = false;
            spawnObj.destroyObjsSelectionMode = ListOneChoiceMode.Oldest;
            spawnObj.SpawnObj(objA);
            yield return new WaitForEndOfFrame();
            Assert.IsTrue(onSpawnRan);
            Assert.IsTrue(onPostSpawnRan);
            Assert.AreEqual(1, spawnObj.CurrSpawnedCount);
            Assert.IsTrue(spawnObj.CurrSpawnedObjs.ContainsKey(objAActualName));
            Assert.AreEqual(1, spawnObj.CurrSpawnedObjs[objAActualName].Count);
            Assert.IsFalse(spawnObj.CurrSpawnedObjs.ContainsKey(objBActualName));

            ResetFlags();
            spawnObj.spawnAsActive = false;
            currAsActive = false;
            spawnObj.SpawnDelay = 5;
            spawnObj.PostSpawnDelay = 3;
            spawnObj.SpawnObj(objA);
            yield return new WaitForSeconds(5.1f);
            Assert.IsTrue(onSpawnRan);
            Assert.IsFalse(onPostSpawnRan);
            Assert.AreEqual(2, spawnObj.CurrSpawnedCount);
            Assert.IsTrue(spawnObj.CurrSpawnedObjs.ContainsKey(objAActualName));
            Assert.AreEqual(2, spawnObj.CurrSpawnedObjs[objAActualName].Count);
            Assert.IsFalse(spawnObj.CurrSpawnedObjs.ContainsKey(objBActualName));
            yield return new WaitForSeconds(3);
            Assert.IsTrue(onSpawnRan);
            Assert.IsTrue(onPostSpawnRan);

            ResetFlags();
            spawnObj.spawnAsActive = false;
            currAsActive = false;
            spawnObj.SpawnDelay = 5;
            spawnObj.PostSpawnDelay = 3;
            spawnObj.SpawnObj(objB);
            yield return new WaitForSeconds(5.1f);
            Assert.IsTrue(onSpawnRan);
            Assert.IsFalse(onPostSpawnRan);
            Assert.AreEqual(3, spawnObj.CurrSpawnedCount);
            Assert.IsTrue(spawnObj.CurrSpawnedObjs.ContainsKey(objAActualName));
            Assert.AreEqual(2, spawnObj.CurrSpawnedObjs[objAActualName].Count);
            Assert.IsTrue(spawnObj.CurrSpawnedObjs.ContainsKey(objBActualName));
            Assert.AreEqual(1, spawnObj.CurrSpawnedObjs[objBActualName].Count);
            yield return new WaitForSeconds(3);
            Assert.IsTrue(onSpawnRan);
            Assert.IsTrue(onPostSpawnRan);

            ResetFlags();
            spawnObj.spawnAsActive = true;
            currAsActive = true;
            spawnObj.SpawnDelay = 3;
            spawnObj.PostSpawnDelay = 5;
            spawnObj.SpawnObj(objA);
            yield return new WaitForSeconds(3.1f);
            Assert.IsTrue(onSpawnRan);
            Assert.IsFalse(onPostSpawnRan);
            Assert.AreEqual(4, spawnObj.CurrSpawnedCount);
            Assert.IsTrue(spawnObj.CurrSpawnedObjs.ContainsKey(objAActualName));
            Assert.AreEqual(3, spawnObj.CurrSpawnedObjs[objAActualName].Count);
            Assert.IsTrue(spawnObj.CurrSpawnedObjs.ContainsKey(objBActualName));
            Assert.AreEqual(1, spawnObj.CurrSpawnedObjs[objBActualName].Count);
            yield return new WaitForSeconds(5);
            Assert.IsTrue(onSpawnRan);
            Assert.IsTrue(onPostSpawnRan);
        }
        [UnityTest]
        public IEnumerator SpawnObj_MaxCounts_Global_Tests()
        {
            spawnObj.spawnAsActive = true;
            currAsActive = true;
            spawnObj.useObjectPool = false;
            currUseObjectPool = false;
            spawnObj.isObjectPoolManaged = false;
            currIsObjectPoolManaged = false;
            spawnObj.SpawnDelay = 0;
            spawnObj.PostSpawnDelay = 0;
            spawnObj.MaxCount = 2;
            spawnObj.ObjsWithMaxCounts = new List<DictionaryGameObjectCount>();
            spawnObj.destroyObjsOnMaxesChanged = false;
            spawnObj.destroyObjsSelectionMode = ListOneChoiceMode.Oldest;

            // Initial
            spawnObj.SpawnObj(objA);
            yield return new WaitForEndOfFrame();
            Assert.IsTrue(onSpawnRan);
            Assert.IsTrue(onPostSpawnRan);
            Assert.AreEqual(1, spawnObj.CurrSpawnedCount);
            Assert.IsTrue(spawnObj.CurrSpawnedObjs.ContainsKey(objAActualName));
            Assert.AreEqual(1, spawnObj.CurrSpawnedObjs[objAActualName].Count);
            Assert.IsFalse(spawnObj.CurrSpawnedObjs.ContainsKey(objBActualName));
            ResetFlags();
            spawnObj.SpawnObj(objA);
            yield return new WaitForEndOfFrame();
            Assert.IsTrue(onSpawnRan);
            Assert.IsTrue(onPostSpawnRan);
            Assert.AreEqual(2, spawnObj.CurrSpawnedCount);
            Assert.IsTrue(spawnObj.CurrSpawnedObjs.ContainsKey(objAActualName));
            Assert.AreEqual(2, spawnObj.CurrSpawnedObjs[objAActualName].Count);
            Assert.IsFalse(spawnObj.CurrSpawnedObjs.ContainsKey(objBActualName));
            ResetFlags();
            spawnObj.SpawnObj(objA);
            yield return new WaitForEndOfFrame();
            Assert.IsFalse(onSpawnRan);
            Assert.IsFalse(onPostSpawnRan);
            Assert.AreEqual(2, spawnObj.CurrSpawnedCount);
            Assert.IsTrue(spawnObj.CurrSpawnedObjs.ContainsKey(objAActualName));
            Assert.AreEqual(2, spawnObj.CurrSpawnedObjs[objAActualName].Count);
            Assert.IsFalse(spawnObj.CurrSpawnedObjs.ContainsKey(objBActualName));

            // Max increased
            spawnObj.MaxCount = 4;
            ResetFlags();
            spawnObj.SpawnObj(objB);
            yield return new WaitForEndOfFrame();
            Assert.IsTrue(onSpawnRan);
            Assert.IsTrue(onPostSpawnRan);
            Assert.AreEqual(3, spawnObj.CurrSpawnedCount);
            Assert.IsTrue(spawnObj.CurrSpawnedObjs.ContainsKey(objAActualName));
            Assert.AreEqual(2, spawnObj.CurrSpawnedObjs[objAActualName].Count);
            Assert.IsTrue(spawnObj.CurrSpawnedObjs.ContainsKey(objBActualName));
            Assert.AreEqual(1, spawnObj.CurrSpawnedObjs[objBActualName].Count);
            ResetFlags();
            spawnObj.SpawnObj(objB);
            yield return new WaitForEndOfFrame();
            Assert.IsTrue(onSpawnRan);
            Assert.IsTrue(onPostSpawnRan);
            Assert.AreEqual(4, spawnObj.CurrSpawnedCount);
            Assert.IsTrue(spawnObj.CurrSpawnedObjs.ContainsKey(objAActualName));
            Assert.AreEqual(2, spawnObj.CurrSpawnedObjs[objAActualName].Count);
            Assert.IsTrue(spawnObj.CurrSpawnedObjs.ContainsKey(objBActualName));
            Assert.AreEqual(2, spawnObj.CurrSpawnedObjs[objBActualName].Count);
            ResetFlags();
            spawnObj.SpawnObj(objB);
            yield return new WaitForEndOfFrame();
            Assert.IsFalse(onSpawnRan);
            Assert.IsFalse(onPostSpawnRan);
            Assert.AreEqual(4, spawnObj.CurrSpawnedCount);
            Assert.IsTrue(spawnObj.CurrSpawnedObjs.ContainsKey(objAActualName));
            Assert.AreEqual(2, spawnObj.CurrSpawnedObjs[objAActualName].Count);
            Assert.IsTrue(spawnObj.CurrSpawnedObjs.ContainsKey(objBActualName));
            Assert.AreEqual(2, spawnObj.CurrSpawnedObjs[objBActualName].Count);

            // Max decreased
            spawnObj.MaxCount = 2;
            ResetFlags();
            spawnObj.SpawnObj(objA);
            yield return new WaitForEndOfFrame();
            Assert.IsFalse(onSpawnRan);
            Assert.IsFalse(onPostSpawnRan);
            Assert.AreEqual(4, spawnObj.CurrSpawnedCount);
            Assert.IsTrue(spawnObj.CurrSpawnedObjs.ContainsKey(objAActualName));
            Assert.AreEqual(2, spawnObj.CurrSpawnedObjs[objAActualName].Count);
            Assert.IsTrue(spawnObj.CurrSpawnedObjs.ContainsKey(objBActualName));
            Assert.AreEqual(2, spawnObj.CurrSpawnedObjs[objBActualName].Count);
            ResetFlags();
            spawnObj.SpawnObj(objB);
            yield return new WaitForEndOfFrame();
            Assert.IsFalse(onSpawnRan);
            Assert.IsFalse(onPostSpawnRan);
            Assert.AreEqual(4, spawnObj.CurrSpawnedCount);
            Assert.IsTrue(spawnObj.CurrSpawnedObjs.ContainsKey(objAActualName));
            Assert.AreEqual(2, spawnObj.CurrSpawnedObjs[objAActualName].Count);
            Assert.IsTrue(spawnObj.CurrSpawnedObjs.ContainsKey(objBActualName));
            Assert.AreEqual(2, spawnObj.CurrSpawnedObjs[objBActualName].Count);

            // Object added before scheduled spawn, objects now at limit before scheduled spawn finishes
            spawnObj.SpawnDelay = 1;
            spawnObj.MaxCount = 5;
            ResetFlags();
            spawnObj.SpawnObj(objA);
            Dictionary<string, List<GameObject>> spawnedObjsDict = (Dictionary<string, List<GameObject>>)ReflectionUtility.GetInstanceField(typeof(SpawnObjectAdvancedNode), spawnObj, "currSpawnedObjs");
            spawnedObjsDict["ObjA"].Add(GameObject.Instantiate(objA));
            spawnedObjs.Add(spawnedObjsDict["ObjA"][spawnedObjsDict["ObjA"].Count - 1]);
            ReflectionUtility.SetInstanceField(typeof(SpawnObjectAdvancedNode), spawnObj, "currSpawnedObjs", spawnedObjsDict);
            yield return new WaitForSeconds(1.25f);
            Assert.IsFalse(onSpawnRan);
            Assert.IsFalse(onPostSpawnRan);
            Assert.AreEqual(5, spawnObj.CurrSpawnedCount);
            Assert.IsTrue(spawnObj.CurrSpawnedObjs.ContainsKey(objAActualName));
            Assert.AreEqual(3, spawnObj.CurrSpawnedObjs[objAActualName].Count);
            Assert.IsTrue(spawnObj.CurrSpawnedObjs.ContainsKey(objBActualName));
            Assert.AreEqual(2, spawnObj.CurrSpawnedObjs[objBActualName].Count);

            // No max
            spawnObj.SpawnDelay = 0;
            spawnObj.MaxCount = -1;
            ResetFlags();
            spawnObj.SpawnObj(objA);
            yield return new WaitForEndOfFrame();
            Assert.IsTrue(onSpawnRan);
            Assert.IsTrue(onPostSpawnRan);
            Assert.AreEqual(6, spawnObj.CurrSpawnedCount);
            Assert.IsTrue(spawnObj.CurrSpawnedObjs.ContainsKey(objAActualName));
            Assert.AreEqual(4, spawnObj.CurrSpawnedObjs[objAActualName].Count);
            Assert.IsTrue(spawnObj.CurrSpawnedObjs.ContainsKey(objBActualName));
            Assert.AreEqual(2, spawnObj.CurrSpawnedObjs[objBActualName].Count);
            ResetFlags();
            spawnObj.SpawnObj(objB);
            yield return new WaitForEndOfFrame();
            Assert.IsTrue(onSpawnRan);
            Assert.IsTrue(onPostSpawnRan);
            Assert.AreEqual(7, spawnObj.CurrSpawnedCount);
            Assert.IsTrue(spawnObj.CurrSpawnedObjs.ContainsKey(objAActualName));
            Assert.AreEqual(4, spawnObj.CurrSpawnedObjs[objAActualName].Count);
            Assert.IsTrue(spawnObj.CurrSpawnedObjs.ContainsKey(objBActualName));
            Assert.AreEqual(3, spawnObj.CurrSpawnedObjs[objBActualName].Count);
        }
        [UnityTest]
        public IEnumerator SpawnObj_MaxCounts_ByObject_Tests()
        {
            spawnObj.spawnAsActive = true;
            currAsActive = true;
            spawnObj.useObjectPool = false;
            currUseObjectPool = false;
            spawnObj.isObjectPoolManaged = false;
            currIsObjectPoolManaged = false;
            spawnObj.SpawnDelay = 0;
            spawnObj.PostSpawnDelay = 0;
            spawnObj.MaxCount = 6;
            spawnObj.ObjsWithMaxCounts = new List<DictionaryGameObjectCount>()
            {
                new DictionaryGameObjectCount() { obj = objA, count = 2 }
            };
            spawnObj.destroyObjsOnMaxesChanged = false;
            spawnObj.destroyObjsSelectionMode = ListOneChoiceMode.Oldest;

            // ObjA hits max, ObjB unlimited
            spawnObj.SpawnObj(objA);
            yield return new WaitForEndOfFrame();
            Assert.IsTrue(onSpawnRan);
            Assert.IsTrue(onPostSpawnRan);
            Assert.AreEqual(1, spawnObj.CurrSpawnedCount);
            Assert.IsTrue(spawnObj.CurrSpawnedObjs.ContainsKey(objAActualName));
            Assert.AreEqual(1, spawnObj.CurrSpawnedObjs[objAActualName].Count);
            Assert.IsFalse(spawnObj.CurrSpawnedObjs.ContainsKey(objBActualName));
            ResetFlags();
            spawnObj.SpawnObj(objA);
            yield return new WaitForEndOfFrame();
            Assert.IsTrue(onSpawnRan);
            Assert.IsTrue(onPostSpawnRan);
            Assert.AreEqual(2, spawnObj.CurrSpawnedCount);
            Assert.IsTrue(spawnObj.CurrSpawnedObjs.ContainsKey(objAActualName));
            Assert.AreEqual(2, spawnObj.CurrSpawnedObjs[objAActualName].Count);
            Assert.IsFalse(spawnObj.CurrSpawnedObjs.ContainsKey(objBActualName));
            ResetFlags();
            spawnObj.SpawnObj(objA);
            yield return new WaitForEndOfFrame();
            Assert.IsFalse(onSpawnRan);
            Assert.IsFalse(onPostSpawnRan);
            Assert.AreEqual(2, spawnObj.CurrSpawnedCount);
            Assert.IsTrue(spawnObj.CurrSpawnedObjs.ContainsKey(objAActualName));
            Assert.AreEqual(2, spawnObj.CurrSpawnedObjs[objAActualName].Count);
            Assert.IsFalse(spawnObj.CurrSpawnedObjs.ContainsKey(objBActualName));
            ResetFlags();
            spawnObj.SpawnObj(objB);
            yield return new WaitForEndOfFrame();
            Assert.IsTrue(onSpawnRan);
            Assert.IsTrue(onPostSpawnRan);
            Assert.AreEqual(3, spawnObj.CurrSpawnedCount);
            Assert.IsTrue(spawnObj.CurrSpawnedObjs.ContainsKey(objAActualName));
            Assert.AreEqual(2, spawnObj.CurrSpawnedObjs[objAActualName].Count);
            Assert.IsTrue(spawnObj.CurrSpawnedObjs.ContainsKey(objBActualName));
            Assert.AreEqual(1, spawnObj.CurrSpawnedObjs[objBActualName].Count);
            ResetFlags();
            spawnObj.SpawnObj(objB);
            yield return new WaitForEndOfFrame();
            Assert.IsTrue(onSpawnRan);
            Assert.IsTrue(onPostSpawnRan);
            Assert.AreEqual(4, spawnObj.CurrSpawnedCount);
            Assert.IsTrue(spawnObj.CurrSpawnedObjs.ContainsKey(objAActualName));
            Assert.AreEqual(2, spawnObj.CurrSpawnedObjs[objAActualName].Count);
            Assert.IsTrue(spawnObj.CurrSpawnedObjs.ContainsKey(objBActualName));
            Assert.AreEqual(2, spawnObj.CurrSpawnedObjs[objBActualName].Count);
            ResetFlags();
            spawnObj.SpawnObj(objB);
            yield return new WaitForEndOfFrame();
            Assert.IsTrue(onSpawnRan);
            Assert.IsTrue(onPostSpawnRan);
            Assert.AreEqual(5, spawnObj.CurrSpawnedCount);
            Assert.IsTrue(spawnObj.CurrSpawnedObjs.ContainsKey(objAActualName));
            Assert.AreEqual(2, spawnObj.CurrSpawnedObjs[objAActualName].Count);
            Assert.IsTrue(spawnObj.CurrSpawnedObjs.ContainsKey(objBActualName));
            Assert.AreEqual(3, spawnObj.CurrSpawnedObjs[objBActualName].Count);

            // ObjB hits max
            spawnObj.ObjsWithMaxCounts = new List<DictionaryGameObjectCount>()
            {
                new DictionaryGameObjectCount() { obj = objA, count = 2 },
                new DictionaryGameObjectCount() { obj = objB, count = 4 }
            };
            ResetFlags();
            spawnObj.SpawnObj(objB);
            yield return new WaitForEndOfFrame();
            Assert.IsTrue(onSpawnRan);
            Assert.IsTrue(onPostSpawnRan);
            Assert.AreEqual(6, spawnObj.CurrSpawnedCount);
            Assert.IsTrue(spawnObj.CurrSpawnedObjs.ContainsKey(objAActualName));
            Assert.AreEqual(2, spawnObj.CurrSpawnedObjs[objAActualName].Count);
            Assert.IsTrue(spawnObj.CurrSpawnedObjs.ContainsKey(objBActualName));
            Assert.AreEqual(4, spawnObj.CurrSpawnedObjs[objBActualName].Count);
            ResetFlags();
            spawnObj.SpawnObj(objB);
            yield return new WaitForEndOfFrame();
            Assert.IsFalse(onSpawnRan);
            Assert.IsFalse(onPostSpawnRan);
            Assert.AreEqual(6, spawnObj.CurrSpawnedCount);
            Assert.IsTrue(spawnObj.CurrSpawnedObjs.ContainsKey(objAActualName));
            Assert.AreEqual(2, spawnObj.CurrSpawnedObjs[objAActualName].Count);
            Assert.IsTrue(spawnObj.CurrSpawnedObjs.ContainsKey(objBActualName));
            Assert.AreEqual(4, spawnObj.CurrSpawnedObjs[objBActualName].Count);
            ResetFlags();

            // At global max
            spawnObj.SpawnObj(objA);
            yield return new WaitForEndOfFrame();
            Assert.IsFalse(onSpawnRan);
            Assert.IsFalse(onPostSpawnRan);
            Assert.AreEqual(6, spawnObj.CurrSpawnedCount);
            Assert.IsTrue(spawnObj.CurrSpawnedObjs.ContainsKey(objAActualName));
            Assert.AreEqual(2, spawnObj.CurrSpawnedObjs[objAActualName].Count);
            Assert.IsTrue(spawnObj.CurrSpawnedObjs.ContainsKey(objBActualName));
            Assert.AreEqual(4, spawnObj.CurrSpawnedObjs[objBActualName].Count);
            ResetFlags();
            spawnObj.SpawnObj(objB);
            yield return new WaitForEndOfFrame();
            Assert.IsFalse(onSpawnRan);
            Assert.IsFalse(onPostSpawnRan);
            Assert.AreEqual(6, spawnObj.CurrSpawnedCount);
            Assert.IsTrue(spawnObj.CurrSpawnedObjs.ContainsKey(objAActualName));
            Assert.AreEqual(2, spawnObj.CurrSpawnedObjs[objAActualName].Count);
            Assert.IsTrue(spawnObj.CurrSpawnedObjs.ContainsKey(objBActualName));
            Assert.AreEqual(4, spawnObj.CurrSpawnedObjs[objBActualName].Count);

            // Object added before scheduled spawn, objects now at limit before scheduled spawn finishes
            spawnObj.SpawnDelay = 1;
            spawnObj.MaxCount = -1;
            spawnObj.ObjsWithMaxCounts = new List<DictionaryGameObjectCount>()
            {
                new DictionaryGameObjectCount() { obj = objA, count = 3 },
                new DictionaryGameObjectCount() { obj = objB, count = 4 }
            };
            spawnObj.SpawnObj(objA);
            Dictionary<string, List<GameObject>> spawnedObjsDict = (Dictionary<string, List<GameObject>>)ReflectionUtility.GetInstanceField(typeof(SpawnObjectAdvancedNode), spawnObj, "currSpawnedObjs");
            spawnedObjsDict["ObjA"].Add(GameObject.Instantiate(objA));
            spawnedObjs.Add(spawnedObjsDict["ObjA"][spawnedObjsDict["ObjA"].Count - 1]);
            ReflectionUtility.SetInstanceField(typeof(SpawnObjectAdvancedNode), spawnObj, "currSpawnedObjs", spawnedObjsDict);
            yield return new WaitForSeconds(1.25f);
            Assert.IsFalse(onSpawnRan);
            Assert.IsFalse(onPostSpawnRan);
            Assert.AreEqual(7, spawnObj.CurrSpawnedCount);
            Assert.IsTrue(spawnObj.CurrSpawnedObjs.ContainsKey(objAActualName));
            Assert.AreEqual(3, spawnObj.CurrSpawnedObjs[objAActualName].Count);
            Assert.IsTrue(spawnObj.CurrSpawnedObjs.ContainsKey(objBActualName));
            Assert.AreEqual(4, spawnObj.CurrSpawnedObjs[objBActualName].Count);
        }
        [UnityTest]
        public IEnumerator SpawnObj_InvalidInput_Tests()
        {
            spawnObj.spawnAsActive = true;
            currAsActive = true;
            spawnObj.useObjectPool = false;
            currUseObjectPool = false;
            spawnObj.isObjectPoolManaged = false;
            currIsObjectPoolManaged = false;
            spawnObj.SpawnDelay = 0;
            spawnObj.PostSpawnDelay = 0;
            spawnObj.MaxCount = -1;
            spawnObj.ObjsWithMaxCounts = new List<DictionaryGameObjectCount>();
            spawnObj.destroyObjsOnMaxesChanged = false;
            spawnObj.destroyObjsSelectionMode = ListOneChoiceMode.Oldest;
            spawnObj.SpawnObj(null);
            yield return new WaitForEndOfFrame();
            Assert.IsFalse(onSpawnRan);
            Assert.IsFalse(onPostSpawnRan);

            ResetFlags();
            spawnObj.SpawnDelay = 5;
            spawnObj.PostSpawnDelay = 3;
            spawnObj.SpawnObj(null);
            yield return new WaitForSeconds(5.1f);
            Assert.IsFalse(onSpawnRan);
            Assert.IsFalse(onPostSpawnRan);
            yield return new WaitForSeconds(3);
            Assert.IsFalse(onSpawnRan);
            Assert.IsFalse(onPostSpawnRan);
        }
        [UnityTest]
        public IEnumerator SpawnObj_ObjPool_Tests()
        {
            spawnObj.spawnAsActive = true;
            currAsActive = true;
            spawnObj.useObjectPool = true;
            currUseObjectPool = true;
            spawnObj.isObjectPoolManaged = false;
            currIsObjectPoolManaged = false;
            spawnObj.SpawnDelay = 0;
            spawnObj.PostSpawnDelay = 0;
            spawnObj.MaxCount = -1;
            spawnObj.ObjsWithMaxCounts = new List<DictionaryGameObjectCount>();
            spawnObj.destroyObjsOnMaxesChanged = false;
            spawnObj.destroyObjsSelectionMode = ListOneChoiceMode.Oldest;
            spawnObj.SpawnObj(objA);
            yield return new WaitForEndOfFrame();
            Assert.IsTrue(onSpawnRan);
            Assert.IsTrue(onPostSpawnRan);

            ResetFlags();
            spawnObj.spawnAsActive = false;
            currAsActive = false;
            spawnObj.SpawnDelay = 5;
            spawnObj.PostSpawnDelay = 3;
            spawnObj.SpawnObj(objA);
            yield return new WaitForSeconds(5.1f);
            Assert.IsTrue(onSpawnRan);
            Assert.IsFalse(onPostSpawnRan);
            yield return new WaitForSeconds(3);
            Assert.IsTrue(onSpawnRan);
            Assert.IsTrue(onPostSpawnRan);

            ResetFlags();
            spawnObj.spawnAsActive = true;
            currAsActive = true;
            spawnObj.isObjectPoolManaged = true;
            currIsObjectPoolManaged = true;
            spawnObj.SpawnDelay = 3;
            spawnObj.PostSpawnDelay = 5;
            spawnObj.SpawnObj(objA);
            yield return new WaitForSeconds(3.1f);
            Assert.IsTrue(onSpawnRan);
            Assert.IsFalse(onPostSpawnRan);
            yield return new WaitForSeconds(5);
            Assert.IsTrue(onSpawnRan);
            Assert.IsTrue(onPostSpawnRan);

            ResetFlags();
            spawnObj.spawnAsActive = false;
            currAsActive = false;
            spawnObj.isObjectPoolManaged = false;
            currIsObjectPoolManaged = false;
            spawnObj.SpawnDelay = 0;
            spawnObj.PostSpawnDelay = 0;
            spawnObj.SpawnObj(objA);
            yield return new WaitForEndOfFrame();
            Assert.IsTrue(onSpawnRan);
            Assert.IsTrue(onPostSpawnRan);
        }
        [UnityTest]
        public IEnumerator SpawnObj_ObjPool_InvalidInput_Tests()
        {
            spawnObj.spawnAsActive = true;
            currAsActive = true;
            spawnObj.useObjectPool = true;
            currUseObjectPool = true;
            spawnObj.isObjectPoolManaged = false;
            currIsObjectPoolManaged = false;
            spawnObj.SpawnDelay = 0;
            spawnObj.PostSpawnDelay = 0;
            spawnObj.MaxCount = -1;
            spawnObj.ObjsWithMaxCounts = new List<DictionaryGameObjectCount>();
            spawnObj.destroyObjsOnMaxesChanged = false;
            spawnObj.destroyObjsSelectionMode = ListOneChoiceMode.Oldest;
            spawnObj.SpawnObj(null);
            yield return new WaitForEndOfFrame();
            Assert.IsFalse(onSpawnRan);
            Assert.IsFalse(onPostSpawnRan);

            ResetFlags();
            spawnObj.isObjectPoolManaged = true;
            currIsObjectPoolManaged = true;
            spawnObj.SpawnDelay = 5;
            spawnObj.PostSpawnDelay = 3;
            spawnObj.SpawnObj(null);
            yield return new WaitForSeconds(5.1f);
            Assert.IsFalse(onSpawnRan);
            Assert.IsFalse(onPostSpawnRan);
            yield return new WaitForSeconds(3);
            Assert.IsFalse(onSpawnRan);
            Assert.IsFalse(onPostSpawnRan);

            ResetFlags();
            GameObject.DestroyImmediate(objPool.gameObject);
            objPool = null;
            spawnObj.SpawnDelay = 5;
            spawnObj.PostSpawnDelay = 3;
            spawnObj.SpawnObj(objA);
            yield return new WaitForSeconds(5.1f);
            Assert.IsFalse(onSpawnRan);
            Assert.IsFalse(onPostSpawnRan);
            yield return new WaitForSeconds(3);
            Assert.IsFalse(onSpawnRan);
            Assert.IsFalse(onPostSpawnRan);

            ResetFlags();
            spawnObj.isObjectPoolManaged = false;
            currIsObjectPoolManaged = false;
            spawnObj.SpawnDelay = 5;
            spawnObj.PostSpawnDelay = 3;
            spawnObj.SpawnObj(objA);
            yield return new WaitForSeconds(5.1f);
            Assert.IsFalse(onSpawnRan);
            Assert.IsFalse(onPostSpawnRan);
            yield return new WaitForSeconds(3);
            Assert.IsFalse(onSpawnRan);
            Assert.IsFalse(onPostSpawnRan);
        }
        [UnityTest]
        public IEnumerator SpawnObj_SpawnRequest_Tests()
        {
            spawnObj.spawnAsActive = true;
            currAsActive = true;
            spawnObj.useObjectPool = false;
            currUseObjectPool = false;
            spawnObj.isObjectPoolManaged = false;
            currIsObjectPoolManaged = false;
            spawnObj.SpawnDelay = 0;
            spawnObj.PostSpawnDelay = 0;
            spawnObj.MaxCount = -1;
            spawnObj.ObjsWithMaxCounts = new List<DictionaryGameObjectCount>();
            spawnObj.destroyObjsOnMaxesChanged = false;
            spawnObj.destroyObjsSelectionMode = ListOneChoiceMode.Oldest;
            spawnRequest.OnSpawnCallback += RequestOnSpawn;
            spawnRequest.OnPostSpawnCallback += RequestOnPostSpawn;
            spawnRequest.OnCompletedCallback += RequestOnCompleted;
            spawnObj.SpawnObj(objA, ref spawnRequest);
            yield return new WaitForEndOfFrame();
            Assert.IsTrue(spawnRequest.spawned);
            Assert.IsTrue(spawnRequest.postSpawned);
            Assert.IsFalse(spawnRequest.failed);
            Assert.IsTrue(spawnRequest.completed);
            Assert.IsTrue(onSpawnRan);
            Assert.IsTrue(onPostSpawnRan);
            Assert.IsTrue(requestOnSpawnRan);
            Assert.IsTrue(requestOnPostSpawnRan);
            Assert.IsTrue(requestOnCompletedRan);

            ResetFlags();
            spawnObj.spawnAsActive = false;
            currAsActive = false;
            spawnObj.SpawnDelay = 5;
            spawnObj.PostSpawnDelay = 3;
            spawnRequest = new SpawnRequest();
            spawnRequest.OnSpawnCallback += RequestOnSpawn;
            spawnRequest.OnPostSpawnCallback += RequestOnPostSpawn;
            spawnRequest.OnCompletedCallback += RequestOnCompleted;
            spawnObj.SpawnObj(objA, ref spawnRequest);
            yield return new WaitForSeconds(5.1f);
            Assert.IsTrue(spawnRequest.spawned);
            Assert.IsFalse(spawnRequest.postSpawned);
            Assert.IsFalse(spawnRequest.failed);
            Assert.IsFalse(spawnRequest.completed);
            Assert.IsTrue(onSpawnRan);
            Assert.IsTrue(spawnRequest.spawned);
            Assert.IsFalse(onPostSpawnRan);
            Assert.IsFalse(spawnRequest.postSpawned);
            Assert.IsTrue(requestOnSpawnRan);
            Assert.IsFalse(requestOnPostSpawnRan);
            Assert.IsFalse(requestOnCompletedRan);
            yield return new WaitForSeconds(3);
            Assert.IsTrue(spawnRequest.spawned);
            Assert.IsTrue(spawnRequest.postSpawned);
            Assert.IsFalse(spawnRequest.failed);
            Assert.IsTrue(spawnRequest.completed);
            Assert.IsTrue(onSpawnRan);
            Assert.IsTrue(onPostSpawnRan);
            Assert.IsTrue(requestOnSpawnRan);
            Assert.IsTrue(requestOnPostSpawnRan);
            Assert.IsTrue(requestOnCompletedRan);

            ResetFlags();
            spawnObj.spawnAsActive = true;
            currAsActive = true;
            spawnObj.SpawnDelay = 3;
            spawnObj.PostSpawnDelay = 5;
            spawnRequest = new SpawnRequest();
            spawnRequest.OnSpawnCallback += RequestOnSpawn;
            spawnRequest.OnPostSpawnCallback += RequestOnPostSpawn;
            spawnRequest.OnCompletedCallback += RequestOnCompleted;
            spawnObj.SpawnObj(objA, ref spawnRequest);
            yield return new WaitForSeconds(3.1f);
            Assert.IsTrue(spawnRequest.spawned);
            Assert.IsFalse(spawnRequest.postSpawned);
            Assert.IsFalse(spawnRequest.failed);
            Assert.IsFalse(spawnRequest.completed);
            Assert.IsTrue(onSpawnRan);
            Assert.IsFalse(onPostSpawnRan);
            Assert.IsTrue(requestOnSpawnRan);
            Assert.IsFalse(requestOnPostSpawnRan);
            Assert.IsFalse(requestOnCompletedRan);
            yield return new WaitForSeconds(5);
            Assert.IsTrue(spawnRequest.spawned);
            Assert.IsTrue(spawnRequest.postSpawned);
            Assert.IsFalse(spawnRequest.failed);
            Assert.IsTrue(spawnRequest.completed);
            Assert.IsTrue(onSpawnRan);
            Assert.IsTrue(onPostSpawnRan);
            Assert.IsTrue(requestOnSpawnRan);
            Assert.IsTrue(requestOnPostSpawnRan);
            Assert.IsTrue(requestOnCompletedRan);

            ResetFlags();
            spawnObj.SpawnDelay = 1;
            spawnObj.PostSpawnDelay = 3;
            spawnRequest = new SpawnRequest();
            spawnRequest.OnSpawnCallback += RequestOnSpawn;
            spawnRequest.OnPostSpawnCallback += RequestOnPostSpawn;
            spawnRequest.OnCompletedCallback += RequestOnCompleted;
            spawnObj.SpawnObj(objA, ref spawnRequest);
            yield return new WaitForSeconds(1.1f);
            Assert.IsTrue(spawnRequest.spawned);
            Assert.IsFalse(spawnRequest.postSpawned);
            Assert.IsFalse(spawnRequest.failed);
            Assert.IsFalse(spawnRequest.completed);
            Assert.IsTrue(onSpawnRan);
            Assert.IsFalse(onPostSpawnRan);
            Assert.IsTrue(requestOnSpawnRan);
            Assert.IsFalse(requestOnPostSpawnRan);
            Assert.IsFalse(requestOnCompletedRan);
            spawnObj.StopCoroutine(spawnRequest.calledCoroutine);
            yield return new WaitForSeconds(4);
            Assert.IsTrue(spawnRequest.spawned);
            Assert.IsFalse(spawnRequest.postSpawned);
            Assert.IsFalse(spawnRequest.failed);
            Assert.IsFalse(spawnRequest.completed);
            Assert.IsTrue(onSpawnRan);
            Assert.IsFalse(onPostSpawnRan);
            Assert.IsTrue(requestOnSpawnRan);
            Assert.IsFalse(requestOnPostSpawnRan);
            Assert.IsFalse(requestOnCompletedRan);

            ResetFlags();
            spawnRequest = new SpawnRequest();
            spawnRequest.OnSpawnCallback += RequestOnSpawn;
            spawnRequest.OnPostSpawnCallback += RequestOnPostSpawn;
            spawnRequest.OnCompletedCallback += RequestOnCompleted;
            spawnObj.SpawnObj(objA, ref spawnRequest);
            spawnObj.StopCoroutine(spawnRequest.calledCoroutine);
            yield return new WaitForSeconds(1.1f);
            Assert.IsFalse(spawnRequest.spawned);
            Assert.IsFalse(spawnRequest.postSpawned);
            Assert.IsFalse(spawnRequest.failed);
            Assert.IsFalse(spawnRequest.completed);
            Assert.IsFalse(onSpawnRan);
            Assert.IsFalse(onPostSpawnRan);
            Assert.IsFalse(requestOnSpawnRan);
            Assert.IsFalse(requestOnPostSpawnRan);
            Assert.IsFalse(requestOnCompletedRan);
            yield return new WaitForSeconds(4);
            Assert.IsFalse(spawnRequest.spawned);
            Assert.IsFalse(spawnRequest.postSpawned);
            Assert.IsFalse(spawnRequest.failed);
            Assert.IsFalse(spawnRequest.completed);
            Assert.IsFalse(onSpawnRan);
            Assert.IsFalse(onPostSpawnRan);
            Assert.IsFalse(requestOnSpawnRan);
            Assert.IsFalse(requestOnPostSpawnRan);
            Assert.IsFalse(requestOnCompletedRan);
        }
        [UnityTest]
        public IEnumerator SpawnObj_SpawnRequest_InvalidInput_Tests()
        {
            spawnObj.spawnAsActive = true;
            currAsActive = true;
            spawnObj.useObjectPool = false;
            currUseObjectPool = false;
            spawnObj.isObjectPoolManaged = false;
            currIsObjectPoolManaged = false;
            spawnObj.SpawnDelay = 0;
            spawnObj.PostSpawnDelay = 0;
            spawnObj.MaxCount = -1;
            spawnObj.ObjsWithMaxCounts = new List<DictionaryGameObjectCount>();
            spawnObj.destroyObjsOnMaxesChanged = false;
            spawnObj.destroyObjsSelectionMode = ListOneChoiceMode.Oldest;
            spawnRequest.OnSpawnCallback += RequestOnSpawn;
            spawnRequest.OnPostSpawnCallback += RequestOnPostSpawn;
            spawnRequest.OnCompletedCallback += RequestFailedOnCompleted;
            spawnObj.SpawnObj(null, ref spawnRequest);
            yield return new WaitForEndOfFrame();
            Assert.IsFalse(onSpawnRan);
            Assert.IsFalse(onPostSpawnRan);
            Assert.IsFalse(requestOnSpawnRan);
            Assert.IsFalse(requestOnPostSpawnRan);
            Assert.IsTrue(requestOnCompletedRan);

            ResetFlags();
            spawnRequest = new SpawnRequest();
            spawnRequest.OnSpawnCallback += RequestOnSpawn;
            spawnRequest.OnPostSpawnCallback += RequestOnPostSpawn;
            spawnRequest.OnCompletedCallback += RequestFailedOnCompleted;
            spawnObj.SpawnObj(null, ref spawnRequest);
            yield return new WaitForEndOfFrame();
            Assert.IsFalse(onSpawnRan);
            Assert.IsFalse(onPostSpawnRan);
            Assert.IsFalse(requestOnSpawnRan);
            Assert.IsFalse(requestOnPostSpawnRan);
            Assert.IsTrue(requestOnCompletedRan);

            ResetFlags();
            spawnRequest = null;
            spawnObj.SpawnObj(null, ref spawnRequest);
            yield return new WaitForEndOfFrame();
            Assert.IsFalse(onSpawnRan);
            Assert.IsFalse(onPostSpawnRan);
            Assert.IsFalse(requestOnSpawnRan);
            Assert.IsFalse(requestOnPostSpawnRan);
            Assert.IsFalse(requestOnCompletedRan);

            ResetFlags();
            spawnRequest = null;
            spawnObj.SpawnObj(objA, ref spawnRequest);
            yield return new WaitForEndOfFrame();
            Assert.IsTrue(onSpawnRan);
            Assert.IsTrue(onPostSpawnRan);
            Assert.IsFalse(requestOnSpawnRan);
            Assert.IsFalse(requestOnPostSpawnRan);
            Assert.IsFalse(requestOnCompletedRan);
        }

        [UnityTest]
        public IEnumerator DestroyObjectsOnMaxChanged_GlobalMax_Tests()
        {
            spawnObj.spawnAsActive = true;
            currAsActive = true;
            spawnObj.useObjectPool = false;
            currUseObjectPool = false;
            spawnObj.isObjectPoolManaged = false;
            currIsObjectPoolManaged = false;
            spawnObj.SpawnDelay = 0;
            spawnObj.PostSpawnDelay = 0;
            spawnObj.MaxCount = 6;
            spawnObj.ObjsWithMaxCounts = new List<DictionaryGameObjectCount>();
            spawnObj.destroyObjsOnMaxesChanged = true;
            spawnObj.destroyObjsSelectionMode = ListOneChoiceMode.Oldest;

            spawnObj.SpawnObj(objA);
            spawnObj.SpawnObj(objA);
            spawnObj.SpawnObj(objA);
            spawnObj.SpawnObj(objB);
            spawnObj.SpawnObj(objB);
            spawnObj.SpawnObj(objB);
            yield return new WaitForEndOfFrame();

            spawnObj.MaxCount = 3;
            yield return new WaitForEndOfFrame();
            string debugMsg = $"Obj Counts - Global Limit = {spawnObj.MaxCount}:\n";
            Dictionary<string, List<GameObject>> spawnedObjsDictAfter = spawnObj.CurrSpawnedObjs;
            CollectionAssert.IsNotEmpty(spawnedObjsDictAfter);
            Assert.AreEqual(3, spawnObj.CurrSpawnedCount);
            Assert.AreEqual(2, spawnedObjsDictAfter.Count);
            Assert.AreEqual(3, spawnedObjsDictAfter["ObjA"].Count + spawnedObjsDictAfter["ObjB"].Count);
            debugMsg += $"- ObjA (No Limit): {spawnedObjsDictAfter["ObjA"].Count}\n";
            debugMsg += $"- ObjB (No Limit): {spawnedObjsDictAfter["ObjB"].Count}\n";

            spawnObj.MaxCount = 1;
            yield return new WaitForEndOfFrame();
            debugMsg = $"Obj Counts - Global Limit = {spawnObj.MaxCount}:\n";
            spawnedObjsDictAfter = spawnObj.CurrSpawnedObjs;
            CollectionAssert.IsNotEmpty(spawnedObjsDictAfter);
            Assert.AreEqual(1, spawnObj.CurrSpawnedCount);
            Assert.AreEqual(2, spawnedObjsDictAfter.Count);
            Assert.AreEqual(1, spawnedObjsDictAfter["ObjA"].Count + spawnedObjsDictAfter["ObjB"].Count);
            debugMsg += $"- ObjA (No Limit): {spawnedObjsDictAfter["ObjA"].Count}\n";
            debugMsg += $"- ObjB (No Limit): {spawnedObjsDictAfter["ObjB"].Count}\n";

            spawnObj.MaxCount = 3;
            yield return new WaitForEndOfFrame();
            debugMsg = $"Obj Counts - Global Limit = {spawnObj.MaxCount}:\n";
            spawnedObjsDictAfter = spawnObj.CurrSpawnedObjs;
            CollectionAssert.IsNotEmpty(spawnedObjsDictAfter);
            Assert.AreEqual(1, spawnObj.CurrSpawnedCount);
            Assert.AreEqual(2, spawnedObjsDictAfter.Count);
            Assert.AreEqual(1, spawnedObjsDictAfter["ObjA"].Count + spawnedObjsDictAfter["ObjB"].Count);
            debugMsg += $"- ObjA (No Limit): {spawnedObjsDictAfter["ObjA"].Count}\n";
            debugMsg += $"- ObjB (No Limit): {spawnedObjsDictAfter["ObjB"].Count}\n";

            spawnObj.MaxCount = 0;
            yield return new WaitForEndOfFrame();
            debugMsg = $"Obj Counts - Global Limit = {spawnObj.MaxCount}:\n";
            spawnedObjsDictAfter = spawnObj.CurrSpawnedObjs;
            CollectionAssert.IsNotEmpty(spawnedObjsDictAfter);
            Assert.AreEqual(0, spawnObj.CurrSpawnedCount);
            Assert.AreEqual(2, spawnedObjsDictAfter.Count);
            Assert.AreEqual(0, spawnedObjsDictAfter["ObjA"].Count + spawnedObjsDictAfter["ObjB"].Count);
            debugMsg += $"- ObjA (No Limit): {spawnedObjsDictAfter["ObjA"].Count}\n";
            debugMsg += $"- ObjB (No Limit): {spawnedObjsDictAfter["ObjB"].Count}\n";

            Assert.Pass(debugMsg);
        }
        [UnityTest]
        public IEnumerator DestroyObjectsOnMaxChanged_PerObjMaxes_Tests()
        {
            spawnObj.spawnAsActive = true;
            currAsActive = true;
            spawnObj.useObjectPool = false;
            currUseObjectPool = false;
            spawnObj.isObjectPoolManaged = false;
            currIsObjectPoolManaged = false;
            spawnObj.SpawnDelay = 0;
            spawnObj.PostSpawnDelay = 0;
            spawnObj.MaxCount = 6;
            spawnObj.ObjsWithMaxCounts = new List<DictionaryGameObjectCount>()
            {
                new DictionaryGameObjectCount() { obj = objA, count = 3 },
                new DictionaryGameObjectCount() { obj = objB, count = 3 }
            };
            spawnObj.destroyObjsOnMaxesChanged = true;
            spawnObj.destroyObjsSelectionMode = ListOneChoiceMode.Oldest;

            spawnObj.SpawnObj(objA);
            spawnObj.SpawnObj(objA);
            spawnObj.SpawnObj(objA);
            spawnObj.SpawnObj(objB);
            spawnObj.SpawnObj(objB);
            spawnObj.SpawnObj(objB);
            yield return new WaitForEndOfFrame();

            string debugMsg = "Obj Counts:\n";
            spawnObj.ObjsWithMaxCounts = new List<DictionaryGameObjectCount>()
            {
                new DictionaryGameObjectCount() { obj = objA, count = 1 },
                new DictionaryGameObjectCount() { obj = objB, count = 2 }
            };
            yield return new WaitForEndOfFrame();
            Dictionary<string, List<GameObject>> spawnedObjsDictAfter = spawnObj.CurrSpawnedObjs;
            CollectionAssert.IsNotEmpty(spawnedObjsDictAfter);
            Assert.AreEqual(3, spawnObj.CurrSpawnedCount);
            Assert.AreEqual(2, spawnedObjsDictAfter.Count);
            Assert.AreEqual(3, spawnedObjsDictAfter["ObjA"].Count + spawnedObjsDictAfter["ObjB"].Count);
            Assert.AreEqual(1, spawnedObjsDictAfter["ObjA"].Count);
            debugMsg += $"- ObjA (Limit = {spawnObj.ObjsWithMaxCounts[0].count}): {spawnedObjsDictAfter["ObjA"].Count}\n";
            Assert.AreEqual(2, spawnedObjsDictAfter["ObjB"].Count);
            debugMsg += $"- ObjB (Limit = {spawnObj.ObjsWithMaxCounts[1].count}): {spawnedObjsDictAfter["ObjB"].Count}\n";

            debugMsg = "Obj Counts:\n";
            spawnObj.ObjsWithMaxCounts = new List<DictionaryGameObjectCount>()
            {
                new DictionaryGameObjectCount() { obj = objA, count = 1 },
                new DictionaryGameObjectCount() { obj = objB, count = 1 }
            };
            yield return new WaitForEndOfFrame();
            spawnedObjsDictAfter = spawnObj.CurrSpawnedObjs;
            CollectionAssert.IsNotEmpty(spawnedObjsDictAfter);
            Assert.AreEqual(2, spawnObj.CurrSpawnedCount);
            Assert.AreEqual(2, spawnedObjsDictAfter.Count);
            Assert.AreEqual(2, spawnedObjsDictAfter["ObjA"].Count + spawnedObjsDictAfter["ObjB"].Count);
            Assert.AreEqual(1, spawnedObjsDictAfter["ObjA"].Count);
            debugMsg += $"- ObjA (Limit = {spawnObj.ObjsWithMaxCounts[0].count}): {spawnedObjsDictAfter["ObjA"].Count}\n";
            Assert.AreEqual(1, spawnedObjsDictAfter["ObjB"].Count);
            debugMsg += $"- ObjB (Limit = {spawnObj.ObjsWithMaxCounts[1].count}): {spawnedObjsDictAfter["ObjB"].Count}\n";

            debugMsg = "Obj Counts:\n";
            spawnObj.ObjsWithMaxCounts = new List<DictionaryGameObjectCount>()
            {
                new DictionaryGameObjectCount() { obj = objA, count = 3 },
                new DictionaryGameObjectCount() { obj = objB, count = 2 }
            };
            yield return new WaitForEndOfFrame();
            spawnedObjsDictAfter = spawnObj.CurrSpawnedObjs;
            CollectionAssert.IsNotEmpty(spawnedObjsDictAfter);
            Assert.AreEqual(2, spawnObj.CurrSpawnedCount);
            Assert.AreEqual(2, spawnedObjsDictAfter.Count);
            Assert.AreEqual(2, spawnedObjsDictAfter["ObjA"].Count + spawnedObjsDictAfter["ObjB"].Count);
            Assert.AreEqual(1, spawnedObjsDictAfter["ObjA"].Count);
            debugMsg += $"- ObjA (Limit = {spawnObj.ObjsWithMaxCounts[0].count}): {spawnedObjsDictAfter["ObjA"].Count}\n";
            Assert.AreEqual(1, spawnedObjsDictAfter["ObjB"].Count);
            debugMsg += $"- ObjB (Limit = {spawnObj.ObjsWithMaxCounts[1].count}): {spawnedObjsDictAfter["ObjB"].Count}\n";

            debugMsg = "Obj Counts:\n";
            spawnObj.ObjsWithMaxCounts = new List<DictionaryGameObjectCount>()
            {
                new DictionaryGameObjectCount() { obj = objA, count = 0 },
                new DictionaryGameObjectCount() { obj = objB, count = 0 }
            };
            yield return new WaitForEndOfFrame();
            spawnedObjsDictAfter = spawnObj.CurrSpawnedObjs;
            CollectionAssert.IsNotEmpty(spawnedObjsDictAfter);
            Assert.AreEqual(0, spawnObj.CurrSpawnedCount);
            Assert.AreEqual(2, spawnedObjsDictAfter.Count);
            Assert.AreEqual(0, spawnedObjsDictAfter["ObjA"].Count + spawnedObjsDictAfter["ObjB"].Count);
            Assert.AreEqual(0, spawnedObjsDictAfter["ObjA"].Count);
            debugMsg += $"- ObjA (Limit = {spawnObj.ObjsWithMaxCounts[0].count}): {spawnedObjsDictAfter["ObjA"].Count}\n";
            Assert.AreEqual(0, spawnedObjsDictAfter["ObjB"].Count);
            debugMsg += $"- ObjB (Limit = {spawnObj.ObjsWithMaxCounts[1].count}): {spawnedObjsDictAfter["ObjB"].Count}\n";

            Assert.Pass(debugMsg);
        }

        [UnityTest]
        public IEnumerator DestroyAnObject_Oldest_Newest_Tests()
        {
            spawnObj.spawnAsActive = true;
            currAsActive = true;
            spawnObj.useObjectPool = false;
            currUseObjectPool = false;
            spawnObj.isObjectPoolManaged = false;
            currIsObjectPoolManaged = false;
            spawnObj.SpawnDelay = 0;
            spawnObj.PostSpawnDelay = 0;
            spawnObj.MaxCount = 6;
            spawnObj.ObjsWithMaxCounts = new List<DictionaryGameObjectCount>();
            spawnObj.destroyObjsOnMaxesChanged = false;
            spawnObj.destroyObjsSelectionMode = ListOneChoiceMode.Oldest;

            spawnObj.SpawnObj(objA);
            spawnObj.SpawnObj(objA);
            spawnObj.SpawnObj(objA);
            spawnObj.SpawnObj(objB);
            spawnObj.SpawnObj(objB);
            spawnObj.SpawnObj(objB);
            yield return new WaitForEndOfFrame();

            Dictionary<string, List<GameObject>> spawnedObjsDictBefore = spawnObj.CurrSpawnedObjs;
            Assert.IsTrue(spawnObj.DestroyAnObject("ObjA"));
            Dictionary<string, List<GameObject>> spawnedObjsDictAfter = spawnObj.CurrSpawnedObjs;
            CollectionAssert.IsNotEmpty(spawnedObjsDictAfter);
            Assert.AreEqual(2, spawnedObjsDictAfter.Count);
            Assert.IsTrue(spawnedObjsDictAfter.ContainsKey("ObjA"));
            Assert.AreEqual(2, spawnedObjsDictAfter["ObjA"].Count);
            Assert.AreNotSame(spawnedObjsDictBefore["ObjA"][0], spawnedObjsDictAfter["ObjA"][0]);
            Assert.AreSame(spawnedObjsDictBefore["ObjA"][1], spawnedObjsDictAfter["ObjA"][0]);
            Assert.AreSame(spawnedObjsDictBefore["ObjA"][2], spawnedObjsDictAfter["ObjA"][1]);
            Assert.IsTrue(spawnedObjsDictAfter.ContainsKey("ObjB"));
            Assert.AreEqual(3, spawnedObjsDictAfter["ObjB"].Count);
            Assert.AreSame(spawnedObjsDictBefore["ObjB"][0], spawnedObjsDictAfter["ObjB"][0]);
            Assert.AreSame(spawnedObjsDictBefore["ObjB"][1], spawnedObjsDictAfter["ObjB"][1]);
            Assert.AreSame(spawnedObjsDictBefore["ObjB"][2], spawnedObjsDictAfter["ObjB"][2]);
            
            Assert.IsTrue(spawnObj.DestroyAnObject("ObjB"));
            spawnedObjsDictAfter = spawnObj.CurrSpawnedObjs;
            CollectionAssert.IsNotEmpty(spawnedObjsDictAfter);
            Assert.AreEqual(2, spawnedObjsDictAfter.Count);
            Assert.IsTrue(spawnedObjsDictAfter.ContainsKey("ObjA"));
            Assert.AreEqual(2, spawnedObjsDictAfter["ObjA"].Count);
            Assert.AreNotSame(spawnedObjsDictBefore["ObjA"][0], spawnedObjsDictAfter["ObjA"][0]);
            Assert.AreSame(spawnedObjsDictBefore["ObjA"][1], spawnedObjsDictAfter["ObjA"][0]);
            Assert.AreSame(spawnedObjsDictBefore["ObjA"][2], spawnedObjsDictAfter["ObjA"][1]);
            Assert.IsTrue(spawnedObjsDictAfter.ContainsKey("ObjB"));
            Assert.AreEqual(2, spawnedObjsDictAfter["ObjB"].Count);
            Assert.AreNotSame(spawnedObjsDictBefore["ObjB"][0], spawnedObjsDictAfter["ObjB"][0]);
            Assert.AreSame(spawnedObjsDictBefore["ObjB"][1], spawnedObjsDictAfter["ObjB"][0]);
            Assert.AreSame(spawnedObjsDictBefore["ObjB"][2], spawnedObjsDictAfter["ObjB"][1]);

            spawnObj.destroyObjsSelectionMode = ListOneChoiceMode.Newest;
            Assert.IsTrue(spawnObj.DestroyAnObject("ObjA"));
            spawnedObjsDictAfter = spawnObj.CurrSpawnedObjs;
            CollectionAssert.IsNotEmpty(spawnedObjsDictAfter);
            Assert.AreEqual(2, spawnedObjsDictAfter.Count);
            Assert.IsTrue(spawnedObjsDictAfter.ContainsKey("ObjA"));
            Assert.AreEqual(1, spawnedObjsDictAfter["ObjA"].Count);
            Assert.AreNotSame(spawnedObjsDictBefore["ObjA"][0], spawnedObjsDictAfter["ObjA"][0]);
            Assert.AreSame(spawnedObjsDictBefore["ObjA"][1], spawnedObjsDictAfter["ObjA"][0]);
            Assert.AreNotSame(spawnedObjsDictBefore["ObjA"][2], spawnedObjsDictAfter["ObjA"][0]);
            Assert.IsTrue(spawnedObjsDictAfter.ContainsKey("ObjB"));
            Assert.AreEqual(2, spawnedObjsDictAfter["ObjB"].Count);
            Assert.AreNotSame(spawnedObjsDictBefore["ObjB"][0], spawnedObjsDictAfter["ObjB"][0]);
            Assert.AreSame(spawnedObjsDictBefore["ObjB"][1], spawnedObjsDictAfter["ObjB"][0]);
            Assert.AreSame(spawnedObjsDictBefore["ObjB"][2], spawnedObjsDictAfter["ObjB"][1]);

            Assert.IsTrue(spawnObj.DestroyAnObject("ObjB"));
            spawnedObjsDictAfter = spawnObj.CurrSpawnedObjs;
            CollectionAssert.IsNotEmpty(spawnedObjsDictAfter);
            Assert.AreEqual(2, spawnedObjsDictAfter.Count);
            Assert.IsTrue(spawnedObjsDictAfter.ContainsKey("ObjA"));
            Assert.AreEqual(1, spawnedObjsDictAfter["ObjA"].Count);
            Assert.AreNotSame(spawnedObjsDictBefore["ObjA"][0], spawnedObjsDictAfter["ObjA"][0]);
            Assert.AreSame(spawnedObjsDictBefore["ObjA"][1], spawnedObjsDictAfter["ObjA"][0]);
            Assert.AreNotSame(spawnedObjsDictBefore["ObjA"][2], spawnedObjsDictAfter["ObjA"][0]);
            Assert.IsTrue(spawnedObjsDictAfter.ContainsKey("ObjB"));
            Assert.AreEqual(1, spawnedObjsDictAfter["ObjB"].Count);
            Assert.AreNotSame(spawnedObjsDictBefore["ObjB"][0], spawnedObjsDictAfter["ObjB"][0]);
            Assert.AreSame(spawnedObjsDictBefore["ObjB"][1], spawnedObjsDictAfter["ObjB"][0]);
            Assert.AreNotSame(spawnedObjsDictBefore["ObjB"][2], spawnedObjsDictAfter["ObjB"][0]);

            Assert.IsTrue(spawnObj.DestroyAnObject("ObjA"));
            Assert.IsTrue(spawnObj.DestroyAnObject("ObjB"));
            spawnedObjsDictAfter = spawnObj.CurrSpawnedObjs;
            Assert.AreEqual(2, spawnedObjsDictAfter.Count);
            CollectionAssert.IsEmpty(spawnedObjsDictAfter["ObjA"]);
            CollectionAssert.IsEmpty(spawnedObjsDictAfter["ObjB"]);

            Assert.IsFalse(spawnObj.DestroyAnObject("ObjA"));
            Assert.IsFalse(spawnObj.DestroyAnObject("ObjB"));
        }
        [UnityTest]
        public IEnumerator DestroyAnObject_Randomly_Tests()
        {
            spawnObj.spawnAsActive = true;
            currAsActive = true;
            spawnObj.useObjectPool = false;
            currUseObjectPool = false;
            spawnObj.isObjectPoolManaged = false;
            currIsObjectPoolManaged = false;
            spawnObj.SpawnDelay = 0;
            spawnObj.PostSpawnDelay = 0;
            spawnObj.MaxCount = 6;
            spawnObj.ObjsWithMaxCounts = new List<DictionaryGameObjectCount>();
            spawnObj.destroyObjsOnMaxesChanged = false;
            spawnObj.destroyObjsSelectionMode = ListOneChoiceMode.Randomly;

            spawnObj.SpawnObj(objA);
            spawnObj.SpawnObj(objA);
            spawnObj.SpawnObj(objA);
            spawnObj.SpawnObj(objB);
            spawnObj.SpawnObj(objB);
            spawnObj.SpawnObj(objB);
            yield return new WaitForEndOfFrame();

            string debugMsg = "Chosen Objs:\n- ObjA:\n";
            List<int> chosenI = new List<int>();
            Dictionary<string, List<GameObject>> spawnedObjsDictBefore = spawnObj.CurrSpawnedObjs;
            Dictionary<string, List<GameObject>> spawnedObjsDictAfter = new Dictionary<string, List<GameObject>>();
            for (int i = 0; i < 3; i++)
            {
                Assert.IsTrue(spawnObj.DestroyAnObject("ObjA"));
                spawnedObjsDictAfter = spawnObj.CurrSpawnedObjs;
                for(int ii = 0; ii < 3; ii++)
                {
                    if (chosenI.Contains(ii)) continue;

                    if(!spawnedObjsDictAfter["ObjA"].Contains(spawnedObjsDictBefore["ObjA"][ii]))
                    {
                        debugMsg += $"  - {ii}\n";
                        chosenI.Add(ii);
                        break;
                    }
                }
            }
            debugMsg += "- ObjB:\n";
            chosenI = new List<int>();
            for (int i = 0; i < 3; i++)
            {
                Assert.IsTrue(spawnObj.DestroyAnObject("ObjB"));
                spawnedObjsDictAfter = spawnObj.CurrSpawnedObjs;
                for (int ii = 0; ii < 3; ii++)
                {
                    if (chosenI.Contains(ii)) continue;

                    if (!spawnedObjsDictAfter["ObjB"].Contains(spawnedObjsDictBefore["ObjB"][ii]))
                    {
                        debugMsg += $"  - {ii}\n";
                        chosenI.Add(ii);
                        break;
                    }
                }
            }
            Assert.Pass(debugMsg);

            Assert.IsFalse(spawnObj.DestroyAnObject("ObjA"));
            Assert.IsFalse(spawnObj.DestroyAnObject("ObjB"));
        }
        [UnityTest]
        public IEnumerator DestroyAnObject_InvalidInput_Tests()
        {
            spawnObj.spawnAsActive = true;
            currAsActive = true;
            spawnObj.useObjectPool = false;
            currUseObjectPool = false;
            spawnObj.isObjectPoolManaged = false;
            currIsObjectPoolManaged = false;
            spawnObj.SpawnDelay = 0;
            spawnObj.PostSpawnDelay = 0;
            spawnObj.MaxCount = 6;
            spawnObj.ObjsWithMaxCounts = new List<DictionaryGameObjectCount>();
            spawnObj.destroyObjsOnMaxesChanged = false;
            spawnObj.destroyObjsSelectionMode = ListOneChoiceMode.Oldest;

            spawnObj.SpawnObj(objA);
            spawnObj.SpawnObj(objA);
            spawnObj.SpawnObj(objA);
            spawnObj.SpawnObj(objB);
            spawnObj.SpawnObj(objB);
            spawnObj.SpawnObj(objB);
            yield return new WaitForEndOfFrame();

            Assert.IsFalse(spawnObj.DestroyAnObject(null));
            Assert.IsFalse(spawnObj.DestroyAnObject(""));
        }

        [UnityTest]
        public IEnumerator DestroyObject_Tests()
        {
            spawnObj.spawnAsActive = true;
            currAsActive = true;
            spawnObj.useObjectPool = false;
            currUseObjectPool = false;
            spawnObj.isObjectPoolManaged = false;
            currIsObjectPoolManaged = false;
            spawnObj.SpawnDelay = 0;
            spawnObj.PostSpawnDelay = 0;
            spawnObj.MaxCount = 6;
            spawnObj.ObjsWithMaxCounts = new List<DictionaryGameObjectCount>();
            spawnObj.destroyObjsOnMaxesChanged = false;
            spawnObj.destroyObjsSelectionMode = ListOneChoiceMode.Oldest;

            spawnObj.SpawnObj(objA);
            spawnObj.SpawnObj(objA);
            spawnObj.SpawnObj(objA);
            spawnObj.SpawnObj(objB);
            spawnObj.SpawnObj(objB);
            spawnObj.SpawnObj(objB);
            yield return new WaitForEndOfFrame();
            
            Dictionary<string, List<GameObject>> spawnedObjsDictBefore = spawnObj.CurrSpawnedObjs;
            Assert.IsTrue(spawnObj.DestroyObject(spawnedObjsDictBefore["ObjA"][1]));
            yield return new WaitForEndOfFrame();
            Dictionary<string, List<GameObject>> spawnedObjsDictAfter = spawnObj.CurrSpawnedObjs;
            CollectionAssert.IsNotEmpty(spawnedObjsDictAfter);
            Assert.AreEqual(2, spawnedObjsDictAfter.Count);
            Assert.IsTrue(spawnedObjsDictAfter.ContainsKey("ObjA"));
            Assert.AreEqual(2, spawnedObjsDictAfter["ObjA"].Count);
            Assert.AreSame(spawnedObjsDictBefore["ObjA"][0], spawnedObjsDictAfter["ObjA"][0]);
            Assert.IsFalse(spawnedObjsDictBefore["ObjA"][1]);
            Assert.AreSame(spawnedObjsDictBefore["ObjA"][2], spawnedObjsDictAfter["ObjA"][1]);
            Assert.IsTrue(spawnedObjsDictAfter.ContainsKey("ObjB"));
            Assert.AreEqual(3, spawnedObjsDictAfter["ObjB"].Count);
            Assert.AreSame(spawnedObjsDictBefore["ObjB"][0], spawnedObjsDictAfter["ObjB"][0]);
            Assert.AreSame(spawnedObjsDictBefore["ObjB"][1], spawnedObjsDictAfter["ObjB"][1]);
            Assert.AreSame(spawnedObjsDictBefore["ObjB"][2], spawnedObjsDictAfter["ObjB"][2]);
            Assert.AreEqual(0, objPool.transform.GetChild(1).childCount);

            Assert.IsTrue(spawnObj.DestroyObject(spawnedObjsDictBefore["ObjB"][1]));
            yield return new WaitForEndOfFrame();
            spawnedObjsDictAfter = spawnObj.CurrSpawnedObjs;
            CollectionAssert.IsNotEmpty(spawnedObjsDictAfter);
            Assert.AreEqual(2, spawnedObjsDictAfter.Count);
            Assert.IsTrue(spawnedObjsDictAfter.ContainsKey("ObjA"));
            Assert.AreEqual(2, spawnedObjsDictAfter["ObjA"].Count);
            Assert.AreSame(spawnedObjsDictBefore["ObjA"][0], spawnedObjsDictAfter["ObjA"][0]);
            Assert.IsFalse(spawnedObjsDictBefore["ObjA"][1]);
            Assert.AreSame(spawnedObjsDictBefore["ObjA"][2], spawnedObjsDictAfter["ObjA"][1]);
            Assert.IsTrue(spawnedObjsDictAfter.ContainsKey("ObjB"));
            Assert.AreEqual(2, spawnedObjsDictAfter["ObjB"].Count);
            Assert.AreSame(spawnedObjsDictBefore["ObjB"][0], spawnedObjsDictAfter["ObjB"][0]);
            Assert.IsFalse(spawnedObjsDictBefore["ObjB"][1]);
            Assert.AreSame(spawnedObjsDictBefore["ObjB"][2], spawnedObjsDictAfter["ObjB"][1]);
            Assert.AreEqual(0, objPool.transform.GetChild(1).childCount);

            Assert.IsTrue(spawnObj.DestroyObject(spawnedObjsDictBefore["ObjA"][0]));
            Assert.IsTrue(spawnObj.DestroyObject(spawnedObjsDictBefore["ObjA"][2]));
            Assert.IsTrue(spawnObj.DestroyObject(spawnedObjsDictBefore["ObjB"][0]));
            Assert.IsTrue(spawnObj.DestroyObject(spawnedObjsDictBefore["ObjB"][2]));
            yield return new WaitForEndOfFrame();
            spawnedObjsDictAfter = spawnObj.CurrSpawnedObjs;
            CollectionAssert.IsNotEmpty(spawnedObjsDictAfter);
            Assert.AreEqual(2, spawnedObjsDictAfter.Count);
            Assert.IsTrue(spawnedObjsDictAfter.ContainsKey("ObjA"));
            Assert.AreEqual(0, spawnedObjsDictAfter["ObjA"].Count);
            Assert.IsFalse(spawnedObjsDictBefore["ObjA"][0]);
            Assert.IsFalse(spawnedObjsDictBefore["ObjA"][1]);
            Assert.IsFalse(spawnedObjsDictBefore["ObjA"][2]);
            Assert.IsTrue(spawnedObjsDictAfter.ContainsKey("ObjB"));
            Assert.AreEqual(0, spawnedObjsDictAfter["ObjB"].Count);
            Assert.IsFalse(spawnedObjsDictBefore["ObjB"][0]);
            Assert.IsFalse(spawnedObjsDictBefore["ObjB"][1]);
            Assert.IsFalse(spawnedObjsDictBefore["ObjB"][2]);
            Assert.AreEqual(0, objPool.transform.GetChild(1).childCount);

            Assert.IsFalse(spawnObj.DestroyObject(objA));
            Assert.IsFalse(spawnObj.DestroyObject(objB));
        }
        [UnityTest]
        public IEnumerator DestroyObject_ObjPool_Tests()
        {
            spawnObj.spawnAsActive = true;
            currAsActive = true;
            spawnObj.useObjectPool = true;
            currUseObjectPool = true;
            spawnObj.isObjectPoolManaged = false;
            currIsObjectPoolManaged = false;
            spawnObj.SpawnDelay = 0;
            spawnObj.PostSpawnDelay = 0;
            spawnObj.MaxCount = 6;
            spawnObj.ObjsWithMaxCounts = new List<DictionaryGameObjectCount>();
            spawnObj.destroyObjsOnMaxesChanged = false;
            spawnObj.destroyObjsSelectionMode = ListOneChoiceMode.Oldest;

            spawnObj.SpawnObj(objA);
            spawnObj.SpawnObj(objA);
            spawnObj.SpawnObj(objA);
            spawnObj.SpawnObj(objB);
            spawnObj.SpawnObj(objB);
            spawnObj.SpawnObj(objB);
            yield return new WaitForEndOfFrame();

            Dictionary<string, List<GameObject>> spawnedObjsDictBefore = spawnObj.CurrSpawnedObjs;
            Assert.IsTrue(spawnObj.DestroyObject(spawnedObjsDictBefore["ObjA"][1]));
            yield return new WaitForEndOfFrame();
            Dictionary<string, List<GameObject>> spawnedObjsDictAfter = spawnObj.CurrSpawnedObjs;
            CollectionAssert.IsNotEmpty(spawnedObjsDictAfter);
            Assert.AreEqual(2, spawnedObjsDictAfter.Count);
            Assert.IsTrue(spawnedObjsDictAfter.ContainsKey("ObjA"));
            Assert.AreEqual(2, spawnedObjsDictAfter["ObjA"].Count);
            Assert.AreSame(spawnedObjsDictBefore["ObjA"][0], spawnedObjsDictAfter["ObjA"][0]);
            Assert.IsTrue(spawnedObjsDictBefore["ObjA"][1]);
            Assert.IsFalse(spawnedObjsDictBefore["ObjA"][1].activeInHierarchy);
            Assert.AreSame(spawnedObjsDictBefore["ObjA"][2], spawnedObjsDictAfter["ObjA"][1]);
            Assert.IsTrue(spawnedObjsDictAfter.ContainsKey("ObjB"));
            Assert.AreEqual(3, spawnedObjsDictAfter["ObjB"].Count);
            Assert.AreSame(spawnedObjsDictBefore["ObjB"][0], spawnedObjsDictAfter["ObjB"][0]);
            Assert.AreSame(spawnedObjsDictBefore["ObjB"][1], spawnedObjsDictAfter["ObjB"][1]);
            Assert.AreSame(spawnedObjsDictBefore["ObjB"][2], spawnedObjsDictAfter["ObjB"][2]);
            Assert.AreEqual(1, objPool.transform.GetChild(1).childCount);
            Assert.AreEqual(1, objPool.transform.GetChild(1).GetChild(0).childCount);

            Assert.IsTrue(spawnObj.DestroyObject(spawnedObjsDictBefore["ObjB"][1]));
            yield return new WaitForEndOfFrame();
            spawnedObjsDictAfter = spawnObj.CurrSpawnedObjs;
            CollectionAssert.IsNotEmpty(spawnedObjsDictAfter);
            Assert.AreEqual(2, spawnedObjsDictAfter.Count);
            Assert.IsTrue(spawnedObjsDictAfter.ContainsKey("ObjA"));
            Assert.AreEqual(2, spawnedObjsDictAfter["ObjA"].Count);
            Assert.AreSame(spawnedObjsDictBefore["ObjA"][0], spawnedObjsDictAfter["ObjA"][0]);
            Assert.IsTrue(spawnedObjsDictBefore["ObjA"][1]);
            Assert.IsFalse(spawnedObjsDictBefore["ObjA"][1].activeInHierarchy);
            Assert.AreSame(spawnedObjsDictBefore["ObjA"][2], spawnedObjsDictAfter["ObjA"][1]);
            Assert.IsTrue(spawnedObjsDictAfter.ContainsKey("ObjB"));
            Assert.AreEqual(2, spawnedObjsDictAfter["ObjB"].Count);
            Assert.AreSame(spawnedObjsDictBefore["ObjB"][0], spawnedObjsDictAfter["ObjB"][0]);
            Assert.IsTrue(spawnedObjsDictBefore["ObjB"][1]);
            Assert.IsFalse(spawnedObjsDictBefore["ObjB"][1].activeInHierarchy);
            Assert.AreSame(spawnedObjsDictBefore["ObjB"][2], spawnedObjsDictAfter["ObjB"][1]);
            Assert.AreEqual(2, objPool.transform.GetChild(1).childCount);
            Assert.AreEqual(1, objPool.transform.GetChild(1).GetChild(0).childCount);
            Assert.AreEqual(1, objPool.transform.GetChild(1).GetChild(1).childCount);

            Assert.IsTrue(spawnObj.DestroyObject(spawnedObjsDictBefore["ObjA"][0]));
            Assert.IsTrue(spawnObj.DestroyObject(spawnedObjsDictBefore["ObjA"][2]));
            Assert.IsTrue(spawnObj.DestroyObject(spawnedObjsDictBefore["ObjB"][0]));
            Assert.IsTrue(spawnObj.DestroyObject(spawnedObjsDictBefore["ObjB"][2]));
            yield return new WaitForEndOfFrame();
            spawnedObjsDictAfter = spawnObj.CurrSpawnedObjs;
            CollectionAssert.IsNotEmpty(spawnedObjsDictAfter);
            Assert.AreEqual(2, spawnedObjsDictAfter.Count);
            Assert.IsTrue(spawnedObjsDictAfter.ContainsKey("ObjA"));
            Assert.AreEqual(0, spawnedObjsDictAfter["ObjA"].Count);
            Assert.IsTrue(spawnedObjsDictBefore["ObjA"][0]);
            Assert.IsFalse(spawnedObjsDictBefore["ObjA"][0].activeInHierarchy);
            Assert.IsTrue(spawnedObjsDictBefore["ObjA"][1]);
            Assert.IsFalse(spawnedObjsDictBefore["ObjA"][1].activeInHierarchy);
            Assert.IsTrue(spawnedObjsDictBefore["ObjA"][2]);
            Assert.IsFalse(spawnedObjsDictBefore["ObjA"][2].activeInHierarchy);
            Assert.IsTrue(spawnedObjsDictAfter.ContainsKey("ObjB"));
            Assert.AreEqual(0, spawnedObjsDictAfter["ObjB"].Count);
            Assert.IsTrue(spawnedObjsDictBefore["ObjB"][0]);
            Assert.IsFalse(spawnedObjsDictBefore["ObjB"][0].activeInHierarchy);
            Assert.IsTrue(spawnedObjsDictBefore["ObjB"][1]);
            Assert.IsFalse(spawnedObjsDictBefore["ObjB"][1].activeInHierarchy);
            Assert.IsTrue(spawnedObjsDictBefore["ObjB"][2]);
            Assert.IsFalse(spawnedObjsDictBefore["ObjB"][2].activeInHierarchy);
            Assert.AreEqual(2, objPool.transform.GetChild(1).childCount);
            Assert.AreEqual(3, objPool.transform.GetChild(1).GetChild(0).childCount);
            Assert.AreEqual(3, objPool.transform.GetChild(1).GetChild(1).childCount);

            Assert.IsFalse(spawnObj.DestroyObject(objA));
            Assert.IsFalse(spawnObj.DestroyObject(objB));
        }
        [UnityTest]
        public IEnumerator DestroyObject_InvalidInput_Tests()
        {
            spawnObj.spawnAsActive = true;
            currAsActive = true;
            spawnObj.useObjectPool = false;
            currUseObjectPool = false;
            spawnObj.isObjectPoolManaged = false;
            currIsObjectPoolManaged = false;
            spawnObj.SpawnDelay = 0;
            spawnObj.PostSpawnDelay = 0;
            spawnObj.MaxCount = 6;
            spawnObj.ObjsWithMaxCounts = new List<DictionaryGameObjectCount>();
            spawnObj.destroyObjsOnMaxesChanged = false;
            spawnObj.destroyObjsSelectionMode = ListOneChoiceMode.Oldest;

            spawnObj.SpawnObj(objA);
            spawnObj.SpawnObj(objA);
            spawnObj.SpawnObj(objA);
            spawnObj.SpawnObj(objB);
            spawnObj.SpawnObj(objB);
            spawnObj.SpawnObj(objB);
            yield return new WaitForEndOfFrame();

            Assert.IsFalse(spawnObj.DestroyObject(null));
        }

        [UnityTest]
        public IEnumerator IsSpawnable_Tests()
        {
            spawnObj.spawnAsActive = true;
            currAsActive = true;
            spawnObj.useObjectPool = false;
            currUseObjectPool = false;
            spawnObj.isObjectPoolManaged = false;
            currIsObjectPoolManaged = false;
            spawnObj.SpawnDelay = 0;
            spawnObj.PostSpawnDelay = 0;
            spawnObj.MaxCount = 3;
            spawnObj.ObjsWithMaxCounts = new List<DictionaryGameObjectCount>();
            spawnObj.destroyObjsOnMaxesChanged = false;
            spawnObj.destroyObjsSelectionMode = ListOneChoiceMode.Oldest;

            Assert.IsTrue(spawnObj.IsSpawnable(objA));
            Assert.IsTrue(spawnObj.IsSpawnable(objB));
            spawnObj.SpawnObj(objA);
            spawnObj.SpawnObj(objA);
            yield return new WaitForEndOfFrame();
            Assert.IsTrue(spawnObj.IsSpawnable(objA));
            Assert.IsTrue(spawnObj.IsSpawnable(objB));
            spawnObj.SpawnObj(objA);
            yield return new WaitForEndOfFrame();
            Assert.IsFalse(spawnObj.IsSpawnable(objA));
            Assert.IsFalse(spawnObj.IsSpawnable(objB));

            spawnObj.MaxCount = 6;
            spawnObj.ObjsWithMaxCounts = new List<DictionaryGameObjectCount>()
            {
                new DictionaryGameObjectCount() { obj = objA, count = 3 }
            };
            Assert.IsFalse(spawnObj.IsSpawnable(objA));
            Assert.IsTrue(spawnObj.IsSpawnable(objB));

            spawnObj.ObjsWithMaxCounts = new List<DictionaryGameObjectCount>()
            {
                new DictionaryGameObjectCount() { obj = objA, count = 3 },
                new DictionaryGameObjectCount() { obj = objB, count = 2 }
            };
            Assert.IsFalse(spawnObj.IsSpawnable(objA));
            Assert.IsTrue(spawnObj.IsSpawnable(objB));
            spawnObj.SpawnObj(objB);
            yield return new WaitForEndOfFrame();
            Assert.IsFalse(spawnObj.IsSpawnable(objA));
            Assert.IsTrue(spawnObj.IsSpawnable(objB));
            spawnObj.SpawnObj(objB);
            yield return new WaitForEndOfFrame();
            Assert.IsFalse(spawnObj.IsSpawnable(objA));
            Assert.IsFalse(spawnObj.IsSpawnable(objB));
        }
        [UnityTest]
        public IEnumerator IsSpawnable_InvalidInput_Tests()
        {
            spawnObj.spawnAsActive = true;
            currAsActive = true;
            spawnObj.useObjectPool = false;
            currUseObjectPool = false;
            spawnObj.isObjectPoolManaged = false;
            currIsObjectPoolManaged = false;
            spawnObj.SpawnDelay = 0;
            spawnObj.PostSpawnDelay = 0;
            spawnObj.MaxCount = 3;
            spawnObj.ObjsWithMaxCounts = new List<DictionaryGameObjectCount>();
            spawnObj.destroyObjsOnMaxesChanged = false;
            spawnObj.destroyObjsSelectionMode = ListOneChoiceMode.Oldest;

            Assert.IsFalse(spawnObj.IsSpawnable(null));
            yield return new WaitForEndOfFrame();
        }

        [SetUp]
        public void Setup()
        {
            spawnObj = new GameObject("SpawnObject").AddComponent<SpawnObjectAdvancedNode>();
            spawnObj.OnSpawn.AddListener(OnSpawn);
            spawnObj.OnPostSpawn.AddListener(OnPostSpawn);

            spawnRequest = new SpawnRequest();

            objA = new GameObject(objAActualName);
            objB = new GameObject(objBActualName);

            objPool = new GameObject("ObjPool").AddComponent<ObjectPoolManager>();
        }
        [TearDown]
        public void TearDown()
        {
            if (spawnObj) GameObject.DestroyImmediate(spawnObj.gameObject);
            spawnObj = null;

            if (objA) GameObject.DestroyImmediate(objA);
            objA = null;
            if (objB) GameObject.DestroyImmediate(objB);
            objB = null;

            if (objPool) GameObject.DestroyImmediate(objPool.gameObject);
            objPool = null;

            foreach(GameObject obj in spawnedObjs)
            {
                if (obj) GameObject.DestroyImmediate(obj);
            }
            spawnedObjs = new List<GameObject>();

            currAsActive = false;
            currUseObjectPool = false;
            currIsObjectPoolManaged = false;
            ResetFlags();
        }

        private void OnSpawn(GameObject spawnedObj)
        {
            onSpawnRan = true;
            Assert.IsNotNull(spawnedObj);
            Assert.AreNotEqual(objA, spawnedObj);
            Assert.IsTrue(spawnedObj.activeInHierarchy == currAsActive);
            if(!currUseObjectPool) Assert.IsNull(spawnedObj.transform.parent);
            else
            {
                if (currIsObjectPoolManaged)
                {
                    Assert.IsNotNull(spawnedObj.transform.parent);
                    Assert.AreEqual($"{GameObjectUtility.GetActualName(spawnedObj).ToUpper()}_ANCHOR", spawnedObj.transform.parent.name);
                    if(currAsActive) Assert.AreEqual("ACTIVE_OBJECTS_POOL", spawnedObj.transform.parent.parent.name);
                    else Assert.AreEqual("INACTIVE_OBJECTS_POOL", spawnedObj.transform.parent.parent.name);
                }
                else Assert.IsNull(spawnedObj.transform.parent);
            }

            spawnedObjs.Add(objA);
        }
        private void OnPostSpawn(GameObject spawnedObj)
        {
            onPostSpawnRan = true;
            Assert.IsNotNull(spawnedObj);
            Assert.AreNotEqual(objA, spawnedObj);
            Assert.IsTrue(spawnedObj.activeInHierarchy == currAsActive);
            if (!currUseObjectPool) Assert.IsNull(spawnedObj.transform.parent);
            else
            {
                if (currIsObjectPoolManaged)
                {
                    Assert.IsNotNull(spawnedObj.transform.parent);
                    Assert.AreEqual($"{GameObjectUtility.GetActualName(spawnedObj).ToUpper()}_ANCHOR", spawnedObj.transform.parent.name);
                    if (currAsActive) Assert.AreEqual("ACTIVE_OBJECTS_POOL", spawnedObj.transform.parent.parent.name);
                    else Assert.AreEqual("INACTIVE_OBJECTS_POOL", spawnedObj.transform.parent.parent.name);
                }
                else Assert.IsNull(spawnedObj.transform.parent);
            }
        }

        private void RequestOnSpawn(SpawnRequest request)
        {
            requestOnSpawnRan = true;
            Assert.IsNotNull(request);
            Assert.IsNotNull(request.spawnedObj);
            Assert.IsTrue(request.spawnedObj.activeInHierarchy == currAsActive);
            if (!currUseObjectPool) Assert.IsNull(request.spawnedObj.transform.parent);
            else
            {
                if (currIsObjectPoolManaged)
                {
                    Assert.IsNotNull(request.spawnedObj.transform.parent);
                    Assert.AreEqual($"{GameObjectUtility.GetActualName(request.spawnedObj).ToUpper()}_ANCHOR", request.spawnedObj.transform.parent.name);
                    if (currAsActive) Assert.AreEqual("ACTIVE_OBJECTS_POOL", request.spawnedObj.transform.parent.parent.name);
                    else Assert.AreEqual("INACTIVE_OBJECTS_POOL", request.spawnedObj.transform.parent.parent.name);
                }
                else Assert.IsNull(request.spawnedObj.transform.parent);
            }
            Assert.AreNotSame(objA, request.spawnedObj);
            Assert.IsTrue(request.spawned);
            Assert.IsFalse(request.postSpawned);
            Assert.IsFalse(request.failed);
            Assert.IsFalse(request.completed);
        }
        private void RequestOnPostSpawn(SpawnRequest request)
        {
            requestOnPostSpawnRan = true;
            Assert.IsNotNull(request);
            Assert.IsNotNull(request.spawnedObj);
            Assert.IsTrue(request.spawnedObj.activeInHierarchy == currAsActive);
            if (!currUseObjectPool) Assert.IsNull(request.spawnedObj.transform.parent);
            else
            {
                if (currIsObjectPoolManaged)
                {
                    Assert.IsNotNull(request.spawnedObj.transform.parent);
                    Assert.AreEqual($"{GameObjectUtility.GetActualName(request.spawnedObj).ToUpper()}_ANCHOR", request.spawnedObj.transform.parent.name);
                    if (currAsActive) Assert.AreEqual("ACTIVE_OBJECTS_POOL", request.spawnedObj.transform.parent.parent.name);
                    else Assert.AreEqual("INACTIVE_OBJECTS_POOL", request.spawnedObj.transform.parent.parent.name);
                }
                else Assert.IsNull(request.spawnedObj.transform.parent);
            }
            Assert.AreNotSame(objA, request.spawnedObj);
            Assert.IsTrue(request.spawned);
            Assert.IsTrue(request.postSpawned);
            Assert.IsFalse(request.failed);
            Assert.IsFalse(request.completed);
        }
        private void RequestOnCompleted(SpawnRequest request)
        {
            requestOnCompletedRan = true;
            Assert.IsNotNull(request);
            Assert.IsNotNull(request.spawnedObj);
            Assert.IsTrue(request.spawnedObj.activeInHierarchy == currAsActive);
            if (!currUseObjectPool) Assert.IsNull(request.spawnedObj.transform.parent);
            else
            {
                if (currIsObjectPoolManaged)
                {
                    Assert.IsNotNull(request.spawnedObj.transform.parent);
                    Assert.AreEqual($"{GameObjectUtility.GetActualName(request.spawnedObj).ToUpper()}_ANCHOR", request.spawnedObj.transform.parent.name);
                    if (currAsActive) Assert.AreEqual("ACTIVE_OBJECTS_POOL", request.spawnedObj.transform.parent.parent.name);
                    else Assert.AreEqual("INACTIVE_OBJECTS_POOL", request.spawnedObj.transform.parent.parent.name);
                }
                else Assert.IsNull(request.spawnedObj.transform.parent);
            }
            Assert.AreNotSame(objA, request.spawnedObj);
            Assert.IsTrue(request.spawned);
            Assert.IsTrue(request.postSpawned);
            Assert.IsFalse(request.failed);
            Assert.IsTrue(request.completed);
        }
        private void RequestFailedOnCompleted(SpawnRequest request)
        {
            requestOnCompletedRan = true;
            Assert.IsNotNull(request);
            Assert.IsNull(request.spawnedObj);
            Assert.IsFalse(request.spawned);
            Assert.IsFalse(request.postSpawned);
            Assert.IsTrue(request.failed);
            Assert.IsTrue(request.completed);
        }

        private void ResetFlags()
        {
            onSpawnRan = false;
            onPostSpawnRan = false;
            requestOnSpawnRan = false;
            requestOnPostSpawnRan = false;
            requestOnCompletedRan = false;
        }
    }
}
