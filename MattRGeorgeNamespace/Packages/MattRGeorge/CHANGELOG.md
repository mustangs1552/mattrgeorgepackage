# Changelog

Format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).
Project uses [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

To see pre-NPM hosted changelog look at: preNPMHosted_CHANGELOG.md.

## [8.1.1] - 10/6/21
### [Added]
- A Unity event to `Unity.Tools.Stats.StatGuage` that can pass the current amount as a percentage (0-1) on value changed.

## [8.1.0] - 10/3/21
### [Added]
- `Unity.Utilities.Components.SceneManagement` that has methods intended to be called from `UnityEvent`s from the inspector.

## [8.0.0] - 9/25/21
### [Added]
- New class and namespace `MattRGeorge.Unity.Utilities.Helpers.GameObjectBundle` which is primarily for object management in tests.
- New `ListUtility.RemoveNullEntries()` for dictionaries and `.RemoveNullEntriesFromDictionaryValue()` for dictionaries with a list as the value.
- New `ListOneChoiceMode` to `General.Utilities.GenericEnumerators`.
- New methods to `ListUtility` that remove null values from `Dictionary` values that are `List<T>`.
- `AssetLibrary` now has random getters for getting objects or lists like `AssetLibraryWithChances` does just without chances.
- Option to decide if `ObjectPool.RemoveObject()` removes that exact instance of the given object or allow the removal of a similar object.
- Unity Event with `GameObject` and `Transform` params.
- `TranslateObject.space` to change between local and world space that the object moves in.
- Option to reset EOL timer when calling `TranslateObject.StartMoving()`.
- Option to destroy object via object pool to `TranslateObject`.
- `ThresholdUnityEventCall.ResetAmout()` now has parameter to allow the option to reset to the start amount instead of 0.
- Variant of `ApplyForceToObject.ApplyForce()` that takes no parameter.
- New `General.Utilities.Static.MathUtiltiy`.
- A param option to use `Object.DestroyImmediate()` instead of `Object.Destroy()` to `DestroyObject.Destroy()`.
- Option to set custom git install path for `GitUtility` and auto versioning settings.
- `General.Utilties.SemanticVersion`, `.Static.FileAccessUtility`, `Unity.Utilities.Components.CameraNavigation`, `.ClickEvent`, and `.DraggableItem2D` from Diagram Editor project.
- Options to require SHIFT, CTRL, and/or ALT keys to pressed to trigger `Unity.Utilities.Components.InputControl`'s key code mode.
- Events to ObjectPoolManager.
- ObjectPoolManager can now use IObjectPoolEvents' on the object's children.
- LayerUtility that can add/remove layers from masks and check to see if a layer is in a layer mask.
- ThresholdUnityEventCall can now increase/decrease current amount by x.
- A way to add custom algorithms for incoming damage and/or heals to Health and HealthPiece.
- Documentation file for code.
- New tests for code that didn't have any where possible.
### [Changed]
- Moved `Unity.Tools.DayNightCycle.DNCAge`, `.DNCDate`, `.DNCDatestamp`, `.DNCTime`, `.DNCTimestamp`, `.Month`, and `.Season` helper classes to `DayNightCycle.Helpers` namespace.
- Moved `Unity.Utilities.Static.LayerOptions` to `Unity.Utilities.Enums`.
- Seperated these classes and enums (added to new local `Enums` namespaces) into their own files: `General.Tools.JSONObject` (class), `General.Utilities.Enums.HorizontalDirections` (enum), `General.Utilities.Enums.ListOneChoiceMode` (enum), `General.Utilities.Enums.ListRandomChoiceMode` (enum), `Unity.Utilities.Components.Enums.EOLMode` (enum), `Unity.Utilities.Components.Enums.InputType` (enum), `Unity.Utilities.Enums.UnityMethods` (enum), `Unity.Utilities.Enums.UnityStartMethods` (enum), `Unity.Utilities.Enums.UnityUpdateMethods` (enum).
- Moved `CustomUnityEvents` to `Unity.Utilities.Helpers` namespace.
- Added new dictionary focused utility helper classes that replace these classes and added to `Unity.Utilities.Helpers` namespace: `NameInt` and `DictionaryStringInt` to `DictionaryNameCount`, `NameGameObject` to `DictionaryNameGameObject`, `NameListGameObject` to `DictionaryNameListGameObject`, `GameObjectWithCount` to `DictionaryGameObjectCount`, `ObjectWithChance<T>` to `DictionaryTObjectChance<T>`, `GameObjectWithChance` to `DictionaryGameObjectChance`, and `GameObjectWithChanceList` to `DictionaryNameListDictionaryGameObjectChance`.
- Re-designing Object Spawner family started.
  - Removed all `Unity.Utilities.Components.ObjectSpawner...` and `.SpawnObject...` classes to be re-designed and re-added as either new or altered versions.
  - Added `Unity.Utilities.Components.SpawnObjectNode` and `.SpawnObjectAdvancedNode`.
- Renamed `General.Utilities.GenericEnumerators` `ListChoiceMode` to `ListRandomChoiceMode`
- Made potential performance improvements for `ListUtility.GetValidRandomObject()` and `.GetRandomObjectWithChance()`.
- `AssetLibraryWithChances.GetRandomObject()` to `.GetRandomObjectWithChance()` to allow for new `AssetLibrary.GetRandomObject()`.
- Various classes have more values made more accessible from private/protected to public or via properties.
- Various classes are now more inheritable with more virtual and protected access.
- `ObjectPoolManager.Instantiate()` will no longer try pooling from the in-active pool if asAsctive is false.
- Removed `ObjectPoolObject.toggleActiveGameObject` due to `ObjectPoolManager` behaviour, it is barely used.
- Moved `Assets.Scripts.Utilities.Components.UIRatioUpdater` and `.UITextMatcher` to `MattRGeorge.Unity.Utilities.Components`.
- `UITextMatcher.headerTextParent` and `.sizeUpHeader` are now public and removed their respective properties.
- `UITextMatcher.MacAttempts` and `.HeaderFontSizeDifference` properties now sets value to 1 as the minnimum instead of ignoring new value when lower than 1.
- `MathUtility.AreAlmostEqual()` default value for `range` is now .1 from .5.
- `TranslateObject.StartMoving()` and `.Stop()` are now virtual.
- Renamed `TranslateObjectsViaNodes` to `TranslateObjectViaNodes`.
- Updated `TranslateObjectViaNodes` according to `TranslateObject.speed` float change to `.direction` Vector3 and forced it to stay along the z-axis.
- `TranslateObject.speed` to `.direction` and made it Vector3 instead of float.
- `Timer.TimeRemaining` will now return 0 if counting up.
- `Timer.StartCountdown()` `time` param now defaults to -1 and will use `countdownTimer` when less than 0.
- `ThresholdUnityEventCall.CurrAmount` set is now protected.
- `RaycasterDetector.layerMask` now defaults to everything.
- Renamed `RaycastDetector.enableAutoMode` and coresponding property to `.auto` and `.Auto` respectivley.
- `RaycasterDetector` now uses a coroutine for auto mode instead of `InvokeRepeating()`.
- Passed `GameObject` is now used as context for `Debug.Log()` in `UnityLoggingUtility.LogMissingValue()`.
- `LookAtTransform.target` and `FollowTransform.target` are no longer considered required at start.
- `LineRenderer.Points` now returns new list of points and removes null values from new list being set.
- `LineRenderer.points` requiring 2 or more points is no longer a requirement when setting.
- `LineRenderer` will now use an existing `UnityEngine.LineRenderer` when `LineRenderer.lineRenderer` is null on start.
- Made `InputControl.InputDown`' set protected from public.
- `ApplyForceToObject.ApplyForce()` now uses coroutine to apply force continuously and its `multiplier` parameter is now manadatory.
- `ApplyForceToObject` will now use a `Rigidbody` that is on it if `ApplyForceToObject.rBody` is null.
- `CollisionDetector.backtraceLayerMask` now defaults to everything.
- `TransformUtility.CheckFOV()` now checks a FOV cone and takes in only one FOV value instead of horizontal and vertical FOV.
- `ParentChildUtility.GetAllChildrenBounds()` now includes obj passed itself.
- Location of `Unity.Utilities.Static.GitUtility` to `General.Utilities.Static`.
- ObjectPoolManager and ObjectPool now have better support for inheritance.
- ObjectPoolObject.addOnStart is now .addOn and uses UnityStartMethods enum.
- uGUITools removed from direct package and into a samples folder.
- Doors and FPS made into individual samples folders and removed Prefabs samples folder.
- All 'onAwake' bools have been changed to use UnityStartMethods enumerator and also supports Start.
- Timer now better supports inheritance.
- Renamed 'ThresholdunityEventCall' to 'ThresholdUnityEventCall'.
- Stats' events now pass thier current amount.
- Health and HealthPiece values more accessible and improved their inheritance support.
### [Fixed]
- Fixed a null exception in `ObjectPoolManager.Instantiate()`.
- Fixed bug where `ObjectPoolManager.OnObjectMadeActive()` and `.OnObjectMadeInactive()` events wouldn't be called if object was found in opposite pool when using `.AddActiveObject()` and `.AddInactiveObject()`.
- Fixed potential bug where `UITextMatcher` would keep adding to `headerTexts` when finding new headers even when already present and `headerText` is no longer modified during runtime.
- Bug where `ThresholdUnityEventCall.ThresholdProgress` wouldn't return decimal.
- `ObjectPresenceMonitor.CheckObjectIn()` and `.CheckObjectOut()` now increments/decrements if the object isn't/is present where before it was only counting and not tracking the objects that actually were checked in.
- Potential error in CoroutineUtility.
### [Removed]
- `ListUtility.RemoveNullUnityObjectEntries()` and added its functionality to `.RemoveNullEntries()`.
- `MattRGeorge.Unity.Tools.Player` and `MattRGeorge.Unity.Tools.InventorySystem` and added it to new `MattRGeorge.Player` package.
- `MattRGeorge.Unity.Objects` and added it to new `MattRGeorge.Objects` package.

## [7.1.1] - 7/20/20
### [Added]
- Events to Door for when it is opened/closed.
### [Fixed]
- CollisionDetector now resets lastPos OnEnable().
- CollisionDetector's backtracing now ignores colliders with isTrigger enabled.

## [7.1.0] - 7/10/20
### [Added]
- DelayedUnityEventCall which when triggered calls a Unity event after a delay.
- ThresholdUnityEventCall which calls a Unity event when a threshold is reached.
- GenericEnumerators with Unity method enums.

## [7.0.0] - 6/30/20
### [Added]
- A coroutine that can replace Invoke() to CoroutineUtility and added an initial delay and real time seconds options to WhileCoroutine().
- TranformUtility.CheckFOV() that can check if an object is within a given FOV, horizontal and vertical, of another object.
- ObjectPoolObject which simply adds the gameobject its attached to to the object pool system.
- ListUtility.RemoveNullUnityObjectEntries() that supports detection of destroyed Unity objects as nulls.
### [Fixed]
- Bug where ListUtility.RemoveNullEntries() wouldn't work with C# objects like strings.
### [Removed]
- AI and put it in its own package: MattRGeorgeAI.

## [6.0.0] - 6/20/20
### [Added]
- Getters and setters for some SpawnObject fields.
- A LockObject that acts as a lock and is purley a ILockable with events when it is unlocked/locked.
### [Changed]
- Door is no longer an ILockable and now uses the new LockObject.
### [Removed]
- SpawnObjectBase. Replaced by SpawnObject.

## [5.4.2] - 6/18/20
### [Added]
- Door now has ValidKeys (property added to ILockable) for required keys like Key.ValidLockableObjs.
- Door methods to link the door with a given Key.
### [Fixed]
- Key and Door now behave as expected with different states of their valid objects lists.

## [5.4.1] - 6/14/20
### [Added]
- Delayed versions of PlayerCamera.Show/HideOverlay().
- UnityEvents class that has public events that get called in various Unity methods like Awake() and Update().
### [Removed]
- PortfolioSite classes.

## [5.4.0] - 6/14/20
### [Added]
- RotateObject that simply rotates the object its attached to.
- PlayerCamera now locks and hides mouse cursor on start.
- PlayerCamera can now show user an overlay for a HUD or loading screen.
- Events for enabling/disbaling FPS player input.
- UnityEvent with a bool parameter.
### [Fixed]
- Spelling of 'event' in StringUnityEvent;

## [5.3.1] - 6/1/20
### [Added]
- Force option to ILockables.Lock/Unlock().
### [Changed]
- Made Key.validLockableObjs public and named Key.ValidLockableObjs.

## [5.3.0] - 5/31/20
### [Added]
- Rotating door.
- Lockables that can be unlocked/locked by the new Key object.
### [Changed]
- Doors no longer move via code. They now use animators and movement settings are edited in the animations.

## [5.2.3] - 5/23/20
### [Added]
- Support for having default weapons on WeaponManager.
- ShootableWeapon can now have unlimited ammo where it ignores incoming ammo amounts and always reloads to full. WeaponManager will also not remove any ammo from the inventory when reloading an unlimited ammo weapon.

## [5.2.1] - 5/22/20
### [Fixed]
- Bug with null objects in FPSPlayerWithWeapons inventory.

## [5.2.0] - 5/22/20
### [Added]
- PlayerCamera for camera controls and PlayerLocomtion for movement controls.
- InputMouseControl to monitor and pass the mouse position delta to whatever is listening to its event.
- A new UnityEvent with a Vector2 parameter.
- New updated FPS player prefabs.
### [Changed]
- Weapon now needs a list of colliders. Will get colliders in children in list is null.

## [5.1.19] - 4/25/20
### [Added]
- Ability to pass -1 to the amount parameter of SpawnObjects methods in ObjectSpawner, ObjectSpawnerViaAssetLibrary, SpawnObjectsAuto, and SpawnObjectsViaAssetLibraryAuto which will allow to spawn up to set maxCount or at all nodes if allowSameNodeSpawn is false.
### [Fixed]
- Using MaxCount as the amount to spawn won't spawn anything if set to -1.

## [5.1.18] - 4/25/20
### [Added]
- MaxCount access to ObjectSpawner, ObjectSpawnerViaAssetLibrary, SpawnObjectsAuto, and SpawnObjectsViaAssetLibraryAuto.

## [5.1.17] - 4/23/20
### [Added]
- Options to include the pre-release and/or build version to dev builds only.
- Tooltips for Auto Versioning Settings window options.
### [Fixed]
- Warning about not using variable 'e' in GitUtility.
- ApplyVersionToPackage no longer inherites from Monobehaviour which it shouldn't have.

## [5.1.14] - 4/21/20
### [Added]
- A coroutine utility with a coroutine wrapped while loop method.

## [5.1.13] - 4/20/20
### [Added]
- Ability to append pre-release and build metadata as per Semantic Versioning (package won't support build due to them not having builds).

## [5.1.11] - 4/19/20
### [Added]
- Ability to toggle auto versioning via platform for bundle version to Auto Versioning Window.
- Package versioning settings to Auto Versioning Window.

## [5.1.10] - 4/18/20
### [Added]
- Auto Versioning Window in ``Window/Auto Versioning Settings`` that can toggle auto versioning for project's version on editor start/build and bundle version increment on build.
### [Changed]
- JSONObject.ToJSON() now sets converted values to lowercase if not strings.
- GitUtility now has a file path to git (ran into missing git errors).

## [5.1.6] - 4/17/20
### [Fixed]
- Errors that could happen if process fails when getting repo based version.

## [5.1.5] - 4/16/20
### [Added]
- TransformManipulator that offers methods for manipulating a Transform.

## [5.1.3] - 4/16/20
### [Added]
- LineRendererCaster that allows you to set Transforms as points instead of Vector3's.

## [5.1.2] - 4/15/20
### [Added]
- Ability for FollowTransform to only affect certain axis.
- LookAtTransform that simply uses Transform.LookAt().

## [5.1.1] - 4/15/20
### [Added]
- A simple component that matches the position and rotation of a target transform.

## [5.1.0] - 4/6/20
### [Added]
- Methods to JSONObject that get converted versions of JSON entries without the need to know what it can be converted to.
- Support for JSONObject.GetValueRootArray() to return mixed converted values.
### [Changed]
- ConversionUtility.TryConvertValue() no longer returns strings with "" inside as that was JSONParser specific.
### [Removed]
- Scripts folder. Runtime, Editor, and Tests folders are now in the package root.
- Removed preNPMHosted_CHANGELOG.md from the package.
- The JSONObject methods that got and returned converted versions of JSON entries.

## [5.0.0] - 4/5/20
### [Added]
- ThrowableWeapon base class with basic throwable behaviour.
- New sample assets for FPS controller and sample weapons and related items.
### [Changed]
- Inventory system to use actual objects rather than counts.
- Ammo functionality to work better and support storing and usage of both raw ammo and ammo in packs.
- Inventory.allowDuplicateSlots is now allowNewDuplicateSLots and is now public.

## [4.0.0] - 3/19/20
### [Added]
- Ability to drop a weapon from the WeaponManager by Weapon.
- Pickup/Drop events for WeaponManager when weapons are pickedup/dropped.
### [Changed]
- How Weapon pickingup/dropping works. WeaponManager now simply calls the Pickup/Drop methods on the Weapon and the Weapon calls what it needs to. This now supports pickingup/dropping weapons from WeaponManager or Weapon.
- Weapon.SetPickedupState() to protected from public.
### [Removed]
- WeaponManager.CheckCanDropWeapon().

## [3.0.1] - 3/18/20
### [Added]
- ApplyForceToObject component that behaves similarly to TranslateObject.
- UnityLoggingUtility.LogMissingValue() that posts an error message to inform user of a missing required value.

## [3.0.0] - 3/17/20
### [Added]
- New base AssetLibrary class that has a list of named object values and list of named object lists.
- New NameListGameObject general helper class for named lists of objects and a to dictionary method
- A to dictionary method to NameGameObject.
### [Changed]
- Locations of PickupableItem and UsableItem from InventorySystem to Player in both namespace and file location.
- Name of PlayerControls to PlayerManager and now inherits from the new AssetLibrary base class.
- Name of previous AssetLibrary to AssetLibraryWithChances which inherites from new AssetLibrary base class.
- IUsable Use() method parameter is now PlayerManager instead of GameObject.
- Projectile user is now PlayerManager instead of GameObject.
- Weapon now uses PlayerManager's inherited AssetLibrary to find desired WeaponManager allowing the ability to have multiple WeaponManagers managing different types of weapons.
### [Removed]
- Second and third parameters of TakeDamage() and Heal() for ITakesDamage.
### [Fixed]
- Discovered bug where the source object and source user of Projectile.OnSpawn() was flipped when calling it from ShootableWeapon.

## [2.0.3] - 3/14/20
### [Added]
- The ability to select no weapon to WeaponManager.

## [2.0.2] - 3/13/20
### [Added]
- The ability to not allow more than one slot with the same item in Inventory.
- Ability to switch to previously selected weapon.
### [Fixed]
- Some discovered bugs related to switching to a weapon that is the same.

## [2.0.1] - 3/12/20
### [Added]
- The ability for the player to pickup/drop ammo via a new AmmoPack.
### [Fixed]
- Bug where weapons were not being dropped.

## [2.0.0] - 3/11/20
### [Added]
- Backtracing to CollisionDetector to improve collision detection on fast moving objects that may have skipped collisions.
- TranslateObjectViaNodes off of TranslateObject that simply adds ability to move an object between nodes in the world.
- DestroyObject that has a destroy method that can be called from an inspector event and can be toggled between Unity destroy and object pool destroy.
- ConversionUtility() with methods that can convert and/or check a string to a primitive type.
- JSONObject.ToJSON() to convert it to JSON text.
- Auto versioning tools using git tags and commits to generate a version and apply it to the project version and can also apply it to a package.json.
- A health stat system.
- PickupableItem and UsableItem which inherites from Item.
- A weapon system.
- TranslateObject which simply translates an object down its z-axis.
- Started adding ToJSON() support for JSONParser.
### [Changed]
- Folder name and namespace of MattRGeorge.General.Misc to .Tools.
- Folder name and namespace of MattRGeorge.Unity.Tools.Misc to .Stats.
- Location of MattRGeorge.General.Enums.GenericEnumerators to MattRGeorge.General.Utilities.
- Location of MattRGeorge.General.Utilities.ReflectionUtility to MattRGeorge.General.Utilities.Static.
- Item.ItemName now returns actual name without any "(clone)"s.
### [Removed]
- MattRGeorge.General.Enums
### [Fixed]
- Couple bugs with Inventory.

## [1.4.2] - 3/7/20
### [Changed]
- Added option to allow spawning more than one objects at the same node to ObjectSpawner and ObjectSpawnerViaAssetLibrary.
- Added OnSpawnFailed() to SpawnObjectBase that can be overrided and is called when the Spawn() coroutine fails to spawn an object.
- Added pending objects count to SpawnObjectBase so you can tell if there are objects still waiting to be spawned in the spawn coroutine.

## [1.4.1] - 3/7/20
### [Changed]
- Inheritance design of SpawnObject family. SpawnObjects and ObjectSpawner inheritance level are now responsible for max counts instead of auto versions.

## [1.4.0] - 3/6/20
### [Added]
- ObjectSpawnerViaAssetLibrary and an auto version with total count limit and per list name count limit.
- Base abstract classes for SpawnObject and ObjectSpawner.
- New methods to AssetLibrary that allows returning the list name that was used.
### [Changed]
- ObjectSpawnerAuto.OnValidate() to protected virtual.
- AssetLibrary list names are now case sensitive.
- Renamed SpawnObjectsAuto.Spawn() to SpawnObject() to match other classes.
### [Fixed]
- Fixed a bug where some SpawnObject classes were not allowing the unlimited option for maxCount.

## [1.3.0] - 3/4/20
### [Added]
- An ObjectSpawner and an auto version which are node based spawners using SpawnObject as the node.
- MIT License.
- New method to ListUtility that can work with different modes for selecting objects from a list and is able to continue past the limits of the invalid indexes list.
- SpawnObject that is now the base class of all SpawnObject classes.
- A logger class that simply allows for calling Debug.Log...() methods from inspector events.
### [Changed]
- SpawnObject family of classes inheritance design.
### [Removed]
- ObjectPool prefab from samples.

## [1.2.2] - 3/1/20
### [Added]
- Static method to convert List<GameObjectWithChance> to List<ObjectWithChance<GameObject>> on GameObjectWithChanceList.

## [1.2.1] - 3/1/20
### [Changed]
- AssetLibrary to better support inheritance.
- Moved private methods for converting GameObjectWithChance (not static) and GameObjectWithChanceList (static) to thier respected helper classes.

## [1.2.0] - 3/1/20
### [Added]
- ListUtility.RemoveNullEntries() which removes all null values from the given list.
- A new family of classes for SpawnObject (SpawnObjects, SpawnObjectAuto (previously SpawnObject), SpawnObjectsAuto, and SpawnObjectsViaAssetLibrary).
- New method to AssetLibrary to include more than one list of objects when choosing an object.
### [Changed]
- MattRGeorge.Unity.Utilities.Components to .Utilities.Static and MattRGeorge.Unity.Utilities.Actions to .Utilities.Components.
- UITextMatcher and UIRatioUpdater to MattRGeorge.Unity.Utilities.Components.
- UIUtilities to MattRGeorge.Unity.Utilities.Static.
- UITextMatcher now using UnityEvent instead of Action for its event.
- Name of GameObjectUtilities and UIUtilities to ...Utility to match the rest.
- SpawnObject design to support multiple kinds of SpawnObject classes (Original is now SpawnObjectAuto).

## [1.1.0] - 2/29/20
### [Added]
- The AssetLibrary to keep lists of assets used by other objects and can be accessed randomly.
- A RaycasterDetector that can check for a raycast result either manually or automatically.
- OnValidate() to all necessary objects.
### [Changed]
- Location of helper classes to HelperClasses.
- All classes to follow new coding habits.

## [1.0.1] - 2/28/20
### [Added]
- Second changelog with old versions from before NPM hosted release.

## [1.0.0] - 2/28/20
- Initial NPM release.