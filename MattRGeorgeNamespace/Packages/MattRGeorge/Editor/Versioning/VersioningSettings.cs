﻿using System.IO;
using UnityEngine;

namespace MattRGeorge.Editor.Versioning
{
    public class VersioningSettings
    {
        public string gitInstallLocation = "";
        public bool addPrereleaseText = false;
        public bool addPrereleaseTextWithDevBuild = false;
        public string prereleaseText = "preview";
        public bool addBuildText = false;
        public bool addBuildTextWithDevBuild = false;
        public bool useBundleVersionForBuild = true;
        public string buildText = "";
        public bool projVerUpdateStart = false;
        public bool projVerUpdateBuild = false;
        public bool bundleVerUpdateBuild = false;
        public bool androidBundleVerUpdateBuild = true;
        public string packageName = "";
        public bool packageVerUpdateStart = false;
        public bool packageVerUpdateBuild = false;

        /// <summary>
        /// The full file path and name of the settings file.
        /// </summary>
        public string FullPath => Path.Combine(filePath, fileName);

        private const string filePath = "Assets/Resources/";
        private const string fileName = "versioningSettings.json";
        VersioningSettings readSettings = null;

        /// <summary>
        /// Loads the values from a JSON file.
        /// </summary>
        public void LoadFromJSONFile()
        {
            if (!File.Exists(FullPath)) return;

            string json = File.ReadAllText(FullPath);
            if (string.IsNullOrEmpty(json)) return;

            readSettings = JsonUtility.FromJson<VersioningSettings>(json);
            if (readSettings == null) return;

            gitInstallLocation = readSettings.gitInstallLocation;
            addPrereleaseText = readSettings.addPrereleaseText;
            addPrereleaseTextWithDevBuild = readSettings.addPrereleaseTextWithDevBuild;
            prereleaseText = readSettings.prereleaseText;
            addBuildText = readSettings.addBuildText;
            addBuildTextWithDevBuild = readSettings.addBuildTextWithDevBuild;
            useBundleVersionForBuild = readSettings.useBundleVersionForBuild;
            buildText = readSettings.buildText;
            projVerUpdateStart = readSettings.projVerUpdateStart;
            projVerUpdateBuild = readSettings.projVerUpdateBuild;
            bundleVerUpdateBuild = readSettings.bundleVerUpdateBuild;
            androidBundleVerUpdateBuild = readSettings.androidBundleVerUpdateBuild;
            packageName = readSettings.packageName;
            packageVerUpdateStart = readSettings.packageVerUpdateStart;
            packageVerUpdateBuild = readSettings.packageVerUpdateBuild;
        }
        /// <summary>
        /// Saves the values to a JSON file.
        /// </summary>
        public void SaveToJSONFile()
        {
            if (!Directory.Exists(filePath)) Directory.CreateDirectory(filePath);

            string json = JsonUtility.ToJson(this);
            File.WriteAllText(FullPath, json);
        }
    }
}
