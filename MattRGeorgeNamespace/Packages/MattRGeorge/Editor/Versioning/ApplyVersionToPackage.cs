﻿using System.IO;
using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using UnityEngine;
using MattRGeorge.Unity.Utilities.Static;
using MattRGeorge.General.Tools;

namespace MattRGeorge.Editor.Versioning
{
    public class ApplyVersionToPackage : IPreprocessBuildWithReport
    {
        public virtual int callbackOrder => 0;

        /// <summary>
        /// Apply the version to the package.json of the package from the given file path.
        /// </summary>
        /// <param name="packagePath">The path of the package where the package.json file is located. Uses package name from Versioning Settings Window if null or empty.</param>
        [MenuItem("Versioning/Apply Git Version to Package")]
        protected static void ApplyGitVersionToPackage()
        {
            if (string.IsNullOrEmpty(VersioningEditorWindow.PackageName)) return;
            string packagePath = Path.Combine($"{Application.dataPath.Replace("/Assets", @"/Packages")}/{VersioningEditorWindow.PackageName}");

            if (string.IsNullOrEmpty(packagePath)) return;
            string version = Versioning.BuildVersionWithPrerelease;
            if (string.IsNullOrEmpty(version)) return;

            string packageJSONPath = packagePath + ((packagePath[packagePath.Length - 1] == '\\') ? "" : @"\") + "package.json";
            if (File.Exists(packageJSONPath))
            {
                string packageText = File.ReadAllText(packageJSONPath);
                JSONObject parsedJSON = JSONParser.ParseJSON(packageText);
                if (parsedJSON.values["version"] == version) return;

                parsedJSON.values["version"] = version;
                File.WriteAllText(packageJSONPath, parsedJSON.ToJSON());
            }
        }

        [InitializeOnLoadMethod]
        public static void OnEditorStart()
        {
            if (VersioningEditorWindow.PackageVerUpdateStart) ApplyGitVersionToPackage();
        }
        public void OnPreprocessBuild(BuildReport report)
        {
            if (VersioningEditorWindow.PackageVerUpdateBuild) ApplyGitVersionToPackage();
        }
    }
}