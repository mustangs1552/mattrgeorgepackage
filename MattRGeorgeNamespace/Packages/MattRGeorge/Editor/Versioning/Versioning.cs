﻿using UnityEditor;
using UnityEditor.Build;
using UnityEditor.Build.Reporting;
using MattRGeorge.General.Utilities.Static;

namespace MattRGeorge.Editor.Versioning
{
    public class Versioning : IPreprocessBuildWithReport, IPostprocessBuildWithReport
    {
        public int callbackOrder => 0;

        private static bool devBuildInProgress = false;

        /// <summary>
        /// The build version from git.
        /// </summary>
        public static string BuildVersion
        {
            get
            {
                string ver = GitUtility.BuildVersion;
                if (string.IsNullOrEmpty(ver)) return "";

                return ver;
            }
        }
        /// <summary>
        /// The build version from git with pre-release appended.
        /// </summary>
        public static string BuildVersionWithPrerelease
        {
            get
            {
                string buildVersion = BuildVersion;
                if (string.IsNullOrEmpty(buildVersion)) return "";

                return buildVersion + PrereleaseSection;
            }
        }
        /// <summary>
        /// The full build version from git with pre-release and build appended.
        /// </summary>
        public static string BuildVersionWithPrereleaseAndBuild
        {
            get
            {
                string buildVersion = BuildVersion;
                if (string.IsNullOrEmpty(buildVersion)) return "";

                return buildVersion + PrereleaseSection + BuildSection;
            }
        }

        /// <summary>
        /// The pre-release section with required '-' prefixed.
        /// </summary>
        private static string PrereleaseSection
        {
            get
            {
                if ((VersioningEditorWindow.AddPrereleaseText || (VersioningEditorWindow.AddPrereleaseTextWithDevBuild && devBuildInProgress)) && !string.IsNullOrEmpty(VersioningEditorWindow.PrereleaseText)) return $"-{VersioningEditorWindow.PrereleaseText}";

                return "";
            }
        }
        /// <summary>
        /// The build section with required '+' prefixed.
        /// </summary>
        private static string BuildSection
        {
            get
            {
                if ((VersioningEditorWindow.AddBuildText || (VersioningEditorWindow.AddBuildTextWithDevBuild && devBuildInProgress)))
                {
#if UNITY_ANDROID
                    if (VersioningEditorWindow.UseBundleVersionForBuild) return $"+{PlayerSettings.Android.bundleVersionCode}";
                    else if (!string.IsNullOrEmpty(VersioningEditorWindow.BuildText)) return $"+{VersioningEditorWindow.BuildText}";
#else
                    if (VersioningEditorWindow.UseBundleVersionForBuild && !string.IsNullOrEmpty(VersioningEditorWindow.BuildText)) return $"+{VersioningEditorWindow.BuildText}";
#endif
                }

                return "";
            }
        }

        /// <summary>
        /// Applies the auto version from git to the project's version in Project Settings.
        /// </summary>
        [MenuItem("Versioning/Apply Git Version to Project Settings")]
        public static void ApplyGitVersionToProjectSettings()
        {
            string ver = BuildVersionWithPrereleaseAndBuild;
            if (string.IsNullOrEmpty(ver)) return;

            PlayerSettings.bundleVersion = ver;
        }

        /// <summary>
        /// Increment the bundle code version by platform.
        /// </summary>
        /// <param name="platform">The built platform.</param>
        public static void IncrementBundleVersionCodes(BuildTarget platform)
        {
            if (platform == BuildTarget.Android && VersioningEditorWindow.AndroidBundleVerUpdateBuild) PlayerSettings.Android.bundleVersionCode += 1;
        }

        [InitializeOnLoadMethod]
        public static void OnEditorStart()
        {
            if (VersioningEditorWindow.ProjVerUpdateStart) ApplyGitVersionToProjectSettings();
        }

        public void OnPreprocessBuild(BuildReport report)
        {
            devBuildInProgress = report.summary.options.HasFlag(BuildOptions.Development);
            if (VersioningEditorWindow.ProjVerUpdateBuild) ApplyGitVersionToProjectSettings();
            if (VersioningEditorWindow.BundleVerUpdateBuild) IncrementBundleVersionCodes(report.summary.platform);
        }
        public void OnPostprocessBuild(BuildReport report)
        {
            devBuildInProgress = false;
            if (!VersioningEditorWindow.AddBuildText && VersioningEditorWindow.AddPrereleaseTextWithDevBuild) ApplyGitVersionToProjectSettings();
        }
    }
}