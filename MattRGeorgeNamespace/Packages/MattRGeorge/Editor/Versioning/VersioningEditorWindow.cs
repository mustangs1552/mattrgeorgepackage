﻿using System.Text.RegularExpressions;
using UnityEditor;
using UnityEngine;
using MattRGeorge.General.Utilities.Static;

namespace MattRGeorge.Editor.Versioning
{
    public class VersioningEditorWindow : EditorWindow
    {
        public static VersioningEditorWindow window = null;
        public static VersioningSettings settings = null;

        /// <summary>
        /// The Git install location to use when interacting with Git.
        /// </summary>
        public static string GitInstallLocation
        {
            get => settings.gitInstallLocation;
            set
            {
                if (GitUtility.SetInstallLocation(value))
                {
                    settings.gitInstallLocation = value;
                    SaveSettings();
                }
            }
        }

        /// <summary>
        /// Append the set pre-release text to the version?
        /// </summary>
        public static bool AddPrereleaseText
        {
            get
            {
                if (settings == null) LoadSettings();
                return settings.addPrereleaseText;
            }
            set
            {
                if (settings == null)
                {
                    LoadSettings();
                    settings.addPrereleaseText = value;
                    SaveSettings();
                }
                else if (value != settings.addPrereleaseText)
                {
                    settings.addPrereleaseText = value;
                    SaveSettings();
                }
            }
        }
        /// <summary>
        /// Append the set pre-release text to the version of a dev build when one is started?
        /// </summary>
        public static bool AddPrereleaseTextWithDevBuild
        {
            get
            {
                if (settings == null) LoadSettings();
                return settings.addPrereleaseTextWithDevBuild;
            }
            set
            {
                if (settings == null)
                {
                    LoadSettings();
                    settings.addPrereleaseTextWithDevBuild = value;
                    SaveSettings();
                }
                else if (value != settings.addPrereleaseTextWithDevBuild)
                {
                    settings.addPrereleaseTextWithDevBuild = value;
                    SaveSettings();
                }
            }
        }
        /// <summary>
        /// The dot separated pre-release text to append to the version.
        /// </summary>
        public static string PrereleaseText
        {
            get
            {
                if (settings == null) LoadSettings();
                return settings.prereleaseText;
            }
            set
            {
                Regex regex = new Regex("[0-9A-Za-z-]");

                if (settings == null)
                {
                    LoadSettings();
                    settings.prereleaseText = value;
                    SaveSettings();
                }
                else if (value != settings.prereleaseText && (regex.IsMatch(value) || value == ""))
                {
                    settings.prereleaseText = value;
                    SaveSettings();
                }
            }
        }

        /// <summary>
        /// Append the build text to the version?
        /// </summary>
        public static bool AddBuildText
        {
            get
            {
                if (settings == null) LoadSettings();
                return settings.addBuildText;
            }
            set
            {
                if (settings == null)
                {
                    LoadSettings();
                    settings.addBuildText = value;
                    SaveSettings();
                }
                else if (value != settings.addBuildText)
                {
                    settings.addBuildText = value;
                    SaveSettings();
                }
            }
        }
        /// <summary>
        /// Append the set build text to the version of a dev build when one is started?
        /// </summary>
        public static bool AddBuildTextWithDevBuild
        {
            get
            {
                if (settings == null) LoadSettings();
                return settings.addBuildTextWithDevBuild;
            }
            set
            {
                if (settings == null)
                {
                    LoadSettings();
                    settings.addBuildTextWithDevBuild = value;
                    SaveSettings();
                }
                else if (value != settings.addBuildTextWithDevBuild)
                {
                    settings.addBuildTextWithDevBuild = value;
                    SaveSettings();
                }
            }
        }
        /// <summary>
        /// Use the platform bundle version code as the build text?
        /// </summary>
        public static bool UseBundleVersionForBuild
        {
            get
            {
                if (settings == null) LoadSettings();
                return settings.useBundleVersionForBuild;
            }
            set
            {
                if (settings == null)
                {
                    LoadSettings();
                    settings.useBundleVersionForBuild = value;
                    SaveSettings();
                }
                else if (value != settings.useBundleVersionForBuild)
                {
                    settings.useBundleVersionForBuild = value;
                    SaveSettings();
                }
            }
        }
        /// <summary>
        /// The dot separated build text to append to the version.
        /// </summary>
        public static string BuildText
        {
            get
            {
                if (settings == null) LoadSettings();
                return settings.buildText;
            }
            set
            {
                Regex regex = new Regex("[0-9A-Za-z-]");

                if (settings == null)
                {
                    LoadSettings();
                    settings.buildText = value;
                    SaveSettings();
                }
                else if (value != settings.buildText && (regex.IsMatch(value) || value == ""))
                {
                    settings.buildText = value;
                    SaveSettings();
                }
            }
        }

        /// <summary>
        /// Allow the change of the project's version in the Project Settings window when the editor is opened?
        /// </summary>
        public static bool ProjVerUpdateStart
        {
            get
            {
                if (settings == null) LoadSettings();
                return settings.projVerUpdateStart;
            }
            set
            {
                if (settings == null)
                {
                    LoadSettings();
                    settings.projVerUpdateStart = value;
                    SaveSettings();
                }
                else if (value != settings.projVerUpdateStart)
                {
                    settings.projVerUpdateStart = value;
                    SaveSettings();
                }
            }
        }
        /// <summary>
        /// Allow the change of the project's version in the Project Settings window when a new build is started?
        /// </summary>
        public static bool ProjVerUpdateBuild
        {
            get
            {
                if (settings == null) LoadSettings();
                return settings.projVerUpdateBuild;
            }
            set
            {
                if (settings == null)
                {
                    LoadSettings();
                    settings.projVerUpdateBuild = value;
                    SaveSettings();
                }
                else if (value != settings.projVerUpdateBuild)
                {
                    settings.projVerUpdateBuild = value;
                    SaveSettings();
                }
            }
        }

        /// <summary>
        /// Allow the change of the bundle version in the Project Settings window when a new build is started?
        /// </summary>
        public static bool BundleVerUpdateBuild
        {
            get
            {
                if (settings == null) LoadSettings();
                return settings.bundleVerUpdateBuild;
            }
            set
            {
                if (settings == null)
                {
                    LoadSettings();
                    settings.bundleVerUpdateBuild = value;
                    SaveSettings();
                }
                else if (value != settings.bundleVerUpdateBuild)
                {
                    settings.bundleVerUpdateBuild = value;
                    SaveSettings();
                }
            }
        }
        /// <summary>
        /// Allow the change of the bundle version in the Project Settings window when a new Android build is started?
        /// </summary>
        public static bool AndroidBundleVerUpdateBuild
        {
            get
            {
                if (settings == null) LoadSettings();
                return settings.androidBundleVerUpdateBuild;
            }
            set
            {
                if (settings == null)
                {
                    LoadSettings();
                    settings.androidBundleVerUpdateBuild = value;
                    SaveSettings();
                }
                else if (value != settings.androidBundleVerUpdateBuild)
                {
                    settings.androidBundleVerUpdateBuild = value;
                    SaveSettings();
                }
            }
        }

        /// <summary>
        /// The name of the package folder to manage the version off.
        /// </summary>
        public static string PackageName
        {
            get
            {
                if (settings == null) LoadSettings();
                return settings.packageName;
            }
            set
            {
                if (settings == null)
                {
                    LoadSettings();
                    settings.packageName = value;
                    SaveSettings();
                }
                else if (value != settings.packageName)
                {
                    settings.packageName = value;
                    SaveSettings();
                }
            }
        }
        /// <summary>
        /// Allow the change of the package's version in the package JSON file when the editor is opened?
        /// </summary>
        public static bool PackageVerUpdateStart
        {
            get
            {
                if (settings == null) LoadSettings();
                return settings.packageVerUpdateStart;
            }
            set
            {
                if (settings == null)
                {
                    LoadSettings();
                    settings.packageVerUpdateStart = value;
                    SaveSettings();
                }
                else if (value != settings.packageVerUpdateStart)
                {
                    settings.packageVerUpdateStart = value;
                    SaveSettings();
                }
            }
        }
        /// <summary>
        /// Allow the change of the package's version in the package JSON file when a new build is started?
        /// </summary>
        public static bool PackageVerUpdateBuild
        {
            get
            {
                if (settings == null) LoadSettings();
                return settings.packageVerUpdateBuild;
            }
            set
            {
                if (settings == null)
                {
                    LoadSettings();
                    settings.packageVerUpdateBuild = value;
                    SaveSettings();
                }
                else if (value != settings.packageVerUpdateBuild)
                {
                    settings.packageVerUpdateBuild = value;
                    SaveSettings();
                }
            }
        }

        private static string gitInstallLocation = "";

        #region Methods
        /// <summary>
        /// Load the file that stores the editor window's values.
        /// </summary>
        public static void LoadSettings()
        {
            if (settings == null) settings = new VersioningSettings();
            settings.LoadFromJSONFile();
            
            GitUtility.GitInstallLocation = settings.gitInstallLocation;
            gitInstallLocation = settings.gitInstallLocation;
        }
        /// <summary>
        /// Save the editor window's values to file.
        /// </summary>
        public static void SaveSettings()
        {
            if (settings == null) settings = new VersioningSettings();
            settings.SaveToJSONFile();
        }

        /// <summary>
        /// Draws the global/general settings section.
        /// </summary>
        private void DrawGlobalSettings()
        {
            GUILayout.Label(new GUIContent("Git Settings"), EditorStyles.boldLabel);
            GUILayout.Space(5);

            GUILayout.Label(new GUIContent("Git Install Location", "The path where Git install folder is located."));
            GUILayout.BeginHorizontal();
            gitInstallLocation = EditorGUILayout.TextField(gitInstallLocation);
            if (GUILayout.Button("Check Valid Path"))
            {
                GitInstallLocation = gitInstallLocation;
                Repaint();
            }
            GUILayout.EndHorizontal();
            GUILayout.Label($"Current Path: {GitUtility.GitInstallLocation}");
        }
        /// <summary>
        /// Draw the section of the editor window that manages the project's versions.
        /// </summary>
        private void DrawProjectVersionSettings()
        {
            GUILayout.Label(new GUIContent("Project Versioning Settings"), EditorStyles.boldLabel);
            GUILayout.Space(5);
            ProjVerUpdateStart = EditorGUILayout.Toggle(new GUIContent("Update On Editor Start", "Update the project version when the editor is started?"), ProjVerUpdateStart);
            ProjVerUpdateBuild = EditorGUILayout.Toggle(new GUIContent("Update On Editor Build", "Update the project version when the a build is started?"), ProjVerUpdateBuild);
        }
        /// <summary>
        /// Draw the section of the editor window that manages the project's bundle versions.
        /// </summary>
        private void DrawBundleVersionSettings()
        {
            GUILayout.Label(new GUIContent("Bundle Versioning Settings"), EditorStyles.boldLabel);
            GUILayout.Space(5);
            BundleVerUpdateBuild = EditorGUILayout.Toggle(new GUIContent("Update Bundle Version Code On Editor Build", "Increment the bundle version code when a build is started?"), BundleVerUpdateBuild);
            
            GUILayout.Space(5);
            GUILayout.Label(new GUIContent("Platforms"), EditorStyles.boldLabel);
            AndroidBundleVerUpdateBuild = EditorGUILayout.Toggle(new GUIContent("Android", "Update for Android builds?"), AndroidBundleVerUpdateBuild);
        }
        /// <summary>
        /// Draw the section of the editor window that manages the package's version.
        /// </summary>
        private void DrawPackageSettings()
        {
            GUILayout.Label(new GUIContent("Package Versioning Settings"), EditorStyles.boldLabel);
            GUILayout.Space(5);

            GUILayout.Label(new GUIContent("Package Name", "The name of the package's folder to apply the version to its 'package.json' file."));
            PackageName = EditorGUILayout.TextField(PackageName);

            PackageVerUpdateStart = EditorGUILayout.Toggle(new GUIContent("Update Package Version On Editor Start", "Update the package version when the editor is started?"), PackageVerUpdateStart);
            PackageVerUpdateBuild = EditorGUILayout.Toggle(new GUIContent("Update Package Version On Editor Build", "Update the package version when a build is started?"), PackageVerUpdateBuild);
        }
        /// <summary>
        /// Draw the section of the editor window that manage's the pre-release and build suffixes options.
        /// </summary>
        private void DrawPrereleaseAndBuildSettings()
        {
            GUILayout.Label(new GUIContent("Pre-release and Build Settings"), EditorStyles.boldLabel);
            GUILayout.Space(5);

            AddPrereleaseText = EditorGUILayout.Toggle(new GUIContent("Add Pre-release Text", "Add pre-release text when getting version?"), AddPrereleaseText);
            AddPrereleaseTextWithDevBuild = EditorGUILayout.Toggle(new GUIContent("Add Pre-release Text to Development Builds", "Add pre-release text to builds with 'Development Build' enabled (will only be applied to the build and not the project)?"), AddPrereleaseTextWithDevBuild);
            GUILayout.Label(new GUIContent("Pre-release Text", "The dot seperated pre-release text to append to the version."));
            PrereleaseText = EditorGUILayout.TextField(PrereleaseText);

            AddBuildText = EditorGUILayout.Toggle(new GUIContent("Add Build Text", "Add build text when getting version?"), AddBuildText);
            AddBuildTextWithDevBuild = EditorGUILayout.Toggle(new GUIContent("Add Build Text to Development Builds", "Add build text to builds with 'Development Build' enabled (will only be applied to the build and not the project)?"), AddBuildTextWithDevBuild);
            UseBundleVersionForBuild = EditorGUILayout.Toggle(new GUIContent("Use Bundle Code for Build Text", "Use the bundle code for the build text or the set build text?"), UseBundleVersionForBuild);
            GUILayout.Label(new GUIContent("Build Text", "The dot seperated build text to append to the version."));
            BuildText = EditorGUILayout.TextField(BuildText);
        }
        #endregion

        private void OnGUI()
        {
            EditorGUIUtility.labelWidth = 300;

            GUILayout.Space(5);
            DrawGlobalSettings();
            GUILayout.Space(20);
            DrawProjectVersionSettings();
            GUILayout.Space(20);
            DrawBundleVersionSettings();
            GUILayout.Space(20);
            DrawPackageSettings();
            GUILayout.Space(20);
            DrawPrereleaseAndBuildSettings();
        }
        private void OnFocus()
        {
            LoadSettings();
        }

        [MenuItem("Window/Auto Versioning Settings")]
        public static void Init()
        {
            window = (VersioningEditorWindow)GetWindow(typeof(VersioningEditorWindow), false, "Auto Versioning Settings");
            LoadSettings();
        }
    }
}
