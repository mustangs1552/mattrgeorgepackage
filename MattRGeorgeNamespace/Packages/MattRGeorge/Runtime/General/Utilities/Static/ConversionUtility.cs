﻿using System;

namespace MattRGeorge.General.Utilities.Static
{
    public static class ConversionUtility
    {
        /// <summary>
        /// Try to convert the given string value to a primitive value.
        /// If not then it will return the string.
        /// Check order: byte, sbyte, short, ushort, int, uint, long, ulong, float, double, decimal, bool.
        /// </summary>
        /// <param name="str">The string to be converted.</param>
        /// <returns>The converted value.</returns>
        public static object TryConvertValue(string str)
        {
            Type temp = null;
            return TryConvertValue(str, out temp);
        }
        /// <summary>
         /// Try to convert the given string value to a primitive value.
         /// If not then it will return the string.
         /// Check order: byte, sbyte, short, ushort, int, uint, long, ulong, float, double, decimal, bool.
         /// </summary>
         /// <param name="str">The string to be converted.</param>
         /// <param name="convertableType">The convertable type.</param>
         /// <returns>The converted value.</returns>
        public static object TryConvertValue(string str, out Type convertableType)
        {
            if (string.IsNullOrEmpty(str))
            {
                convertableType = null;
                return str;
            }

            if (CanConvertToPrimitiveType(str, out convertableType))
            {
                if (convertableType == typeof(byte))
                {
                    byte byteResult = 0;
                    byte.TryParse(str, out byteResult);
                    return byteResult;
                }
                else if (convertableType == typeof(sbyte))
                {
                    sbyte sbyteResult = 0;
                    sbyte.TryParse(str, out sbyteResult);
                    return sbyteResult;
                }
                else if (convertableType == typeof(short))
                {
                    short shortResult = 0;
                    short.TryParse(str, out shortResult);
                    return shortResult;
                }
                else if (convertableType == typeof(ushort))
                {
                    ushort ushortResult = 0;
                    ushort.TryParse(str, out ushortResult);
                    return ushortResult;
                }
                else if (convertableType == typeof(int))
                {
                    int intResult = 0;
                    int.TryParse(str, out intResult);
                    return intResult;
                }
                else if (convertableType == typeof(uint))
                {
                    uint uintResult = 0;
                    uint.TryParse(str, out uintResult);
                    return uintResult;
                }
                else if (convertableType == typeof(long))
                {
                    long longResult = 0;
                    long.TryParse(str, out longResult);
                    return longResult;
                }
                else if (convertableType == typeof(ulong))
                {
                    ulong ulongResult = 0;
                    ulong.TryParse(str, out ulongResult);
                    return ulongResult;
                }
                else if (convertableType == typeof(float))
                {
                    float floatResult = 0;
                    float.TryParse(str, out floatResult);
                    return floatResult;
                }
                else if (convertableType == typeof(double))
                {
                    double doubleResult = 0;
                    double.TryParse(str, out doubleResult);
                    return doubleResult;
                }
                else if (convertableType == typeof(decimal))
                {
                    decimal decimalResult = 0;
                    decimal.TryParse(str, out decimalResult);
                    return decimalResult;
                }
                else if (convertableType == typeof(bool))
                {
                    bool boolResult = false;
                    bool.TryParse(str, out boolResult);
                    return boolResult;
                }
            }

            return str;
        }
        /// <summary>
        /// Check to see if the given string can be converted to a primitive type.
        /// </summary>
        /// <param name="str">The string to be converted.</param>
        /// <param name="convertableType">The convertable type.</param>
        /// <returns>True if it can be converted to a primitive type.</returns>
        public static bool CanConvertToPrimitiveType(string str, out Type convertableType)
        {
            if (string.IsNullOrEmpty(str))
            {
                convertableType = null;
                return false;
            }

            byte byteResult = 0;
            if (byte.TryParse(str, out byteResult))
            {
                convertableType = typeof(byte);
                return true;
            }
            sbyte sbyteResult = 0;
            if (sbyte.TryParse(str, out sbyteResult))
            {
                convertableType = typeof(sbyte);
                return true;
            }

            short shortResult = 0;
            if (short.TryParse(str, out shortResult))
            {
                convertableType = typeof(short);
                return true;
            }
            ushort ushortResult = 0;
            if (ushort.TryParse(str, out ushortResult))
            {
                convertableType = typeof(ushort);
                return true;
            }

            int intResult = 0;
            if (int.TryParse(str, out intResult))
            {
                convertableType = typeof(int);
                return true;
            }
            uint uintResult = 0;
            if (uint.TryParse(str, out uintResult))
            {
                convertableType = typeof(uint);
                return true;
            }

            long longResult = 0;
            if (long.TryParse(str, out longResult))
            {
                convertableType = typeof(long);
                return true;
            }
            ulong ulongResult = 0;
            if (ulong.TryParse(str, out ulongResult))
            {
                convertableType = typeof(ulong);
                return true;
            }

            float floatResult = 0;
            if (float.TryParse(str, out floatResult))
            {
                convertableType = typeof(float);
                return true;
            }
            double doubleResult = 0;
            if (double.TryParse(str, out doubleResult))
            {
                convertableType = typeof(double);
                return true;
            }
            decimal decimalResult = 0;
            if (decimal.TryParse(str, out decimalResult))
            {
                convertableType = typeof(decimal);
                return true;
            }

            bool boolResult = false;
            if (bool.TryParse(str, out boolResult))
            {
                convertableType = typeof(bool);
                return true;
            }

            convertableType = typeof(string);
            return false;
        }
    }
}
