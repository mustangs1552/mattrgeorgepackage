﻿namespace MattRGeorge.General.Utilities.Static
{
    /// <summary>
    /// This utility has general methods to help with performing number related actions.
    /// </summary>
    public static class MathUtility
    {
        /// <summary>
        /// Is the given number almost equal to the get target number?
        /// </summary>
        /// <param name="num">The number to check.</param>
        /// <param name="targetNum">What the given number should almost be.</param>
        /// <param name="range">The range that the given number should fall within the target number.</param>
        /// <returns>True if almost equal to given target number.</returns>
        public static bool AreAlmostEqual(float num, float targetNum, float range = .1f)
        {
            return num >= targetNum - range && num <= targetNum + range;
        }
    }
}
