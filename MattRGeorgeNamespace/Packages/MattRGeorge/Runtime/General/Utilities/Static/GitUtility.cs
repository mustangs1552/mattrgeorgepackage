﻿/* MIT License
Copyright (c) 2016 RedBlueGames
Code written by Doug Cox
https://blog.redbluegames.com/version-numbering-for-games-in-unity-and-git-1d05fca83022
*/

using System;
using System.IO;
using System.Diagnostics;

namespace MattRGeorge.General.Utilities.Static
{
    public static class GitUtility
    {
        /// <summary>
        /// The install location of Git.
        /// Valid path: "{installLocation}\cmd\git.exe"
        /// </summary>
        public static string GitInstallLocation
        {
            get => gitInstallLocation;
            set => SetInstallLocation(value);
        }

        private static string gitInstallLocation = @"C:\Program Files\Git";

        /// <summary>
        /// Set the install location of Git.
        /// Valid path: "{installLocation}\cmd\git.exe"
        /// </summary>
        /// <param name="installLocation">The install location to set.</param>
        /// <returns>True if successful.</returns>
        public static bool SetInstallLocation(string installLocation)
        {
            if (string.IsNullOrEmpty(installLocation) ||
                !Directory.Exists(Environment.ExpandEnvironmentVariables(installLocation)) ||
                !File.Exists(Environment.ExpandEnvironmentVariables($@"{installLocation}\cmd\git.exe")))
            {
                return false;
            }

            gitInstallLocation = Environment.ExpandEnvironmentVariables(installLocation);
            return true;
        }

        /// <summary>
        /// Retrieves the build version from git based on the most recent matching tag and
        /// commit history. This returns the version as: {major.minor.build} where 'build'
        /// represents the nth commit after the tagged commit.
        /// Note: The initial 'v' and the commit hash code are removed.
        /// </summary>
        public static string BuildVersion
        {
            get
            {
                string version = Run(@"describe --tags --long --match ""v[0-9]*""");
                if (string.IsNullOrEmpty(version)) return "";
                // Remove initial 'v' and ending git commit hash.
                version = version.Replace('-', '.');
                version = version.Substring(1, version.LastIndexOf('.') - 1);
                return version;
            }
        }
        /// <summary>
        /// The currently active branch.
        /// </summary>
        public static string Branch => Run(@"rev-parse --abbrev-ref HEAD");
        /// <summary>
        /// Returns a listing of all uncommitted or untracked (added) files.
        /// </summary>
        public static string Status => Run(@"status --porcelain");

        /// <summary>
        /// Runs git.exe with the specified arguments and returns the output.
        /// </summary>
        public static string Run(string arguments)
        {
            try
            {
                using (Process process = new Process())
                {
                    process.StartInfo = new ProcessStartInfo()
                    {
                        FileName = $@"{gitInstallLocation}\cmd\git",
                        Arguments = arguments,
                        UseShellExecute = false,
                        RedirectStandardOutput = true,
                        CreateNoWindow = true
                    };
                    process.Start();

                    while (!process.StandardOutput.EndOfStream) return process.StandardOutput.ReadLine();
                    return "";
                }
            }
            catch (Exception e)
            {
                e.ToString();
                return "";
            }
        }
    }
}