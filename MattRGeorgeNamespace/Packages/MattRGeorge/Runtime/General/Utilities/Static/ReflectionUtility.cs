﻿using System;
using System.Reflection;

namespace MattRGeorge.General.Utilities.Static
{
    public static class ReflectionUtility
    {
        /// <summary>
        /// Uses reflection to get the field value from an object.
        /// </summary>
        /// <param name="type">The instance type.</param>
        /// <param name="instance">The instance object.</param>
        /// <param name="fieldName">The field's name which is to be fetched.</param>
        /// <returns>The field value from the object.</returns>
        public static object GetInstanceField(Type type, object instance, string fieldName)
        {
            if (type == null || instance == null || string.IsNullOrEmpty(fieldName)) return default;

            BindingFlags bindFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static;
            FieldInfo field = type.GetField(fieldName, bindFlags);
            if (field == null) return default;
            return field.GetValue(instance);
        }
        /// <summary>
        /// Uses reflection to set the field value of an object.
        /// </summary>
        /// <param name="type">The instance type.</param>
        /// <param name="instance">The instance object.</param>
        /// <param name="fieldName">The field's name which is to be fetched.</param>
        /// <param name="value">The value to assign the field.</param>
        public static void SetInstanceField(Type type, object instance, string fieldName, object value)
        {
            if (type == null || instance == null || string.IsNullOrEmpty(fieldName)) return;

            BindingFlags bindFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Static;
            FieldInfo field = type.GetField(fieldName, bindFlags);
            if (field == null) return;
            field.SetValue(instance, value);
        }
    }
}
