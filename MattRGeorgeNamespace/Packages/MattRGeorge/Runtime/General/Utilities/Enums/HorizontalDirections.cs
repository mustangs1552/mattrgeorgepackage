﻿namespace MattRGeorge.General.Utilities.Enums
{
    /// <summary>
    /// Directions on the horizontal axis includeing "None" and "Both".
    /// </summary>
    public enum HorizontalDirections
    {
        None,

        Left,
        Right,

        Both
    }
}
