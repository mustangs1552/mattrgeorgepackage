﻿namespace MattRGeorge.General.Utilities.Enums
{
    /// <summary>
    /// Modes for selecting from a list multiple times.
    /// </summary>
    public enum ListRandomChoiceMode
    {
        Randomly,
        AllRandomly,
        AllSequentially
    }
}
