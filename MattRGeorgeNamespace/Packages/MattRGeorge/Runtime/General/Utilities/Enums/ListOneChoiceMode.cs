﻿namespace MattRGeorge.General.Utilities.Enums
{
    /// <summary>
    /// Modes for selecting from a list once.
    /// </summary>
    public enum ListOneChoiceMode
    {
        Oldest,
        Newest,
        Randomly
    }
}
