﻿using System;
using System.Linq;
using System.Collections.Generic;
using MattRGeorge.General.Utilities.Static;

namespace MattRGeorge.General.Tools
{
    /// <summary>
    /// A parsed JSON object with the values in dictionaries.
    /// </summary>
    public class JSONObject
    {
        /// <summary>
        /// Normal values of a JSON object.
        /// If there is only one value with no key then access it using "SINGLE_VALUE" as the key.
        /// </summary>
        public Dictionary<string, string> values = new Dictionary<string, string>();
        /// <summary>
        /// Nested JSON objects within this JSON object.
        /// </summary>
        public Dictionary<string, JSONObject> jsonObjects = new Dictionary<string, JSONObject>();
        /// <summary>
        /// An array of nested JSON objects within this JSON object.
        /// If there is only one array with no key then access it using "ROOT_ARRAY" as the key.
        /// </summary>
        public Dictionary<string, List<JSONObject>> objArrays = new Dictionary<string, List<JSONObject>>();
        /// <summary>
        /// An array of normal values within this JSON object.
        /// If there is only one array with no key then access it using "ROOT_ARRAY" as the key.
        /// </summary>
        public Dictionary<string, List<string>> valueArrays = new Dictionary<string, List<string>>();

        /// <summary>
        /// Return the value under the "SINGLE_VALUE" key in the "values" dictionary as a string.
        /// </summary>
        /// <returns>The string value.</returns>
        public virtual string GetSingleStringValue()
        {
            if (values.ContainsKey("SINGLE_VALUE")) return (values["SINGLE_VALUE"].ToLower() != "null") ? values["SINGLE_VALUE"] : null;

            return "";
        }
        /// <summary>
        /// Return the value under the "SINGLE_VALUE" key in the "values" dictionary either as a primitive type or a string.
        /// </summary>
        /// <returns>The value.</returns>
        public virtual object GetSingleValue()
        {
            if (!values.ContainsKey("SINGLE_VALUE") || values["SINGLE_VALUE"].ToLower() == "null") return null;

            return ConversionUtility.TryConvertValue(values["SINGLE_VALUE"]);
        }
        /// <summary>
        /// Return the values under the "ROOT_ARRAY" key in the "valueArrays" dictionary as strings.
        /// </summary>
        /// <returns>The list of string values.</returns>
        public virtual List<string> GetStringRootArray()
        {
            return (valueArrays.ContainsKey("ROOT_ARRAY")) ? valueArrays["ROOT_ARRAY"] : new List<string>();
        }
        /// <summary>
        /// Return the values under the "ROOT_ARRAY" key in the "valueArrays" dictionary as primitive types or strings.
        /// </summary>
        /// <returns>The list of values.</returns>
        public virtual List<object> GetValueRootArray()
        {
            if (!valueArrays.ContainsKey("ROOT_ARRAY")) return new List<object>();

            List<object> values = new List<object>();
            foreach (string value in valueArrays["ROOT_ARRAY"]) values.Add(ConversionUtility.TryConvertValue(value));
            return values;
        }
        /// <summary>
        /// Return the JSONObjects under the "ROOT_ARRAY" key in the "objArrays" dictionary.
        /// </summary>
        /// <returns>The list of JSONObjects.</returns>
        public virtual List<JSONObject> GetObjectRootArray()
        {
            return (objArrays.ContainsKey("ROOT_ARRAY")) ? objArrays["ROOT_ARRAY"] : new List<JSONObject>();
        }

        /// <summary>
        /// Converts this JSONObject to JSON text.
        /// Will write out object in this order: values, value arrays, JSONObjects, JSONObject arrays.
        /// </summary>
        /// <returns>Full string of JSONObject in json format.</returns>
        public virtual string ToJSON()
        {
            string json = "";

            Type convertableType = null;
            object convertedValue = null;
            if (!string.IsNullOrEmpty(GetSingleStringValue()))
            {
                convertedValue = ConversionUtility.TryConvertValue(GetSingleStringValue(), out convertableType);
                json += (convertableType != typeof(string)) ? convertedValue.ToString().ToLower() : $"\"{convertedValue.ToString()}\"";
            }
            else
            {
                if (GetStringRootArray().Count > 0)
                {
                    json += "[";
                    foreach (string valueArray in GetStringRootArray())
                    {
                        convertedValue = ConversionUtility.TryConvertValue(valueArray, out convertableType);
                        json += (convertableType != typeof(string)) ? $"{convertedValue.ToString().ToLower()}," : $"\"{convertedValue.ToString()}\",";
                    }
                    json = RemoveTrailingComma(json);
                    json += "]";
                }
                else if (GetObjectRootArray().Count > 0)
                {
                    json += "{";

                    json += "[";
                    GetObjectRootArray().ForEach(x => json += $"{x.ToJSON()},");
                    json = RemoveTrailingComma(json);
                    json += "]";
                }
                else
                {
                    json += "{";

                    json += ValuesToJSON();

                    json = CheckAddComma(json);
                    json += ValueArraysToJSON();

                    json = CheckAddComma(json);
                    foreach (KeyValuePair<string, JSONObject> obj in jsonObjects) json += $"\"{obj.Key}\":{obj.Value.ToJSON()},";

                    json = CheckAddComma(json);
                    foreach (KeyValuePair<string, List<JSONObject>> objArray in objArrays)
                    {
                        json += $"\"{objArray.Key}\":[";
                        foreach (JSONObject jsonObj in objArray.Value)
                        {
                            json += $"{jsonObj.ToJSON()}";
                            if (jsonObj != objArray.Value.Last()) json += ",";
                        }
                        json += "]";
                    }
                }
            }

            json = RemoveTrailingComma(json);
            return json + ((json != null && json.Length > 0 && json[0] == '{') ? "}" : "");
        }

        /// <summary>
        /// Converts all values to JSON text.
        /// </summary>
        /// <returns>Full string of values in json format.</returns>
        protected virtual string ValuesToJSON()
        {
            if (values == null || values.Count == 0) return "";
            string json = "";

            Type convertableType = null;
            object convertedValue = null;
            foreach (KeyValuePair<string, string> value in values)
            {
                convertedValue = ConversionUtility.TryConvertValue(value.Value, out convertableType);
                json += $"\"{value.Key}\":";
                json += (convertableType != typeof(string)) ? $"{convertedValue.ToString().ToLower()}," : $"\"{convertedValue.ToString()}\",";
            }
            json = RemoveTrailingComma(json);

            return json;
        }
        /// <summary>
        /// Converts all value arrays to JSON text.
        /// </summary>
        /// <returns>Full string of value arrays in json format.</returns>
        protected virtual string ValueArraysToJSON()
        {
            string json = "";

            Type convertableType = null;
            object convertedValue = null;
            foreach (KeyValuePair<string, List<string>> valueArray in valueArrays)
            {
                json += $"\"{valueArray.Key}\":[";
                foreach (string value in valueArray.Value)
                {
                    convertedValue = ConversionUtility.TryConvertValue(value, out convertableType);
                    json += (convertableType != typeof(string)) ? $"{convertedValue.ToString().ToLower()}," : $"\"{convertedValue.ToString()}\",";
                }
                json = RemoveTrailingComma(json);
                json += "]";
            }

            return json;
        }

        /// <summary>
        /// Checks to see if the given JSON text should have a comma appended.
        /// </summary>
        /// <param name="json">The JSON to check.</param>
        /// <returns>The modified JSON text.</returns>
        protected virtual string CheckAddComma(string json)
        {
            if (string.IsNullOrEmpty(json)) return "";

            if (json[json.Length - 1] != ',' && json[json.Length - 1] != '{' && json[json.Length - 1] != '[') return json += ',';

            return json;
        }
        /// <summary>
        /// Checks to see if the given JSON text should not have a comma at the end.
        /// </summary>
        /// <param name="json">The JSON to check.</param>
        /// <returns>The modified JSON text.</returns>
        protected virtual string RemoveTrailingComma(string json)
        {
            if (string.IsNullOrEmpty(json)) return "";

            if (json[json.Length - 1] == ',') return json.Remove(json.Length - 1);

            return json;
        }
    }
}