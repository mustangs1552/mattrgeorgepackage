﻿using UnityEngine;
using System.Collections.Generic;
using MattRGeorge.Unity.Utilities.Helpers;

namespace MattRGeorge.Unity.Tools.ObjectPooling
{
    /// <summary>
    /// This class manages and provides access to both pools of objects: Active and Inactive objects.
    /// </summary>
    public class ObjectPoolManager : MonoBehaviour
    {
        public static ObjectPoolManager SINGLETON = null;

        public GameObjectUnityEvent OnObjectMadeActive = new GameObjectUnityEvent();
        public GameObjectUnityEvent OnObjectMadeInactive = new GameObjectUnityEvent();

        /// <summary>
        /// All the active gameobjects that are pooled.
        /// </summary>
        public virtual List<GameObject> AllGameObjects
        {
            get
            {
                return activePool.AllGameObjects;
            }
        }
        /// <summary>
        /// All the active gameobjects' transforms that are pooled.
        /// </summary>
        public virtual List<Transform> AllTransforms
        {
            get
            {
                return activePool.AllTransforms;
            }
        }
        
        protected ObjectPool activePool = null;
        protected ObjectPool inactivePool = null;

        /// <summary>
        /// Get an in-active object from the pool or instantiate a new one to be used as an active object.
        /// Then enable it.
        /// </summary>
        /// <param name="obj">The kind of object desired.</param>
        /// <param name="asActive">Instantiate object as in-active or active?</param>
        /// <param name="poolManaged">Have the pool manager manage this object and parent it to the pool objects under this manager.</param>
        /// <returns>The object that has been grabbed/created.</returns>
        public virtual GameObject Instantiate(GameObject obj, bool asActive = true, bool poolManaged = true)
        {
            return Instantiate(obj, null, null, null, asActive, poolManaged);
        }
        /// <summary>
        /// Get an in-active object from the pool or instantiate a new one to be used as an active object.
        /// Then enable it.
        /// </summary>
        /// <param name="obj">The kind of object desired.</param>
        /// <param name="parent">The for the returned object (only available when not pool managed).</param>
        /// <param name="asActive">Instantiate object as in-active or active?</param>
        /// <param name="poolManaged">Have the pool manager manage this object and parent it to the pool objects under this manager.</param>
        /// <returns>The object that has been grabbed/created.</returns>
        public virtual GameObject Instantiate(GameObject obj, Transform parent, bool asActive = true, bool poolManaged = true)
        {
            return Instantiate(obj, null, null, parent, asActive, poolManaged);
        }
        //public GameObject Instantiate(GameObject obj, Transform parent, bool instantiateInWorldSpace, bool asActive = true, bool poolManaged = true)
        /// <summary>
        /// Get an in-active object from the pool or instantiate a new one to be used as an active object.
        /// Then enable it.
        /// </summary>
        /// <param name="obj">The kind of object desired.</param>
        /// <param name="position">The position of the returned object.</param>
        /// <param name="rotation">The rotation of the returned object.</param>
        /// <param name="asActive">Instantiate object as in-active or active?</param>
        /// <param name="poolManaged">Have the pool manager manage this object and parent it to the pool objects under this manager.</param>
        /// <returns>The object that has been grabbed/created.</returns>
        public virtual GameObject Instantiate(GameObject obj, Vector3 position, Quaternion rotation, bool asActive = true, bool poolManaged = true)
        {
            return Instantiate(obj, position, rotation, null, asActive, poolManaged);
        }
        /// <summary>
        /// Get an in-active object from the pool or instantiate a new one to be used as an active object.
        /// Then enable it.
        /// </summary>
        /// <param name="obj">The kind of object desired.</param>
        /// <param name="position">The position of the returned object.</param>
        /// <param name="rotation">The rotation of the returned object.</param>
        /// <param name="parent">The for the returned object (only available when not pool managed).</param>
        /// <param name="asActive">Instantiate object as in-active or active?</param>
        /// <param name="poolManaged">Have the pool manager manage this object and parent it to the pool objects under this manager.</param>
        /// <returns>The object that has been grabbed/created.</returns>
        public virtual GameObject Instantiate(GameObject obj, Vector3? position, Quaternion? rotation, Transform parent, bool asActive = true, bool poolManaged = true)
        {
            if (!obj) return null;

            GameObject instantiatedObj = (!asActive) ? null : inactivePool.RemoveObject(obj, false);
            if (!instantiatedObj)
            {
                if (position == null && rotation == null && !parent) instantiatedObj = Object.Instantiate(obj);
                else if (position == null && rotation == null && parent) instantiatedObj = Object.Instantiate(obj, parent);
                else if (position != null && rotation != null && !parent) instantiatedObj = Object.Instantiate(obj, (Vector3)position, (Quaternion)rotation);
                else if (position != null && rotation != null && parent) instantiatedObj = Object.Instantiate(obj, (Vector3)position, (Quaternion)rotation, parent);
            }
            else
            {
                if (position != null) instantiatedObj.transform.position = (Vector3)position;
                if (rotation != null) instantiatedObj.transform.rotation = (Quaternion)rotation;
                if (parent) instantiatedObj.transform.SetParent(parent);
            }

            if (poolManaged)
            {
                if (asActive)
                {
                    activePool.AddObject(instantiatedObj);
                    OnObjectMadeActive?.Invoke(instantiatedObj);
                }
                else
                {
                    inactivePool.AddObject(instantiatedObj);
                    OnObjectMadeInactive?.Invoke(instantiatedObj);
                }
            }

            if (instantiatedObj)
            {
                IObjectPoolEvents[] events = instantiatedObj.GetComponentsInChildren<IObjectPoolEvents>();
                if (events != null)
                {
                    foreach (IObjectPoolEvents evnt in events) evnt.OnInstantiatedByObjectPool();
                }
            }
            UpdateObjectActiveState();
            return instantiatedObj;
        }

        /// <summary>
        /// Remove the given object from the active pool and add it to the in-active pool.
        /// Then disable it.
        /// </summary>
        /// <param name="obj">The object to destroy.</param>
        public virtual void Destroy(GameObject obj)
        {
            if (!obj) return;

            activePool.RemoveObject(obj);
            inactivePool.AddObject(obj);
            OnObjectMadeInactive?.Invoke(obj);

            IObjectPoolEvents[] events = obj.GetComponentsInChildren<IObjectPoolEvents>();
            if (events != null)
            {
                foreach (IObjectPoolEvents evnt in events) evnt.OnDestroyedByObjectPool();
            }
            UpdateObjectActiveState();
        }

        /// <summary>
        /// Add the already existing given object to the pool of active objects.
        /// Will move the object from the pool of inactive objects to active objects if already exists there.
        /// </summary>
        /// <param name="obj">The existing object to add.</param>
        /// <returns>True if successful.</returns>
        public virtual bool AddActiveObject(GameObject obj)
        {
            if (!obj || activePool.CheckIfExist(obj)) return false;
            
            inactivePool.RemoveObject(obj);
            activePool.AddObject(obj);
            OnObjectMadeActive?.Invoke(obj);

            UpdateObjectActiveState();
            return true;
        }
        /// <summary>
        /// Add the already existing given object to the pool of inactive objects.
        /// Will move the object from the pool of active objects to inactive objects if already exists there.
        /// </summary>
        /// <param name="obj">The existing object to add.</param>
        /// <returns>True if successful.</returns>
        public virtual bool AddInactiveObject(GameObject obj)
        {
            if (!obj || inactivePool.CheckIfExist(obj)) return false;
            
            activePool.RemoveObject(obj);
            inactivePool.AddObject(obj);
            OnObjectMadeInactive?.Invoke(obj);

            UpdateObjectActiveState();
            return true;
        }

        /// <summary>
        /// Check if the given object exists in either pool.
        /// </summary>
        /// <param name="obj">The object to check.</param>
        /// <returns>True if exists.</returns>
        public virtual bool CheckIfExists(GameObject obj)
        {
            return activePool.CheckIfExist(obj) || inactivePool.CheckIfExist(obj);
        }

        /// <summary>
        /// Gets all the active objects from the pool of the given object.
        /// </summary>
        /// <param name="obj">The object that goes with the desired pool.</param>
        /// <returns>The objects from the desired pool.</returns>
        public virtual List<GameObject> GetActiveObjects(GameObject obj)
        {
            return activePool.GetPool(obj);
        }
        /// <summary>
        /// Gets all of the given components off all the objects that are in the active objects pool.
        /// </summary>
        /// <typeparam name="T">The component to get from objects.</typeparam>
        /// <returns>A list of all the components found.</returns>
        public virtual List<T> GetActiveObjectsComponents<T>()
        {
            return activePool.GetObjectComponents<T>();
        }
        /// <summary>
        /// Get all the active gamobjects with the given tag.
        /// </summary>
        /// <param name="tag">The tag to search.</param>
        /// <returns>All active gameobjects with the given tag.</returns>
        public virtual List<GameObject> GetActiveObjectsWithTag(string tag)
        {
            return activePool.GetObjectsWithTag(tag);
        }
        /// <summary>
        /// Get all the active gamobjects' transforms with the given tag.
        /// </summary>
        /// <param name="tag">The tag to search.</param>
        /// <returns>All active gameobjects' transforms with the given tag.</returns>
        public virtual List<Transform> GetActiveTransformsWithTag(string tag)
        {
            return activePool.GetTransformsWithTag(tag);
        }

        /// <summary>
        /// Returns all the pools with their names and object counts from both active and in-active object pools.
        /// </summary>
        /// <returns>All the pools with their names and object counts from both active and in-active object pools.</returns>
        public override string ToString()
        {
            string returnStr = "Active Pool: " + ActivePoolToString();
            returnStr += "\nIn-active Pool: " + InactivePoolToString();
            return returnStr;
        }
        /// <summary>
        /// Returns all the pools with their names and object counts from the active object pool.
        /// </summary>
        /// <returns>All the pools with their names and object counts from the active object pool.</returns>
        public virtual string ActivePoolToString()
        {
            return activePool.ToString();
        }
        /// <summary>
        /// Returns all the pools with their names and object counts from the in-active object pool.
        /// </summary>
        /// <returns>All the pools with their names and object counts from the in-active object pool.</returns>
        public virtual string InactivePoolToString()
        {
            return inactivePool.ToString();
        }

        /// <summary>
        /// Create and setup the object pool objects.
        /// </summary>
        protected virtual void CreatePools()
        {
            GameObject activePoolGO = new GameObject("ACTIVE_OBJECTS_POOL");
            activePool = activePoolGO.AddComponent<ObjectPool>();
            activePool.transform.SetParent(transform);

            GameObject inactivePoolGO = new GameObject("INACTIVE_OBJECTS_POOL");
            inactivePool = inactivePoolGO.AddComponent<ObjectPool>();
            inactivePool.transform.SetParent(transform);
        }

        /// <summary>
        /// Initial setup.
        /// </summary>
        protected virtual void Setup()
        {
            if (!ObjectPoolManager.SINGLETON) SINGLETON = this;
            else
            {
                Debug.LogError("More than one 'ObjectPoolManager' singletons detected!");
                Destroy(this);
            }

            CreatePools();
        }

        /// <summary>
        /// Makes sure that all the inactive objects are disabled.
        /// </summary>
        protected virtual void UpdateObjectActiveState()
        {
            inactivePool.SetObjectsActive(false);
        }

        protected virtual void Awake()
        {
            Setup();
        }
    }
}