﻿using UnityEngine;
using MattRGeorge.Unity.Utilities.Enums;

namespace MattRGeorge.Unity.Tools.ObjectPooling
{
    public class ObjectPoolObject : MonoBehaviour
    {
        [Tooltip("Add this object to the object pool as an active object?")]
        public bool addAsActive = true;
        [Tooltip("Add this object to the object pool on Start/Awake?")]
        [SerializeField] protected UnityStartMethods addOn = UnityStartMethods.None;

        /// <summary>
        /// Add this existing gameobject to the object pool system.
        /// </summary>
        public virtual void AddToObjectPool()
        {
            if (!ObjectPoolManager.SINGLETON) return;

            if (addAsActive) ObjectPoolManager.SINGLETON.AddActiveObject(gameObject);
            else ObjectPoolManager.SINGLETON.AddInactiveObject(gameObject);
        }

        protected virtual void Awake()
        {
            if (addOn == UnityStartMethods.Awake) AddToObjectPool();
        }
        protected virtual void Start()
        {
            if (addOn == UnityStartMethods.Start) AddToObjectPool();
        }
    }
}
