﻿using UnityEngine;
using System.Collections.Generic;
using MattRGeorge.Unity.Utilities.Static;

namespace MattRGeorge.Unity.Tools.ObjectPooling
{
    /// <summary>
    /// Manages multiple pools of objects.
    /// Seperates each object given into their own pools by their name.
    /// </summary>
    public class ObjectPool : MonoBehaviour
    {        
        /// <summary>
        /// All the gameobjects that are pooled.
        /// </summary>
        public virtual List<GameObject> AllGameObjects
        {
            get
            {
                List<GameObject> objs = new List<GameObject>();
                foreach (KeyValuePair<string, List<GameObject>> pair in pools)
                {
                    foreach (GameObject obj in pair.Value)
                    {
                        if (obj) objs.Add(obj);
                    }
                }
                return objs;
            }
        }
        /// <summary>
        /// All the gameobjects' transforms that are pooled.
        /// </summary>
        public virtual List<Transform> AllTransforms
        {
            get
            {
                List<Transform> objs = new List<Transform>();
                foreach (KeyValuePair<string, List<GameObject>> pair in pools)
                {
                    foreach (GameObject obj in pair.Value)
                    {
                        if (obj) objs.Add(obj.transform);
                    }
                }
                return objs;
            }
        }

        protected Dictionary<string, List<GameObject>> pools = new Dictionary<string, List<GameObject>>();
        protected Dictionary<string, Transform> poolAnchers = new Dictionary<string, Transform>();

        /// <summary>
        /// Add the given object to the proper pool.
        /// </summary>
        /// <param name="obj">The object to be added.</param>
        /// <returns>True if successful.</returns>
        public virtual bool AddObject(GameObject obj)
        {
            if (!obj) return false;

            string actualName = GameObjectUtility.GetActualName(obj);
            if (!pools.ContainsKey(actualName)) CreateNewPool(actualName);

            if (!pools[actualName].Contains(obj))
            {
                pools[actualName].Add(obj);
                obj.transform.SetParent(poolAnchers[actualName]);
                return true;
            }

            return false;
        }

        /// <summary>
        /// Remove the given object from the pool.
        /// If it doesn't exist in pool then remove and return one of the objects from the pool that matches the given object.
        /// </summary>
        /// <param name="obj">The object to remove.</param>
        /// <param name="exactInstance">Remove only that instance of the given object?</param>
        /// <returns>The object removed.</returns>
        public virtual GameObject RemoveObject(GameObject obj, bool exactInstance = true)
        {
            if (!obj) return null;

            if (RemoveSpecificObject(obj)) return obj;
            else if(!exactInstance)
            {
                string actualName = GameObjectUtility.GetActualName(obj);
                if (pools.ContainsKey(actualName) && pools[actualName].Count > 0)
                {
                    GameObject removedObj = pools[actualName][0];
                    pools[actualName].RemoveAt(0);
                    removedObj.transform.SetParent(null);
                    return removedObj;
                }
            }

            return null;
        }

        /// <summary>
        /// Get the desired pool of objects by the given gameobject.
        /// </summary>
        /// <param name="gameobjectType">The type of pool to get.</param>
        /// <returns>The desired pool.</returns>
        public virtual List<GameObject> GetPool(GameObject gameobjectType)
        {
            if (!gameobjectType) return new List<GameObject>();

            string actualName = GameObjectUtility.GetActualName(gameobjectType);

            if (pools.ContainsKey(actualName)) return ListUtility.RemoveNullEntries(pools[actualName]);
            return new List<GameObject>();
        }

        /// <summary>
        /// Check if the given object exist in this pool.
        /// </summary>
        /// <param name="obj">The object t0 check.</param>
        /// <returns>True if exists.</returns>
        public virtual bool CheckIfExist(GameObject obj)
        {
            if (!obj) return false;

            string actualName = GameObjectUtility.GetActualName(obj);
            return pools.ContainsKey(actualName) && pools[actualName].Contains(obj);
        }

        /// <summary>
        /// Gets all of the given components of all the objects that are pooled.
        /// </summary>
        /// <typeparam name="T">The component to get from objects.</typeparam>
        /// <returns>A list of all the components found.</returns>
        public virtual List<T> GetObjectComponents<T>()
        {
            List<T> objs = new List<T>();

            foreach (KeyValuePair<string, List<GameObject>> pool in pools)
            {
                if (pool.Value != null && pool.Value.Count > 0)
                {
                    T comp = pool.Value[0].GetComponent<T>();
                    if (comp != null && !comp.Equals(null)) pool.Value.ForEach(x => objs.Add(x.GetComponent<T>()));
                }
            }

            return ListUtility.RemoveNullEntries(objs);
        }
        /// <summary>
        /// Get all the gamobjects with the given tag.
        /// </summary>
        /// <param name="tag">The tag to search.</param>
        /// <returns>All gameobjects with the given tag.</returns>
        public virtual List<GameObject> GetObjectsWithTag(string tag)
        {
            if (tag == null) return new List<GameObject>();

            List<GameObject> objs = new List<GameObject>();

            foreach (KeyValuePair<string, List<GameObject>> pool in pools)
            {
                if (pool.Value != null && pool.Value.Count > 0 && pool.Value[0].tag != null && pool.Value[0].tag == tag) pool.Value.ForEach(x => objs.Add(x));
            }

            return ListUtility.RemoveNullEntries(objs);
        }
        /// <summary>
        /// Get all the gamobjects' transforms with the given tag.
        /// </summary>
        /// <param name="tag">The tag to search.</param>
        /// <returns>All gameobjects' transforms with the given tag.</returns>
        public virtual List<Transform> GetTransformsWithTag(string tag)
        {
            if (tag == null) return new List<Transform>();

            List<Transform> objs = new List<Transform>();

            foreach (KeyValuePair<string, List<GameObject>> pool in pools)
            {
                if (pool.Value != null && pool.Value.Count > 0 && pool.Value[0].tag != null && pool.Value[0].tag == tag) pool.Value.ForEach(x => objs.Add(x.transform));
            }

            return ListUtility.RemoveNullEntries(objs);
        }

        /// <summary>
        /// Returns all the pools with their names and object counts.
        /// </summary>
        /// <returns>All the pools with their names and object counts.</returns>
        public override string ToString()
        {
            string returnStr = pools.Count + " Total Object Pool(s)";
            foreach (KeyValuePair<string, List<GameObject>> pair in pools) returnStr += "\n- " + pair.Key + ": " + pair.Value.Count;
            return returnStr;
        }

        /// <summary>
        /// Sets all the objects to the desired active state.
        /// If poolName is set then it will set all the objects' active state in that pool.
        /// </summary>
        /// <param name="active">Desired active state.</param>
        /// <param name="poolName">The name of the objects whose desired pool to set (Optional).</param>
        public virtual void SetObjectsActive(bool active, string poolName = null)
        {
            if (poolName != null && poolName.Length > 0)
            {
                if (pools.ContainsKey(poolName)) pools[poolName].ForEach(x => x.SetActive(active));
            }
            else
            {
                foreach (KeyValuePair<string, List<GameObject>> pool in pools)
                {
                    pool.Value.ForEach(x => x.SetActive(active));
                }
            }
        }

        /// <summary>
        /// Create a new pool with its anchor.
        /// </summary>
        /// <param name="poolName">The name of the new pool.</param>
        protected virtual void CreateNewPool(string poolName)
        {
            if (string.IsNullOrEmpty(poolName)) return;

            pools.Add(poolName, new List<GameObject>());
            poolAnchers.Add(poolName, new GameObject(poolName.ToUpper() + "_ANCHOR").transform);
            poolAnchers[poolName].SetParent(transform);
        }

        /// <summary>
        /// Remove the given object from the pool if it exists there.
        /// </summary>
        /// <param name="obj">The object to remove.</param>
        /// <returns>True if successful.</returns>
        protected virtual bool RemoveSpecificObject(GameObject obj)
        {
            if (!obj) return false;

            string actualName = GameObjectUtility.GetActualName(obj);
            if (pools.ContainsKey(actualName))
            {
                bool success = pools[actualName].Remove(obj);
                if (success) obj.transform.SetParent(null);
                return success;
            }

            return false;
        }
    }
}