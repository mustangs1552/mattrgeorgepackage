﻿namespace MattRGeorge.Unity.Tools.ObjectPooling
{
    public interface IObjectPoolEvents
    {
        void OnInstantiatedByObjectPool();
        void OnDestroyedByObjectPool();
    }
}
