﻿using System;
using UnityEngine;
using MattRGeorge.Unity.Utilities.Helpers;

namespace MattRGeorge.Unity.Tools.Stats
{
    /// <summary>
    /// A simple stat with starting and active values.
    /// Uses a max value value and a minnimum value of 0,.
    /// Best used for gauges such as health or energy.
    /// Does nothing on its own.
    /// </summary>
    [Serializable]
    public class StatGauge : Stat
    {
        [Tooltip("The starting max amount allowed.")]
        [SerializeField] protected float startingMaxAmount = 100;
        [Tooltip("Update the current amount to keep the same percentage of full when the max amount is changed?")]
        [SerializeField] protected bool updateWithMaxAmount = true;

        public override float CurrAmount
        {
            get
            {
                return currAmount;
            }
            set
            {
                if (value <= 0)
                {
                    if (!reachedMinLimit)
                    {
                        reachedMinLimit = true;
                        reachedMaxLimit = false;
                        currAmount = 0;
                        OnMinReached?.Invoke(CurrAmount);
                        OnChanged?.Invoke(CurrAmount);
                        OnChangedPerc?.Invoke(0);
                    }
                }
                else if (value >= CurrMaxAmount)
                {
                    if (!reachedMaxLimit)
                    {
                        reachedMinLimit = false;
                        reachedMaxLimit = true;
                        currAmount = CurrMaxAmount;
                        OnMaxReached?.Invoke(CurrAmount);
                        OnChanged?.Invoke(CurrAmount);
                        OnChangedPerc?.Invoke(1);
                    }
                }
                else if (value != currAmount)
                {
                    reachedMinLimit = false;
                    reachedMaxLimit = false;
                    currAmount = value;
                    OnChanged?.Invoke(CurrAmount);
                    OnChangedPerc?.Invoke(CurrAmount / CurrMaxAmount);
                }
            }
        }
        /// <summary>
        /// The current max amount allowed.
        /// </summary>
        public virtual float CurrMaxAmount
        {
            get
            {
                return currMaxAmount;
            }
            set
            {
                if (value > 0)
                {
                    float amountPerc = -1;
                    if (updateWithMaxAmount) amountPerc = CurrAmountPerc;

                    currMaxAmount = value;

                    reachedMaxLimit = false;
                    CurrAmount = (amountPerc != -1) ? currMaxAmount * amountPerc : CurrAmount;
                }
            }
        }
        /// <summary>
        /// The precentage of current amount out of the current max amount.
        /// </summary>
        public virtual float CurrAmountPerc
        {
            get
            {
                return CurrAmount / currMaxAmount;
            }
        }

        public FloatUnityEvent OnChangedPerc = new FloatUnityEvent();
        public FloatUnityEvent OnMinReached = new FloatUnityEvent();
        public FloatUnityEvent OnMaxReached = new FloatUnityEvent();

        protected float currMaxAmount = 0;
        protected bool reachedMinLimit = false;
        protected bool reachedMaxLimit = false;

        /// <summary>
        /// Create a new stat gauge.
        /// </summary>
        /// <param name="startingAmount">The starting amount.</param>
        /// <param name="updateWithMaxAmount">Update current amount with max amount?</param>
        public StatGauge(float startingAmount, float startingMaxAmount, bool updateWithMaxAmount) : base(startingAmount)
        {
            this.startingMaxAmount = startingMaxAmount;
            this.updateWithMaxAmount = updateWithMaxAmount;
            StatReset();
        }

        /// <summary>
        /// Set the current amount via a percentage of the range.
        /// </summary>
        /// <param name="percentage">A value between 0 and 1.</param>
        public virtual void SetAmount(float percentage)
        {
            if (percentage < 0 || percentage > 1) return;
            CurrAmount = percentage * CurrMaxAmount;
        }

        public override void StatReset()
        {
            CurrMaxAmount = startingMaxAmount;
            CurrAmount = startingAmount;
            reachedMinLimit = false;
            reachedMaxLimit = false;
        }
    }
}