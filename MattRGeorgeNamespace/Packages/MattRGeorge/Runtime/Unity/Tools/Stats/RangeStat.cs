﻿using System;
using UnityEngine;

namespace MattRGeorge.Unity.Tools.Stats
{
    /// <summary>
    /// A stat with a minimum and maximum amount.
    /// Does nothing on its own.
    /// </summary>
    [Serializable]
    public class RangeStat : StatGauge
    {
        [Tooltip("The starting minimum amount.")]
        [SerializeField] protected float startingMinAmount = 0;

        /// <summary>
        /// The current minimum amount.
        /// </summary>
        public virtual float CurrMinAmount
        {
            get
            {
                return currMinAmount;
            }
            set
            {
                if (value < ((initialValuesSet) ? CurrMaxAmount : startingMaxAmount))
                {
                    float amountPerc = -1;
                    if (updateWithMaxAmount) amountPerc = CurrAmountPerc;

                    currMinAmount = value;

                    reachedMinLimit = false;
                    CurrAmount = (amountPerc != -1) ? (CurrMaxAmount - currMinAmount) * amountPerc + currMinAmount : CurrAmount;
                }
            }
        }
        /// <summary>
        /// The current amount.
        /// </summary>
        public override float CurrAmount
        {
            get
            {
                return currAmount;
            }
            set
            {
                if (value <= CurrMinAmount)
                {
                    if (!reachedMinLimit)
                    {
                        reachedMinLimit = true;
                        reachedMaxLimit = false;
                        currAmount = CurrMinAmount;
                        OnMinReached?.Invoke(CurrAmount);
                        OnChanged?.Invoke(CurrAmount);
                        OnChangedPerc?.Invoke(0);
                    }
                }
                else if (value >= CurrMaxAmount)
                {
                    if (!reachedMaxLimit)
                    {
                        reachedMinLimit = false;
                        reachedMaxLimit = true;
                        currAmount = CurrMaxAmount;
                        OnMaxReached?.Invoke(CurrAmount);
                        OnChanged?.Invoke(CurrAmount);
                        OnChangedPerc?.Invoke(1);
                    }
                }
                else if (value != currAmount)
                {
                    reachedMinLimit = false;
                    reachedMaxLimit = false;
                    currAmount = value;
                    OnChanged?.Invoke(CurrAmount);
                    OnChangedPerc?.Invoke(CurrAmount / CurrMaxAmount);
                }
            }
        }
        /// <summary>
        /// The current amount as a percentage (0-1).
        /// </summary>
        public override float CurrAmountPerc
        {
            get
            {
                return CurrAmount / (CurrMaxAmount - CurrMinAmount);
            }
        }

        protected float currMinAmount = 0;
        protected bool initialValuesSet = false;

        /// <summary>
        /// Create a new range stat and reset values.
        /// </summary>
        /// <param name="startingAmount">The starting amount.</param>
        /// <param name="updateWithMaxAmount">Update current amount with max amount?</param>
        /// <param name="sma">The starting minimum amount.</param>
        public RangeStat(float startingAmount, float startingMaxAmount, bool updateWithMaxAmount, float startingMinAmount) : base(startingAmount, startingMaxAmount, updateWithMaxAmount)
        {
            this.startingMinAmount = startingMinAmount;
            StatReset();
        }
        
        /// <summary>
         /// Set the current amount via a percentage of the range.
         /// </summary>
         /// <param name="percentage">A value between 0 and 1.</param>
        public override void SetAmount(float percentage)
        {
            if (percentage < 0 || percentage > 1) return;
            CurrAmount = percentage * (CurrMaxAmount - CurrMinAmount) + CurrMinAmount;
        }

        /// <summary>
        /// Reset all the current values to their starting values.
        /// </summary>
        public override void StatReset()
        {
            base.StatReset();

            initialValuesSet = false;

            CurrMinAmount = startingMinAmount;
            reachedMinLimit = false;
            CurrAmount = startingAmount;
            reachedMinLimit = false;
            initialValuesSet = true;
        }
    }
}