﻿using System;
using UnityEngine;

namespace MattRGeorge.Unity.Tools.Stats
{
    /// <summary>
    /// An extension of a simple gauge stat, this adds the stat's ability to decrease/increase via an update.
    /// The update method must be called by another class, does nothing on its own.
    /// </summary>
    [Serializable]
    public class DynamicStatGauge : StatGauge
    {
        [Tooltip("The starting amount adjusted for each stat update.")]
        [SerializeField] protected float startingUpdateAmount = -1;

        /// <summary>
        /// The current amount adjusted for each stat update.
        /// </summary>
        public virtual float CurrUpdateAmount
        {
            get
            {
                return currUpdateAmount;
            }
            set
            {
                currUpdateAmount = value;
            }
        }

        protected float currUpdateAmount = 0;

        /// <summary>
        /// Create a new dynamic stat gauge.
        /// </summary>
        /// <param name="startingAmount">The starting amount.</param>
        /// <param name="updateWithMaxAmount">Update current amount with max amount?</param>
        /// <param name="startingUpdateAmount">The starting update amount.</param>
        public DynamicStatGauge(float startingAmount, float startingMaxAmount, bool updateWithMaxAmount, float startingUpdateAmount) : base(startingAmount, startingMaxAmount, updateWithMaxAmount)
        {
            this.startingUpdateAmount = startingUpdateAmount;
            StatReset();
        }

        /// <summary>
        /// Update this stat via the current update amount.
        /// </summary>
        public virtual void StatUpdate()
        {
            CurrAmount += CurrUpdateAmount;
        }
        public override void StatReset()
        {
            base.StatReset();
            CurrUpdateAmount = startingUpdateAmount;
        }
    }
}