﻿namespace MattRGeorge.Unity.Tools.Stats
{
    public interface IHasHealth
    {
        void Revive(bool resetStats);
        void Kill();
    }
}
