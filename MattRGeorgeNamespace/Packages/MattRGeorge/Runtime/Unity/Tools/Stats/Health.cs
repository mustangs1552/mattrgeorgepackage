﻿using System.Collections.Generic;
using UnityEngine;
using MattRGeorge.Unity.Utilities.Static;

namespace MattRGeorge.Unity.Tools.Stats
{
    /// <summary>
    /// The main health object.
    /// This class is where the health stat is stored and managed.
    /// Uses HealthPieces as seperate colliders that can provide their own modified value of the damage.
    /// The HealthPieces are used for things like headshots.
    /// </summary>
    public class Health : MonoBehaviour, IHasHealth, ITakesDamage
    {
        [Tooltip("The health stat.")]
        [SerializeField] protected DynamicStatGauge health = new DynamicStatGauge(100, 100, true, 0);
        [Tooltip("The passive heal (or damage) stat used with the health stat.")]
        [SerializeField] protected float passiveHealRate = 1;
        [Tooltip("The damage multiplier that is applied to all incoming damage including health pieces.")]
        public Stat damageMultiplier = new Stat(1);
        [Tooltip("The cooldown for how often damage can be applied.")]
        [SerializeField] protected float dmgHealthUpdateCooldown = .1f;
        [Tooltip("The cooldown for how often heals can be applied.")]
        [SerializeField] protected float healHealthUpdateCooldown = .1f;
        [Tooltip("The health pieces that belongs to this health object.")]
        [SerializeField] protected List<HealthPiece> healthPieces = new List<HealthPiece>();
        [Tooltip("Start updating health passively?")]
        [SerializeField] protected bool updatingHealth = true;

        /// <summary>
        /// The current amount of health.
        /// </summary>
        public virtual float CurrHealth => health.CurrAmount;
        /// <summary>
        /// The current maximum amount of health allowed.
        /// </summary>
        public virtual float CurrMaxHealth
        {
            get => health.CurrMaxAmount;
            set
            {
                health.CurrMaxAmount = value;
            }
        }
        /// <summary>
        /// The amount that is passively healed/damaged over time.
        /// </summary>
        public virtual float CurrHealthUpdateAmount
        {
            get => health.CurrUpdateAmount;
            set
            {
                health.CurrUpdateAmount = value;
            }
        }
        /// <summary>
        /// The passive heal (or damage) stat used with the health stat.
        /// </summary>
        public virtual float PassiveHealRate
        {
            get => passiveHealRate;
            set
            {
                passiveHealRate = (value < 0) ? 0 : value;
                UpdatingHealth = updatingHealth;
            }
        }
        /// <summary>
        /// Is the health being updated pasively?
        /// </summary>
        public virtual bool UpdatingHealth
        {
            get => updatingHealth;
            set
            {
                updatingHealth = value;

                if (healthUpdateCoroutine != null) StopCoroutine(healthUpdateCoroutine);
                if (updatingHealth) healthUpdateCoroutine = StartCoroutine(CoroutineUtility.WhileCoroutine(UpdateHealth, null, passiveHealRate, passiveHealRate));
            }
        }

        /// <summary>
        /// The health pieces that belongs to this health object.
        /// Get returns duplicate.
        /// </summary>
        public virtual List<HealthPiece> HealthPieces
        {
            get => new List<HealthPiece>(healthPieces);
            set
            {
                if (value == null)
                {
                    healthPieces = new List<HealthPiece>();
                    return;
                }
                healthPieces = ListUtility.RemoveNullEntries(value);
                healthPieces.ForEach(x => x.Setup(this));
            }
        }

        protected float lastDmgHealthUpdate = 0;
        protected float lastHealHealthUpdate = 0;
        protected Coroutine healthUpdateCoroutine = null;

        /// <summary>
        /// Applies the given amount of damage.
        /// </summary>
        /// <param name="amount">The amount of damage to be dealt.</param>
        /// <param name="sourceObject">The object that represents the source of the damage.</param>
        /// <param name="sourceUser">The user that the damage came from.</param>
        public virtual void TakeDamage(float amount)
        {
            if (amount <= 0) return;

            if (Time.time - lastDmgHealthUpdate >= dmgHealthUpdateCooldown)
            {
                health.CurrAmount -= DamageAlgorithm(amount);
                lastDmgHealthUpdate = Time.time;
            }
        }
        /// <summary>
        /// Applies the given amount of health to heal.
        /// </summary>
        /// <param name="amount">The amount of health to be healed.</param>
        /// <param name="sourceObject">The object that represents the source of the health.</param>
        /// <param name="sourceUser">The user that the heal came from.</param>
        public virtual void Heal(float amount)
        {
            if (amount <= 0) return;

            if (Time.time - lastHealHealthUpdate >= healHealthUpdateCooldown)
            {
                health.CurrAmount += HealAlgorithm(amount);
                lastHealHealthUpdate = Time.time;
            }
        }

        /// <summary>
        /// The way that incoming damage is calculated for how much health is ultimately dealt.
        /// </summary>
        /// <param name="amount">The amount of damage to be dealt.</param>
        /// <returns>The final amount of damage to be dealt.</returns>
        public virtual float DamageAlgorithm(float amount)
        {
            if (amount <= 0) return 0;

            return amount * damageMultiplier.CurrAmount;
        }
        /// <summary>
        /// The way that incoming heals are calculated for how much health is ultimately healed.
        /// </summary>
        /// <param name="amount">The amount of health to be healed.</param>
        /// <returns>The final amount of health to be healed.</returns>
        public virtual float HealAlgorithm(float amount)
        {
            if (amount <= 0) return 0;

            return amount;
        }

        /// <summary>
        /// Revive this object and reset the health stat.
        /// </summary>
        /// <param name="resetStats">Reset all other stats as well?</param>
        public virtual void Revive(bool resetStats)
        {
            if (!resetStats)  health.StatReset();
            else ResetStats();
        }
        /// <summary>
        /// Kill this object.
        /// </summary>
        public virtual void Kill()
        {
            health.CurrAmount -= health.CurrMaxAmount;
        }

        /// <summary>
        /// Reset all stats to default.
        /// </summary>
        public virtual void ResetStats()
        {
            health.StatReset();
            damageMultiplier.StatReset();
        }

        /// <summary>
        /// Update the health for the passive heal/damage amount.
        /// </summary>
        /// <returns>True if should no longer update health.</returns>
        protected virtual bool UpdateHealth()
        {
            health.StatUpdate();
            return updatingHealth;
        }

        /// <summary>
        /// Setup the stats.
        /// </summary>
        protected virtual void SetupStats()
        {
            HealthPieces = healthPieces;

            ResetStats();
            UpdatingHealth = updatingHealth;
        }

        protected virtual void OnValidate()
        {
            PassiveHealRate = passiveHealRate;
            if (dmgHealthUpdateCooldown < 0) dmgHealthUpdateCooldown = 0;
            if (healHealthUpdateCooldown < 0) healHealthUpdateCooldown = 0;
            HealthPieces = healthPieces;
        }

        protected virtual void Awake()
        {
            SetupStats();
        }
    }
}
