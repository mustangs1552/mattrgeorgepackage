﻿using UnityEngine;

namespace MattRGeorge.Unity.Tools.Stats
{
    /// <summary>
    /// Requires a Health object, this object has no health.
    /// This object represents a part that takes a different amount of damage like a headshot.
    /// </summary>
    public class HealthPiece : MonoBehaviour, ITakesDamage
    {
        [Tooltip("The damage multiplier that is applied to all incoming damage.")]
        public Stat damageMultiplier = new Stat(1);

        protected Health mainHealth = null;

        /// <summary>
        /// Applies the given amount of damage using the algorithm and passes it on to the main health object.
        /// </summary>
        /// <param name="amount">The amount of damage to be dealt.</param>
        /// <param name="sourceObject">The object that represents the source of the damage.</param>
        /// <param name="sourceUser">The user that the damage came from.</param>
        public virtual void TakeDamage(float amount)
        {
            if (!mainHealth) return;

            mainHealth.TakeDamage(DamageAlgorithm(amount));
        }
        /// <summary>
        /// Passes on the given amount of health to heal using the algorithm to the main health object.
        /// </summary>
        /// <param name="amount">The amount of health to be healed.</param>
        /// <param name="sourceObject">The object that represents the source of the health.</param>
        /// <param name="sourceUser">The user that the heal came from.</param>
        public virtual void Heal(float amount)
        {
            if (!mainHealth) return;

            mainHealth.Heal(HealAlgorithm(amount));
        }

        /// <summary>
        /// The way that incoming damage is calculated for how much health is ultimately dealt.
        /// </summary>
        /// <param name="amount">The amount of damage to be dealt.</param>
        /// <returns>The final amount of damage to be dealt.</returns>
        public virtual float DamageAlgorithm(float amount)
        {
            if (amount <= 0) return 0;

            return amount * damageMultiplier.CurrAmount;
        }
        /// <summary>
        /// The way that incoming heals are calculated for how much health is ultimately healed.
        /// </summary>
        /// <param name="amount">The amount of health to be healed.</param>
        /// <returns>The final amount of health to be healed.</returns>
        public virtual float HealAlgorithm(float amount)
        {
            if (amount <= 0) return 0;

            return amount;
        }

        /// <summary>
        /// Initial setup.
        /// Needs to be called so that the main health object can be set.
        /// </summary>
        /// <param name="mainHealth">The main health object this piece belongs to.</param>
        public virtual void Setup(Health mainHealth)
        {
            this.mainHealth = mainHealth;
            damageMultiplier.StatReset();
        }
    }
}
