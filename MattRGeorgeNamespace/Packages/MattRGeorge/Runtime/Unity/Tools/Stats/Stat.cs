﻿using System;
using UnityEngine;
using MattRGeorge.Unity.Utilities.Helpers;

namespace MattRGeorge.Unity.Tools.Stats
{
    /// <summary>
    /// A stat with a single value that can be changed.
    /// </summary>
    [Serializable]
    public class Stat
    {
        [Tooltip("The starting value for this stat.")]
        [SerializeField] protected float startingAmount = 1;

        /// <summary>
        /// The current amount of this stat.
        /// </summary>
        public virtual float CurrAmount
        {
            get
            {
                return currAmount;
            }
            set
            {
                currAmount = value;
                OnChanged?.Invoke(CurrAmount);
            }
        }

        public FloatUnityEvent OnChanged = new FloatUnityEvent();

        protected float currAmount = 0;

        /// <summary>
        /// Create a new stat.
        /// </summary>
        /// <param name="startingAmount">The starting amount.</param>
        public Stat(float startingAmount)
        {
            this.startingAmount = startingAmount;
            StatReset();
        }

        /// <summary>
        /// Reset all the current values to their starting values.
        /// </summary>
        public virtual void StatReset()
        {
            CurrAmount = startingAmount;
        }
    }
}