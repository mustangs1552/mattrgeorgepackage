﻿using UnityEngine;

namespace MattRGeorge.Unity.Tools.Stats
{
    public interface ITakesDamage
    {
        void TakeDamage(float amount);
        void Heal(float amount);
    }
}
