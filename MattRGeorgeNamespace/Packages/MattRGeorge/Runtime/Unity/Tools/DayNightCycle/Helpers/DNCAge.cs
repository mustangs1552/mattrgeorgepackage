﻿namespace MattRGeorge.Unity.Tools.DayNightCycle.Helpers
{
    /// <summary>
    /// A helper class for storing values for an age.
    /// </summary>
    public class DNCAge
    {
        public int days = 0;
        public int months = 0;
        public int years = 0;
        public ulong totalDays = 0;

        public DNCAge()
        {

        }
        public DNCAge(int d, int m, int y, ulong td)
        {
            days = d;
            months = m;
            years = y;
            totalDays = td;
        }

        /// <summary>
        /// Returns the age in the format: D, M, Y, TotalDays.
        /// </summary>
        /// <returns>The age in the format: D, M, Y, TotalDays</returns>
        public override string ToString()
        {
            return string.Format("{0}, {1}, {2}, {3}", days, months, years, totalDays);
        }
    }
}