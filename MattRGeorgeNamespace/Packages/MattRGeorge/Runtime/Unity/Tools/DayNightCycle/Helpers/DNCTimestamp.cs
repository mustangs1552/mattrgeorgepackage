﻿namespace MattRGeorge.Unity.Tools.DayNightCycle.Helpers
{
    /// <summary>
    /// A timestamp of the current time.
    /// These values can't be changed once they are set.
    /// </summary>
    public class DNCTimestamp
    {
        public DNCTime time = new DNCTime();
        public ulong totalSecs = 0;
        public ulong totalTimeUpdates = 0;
        public float dayProgress = 0;

        public DNCTimestamp()
        {

        }
        public DNCTimestamp(int sec, int min, int hr, ulong ts, ulong ttu, float dp)
        {
            time = new DNCTime(sec, min, hr);
            totalSecs = ts;
            totalTimeUpdates = ttu;
            dayProgress = dp;
        }
        public DNCTimestamp(DNCTime t, ulong ts, ulong ttu, float dp)
        {
            time = t;
            totalSecs = ts;
            totalTimeUpdates = ttu;
            dayProgress = dp;
        }

        /// <summary>
        /// Returns the time in the format: H:M:S TotalSec:TotalTimeUpdates DayProgress.
        /// </summary>
        /// <returns>The time in the format: H:M:S TotalSec:TotalTimeUpdates DayProgress.</returns>
        public override string ToString()
        {
            return string.Format("{0} {1}:{2} {3}", time.ToString(), totalSecs, totalTimeUpdates, dayProgress);
        }
    }
}