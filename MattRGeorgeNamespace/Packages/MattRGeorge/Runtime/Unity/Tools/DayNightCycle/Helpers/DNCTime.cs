﻿using System;

namespace MattRGeorge.Unity.Tools.DayNightCycle.Helpers
{
    /// <summary>
    /// A helper class that stores a time.
    /// </summary>
    [Serializable]
    public class DNCTime
    {
        public int sec = 0;
        public int min = 0;
        public int hr = 0;

        public DNCTime()
        {

        }
        public DNCTime(int s, int m, int h)
        {
            sec = s;
            min = m;
            hr = h;
        }

        /// <summary>
        /// Returns the time in the format: H:M:S.
        /// </summary>
        /// <returns>The time in the format: H:M:S.</returns>
        public override string ToString()
        {
            return string.Format("{0}:{1}:{2}", hr, min, sec);
        }
    }
}