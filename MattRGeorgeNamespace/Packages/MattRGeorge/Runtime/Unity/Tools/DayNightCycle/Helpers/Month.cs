﻿using System;
using UnityEngine;

namespace MattRGeorge.Unity.Tools.DayNightCycle.Helpers
{
    /// <summary>
    /// A month.
    /// </summary>
    [Serializable]
    public class Month
    {
        [Tooltip("The name.")]
        public string name = "";
        [Tooltip("The total days in the month.")]
        public int days = 0;
        [Tooltip("The amount to adjust the days by during a leap year.")]
        public int leapYearDayAdjustment = 0;

        public Month()
        {

        }
        public Month(string n, int d)
        {
            name = n;
            days = d;
        }
        public Month(string n, int d, int lyd)
        {
            name = n;
            days = d;
            leapYearDayAdjustment = lyd;
        }

        /// <summary>
        /// Returns the month in the format: Name Days LeapYearAdjustment.
        /// </summary>
        /// <returns>The month in the format: Name Days LeapYearAdjustment.</returns>
        public override string ToString()
        {
            return string.Format("{0} {1} {2}", name, days, leapYearDayAdjustment);
        }
    }
}