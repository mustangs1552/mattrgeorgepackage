﻿using System;
using UnityEngine;

namespace MattRGeorge.Unity.Tools.DayNightCycle.Helpers
{
    /// <summary>
    /// A helper class that stores a date.
    /// </summary>
    [Serializable]
    public class DNCDate
    {
        [Tooltip("The day as a 0 base value.")]
        public int day = 0;
        [Tooltip("The month as a 0 base value.")]
        public int month = 0;
        [Tooltip("The year.")]
        public int year = 0;

        public DNCDate()
        {

        }
        public DNCDate(int d, int m, int y)
        {
            day = d;
            month = m;
            year = y;
        }

        /// <summary>
        /// Returns the date in the format: M/D/Y.
        /// </summary>
        /// <returns>The date in the format: M/D/Y</returns>
        public override string ToString()
        {
            return string.Format("{0}/{1}/{2}", day, month, year);
        }
    }
}