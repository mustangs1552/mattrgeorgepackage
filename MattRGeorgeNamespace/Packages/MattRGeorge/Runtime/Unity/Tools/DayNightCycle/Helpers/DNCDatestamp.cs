﻿namespace MattRGeorge.Unity.Tools.DayNightCycle.Helpers
{
    /// <summary>
    /// The timestamp of a date.
    /// </summary>
    public class DNCDatestamp
    {
        public DNCDate date = new DNCDate();
        public int season = 0;
        public bool isLeapYear = false;
        public ulong totalDays = 0;

        public DNCDatestamp()
        {

        }
        public DNCDatestamp(int d, int m, int y, int s, bool ly, ulong td)
        {
            date = new DNCDate(d, m, y);
            season = s;
            isLeapYear = ly;
            totalDays = td;
        }
        public DNCDatestamp(DNCDate d, int s, bool ly, ulong td)
        {
            date = d;
            season = s;
            isLeapYear = ly;
            totalDays = td;
        }

        /// <summary>
        /// Returns the date in the format: M/D/Y Season LeapYear TotalDays.
        /// </summary>
        /// <returns>The date in the format: M/D/Y Season LeapYear TotalDays</returns>
        public override string ToString()
        {
            return string.Format("{0} {1} {2} {3}", date.ToString(), season, isLeapYear, totalDays);
        }
    }
}