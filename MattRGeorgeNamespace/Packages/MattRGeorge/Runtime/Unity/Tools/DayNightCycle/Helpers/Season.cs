﻿using System;
using UnityEngine;

namespace MattRGeorge.Unity.Tools.DayNightCycle.Helpers
{
    /// <summary>
    /// A season.
    /// </summary>
    [Serializable]
    public class Season
    {
        [Tooltip("The name.")]
        public string name = "";
        [Tooltip("The starting date for the season.")]
        public DNCDate startDate = new DNCDate();

        public Season()
        {

        }
        public Season(string n, DNCDate sd)
        {
            name = n;
            startDate = sd;
        }
        public Season(string n, int d, int m)
        {
            name = n;
            startDate = new DNCDate(d, m, 0);
        }

        /// <summary>
        /// Returns the season in the format: Name StartDate.
        /// </summary>
        /// <returns>The season in the format: Name StartDate.</returns>
        public override string ToString()
        {
            return string.Format("{0} {1}", name, startDate.ToString());
        }
    }
}