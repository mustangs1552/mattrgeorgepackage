﻿using UnityEngine;

namespace MattRGeorge.Unity.Tools.DayNightCycle
{
    /// <summary>
    /// The main access point of the day night cycle system.
    /// This class has its own time and date engines that other objects are able to access to update what they need to.
    /// </summary>
    public class DayNightCycle : MonoBehaviour
    {
        public static DayNightCycle SINGLETON = null;

        [Tooltip("Setup a singleton?")]
        [SerializeField] private bool useSingleton = true;

        /// <summary>
        /// The time engine that this day night cycle is using.
        /// </summary>
        public TimeEngine TimeEngineObj
        {
            get
            {
                return timeEngine;
            }
        }
        /// <summary>
        /// The date engine that this day night cycle is using.
        /// </summary>
        public DateEngine DateEngineObj
        {
            get
            {
                return dateEngine;
            }
        }

        private TimeEngine timeEngine = null;
        private DateEngine dateEngine = null;

        #region Methods
        /// <summary>
        /// Initial setup.
        /// </summary>
        private void Setup()
        {
            if (useSingleton)
            {
                if (DayNightCycle.SINGLETON == null) SINGLETON = this;
                else
                {
                    Debug.LogError("More than one 'DayNightCycle' singletons detected!");
                    Destroy(this);
                }
            }

            if (timeEngine == null) Debug.LogError("No 'timeEngine' assigned!");
            if (dateEngine == null) Debug.LogError("No 'dateEngine' assigned!");
        }
        /// <summary>
        /// Initital setup after Setup().
        /// </summary>
        private void PostSetup()
        {
            timeEngine.StartTime();
            dateEngine.StartDate();
        }
        #endregion

        private void Awake()
        {
            Setup();
        }
        private void Start()
        {
            PostSetup();
        }
    }
}
