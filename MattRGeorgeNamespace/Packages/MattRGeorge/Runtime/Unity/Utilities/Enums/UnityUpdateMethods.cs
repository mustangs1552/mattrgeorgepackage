﻿namespace MattRGeorge.Unity.Utilities.Enums
{
    /// <summary>
    /// Unity update methods.
    /// </summary>
    public enum UnityUpdateMethods
    {
        None,
        Update,
        FixedUpdate
    }
}
