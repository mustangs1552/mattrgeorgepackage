﻿namespace MattRGeorge.Unity.Utilities.Enums
{
    /// <summary>
    /// Unity start and update methods.
    /// </summary>
    public enum UnityMethods
    {
        None,
        Awake,
        Start,
        Update,
        FixedUpdate
    }
}
