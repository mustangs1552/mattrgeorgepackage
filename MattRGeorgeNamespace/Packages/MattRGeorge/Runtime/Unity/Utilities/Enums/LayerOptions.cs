﻿namespace MattRGeorge.Unity.Utilities.Enums
{
    /// <summary>
    /// Layer values for "Everything" and "Nothing".
    /// </summary>
    public enum LayerOptions
    {
        Everything = ~0,
        Nothing = 0
    }
}
