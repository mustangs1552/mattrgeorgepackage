﻿namespace MattRGeorge.Unity.Utilities.Enums
{
    /// <summary>
    /// Unity start methods.
    /// </summary>
    public enum UnityStartMethods
    {
        None,
        Awake,
        Start
    }
}
