﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections.Generic;

namespace MattRGeorge.Unity.Utilities.Static
{
    /// <summary>
    /// Contains utitlity methods for working with UIs.
    /// </summary>
    public static class UIUtility
    {
        /// <summary>
        /// Cast a ray from the mouse and return all the given components from the UIs that it hits.
        /// </summary>
        /// <typeparam name="T">The monobehaviour component to get.</typeparam>
        /// <returns>A list of the found monobehaviour components.</returns>
        public static List<T> CastRayFromMouse<T>()
        {
            PointerEventData pointerEventData = new PointerEventData(EventSystem.current);
            pointerEventData.position = Input.mousePosition;
            List<RaycastResult> results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(pointerEventData, results);

            List<T> returnResults = new List<T>();
            foreach(RaycastResult obj in results)
            {
                T resultType = obj.gameObject.GetComponent<T>();
                if (resultType != null) returnResults.Add(resultType);
            }
            return returnResults;
        }
    }
}
