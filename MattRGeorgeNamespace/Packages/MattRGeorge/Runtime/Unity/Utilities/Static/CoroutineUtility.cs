﻿using System;
using System.Collections;
using UnityEngine;

namespace MattRGeorge.Unity.Utilities.Static
{
    public static class CoroutineUtility
    {
        /// <summary>
        /// Runs a while loop within a coroutine so Unity doesn't lock up in the case of an infinite loop.
        /// </summary>
        /// <param name="CodeBlock">The code to run in the while loop returning whether or not to continue.</param>
        /// <param name="OnFinished">Called when the while loop finishes (optional).</param>
        /// <param name="initialDelay">The delay before starting the while loop.</param>
        /// <param name="timeBetweenLoops">Time to wait between loops (optional).</param>
        /// <param name="useRealtimeSecs">Use WaitForSecondsRealtime instead of WaitForSeconds?</param>
        public static IEnumerator WhileCoroutine(Func<bool> CodeBlock, Action OnFinished = null, float initialDelay = 0, float timeBetweenLoops = 0, bool useRealtimeSecs = false)
        {
            if (CodeBlock == null) yield break;
            if (initialDelay > 0)
            {
                if (!useRealtimeSecs) yield return new WaitForSeconds(initialDelay);
                else yield return new WaitForSecondsRealtime(initialDelay);
            }

            while (CodeBlock())
            {
                if (timeBetweenLoops > 0)
                {
                    if (!useRealtimeSecs) yield return new WaitForSeconds(timeBetweenLoops);
                    else yield return new WaitForSecondsRealtime(timeBetweenLoops);
                }
                else yield return new WaitForEndOfFrame();
            }

            OnFinished?.Invoke();
        }

        /// <summary>
        /// A coroutine version that is intended to replace Invoke().
        /// </summary>
        /// <param name="method">The method to call.</param>
        /// <param name="delay">The delay before calling the given method.</param>
        /// <param name="useRealtimeSecs">Use WaitForSecondsRealtime instead of WaitForSeconds?</param>
        public static IEnumerator DelayedMethodCoroutine(Action method, float delay, bool useRealtimeSecs = false)
        {
            if (method == null || delay < 0) yield break;

            if (delay > 0)
            {
                if (!useRealtimeSecs) yield return new WaitForSeconds(delay);
                else yield return new WaitForSecondsRealtime(delay);
            }
            else yield return new WaitForEndOfFrame();

            method?.Invoke();
        }
    }
}
