﻿using UnityEngine;

namespace MattRGeorge.Unity.Utilities.Static
{
    /// <summary>
    /// Functions for actions on the parents and children of a hierarchy.
    /// </summary>
    public static class ParentChildUtility
    {
        /// <summary>
        /// Returns the Transform of the highest parent of the given child.
        /// </summary>
        /// <param name="child">The child to find the highest parent of.</param>
        /// <returns>The highest parent.</returns>
        public static Transform GetHighestParent(Transform child)
        {
            if (!child) return null;

            Transform currParent = child;
            Transform highestParent = null;
            while (highestParent == null)
            {
                if (!currParent.parent) highestParent = currParent;
                else currParent = currParent.parent;
            }

            return highestParent;
        }

        /// <summary>
        /// Checks to see if the given child has the given parent as a parent.
        /// </summary>
        /// <param name="child">The child to start at.</param>
        /// <param name="parent">The parent to check for.</param>
        /// <returns>True if the child does have the given parent as a parent.</returns>
        public static bool IsParent(Transform child, Transform parent)
        {
            if (!child || !parent || child == parent) return false;

            Transform nextChild = child;
            while(true)
            {
                if (!nextChild.parent) return false;
                else if (nextChild.parent == parent) return true;

                nextChild = nextChild.parent;
            }
        }

        /// <summary>
        /// Iterates through all of an object's children (including beyond the first level of children) and encapsulates thier bounds with this object's.
        /// </summary>
        /// <param name="obj">The object parent to be iterated through.</param>
        /// <returns>The bounds of this object and its children.</returns>
        public static Bounds GetAllChildrenBounds(Transform obj)
        {
            if (!obj) return new Bounds();

            Renderer[] renderers = obj.GetComponentsInChildren<Renderer>();
            if (renderers.Length == 0) return new Bounds();
            Bounds objBounds = renderers[0].bounds;

            for (int i = 1; i < renderers.Length; i++)
            {
                objBounds.Encapsulate(renderers[i].bounds);
            }

            return objBounds;
        }

        /// <summary>
        /// Iterates through all of an object's children (including beyond the first level of children) and adds a Mesh Collider to any that have renderers.
        /// </summary>
        /// <param name="obj">The object to be iterated through.</param>
        /// <param name="includeThisObj">Add a Mesh Collider to `obj` also?</param>
        public static void AddChildrenMeshColliders(Transform obj, bool includeThisObj = false)
        {
            if (!obj) return;

            if(includeThisObj)
            {
                if (obj.GetComponent<Renderer>()) obj.gameObject.AddComponent<MeshCollider>();
            }

            Transform currChild = null;
            Renderer currRenderer = null;
            for (int i = 0; i < obj.childCount; i++)
            {
                currChild = obj.GetChild(i);
                currRenderer = currChild.GetComponent<Renderer>();
                if (currRenderer) currChild.gameObject.AddComponent<MeshCollider>();
                if (currChild.childCount > 0) AddChildrenMeshColliders(currChild);
            }
        }
    }
}
