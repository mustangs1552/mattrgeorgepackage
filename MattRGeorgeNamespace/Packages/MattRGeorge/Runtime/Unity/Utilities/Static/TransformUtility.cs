﻿using System.Collections.Generic;
using UnityEngine;

namespace MattRGeorge.Unity.Utilities.Static
{
    /// <summary>
    /// A utility for Unity's Transform component.
    /// </summary>
    public static class TransformUtility
    {
        /// <summary>
        /// Get the closest transform to the givin position from the list of objects.
        /// </summary>
        /// <param name="startingPoint">The position to measure closest to.</param>
        /// <param name="colliders">The list of objects to go through.</param>
        /// <returns>The closest Transform to the starting point.</returns>
        public static Transform GetClosest(Vector3 startingPoint, List<Collider> colliders)
        {
            if (colliders == null || colliders.Count == 0) return null;

            List<Transform> trans = new List<Transform>();
            colliders.ForEach(x => trans.Add(x.transform));
            return GetClosest(startingPoint, trans);
        }
        /// <summary>
        /// Get the closest transform to the givin position from the list of objects.
        /// </summary>
        /// <param name="startingPoint">The position to measure closest to.</param>
        /// <param name="transforms">The list of objects to go through.</param>
        /// <returns>The closest Transform to the starting point.</returns>
        public static Transform GetClosest(Vector3 startingPoint, List<Transform> transforms)
        {
            if (transforms == null || transforms.Count == 0) return null;

            float currClosestDist = 0;
            float currDist = 0;
            Transform currClosestTrans = null;
            foreach (Transform trans in transforms)
            {
                currDist = Vector3.Distance(startingPoint, trans.position);

                if (!currClosestTrans || currDist < currClosestDist)
                {
                    currClosestTrans = trans;
                    currClosestDist = currDist;
                }
            }

            return currClosestTrans;
        }

        /// <summary>
        /// Check to see if the given object is within the FOV cone range of the given FOV source object.
        /// </summary>
        /// <param name="self">The FOV source object.</param>
        /// <param name="obj">The object to check.</param>
        /// <param name="fov">The FOV cone degrees (0-359).</param>
        /// <returns>True if within FOV cone range.</returns>
        public static bool CheckFOV(Transform self, Transform obj, float fov)
        {
            if (!self || !obj || fov < 0 || fov > 360) return false;

            Vector3 targetDir = obj.position - self.position;
            float angleForward = Vector3.Angle(targetDir, self.forward);
            float angleBackward = Vector3.Angle(targetDir, -self.forward);
            return ((targetDir.z >= 0) ? angleForward : 90 - angleBackward + 90) <= fov / 2;
        }
    }
}
