﻿using UnityEngine;

namespace MattRGeorge.Unity.Utilities.Static
{
    /// <summary>
    /// Utilities for Unity GameObjects.
    /// </summary>
    public static class GameObjectUtility
    {
        /// <summary>
        /// Returns the actual name of the given GameObject (The name without any trailing "(Clone)"s).
        /// </summary>
        /// <param name="obj">The object to get the actual name of.</param>
        /// <returns>The actual name.</returns>
        public static string GetActualName(GameObject obj)
        {
            if (!obj) return "";

            string name = "";
            for (int i = 0; i < obj.name.Length; i++)
            {
                if (obj.name[i] == '(' && i + 7 <= obj.name.Length && obj.name.Substring(i, 7).ToLower() == "(clone)") break;
                name += obj.name[i];
            }

            return name;
        }
    }
}