﻿using UnityEngine;

namespace MattRGeorge.Unity.Utilities.Static
{
    /// <summary>
    /// Utilities for math actions using Unity's math code.
    /// </summary>
    public static class UnityMathUtility
    {
        /// <summary>
        /// Find the angle difference between two Transforms.
        /// </summary>
        /// <param name="rotA">Transform A.</param>
        /// <param name="rotB">Transform B.</param>
        /// <param name="direction">The forward direction.</param>
        /// <returns>The angle difference between -180-180.</returns>
        public static float FindAngleXZ(Transform rotA, Transform rotB, Vector3 direction)
        {
            if (!rotA || !rotB) return 0;

            Vector3 forwardA = rotA.rotation * direction;
            Vector3 forwardB = rotB.rotation * direction;
            float angleA = Mathf.Atan2(forwardA.x, forwardA.z) * Mathf.Rad2Deg;
            float angleB = Mathf.Atan2(forwardB.x, forwardB.z) * Mathf.Rad2Deg;
            return Mathf.DeltaAngle(angleA, angleB);
        }
    }
}
