﻿using UnityEngine;

namespace MattRGeorge.Unity.Utilities.Static
{
    public static class LayerUtility
    {
        /// <summary>
        /// Check to see if the given object's layer is in the given layer mask.
        /// </summary>
        /// <param name="layerMask">The layer mask.</param>
        /// <param name="layerName">The object.</param>
        /// <returns>True if the object's layer is in the layer mask.</returns>
        public static bool InLayerMask(LayerMask layerMask, GameObject obj)
        {
            if (!obj) return false;

            return InLayerMask(layerMask, LayerMask.LayerToName(obj.layer));
        }
        /// <summary>
        /// Check to see if the given layer is in the given layer mask.
        /// </summary>
        /// <param name="layerMask">The layer mask.</param>
        /// <param name="layerName">The name of the layer.</param>
        /// <returns>True if the layer is in the layer mask.</returns>
        public static bool InLayerMask(LayerMask layerMask, string layerName)
        {
            if (string.IsNullOrEmpty(layerName)) return false;

            return (layerMask.value & 1 << LayerMask.NameToLayer(layerName)) != 0;
        }

        /// <summary>
        /// Add the given layer to the referenced layer mask.
        /// </summary>
        /// <param name="layerName">Name of the layer.</param>
        /// <param name="layerMask">Reference to the layer mask.</param>
        public static void AddLayerToLayerMask(string layerName, ref LayerMask layerMask)
        {
            if (string.IsNullOrEmpty(layerName)) return;

            layerMask |= 1 << LayerMask.NameToLayer(layerName);
        }
        /// <summary>
        /// Remove the given layer from the referenced layer mask.
        /// </summary>
        /// <param name="layerName">Name of the layer.</param>
        /// <param name="layerMask">Reference to the layer mask.</param>
        public static void RemoveLayerFromLayerMask(string layerName, ref LayerMask layerMask)
        {
            if (string.IsNullOrEmpty(layerName)) return;

            layerMask &= ~(1 << LayerMask.NameToLayer(layerName));
        }
    }
}
