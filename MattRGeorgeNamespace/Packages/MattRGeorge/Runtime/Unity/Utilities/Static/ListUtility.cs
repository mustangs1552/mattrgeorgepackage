﻿using System;
using System.Collections.Generic;
using MattRGeorge.General.Utilities.Enums;
using MattRGeorge.Unity.Utilities.Helpers;

namespace MattRGeorge.Unity.Utilities.Static
{
    /// <summary>
    /// Utilities for Lists, Dictionaries, etc.
    /// </summary>
    public static class ListUtility
    {
        /// <summary>
        /// Returns a new list that contains no null values.
        /// Will also check for destroyed `UnityEngine.Object` cases.
        /// </summary>
        /// <typeparam name="T">The List's contents' type.</typeparam>
        /// <param name="list">The list to remove nulls from.</param>
        /// <returns>A new list of the given list with no null values.</returns>
        public static List<T> RemoveNullEntries<T>(List<T> list)
        {
            if (list == null || list.Count == 0) return list;

            List<T> nullessList = new List<T>();
            UnityEngine.Object currObj = null;
            foreach (T obj in list)
            {
                if (typeof(T).IsSubclassOf(typeof(UnityEngine.Object)))
                {
                    currObj = obj as UnityEngine.Object;
                    if (currObj) nullessList.Add(obj);
                }
                else if (obj != null) nullessList.Add(obj);
            }
            return nullessList;
        }
        /// <summary>
        /// Returns a new dictionary with no null values.
        /// Will also check for destroyed `UnityEngine.Object` cases for the key and value.
        /// </summary>
        /// <typeparam name="K">The dictionary's key type.</typeparam>
        /// <typeparam name="V">The dictionary's value type.</typeparam>
        /// <param name="dictionary">The dictionary to remove nulls from.</param>
        /// <returns>A new dictionary of the given dictionary with no null values.</returns>
        public static Dictionary<K, V> RemoveNullEntries<K, V>(Dictionary<K, V> dictionary)
        {
            if (dictionary == null || dictionary.Count == 0) return dictionary;

            Dictionary<K, V> returnDict = new Dictionary<K, V>();
            UnityEngine.Object currObj = null;
            foreach (KeyValuePair<K, V> pair in dictionary)
            {
                // Check Key for `UnityEngine.Object` null (normal null isn't possible as key)
                if (typeof(K).IsSubclassOf(typeof(UnityEngine.Object)))
                {
                    currObj = pair.Key as UnityEngine.Object;
                    if (!currObj) continue;
                }

                // Check Value if after Key was confirmed valid
                if (typeof(V).IsSubclassOf(typeof(UnityEngine.Object)))
                {
                    currObj = pair.Value as UnityEngine.Object;
                    if(currObj) returnDict.Add(pair.Key, pair.Value);
                }
                else if(pair.Value != null) returnDict.Add(pair.Key, pair.Value);
            }

            return returnDict;
        }

        /// <summary>
        /// Returns a new dictionary with list as its value with no null values.
        /// Will also check for destroyed `UnityEngine.Object` cases for the key and value.
        /// </summary>
        /// <typeparam name="K">The dictionary's key type.</typeparam>
        /// <typeparam name="V">The dictionary's List's value type.</typeparam>
        /// <param name="dictionary">The dictionary to remove nulls from.</param>
        /// <returns>A new dictionary of the given dictionary with no null values.</returns>
        public static Dictionary<K, List<V>> RemoveNullEntriesFromDictionaryValue<K, V>(Dictionary<K, List<V>> dictionary)
        {
            if (dictionary == null || dictionary.Count == 0) return dictionary;

            Dictionary<K, List<V>> returnDict = new Dictionary<K, List<V>>();
            UnityEngine.Object currObj = null;
            foreach (KeyValuePair<K, List<V>> pair in dictionary)
            {
                // Check Key for `UnityEngine.Object` null (normal null isn't possible as key)
                if (typeof(K).IsSubclassOf(typeof(UnityEngine.Object)))
                {
                    currObj = pair.Key as UnityEngine.Object;
                    if (currObj && pair.Value != null) returnDict.Add(pair.Key, RemoveNullEntries(pair.Value));
                }
                else if (pair.Value != null) returnDict.Add(pair.Key, RemoveNullEntries(pair.Value));
            }

            return returnDict;
        }

        /// <summary>
        /// Gets an object from the given list by the given RandomListMode.
        /// Modes:
        ///     Randomly: Just picks a random object from the given list. Does not use 'usedIndexes'.
        ///     AllRandomly: Picks a random object from the given list and will not pick the same one twice until they have all been chosen. Uses 'usedIndexes'.
        ///     AllSequentially: Picks an object in the order they appear in the given list. Uses 'usedIndexes'.
        /// </summary>
        /// <param name="mode">The mode to use.</param>
        /// <param name="objs">The list of objects to choose from.</param>
        /// <param name="usedIndexes">The indexes to not use (Not used when `mode == ListChoiceMode.Randomly`).</param>
        /// <returns>The chosen object.</returns>
        public static T GetObjectByMode<T>(ListRandomChoiceMode mode, List<T> objs, ref HashSet<int> usedIndexes)
        {
            if (objs == null || objs.Count == 0) return default;
            if (mode != ListRandomChoiceMode.Randomly && (usedIndexes == null || usedIndexes.Count >= objs.Count)) usedIndexes = new HashSet<int>();

            T chosenObj = default;
            if (mode == ListRandomChoiceMode.Randomly)
            {
                int randNum = UnityEngine.Random.Range(0, objs.Count);
                chosenObj = objs[randNum];
            }
            else if (mode == ListRandomChoiceMode.AllRandomly) chosenObj = GetValidRandomObject(objs, ref usedIndexes);
            else
            {
                for (int i = 0; i < objs.Count; i++)
                {
                    if (objs[i] == null || usedIndexes.Contains(i)) continue;

                    chosenObj = objs[i];
                    usedIndexes.Add(i);
                    break;
                }
            }

            return chosenObj;
        }

        /// <summary>
        /// Get a random object from the given list with an index that is not present in the given list of invalid indexes.
        /// </summary>
        /// <param name="objs">The list of objects to choose from.</param>
        /// <param name="invalidIndexes">The list of indexes to not choose from.</param>
        /// <returns>The chosen object.</returns>
        public static T GetValidRandomObject<T>(List<T> objs, ref HashSet<int> invalidIndexes)
        {
            if (invalidIndexes == null) invalidIndexes = new HashSet<int>();
            if (objs == null || objs.Count == 0 || objs.Count <= invalidIndexes.Count) return default;

            List<Tuple<int, T>> validObjs = new List<Tuple<int, T>>();
            for(int i = 0; i < objs.Count; i++)
            {
                if (!invalidIndexes.Contains(i)) validObjs.Add(new Tuple<int, T>(i, objs[i]));
            }

            int randNum = UnityEngine.Random.Range(0, validObjs.Count);
            Tuple<int, T> chosenObj = validObjs[randNum];
            invalidIndexes.Add(chosenObj.Item1);
            return chosenObj.Item2;
        }

        /// <summary>
        /// Gets a random object from the given list using the chance values of the objects.
        /// </summary>
        /// <typeparam name="T">The type of objects in the list.</typeparam>
        /// <param name="objs">The list of objects with chance values.</param>
        /// <returns>The chosen object.</returns>
        public static T GetRandomObjectWithChance<T>(List<DictionaryTObjectChance<T>> objs)
        {
            HashSet<int> temp = new HashSet<int>();
            return GetRandomObjectWithChance(objs, ref temp);
        }
        /// <summary>
        /// Gets a random object from the given list using the chance values of the objects.
        /// Will not return objects at the indexes provided in the invalidIndexes.
        /// </summary>
        /// <typeparam name="T">The type of objects in the list.</typeparam>
        /// <param name="objs">The list of objects with chance values.</param>
        /// <param name="invalidIndexes">The list of invalid object indexes.</param>
        /// <returns>The chosen object.</returns>
        public static T GetRandomObjectWithChance<T>(List<DictionaryTObjectChance<T>> objs, ref HashSet<int> invalidIndexes)
        {
            if (invalidIndexes == null) invalidIndexes = new HashSet<int>();
            if (objs == null || objs.Count == 0 || objs.Count <= invalidIndexes.Count) return default;

            List<int> validIndexes = new List<int>();
            List<DictionaryTObjectChance<T>> validObjs = new List<DictionaryTObjectChance<T>>();
            int sumChances = 0;
            for (int i = 0; i < objs.Count; i++)
            {
                if (!invalidIndexes.Contains(i) && objs[i].chance > 0)
                {
                    sumChances += objs[i].chance;
                    validIndexes.Add(i);
                    validObjs.Add(objs[i]);
                }
            }
            if (validObjs.Count == 0) return default;

            int selectedI = 0;
            int randNum = UnityEngine.Random.Range(0, sumChances);
            T selectedObj = GetObjectWithChanceByChance(validObjs, randNum, out selectedI);

            invalidIndexes.Add(validIndexes[selectedI]);
            return selectedObj;
        }

        /// <summary>
        /// Puts the given list of objects with a chance value for each into a list to be used to select using a random index.
        /// </summary>
        /// <typeparam name="T">The type of the objects.</typeparam>
        /// <param name="objs">The list of the objects with chances.</param>
        /// <returns>Returns a list with duplicates of the objects by their chance value.</returns>
        public static List<T> GetObjsWithChancesApplied<T>(List<DictionaryTObjectChance<T>> objs)
        {
            if (objs == null || objs.Count == 0) return new List<T>();

            List<T> objsWithChances = new List<T>();
            foreach (DictionaryTObjectChance<T> obj in objs)
            {
                if (obj.chance <= 0) continue;
                for (int i = 0; i < obj.chance; i++) objsWithChances.Add(obj.obj);
            }

            return objsWithChances;
        }

        /// <summary>
        /// Gets an object from the list with chances for each using those chances as the index.
        /// </summary>
        /// <typeparam name="T">The type of the objects in teh list.</typeparam>
        /// <param name="objs">The list of objects with chance values.</param>
        /// <param name="chanceIndex">The index in chances of the desired object.</param>
        /// <returns>The desired object.</returns>
        public static T GetObjectWithChanceByChance<T>(List<DictionaryTObjectChance<T>> objs, int chanceIndex)
        {
            int temp = -1;
            return GetObjectWithChanceByChance(objs, chanceIndex, out temp);
        }
        /// <summary>
        /// Gets an object from the list with chances for each using those chances as the index.
        /// </summary>
        /// <typeparam name="T">The type of the objects in teh list.</typeparam>
        /// <param name="objs">The list of objects with chance values.</param>
        /// <param name="chanceIndex">The index in chances of the desired object.</param>
        /// <param name="selectedIndex">The index of the desired object in the list.</param>
        /// <returns>The desired object.</returns>
        public static T GetObjectWithChanceByChance<T>(List<DictionaryTObjectChance<T>> objs, int chanceIndex, out int selectedIndex)
        {
            selectedIndex = -1;
            if (objs == null || objs.Count == 0 || chanceIndex < 0) return default;

            T selectedObj = default;
            int currSumChances = 0;
            for (int i = 0; i < objs.Count; i++)
            {
                if (objs[i] != null && objs[i].chance <= 0) continue;

                if (chanceIndex >= currSumChances && chanceIndex < currSumChances + objs[i].chance)
                {
                    selectedObj = objs[i].obj;
                    selectedIndex = i;
                    break;
                }
                currSumChances += objs[i].chance;
            }

            return selectedObj;
        }
    }
}
