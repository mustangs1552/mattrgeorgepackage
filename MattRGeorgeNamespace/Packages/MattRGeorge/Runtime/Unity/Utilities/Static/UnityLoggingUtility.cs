﻿using System;
using UnityEngine;

namespace MattRGeorge.Unity.Utilities.Static
{
    public static class UnityLoggingUtility
    {
        /// <summary>
        /// Log an error message for missing value of the given variable name in the given class on the given object.
        /// </summary>
        /// <param name="classType">The type of the class with the missing value.</param>
        /// <param name="variableName">The name of the variable with the missing value.</param>
        /// <param name="obj">The object with the missing value.</param>
        public static void LogMissingValue(Type classType, string variableName, GameObject obj)
        {
            if (obj) Debug.LogError($"'{classType}.{variableName}' on '{obj.name}' is required!", obj);
            else Debug.LogError($"'{classType}.{variableName}' is required!");
        }
    }
}
