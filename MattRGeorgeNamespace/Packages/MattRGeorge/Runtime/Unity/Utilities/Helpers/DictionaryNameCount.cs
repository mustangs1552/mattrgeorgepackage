﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace MattRGeorge.Unity.Utilities.Helpers
{
    /// <summary>
    /// Dictionary like class that uses a string name (key) and s int count (value.)
    /// </summary>
    [Serializable]
    public class DictionaryNameCount
    {
        public string name = "";
        public int count = 0;

        /// <summary>
        /// Converts the given list of this object type to a dictionary.
        /// </summary>
        /// <param name="list">The list of objects of this type.</param>
        /// <returns>The converted dictionary.</returns>
        public static Dictionary<string, int> ToDictionary(List<DictionaryNameCount> list)
        {
            if (list == null || list.Count == 0) return new Dictionary<string, int>();

            Dictionary<string, int> dict = new Dictionary<string, int>();
            foreach (DictionaryNameCount item in list)
            {
                if (item == null) continue;
                if(dict.ContainsKey(item.name))
                {
                    Debug.LogError($"'{typeof(DictionaryNameCount)}' to 'Dictionairy': '{item.name}' key already exists!");
                    continue;
                }

                dict.Add(item.name, item.count);
            }

            return dict;
        }
    }
}
