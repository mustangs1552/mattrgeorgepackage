﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace MattRGeorge.Unity.Utilities.Helpers
{
    /// <summary>
    /// Dictionary like class that uses a string name (key) and a list of DictionaryGameObjectChance (value.)
    /// </summary>
    [Serializable]
    public class DictionaryNameListDictionaryGameObjectChance
    {
        public string name = "";
        public List<DictionaryGameObjectChance> objs = null;

        /// <summary>
        /// Converts the given list of this object type to a dictionary.
        /// </summary>
        /// <param name="list">The list of objects of this type.</param>
        /// <returns>The converted dictionary.</returns>
        public static Dictionary<string, List<DictionaryGameObjectChance>> ToDictionary(List<DictionaryNameListDictionaryGameObjectChance> list)
        {
            if (list == null || list.Count == 0) return new Dictionary<string, List<DictionaryGameObjectChance>>();

            Dictionary<string, List<DictionaryGameObjectChance>> dict = new Dictionary<string, List<DictionaryGameObjectChance>>();
            foreach (DictionaryNameListDictionaryGameObjectChance item in list)
            {
                if (item == null) continue;
                if (dict.ContainsKey(item.name))
                {
                    Debug.LogError($"'{typeof(DictionaryNameListDictionaryGameObjectChance)}' to 'Dictionairy': '{item.name}' key already exists!");
                    continue;
                }

                dict.Add(item.name, item.objs);
            }

            return dict;
        }
        /// <summary>
        /// Converts the given list of this object type to a dictionary and converts its values to DictionaryTObjectChance.
        /// </summary>
        /// <param name="list">The list of objects of this type.</param>
        /// <returns>The converted dictionary.</returns>
        public static Dictionary<string, List<DictionaryTObjectChance<GameObject>>> ToDictionaryTObjectChance(List<DictionaryNameListDictionaryGameObjectChance> list)
        {
            if (list == null || list.Count == 0) return new Dictionary<string, List<DictionaryTObjectChance<GameObject>>>();

            Dictionary<string, List<DictionaryTObjectChance<GameObject>>> dict = new Dictionary<string, List<DictionaryTObjectChance<GameObject>>>();
            List<DictionaryTObjectChance<GameObject>> valueList = new List<DictionaryTObjectChance<GameObject>>();
            foreach (DictionaryNameListDictionaryGameObjectChance item in list)
            {
                if (item == null) continue;
                if (dict.ContainsKey(item.name))
                {
                    Debug.LogError($"'{typeof(DictionaryNameListDictionaryGameObjectChance)}' to 'Dictionairy': '{item.name}' key already exists!");
                    continue;
                }

                if (item.objs == null) dict.Add(item.name, new List<DictionaryTObjectChance<GameObject>>());
                else
                {
                    valueList = new List<DictionaryTObjectChance<GameObject>>();
                    foreach (DictionaryGameObjectChance obj in item.objs)
                    {
                        if (obj == null) continue;

                        valueList.Add(obj.ToDictionaryTObjectChance());
                    }
                    dict.Add(item.name, valueList);
                }
            }

            return dict;
        }
        /// <summary>
        /// Converts the given list of this object type to a dictionary including its values to dictionaries.
        /// </summary>
        /// <param name="list">The list of objects of this type.</param>
        /// <returns>The converted dictionary.</returns>
        public static Dictionary<string, Dictionary<GameObject, int>> ToFullDictionary(List<DictionaryNameListDictionaryGameObjectChance> list)
        {
            if (list == null || list.Count == 0) return new Dictionary<string, Dictionary<GameObject, int>>();

            Dictionary<string, Dictionary<GameObject, int>> dict = new Dictionary<string, Dictionary<GameObject, int>>();
            foreach (DictionaryNameListDictionaryGameObjectChance item in list)
            {
                if (item == null) continue;
                if (dict.ContainsKey(item.name))
                {
                    Debug.LogError($"'{typeof(DictionaryNameListDictionaryGameObjectChance)}' to 'Dictionairy': '{item.name}' key already exists!");
                    continue;
                }

                dict.Add(item.name, DictionaryGameObjectChance.ToDictionary(item.objs));
            }

            return dict;
        }
    }
}
