﻿using System;
using UnityEngine;
using UnityEngine.Events;

namespace MattRGeorge.Unity.Utilities.Helpers
{
    [Serializable]
    public class IntUnityEvent : UnityEvent<int> { }

    [Serializable]
    public class FloatUnityEvent : UnityEvent<float> { }

    [Serializable]
    public class StringUnityEvent : UnityEvent<string> { }

    [Serializable]
    public class BoolUnityEvent : UnityEvent<bool> { }

    [Serializable]
    public class Vector2UnityEvent : UnityEvent<Vector2> { }

    [Serializable]
    public class GameObjectUnityEvent : UnityEvent<GameObject> { }
    [Serializable]
    public class GameObjectIntUnityEvent : UnityEvent<GameObject, int> { }
    [Serializable]
    public class GameObjectTransformUnityEvent : UnityEvent<GameObject, Transform> { }
    [Serializable]
    public class GameObjectCollisionUnityEvent : UnityEvent<GameObject, Collision> { }
    [Serializable]
    public class GameObjectColliderUnityEvent : UnityEvent<GameObject, Collider> { }

    [Serializable]
    public class RaycastHitUnityEvent : UnityEvent<RaycastHit> { }
}
