﻿using System.Collections.Generic;
using UnityEngine;

namespace MattRGeorge.Unity.Utilities.Helpers
{
    /// <summary>
    /// Dictionary like class that uses a generic object (key) and int chance (value.)
    /// </summary>
    /// <typeparam name="T">The type of the object key.</typeparam>
    public class DictionaryTObjectChance<T>
    {
        public T obj = default;
        public int chance = 1;

        /// <summary>
        /// Converts the given list of this object type to a dictionary.
        /// </summary>
        /// <param name="list">The list of objects of this type.</param>
        /// <returns>The converted dictionary.</returns>
        public static Dictionary<T, int> ToDictionary(List<DictionaryTObjectChance<T>> list)
        {
            if (list == null || list.Count == 0) return new Dictionary<T, int>();

            Dictionary<T, int> dict = new Dictionary<T, int>();
            foreach (DictionaryTObjectChance<T> item in list)
            {
                if (item == null) continue;
                if (dict.ContainsKey(item.obj))
                {
                    Debug.LogError($"'{typeof(DictionaryTObjectChance<T>)}' to 'Dictionairy': '{item.obj}' key already exists!");
                    continue;
                }

                dict.Add(item.obj, item.chance);
            }

            return dict;
        }
    }
}
