﻿using System;
using System.Collections.Generic;
using UnityEngine;
using MattRGeorge.Unity.Utilities.Static;

namespace MattRGeorge.Unity.Utilities.Helpers
{
    /// <summary>
    /// A helper class that can be used to store a list of `GameObjects` and can destroy them.
    /// Added for tests as object management but can be used by `Monobehaviours` with the `objs` field exposed in the inspector.
    /// </summary>
    [Serializable]
    public class GameObjectBundle
    {
        [Tooltip("The starting objects to be stored and managed.")]
        [SerializeField] protected List<GameObject> objs = new List<GameObject>();

        /// <summary>
        /// The starting objects to be stored and managed.
        /// </summary>
        public virtual List<GameObject> Objs
        {
            get => objs;
            set
            {
                if (value == null) objs = new List<GameObject>();
                else objs = ListUtility.RemoveNullEntries(value);
            }
        }

        /// <summary>
        /// Create a new `GameObject` with the given name and add it to the managed list.
        /// </summary>
        /// <param name="name">The name to give the object.</param>
        /// <returns>The created `GameObject`.</returns>
        public virtual GameObject CreateNewGameObject(string name)
        {
            GameObject obj = (string.IsNullOrEmpty(name)) ? new GameObject() : new GameObject(name);
            objs.Add(obj);
            return obj;
        }

        /// <summary>
        /// Destroys all the objs in the managed list and resets the list.
        /// </summary>
        /// <param name="isImmediate">Use `GameObject.DestroyImmediate()` instead of `.Destroy()`.</param>
        public virtual void DestroyObjs(bool isImmediate = false)
        {
            foreach(UnityEngine.Object obj in objs)
            {
                if(obj)
                {
                    if (isImmediate) objs.ForEach(x => GameObject.DestroyImmediate(x));
                    else objs.ForEach(x => GameObject.Destroy(x));
                }
            }

            objs = new List<GameObject>();
        }
    }
}
