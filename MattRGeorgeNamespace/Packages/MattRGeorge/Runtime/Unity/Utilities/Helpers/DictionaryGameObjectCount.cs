﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace MattRGeorge.Unity.Utilities.Helpers
{
    /// <summary>
    /// Dictionary like class that uses a GameObject object (key) and int count (value.)
    /// </summary>
    [Serializable]
    public class DictionaryGameObjectCount
    {
        public GameObject obj = null;
        public int count = 0;

        /// <summary>
        /// Converts the given list of this object type to a dictionary.
        /// </summary>
        /// <param name="list">The list of objects of this type.</param>
        /// <returns>The converted dictionary.</returns>
        public static Dictionary<GameObject, int> ToDictionary(List<DictionaryGameObjectCount> list)
        {
            if (list == null || list.Count == 0) return new Dictionary<GameObject, int>();

            Dictionary<GameObject, int> dict = new Dictionary<GameObject, int>();
            foreach (DictionaryGameObjectCount item in list)
            {
                if (item == null) continue;
                if (dict.ContainsKey(item.obj))
                {
                    Debug.LogError($"'{typeof(DictionaryGameObjectCount)}' to 'Dictionairy': '{item.obj}' key already exists!");
                    continue;
                }

                dict.Add(item.obj, item.count);
            }

            return dict;
        }
    }
}
