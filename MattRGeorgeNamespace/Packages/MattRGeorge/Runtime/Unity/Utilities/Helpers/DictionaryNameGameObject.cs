﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace MattRGeorge.Unity.Utilities.Helpers
{
    /// <summary>
    /// Dictionary like class that uses a string name (key) and s GameObject object (value.)
    /// </summary>
    [Serializable]
    public class DictionaryNameGameObject
    {
        public string name = "";
        public GameObject obj = null;

        /// <summary>
        /// Converts the given list of this object type to a dictionary.
        /// </summary>
        /// <param name="list">The list of objects of this type.</param>
        /// <returns>The converted dictionary.</returns>
        public static Dictionary<string, GameObject> ToDictionary(List<DictionaryNameGameObject> list)
        {
            if (list == null || list.Count == 0) return new Dictionary<string, GameObject>();

            Dictionary<string, GameObject> dict = new Dictionary<string, GameObject>();
            foreach (DictionaryNameGameObject item in list)
            {
                if (item == null) continue;
                if (dict.ContainsKey(item.name))
                {
                    Debug.LogError($"'{typeof(DictionaryNameGameObject)}' to 'Dictionairy': '{item.name}' key already exists!");
                    continue;
                }

                dict.Add(item.name, item.obj);
            }

            return dict;
        }
    }
}
