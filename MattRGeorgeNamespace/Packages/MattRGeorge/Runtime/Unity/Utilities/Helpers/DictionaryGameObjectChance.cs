﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace MattRGeorge.Unity.Utilities.Helpers
{
    /// <summary>
    /// Dictionary like class that uses a GameObject object (key) and int chance (value.)
    /// </summary>
    [Serializable]
    public class DictionaryGameObjectChance
    {
        public GameObject obj = null;
        public int chance = 0;

        /// <summary>
        /// Converts the given list of this object type to a dictionary.
        /// </summary>
        /// <param name="list">The list of objects of this type.</param>
        /// <returns>The converted dictionary.</returns>
        public static Dictionary<GameObject, int> ToDictionary(List<DictionaryGameObjectChance> list)
        {
            if (list == null || list.Count == 0) return new Dictionary<GameObject, int>();

            Dictionary<GameObject, int> dict = new Dictionary<GameObject, int>();
            foreach (DictionaryGameObjectChance item in list)
            {
                if (item == null) continue;
                if (dict.ContainsKey(item.obj))
                {
                    Debug.LogError($"'{typeof(DictionaryGameObjectChance)}' to 'Dictionairy': '{item.obj}' key already exists!");
                    continue;
                }

                dict.Add(item.obj, item.chance);
            }

            return dict;
        }

        /// <summary>
        /// Converts this object to DictionaryTObjectChance<GameObject>.
        /// </summary>
        /// <returns>The converted object.</returns>
        public DictionaryTObjectChance<GameObject> ToDictionaryTObjectChance()
        {
            return new DictionaryTObjectChance<GameObject>()
            {
                obj = this.obj,
                chance = this.chance
            };
        }
    }
}
