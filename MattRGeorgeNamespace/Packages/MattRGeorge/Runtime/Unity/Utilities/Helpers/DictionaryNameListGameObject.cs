﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace MattRGeorge.Unity.Utilities.Helpers
{
    /// <summary>
    /// Dictionary like class that uses a string name (key) and a list of GameObjects (value.)
    /// </summary>
    [Serializable]
    public class DictionaryNameListGameObject
    {
        public string name = "";
        public List<GameObject> objs = null;

        /// <summary>
        /// Converts the given list of this object type to a dictionary.
        /// </summary>
        /// <param name="list">The list of objects of this type.</param>
        /// <returns>The converted dictionary.</returns>
        public static Dictionary<string, List<GameObject>> ToDictionary(List<DictionaryNameListGameObject> list)
        {
            if (list == null || list.Count == 0) return new Dictionary<string, List<GameObject>>();

            Dictionary<string, List<GameObject>> dict = new Dictionary<string, List<GameObject>>();
            foreach (DictionaryNameListGameObject item in list)
            {
                if (item == null) continue;
                if (dict.ContainsKey(item.name))
                {
                    Debug.LogError($"'{typeof(DictionaryNameListGameObject)}' to 'Dictionairy': '{item.name}' key already exists!");
                    continue;
                }

                dict.Add(item.name, item.objs);
            }

            return dict;
        }
    }
}
