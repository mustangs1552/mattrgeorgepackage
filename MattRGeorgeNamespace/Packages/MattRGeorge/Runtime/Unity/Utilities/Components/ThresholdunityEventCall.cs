﻿using UnityEngine;
using UnityEngine.Events;

namespace MattRGeorge.Unity.Utilities.Components
{
    public class ThresholdUnityEventCall : MonoBehaviour
    {
        [Tooltip("Threshold to reach to call this event.")]
        [SerializeField] protected int threshold = 3;
        [Tooltip("The starting amount towards the threshold.")]
        [SerializeField] protected int startAmount = 0;
        [Tooltip("Reset the current amount automatically when the threshold is reached?")]
        public bool autoResetAmountOnCalled = true;
        public UnityEvent OnAmountIncreased = new UnityEvent();
        public UnityEvent OnAmountDecreased = new UnityEvent();
        public UnityEvent OnThresholdReached = new UnityEvent();

        /// <summary>
        /// Threshold to reach to call this event.
        /// </summary>
        public virtual int Threshold
        {
            get => threshold;
            set
            {
                threshold = (value < 1) ? 1 : value;
            }
        }
        /// <summary>
        /// The starting amount towards the threshold.
        /// </summary>
        public virtual int StartAmount
        {
            get => startAmount;
            set
            {
                if (value < 0) startAmount = 0;
                else if (value > Threshold) startAmount = Threshold;
                else startAmount = value;
            }
        }
        /// <summary>
        /// Was the event already called?
        /// </summary>
        public virtual bool Called => called;

        /// <summary>
        /// The percentage progress towards the threshold (0-1).
        /// </summary>
        public virtual float ThresholdProgress => (float)CurrAmount / (float)Threshold;

        /// <summary>
        /// The current amount towards the threshold.
        /// </summary>
        public virtual int CurrAmount
        {
            get => currAmount;
            protected set
            {
                if (value > Threshold) currAmount = Threshold;
                else
                {
                    currAmount = (value < 0) ? 0 : value;
                    called = false;
                }
            }
        }

        protected int currAmount = 0;
        protected bool called = false;

        /// <summary>
        /// Increase the amount by the amount given.
        /// </summary>
        /// <param name="amount">The amount.</param>
        public virtual void IncreaseAmount(int amount = 1)
        {
            if (amount <= 0) return;

            for (int i = 0; i < amount; i++) IncreaseAmountByOne();
        }
        /// <summary>
        /// Decrease the amount by the amount given.
        /// </summary>
        /// <param name="amount">The amount.</param>
        public virtual void DecreaseAmount(int amount = 1)
        {
            if (amount <= 0) return;

            for (int i = 0; i < amount; i++) DecreaseAmountByOne();
        }

        /// <summary>
        /// Reset the progress towards the threshold.
        /// </summary>
        /// <param name="toStartAmount">Reset to the start amount instead of 0?</param>
        public virtual void ResetAmount(bool toStartAmount = false)
        {
            CurrAmount = (toStartAmount) ? StartAmount : 0;
        }

        /// <summary>
        /// Increase the amount by one.
        /// </summary>
        protected virtual void IncreaseAmountByOne()
        {
            int amountBefore = CurrAmount;
            CurrAmount++;
            if (amountBefore != CurrAmount) OnAmountIncreased?.Invoke();

            if (CurrAmount >= Threshold) ThresholdReached();
        }
        /// <summary>
        /// Decrease the amount by one.
        /// </summary>
        protected virtual void DecreaseAmountByOne()
        {
            int amountBefore = CurrAmount;
            CurrAmount--;
            if (amountBefore != CurrAmount) OnAmountDecreased?.Invoke();
        }

        /// <summary>
        /// The threshold was reached.
        /// </summary>
        protected virtual void ThresholdReached()
        {
            if (!Called) OnThresholdReached?.Invoke();

            if (autoResetAmountOnCalled) ResetAmount();
            else called = true;
        }

        protected virtual void OnValidate()
        {
            Threshold = threshold;
            StartAmount = startAmount;
        }

        protected virtual void Awake()
        {
            CurrAmount = StartAmount;
        }
    }
}
