﻿using UnityEngine;
using MattRGeorge.Unity.Tools.ObjectPooling;

namespace MattRGeorge.Unity.Utilities.Components
{
    public class DestroyObject : MonoBehaviour
    {
        [Tooltip("Destroy object via object pooling system?")]
        public bool useObjectPoolSystem = true;

        /// <summary>
        /// Destroy this object using settings.
        /// </summary>
        /// <param name="immediate">Use `Object.DestroyImmediate()` instead of `Object.Destroy()`? (Does nothing when `useObjectPoolSystem` is true)</param>
        public virtual void Destroy(bool immediate = false)
        {
            if (useObjectPoolSystem && ObjectPoolManager.SINGLETON) ObjectPoolManager.SINGLETON.Destroy(gameObject);
            else if (!useObjectPoolSystem)
            {
                if (immediate) DestroyImmediate(gameObject);
                else Object.Destroy(gameObject);
            }
        }
    }
}
