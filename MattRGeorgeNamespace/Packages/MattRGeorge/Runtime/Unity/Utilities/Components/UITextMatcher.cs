﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using MattRGeorge.Unity.Utilities.Static;

namespace MattRGeorge.Unity.Utilities.Components
{
    /// <summary>
    /// Updates all the Text components in the children of this object font size to the smallest best fit font size.
    /// Can also size up or down texts to support larger texts as headers.
    /// </summary>
    public class UITextMatcher : MonoBehaviour
    {
        [Tooltip("The maximum number of attempts before giving up updating fonts.")]
        [SerializeField] protected int maxAttempts = 5;
        [Tooltip("Update the font sizes again when re-enabled?")]
        [SerializeField] protected bool updateFontsOnEnable = false;

        [Header("Header Settings")]
        [Tooltip("The header text objects.")]
        [SerializeField] protected List<Text> headerTexts = new List<Text>();
        [Tooltip("The parent of the header text objects.")]
        public GameObject headerTextParent = null;
        [Tooltip("The size difference desired for the header text objects vs the rest of the text objects.")]
        public int headerFontSizeDifference = 5;
        [Tooltip("Size up the headers from the rest of the text objects or size down the non-header text objects.")]
        public bool sizeUpHeader = false;
        public UnityEvent OnFinishFontUpdate = new UnityEvent();

        /// <summary>
        /// The maximum attempts alloweed before giving up updating the text.
        /// </summary>
        public virtual int MaxAttempts
        {
            get => maxAttempts;
            set
            {
                maxAttempts = (value > 0) ? value : 1;
            }
        }
        /// <summary>
        /// The size difference desired for the header text objects vs the rest of the text objects.
        /// </summary>
        public virtual int HeaderFontSizeDifference
        {
            get => headerFontSizeDifference;
            set
            {
                headerFontSizeDifference = (value > 0) ? value : 1;
            }
        }

        /// <summary>
        /// The header text objects.
        /// </summary>
        public virtual List<Text> HeaderTexts
        {
            get => new List<Text>(headerTexts);
            set
            {
                if (value == null) headerTexts = new List<Text>();
                else headerTexts = ListUtility.RemoveNullEntries(value);
            }
        }

        protected Text[] texts = null;
        protected List<Text> finalHeaderTexts = new List<Text>();
        protected int smallestFont = 0;
        protected int findFontAttemptCount = 0;
        protected int setFontAttemptCount = 0;

        /// <summary>
        /// Update the font sizes of all the texts.
        /// </summary>
        public virtual void StartUpdateFontSizes(bool findNewTexts = false)
        {
            if (findNewTexts) FindTexts();
            foreach (Text text in texts) text.resizeTextForBestFit = true;

            findFontAttemptCount = 0;
            setFontAttemptCount = 0;
            StartCoroutine(UpdateFontSizes());
        }

        /// <summary>
        /// Find the smallest font size from the best fit texts.
        /// </summary>
        protected virtual void FindSmallestFontSize()
        {
            foreach(Text text in texts)
            {
                if (text.resizeTextForBestFit)
                {
                    if (smallestFont == 0 || smallestFont > text.cachedTextGenerator.fontSizeUsedForBestFit)
                    {
                        smallestFont = text.cachedTextGenerator.fontSizeUsedForBestFit;
                        if(smallestFont != 0) text.resizeTextForBestFit = false;
                    }
                }
            }
        }

        /// <summary>
        /// Set the current smallest font size to all the non-best fit texts.
        /// </summary>
        protected virtual void SetFontSizes()
        {
            foreach (Text text in texts) text.fontSize = smallestFont;
        }

        /// <summary>
        /// Disable the best fit option on all texts.
        /// </summary>
        protected virtual void DisableBestFit()
        {
            foreach (Text text in texts) text.resizeTextForBestFit = false;
        }

        /// <summary>
        /// Update the size of the headers to be larger than the rest of the text objects.
        /// </summary>
        protected virtual void UpdateHeaders()
        {
            if (finalHeaderTexts != null && finalHeaderTexts.Count > 0)
            {
                foreach (Text text in texts)
                {
                    bool isHeader = false;
                    foreach (Text header in finalHeaderTexts)
                    {
                        if (text == header)
                        {
                            isHeader = true;
                            break;
                        }
                    }

                    if (!sizeUpHeader && !isHeader) text.fontSize = (text.fontSize - headerFontSizeDifference <= 0) ? 1 : text.fontSize - headerFontSizeDifference;
                    else if(sizeUpHeader && isHeader) text.fontSize = text.fontSize + headerFontSizeDifference;
                }
            }
        }
        /// <summary>
        /// Update the font sizes using the best fit sizes.
        /// Wait 'til the end of the frame to move on and try again if the best fit size was not updated yet.
        /// </summary>
        protected virtual IEnumerator UpdateFontSizes()
        {
            yield return 0;

            while (smallestFont == 0 && findFontAttemptCount < maxAttempts)
            {
                FindSmallestFontSize();
                findFontAttemptCount++;

                yield return 0;
            }

            while (!CheckFonts() && setFontAttemptCount < maxAttempts)
            {
                setFontAttemptCount++;
                SetFontSizes();

                yield return 0;
            }

            DisableBestFit();
            UpdateHeaders();

            OnFinishFontUpdate?.Invoke();
        }

        /// <summary>
        /// Check to make sure that the texts all have the correct font size.
        /// </summary>
        /// <returns>True if all texts are the correct size.</returns>
        protected virtual bool CheckFonts()
        {
            if (smallestFont == 0) return false;

            foreach(Text text in texts)
            {
                if(text.fontSize != smallestFont) return false;
            }

            return true;
        }

        /// <summary>
        /// Find all the Text components in the children of this object.
        /// </summary>
        protected virtual void FindTexts()
        {
            texts = GetComponentsInChildren<Text>();

            finalHeaderTexts = new List<Text>(headerTexts);
            if(headerTextParent != null)
            {
                Text[] headerParentTexts = headerTextParent.GetComponentsInChildren<Text>();
                foreach(Text parentText in headerParentTexts) finalHeaderTexts.Add(parentText);
            }
        }

        protected virtual void OnValidate()
        {
            MaxAttempts = maxAttempts;
            HeaderFontSizeDifference = headerFontSizeDifference;
            HeaderTexts = headerTexts;
        }

        protected virtual void Awake()
        {
            StartUpdateFontSizes(true);
        }

        protected virtual void OnEnable()
        {
            if(updateFontsOnEnable) StartUpdateFontSizes(false);
        }
    }
}
