﻿using UnityEngine;
using MattRGeorge.Unity.Utilities.Enums;

namespace MattRGeorge.Unity.Utilities.Components
{
    public class RotateObject : MonoBehaviour
    {
        [Tooltip("The rotations to be applied and their speeds.")]
        public Vector3 rotations = Vector3.zero;
        [Tooltip("Start rotating on awake/start?")]
        public UnityStartMethods rotateOn = UnityStartMethods.None;

        protected bool isRotating = false;

        /// <summary>
        /// Start rotating this object.
        /// </summary>
        public virtual void StartRotating()
        {
            isRotating = true;
        }
        /// <summary>
        /// Stop rotating this object.
        /// </summary>
        public virtual void StopRotating()
        {
            isRotating = false;
        }

        /// <summary>
        /// Rotate this object.
        /// </summary>
        protected virtual void DoRotateObject()
        {
            if (!isRotating) return;

            transform.Rotate(rotations * Time.deltaTime);
        }

        protected virtual void Awake()
        {
            if (rotateOn == UnityStartMethods.Awake) StartRotating();
        }
        protected virtual void Start()
        {
            if (rotateOn == UnityStartMethods.Start) StartRotating();
        }

        protected virtual void Update()
        {
            DoRotateObject();
        }
    }
}
