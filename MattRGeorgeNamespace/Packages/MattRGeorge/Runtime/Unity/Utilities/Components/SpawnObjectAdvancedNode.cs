﻿using System.Collections.Generic;
using UnityEngine;
using MattRGeorge.General.Utilities.Enums;
using MattRGeorge.Unity.Utilities.Static;
using MattRGeorge.Unity.Utilities.Helpers;
using MattRGeorge.Unity.Tools.ObjectPooling;

namespace MattRGeorge.Unity.Utilities.Components
{
    /// <summary>
    /// A `SpawnObjectNode` that still doesn't spawn objects on its own but does have knowledge of what objects it has spawned and has ways to manage those objects.
    /// </summary>
    public class SpawnObjectAdvancedNode : SpawnObjectNode
    {
        [Tooltip("Max amount of spawned objects that this node can have at once.")]
        [SerializeField] protected int maxCount = -1;
        [Tooltip("Max amount of spawned objects by object that this node can have at once.")]
        [SerializeField] protected List<DictionaryGameObjectCount> objsWithMaxCounts = new List<DictionaryGameObjectCount>();
        [Tooltip("Automatically destroy excess objects when max values are lowered?")]
        public bool destroyObjsOnMaxesChanged = false;
        [Tooltip("The way to choose which objects are destroyed.")]
        public ListOneChoiceMode destroyObjsSelectionMode = ListOneChoiceMode.Oldest;

        /// <summary>
        /// Max amount of spawned objects that this node can have at once.
        /// </summary>
        public virtual int MaxCount
        {
            get => maxCount;
            set
            {
                maxCount = value;
                if (maxCount >= 0 && destroyObjsOnMaxesChanged) DestroyExcessObjects();
            }
        }
        /// <summary>
        /// The total number of all currently spawned objects.
        /// </summary>
        public virtual int CurrSpawnedCount
        {
            get
            {
                int count = 0;
                foreach (KeyValuePair<string, List<GameObject>> pair in currSpawnedObjs) count += pair.Value.Count;
                return count;
            }
        }

        /// <summary>
        /// Max amount of spawned objects by object that this node can have at once.
        /// </summary>
        public virtual List<DictionaryGameObjectCount> ObjsWithMaxCounts
        {
            get => ListUtility.RemoveNullEntries(objsWithMaxCounts);
            set
            {
                if (value == null) objsWithMaxCounts = new List<DictionaryGameObjectCount>();
                else
                {
                    List<DictionaryGameObjectCount> objCounts = new List<DictionaryGameObjectCount>();
                    foreach (DictionaryGameObjectCount obj in value)
                    {
                        if (obj != null && obj.obj)
                        {
                            if (obj.count < 0) obj.count = 0;
                            objCounts.Add(obj);
                        }
                    }

                    objsWithMaxCounts = objCounts;
                }

                ResetDictionary();
                if (destroyObjsOnMaxesChanged) DestroyExcessObjects();
            }
        }

        /// <summary>
        /// All the currently spawned objects.
        /// </summary>
        public virtual Dictionary<string, List<GameObject>> CurrSpawnedObjs
        {
            get => ListUtility.RemoveNullEntriesFromDictionaryValue(currSpawnedObjs);
            protected set
            {
                if (value == null) currSpawnedObjs = new Dictionary<string, List<GameObject>>();
                else currSpawnedObjs = ListUtility.RemoveNullEntriesFromDictionaryValue(value);
            }
        }

        /// <summary>
        /// The dictionary version of `ObjsWithMaxCounts` primarly used.
        /// </summary>
        protected virtual Dictionary<string, int> ObjsWithMaxCountsDict
        {
            set
            {
                if (value == null) objsWithMaxCountsDict = new Dictionary<string, int>();
                else
                {
                    objsWithMaxCountsDict = new Dictionary<string, int>();
                    foreach (KeyValuePair<string, int> pair in value)
                    {
                        if (!string.IsNullOrEmpty(pair.Key) && pair.Value > 0) objsWithMaxCountsDict.Add(pair.Key, pair.Value);
                    }
                }
            }
        }

        protected Dictionary<string, int> objsWithMaxCountsDict = new Dictionary<string, int>();
        protected Dictionary<string, List<GameObject>> currSpawnedObjs = new Dictionary<string, List<GameObject>>();

        /// <summary>
        /// Destroy an object with the given actual name via `destroyObjsSelectionMode`.
        /// </summary>
        /// <param name="objActualNameKey">The actual name of the desired object.</param>
        /// <returns>True when successful.</returns>
        public virtual bool DestroyAnObject(string objActualNameKey)
        {
            Dictionary<string, List<GameObject>> spawnedObjs = currSpawnedObjs;
            if (string.IsNullOrEmpty(objActualNameKey) || !spawnedObjs.ContainsKey(objActualNameKey) || spawnedObjs[objActualNameKey].Count == 0) return false;

            int chosenI = 0;
            GameObject obj = null;
            if (destroyObjsSelectionMode == ListOneChoiceMode.Oldest)
            {
                chosenI = 0;
                obj = spawnedObjs[objActualNameKey][chosenI];
                if (DestroyObject(obj)) spawnedObjs[objActualNameKey].RemoveAt(chosenI);
            }
            else if (destroyObjsSelectionMode == ListOneChoiceMode.Newest)
            {
                chosenI = spawnedObjs[objActualNameKey].Count - 1;
                obj = spawnedObjs[objActualNameKey][chosenI];
                if (DestroyObject(obj)) spawnedObjs[objActualNameKey].RemoveAt(chosenI);
            }
            else
            {
                chosenI = Random.Range(0, spawnedObjs[objActualNameKey].Count);
                obj = spawnedObjs[objActualNameKey][chosenI];
                if (DestroyObject(obj)) spawnedObjs[objActualNameKey].RemoveAt(chosenI);
            }

            CurrSpawnedObjs = spawnedObjs;
            return true;
        }
        /// <summary>
        /// Destroy the selected object if it is currently a spawned object by this node.
        /// </summary>
        /// <param name="obj">The object to be destroyed.</param>
        /// <returns>True if successful.</returns>
        public virtual bool DestroyObject(GameObject obj)
        {
            if (!obj) return false;
            string objActualName = GameObjectUtility.GetActualName(obj);
            if (string.IsNullOrEmpty(objActualName) || !currSpawnedObjs.ContainsKey(objActualName) || !currSpawnedObjs[objActualName].Contains(obj)) return false;

            if (useObjectPool && ObjectPoolManager.SINGLETON)
            {
                currSpawnedObjs[objActualName].Remove(obj);
                ObjectPoolManager.SINGLETON.Destroy(obj);
            }
            else GameObject.Destroy(obj);

            return true;
        }

        /// <summary>
        /// Is the given object spawnable if spawned now?
        /// </summary>
        /// <param name="obj">The object to check.</param>
        /// <returns>True if spawnable now.</returns>
        public override bool IsSpawnable(GameObject obj)
        {
            if (!obj || (MaxCount > -1 && CurrSpawnedCount >= MaxCount)) return false;

            string objActualName = GameObjectUtility.GetActualName(obj);
            return !string.IsNullOrEmpty(objActualName) && 
            (
                objsWithMaxCountsDict.Count == 0 || !objsWithMaxCountsDict.ContainsKey(objActualName) ||
                (!currSpawnedObjs.ContainsKey(objActualName) && objsWithMaxCountsDict.ContainsKey(objActualName) && objsWithMaxCountsDict[objActualName] > 0) ||
                (objsWithMaxCountsDict.ContainsKey(objActualName) && currSpawnedObjs.ContainsKey(objActualName) && currSpawnedObjs[objActualName].Count < objsWithMaxCountsDict[objActualName])
            );
        }

        /// <summary>
        /// Destroys any excess objects if there are more currently spawned than current maxes.
        /// Note: Doesn't follow `destroyObjsSelectionMode` when selecting object dictionary keys.
        /// </summary>
        protected virtual void DestroyExcessObjects()
        {
            // Setup lists
            Dictionary<string, List<GameObject>> spawnedObjs = currSpawnedObjs;
            List<string> keys = new List<string>();
            List<string> excessObjskeys = new List<string>();
            List<int> excessObjsValuesCounts = new List<int>();
            foreach (KeyValuePair<string, List<GameObject>> pair in spawnedObjs)
            {
                if (pair.Value.Count == 0) continue;

                keys.Add(pair.Key);

                if (objsWithMaxCountsDict.ContainsKey(pair.Key) && pair.Value.Count > objsWithMaxCountsDict[pair.Key])
                {
                    excessObjskeys.Add(pair.Key);
                    excessObjsValuesCounts.Add(pair.Value.Count - objsWithMaxCountsDict[pair.Key]);
                }
            }

            // Global excess
            int randNum = 0;
            int totalGlobalExcessObjs = CurrSpawnedCount - MaxCount;
            if (totalGlobalExcessObjs > 0)
            {
                for (int i = 0; i < totalGlobalExcessObjs; i++)
                {
                    // Start with excess by group
                    if (excessObjskeys.Count > 0)
                    {
                        randNum = Random.Range(0, excessObjskeys.Count);
                        DestroyAnObject(excessObjskeys[randNum]);
                        excessObjsValuesCounts[randNum]--;
                        if (excessObjsValuesCounts[randNum] == 0)
                        {
                            excessObjskeys.RemoveAt(randNum);
                            excessObjsValuesCounts.RemoveAt(randNum);
                        }
                    }
                    // Finish with randomly by all
                    else
                    {
                        randNum = Random.Range(0, keys.Count);
                        if (DestroyAnObject(keys[randNum]) && currSpawnedObjs[keys[randNum]].Count == 0)
                        {
                            keys.RemoveAt(randNum);
                        }
                    }
                }
            }

            // Remove remaining excess by group if some remains
            for (int i = 0; i < excessObjskeys.Count; i++)
            {
                for (int ii = 0; ii < excessObjsValuesCounts[i]; ++ii) DestroyAnObject(excessObjskeys[i]);
            }
        }

        /// <summary>
        /// Adds the object to list of currently spawned objects.
        /// Called via `SpawnObjectNode.OnSpawn()`.
        /// </summary>
        /// <param name="obj">The object to be added.</param>
        protected virtual void AddObject(GameObject obj)
        {
            if (!obj) return;

            Dictionary<string, List<GameObject>> spawnedObjs = currSpawnedObjs;
            string objActualName = GameObjectUtility.GetActualName(obj);
            if (!currSpawnedObjs.ContainsKey(objActualName)) spawnedObjs.Add(objActualName, new List<GameObject>());
            spawnedObjs[objActualName].Add(obj);
            CurrSpawnedObjs = spawnedObjs;
        }

        /// <summary>
        /// Reset the maxes by object dicitonary to match current value of publicly accessable non-dictionary list.
        /// </summary>
        protected virtual void ResetDictionary()
        {
            objsWithMaxCountsDict = new Dictionary<string, int>();
            foreach(DictionaryGameObjectCount obj in objsWithMaxCounts)
            {
                if (!obj.obj) continue;

                objsWithMaxCountsDict.Add(GameObjectUtility.GetActualName(obj.obj), obj.count);
            }
        }

        protected override void OnValidate()
        {
            base.OnValidate();

            ObjsWithMaxCounts = objsWithMaxCounts;
        }

        protected virtual void Awake()
        {
            OnSpawn.AddListener(AddObject);
            ResetDictionary();
        }
    }
}
