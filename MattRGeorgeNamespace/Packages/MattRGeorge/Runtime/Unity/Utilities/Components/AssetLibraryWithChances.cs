﻿using System.Collections.Generic;
using UnityEngine;
using MattRGeorge.Unity.Utilities.Static;
using MattRGeorge.Unity.Utilities.Helpers;

namespace MattRGeorge.Unity.Utilities.Components
{
    public class AssetLibraryWithChances : AssetLibrary
    {
        [Tooltip("The lists of objects with chances.")]
        [SerializeField] protected List<DictionaryNameListDictionaryGameObjectChance> objsWithChance = new List<DictionaryNameListDictionaryGameObjectChance>();

        protected Dictionary<string, List<DictionaryTObjectChance<GameObject>>> objsObjsWithChance = new Dictionary<string, List<DictionaryTObjectChance<GameObject>>>();

        /// <summary>
        /// The lists of objects with chances.
        /// </summary>
        public virtual List<DictionaryNameListDictionaryGameObjectChance> ObjsWithChance
        {
            get => new List<DictionaryNameListDictionaryGameObjectChance>(objsWithChance);
            set
            {
                if (value == null) objsWithChance = new List<DictionaryNameListDictionaryGameObjectChance>();
                else objsWithChance = ListUtility.RemoveNullEntries(value);
                
                ClearGeneratedLists();
            }
        }

        /// <summary>
        /// Gets the gameobjects from the desired list.
        /// </summary>
        /// <param name="listName">The name of the desired list.</param>
        /// <returns>The gameobjects from that list.</returns>
        public virtual List<GameObject> GetList(string listName)
        {
            if (objsObjsWithChance == null || objsObjsWithChance.Count != objsWithChance.Count) objsObjsWithChance = DictionaryNameListDictionaryGameObjectChance.ToDictionaryTObjectChance(objsWithChance);
            if (string.IsNullOrEmpty(listName) || !objsObjsWithChance.ContainsKey(listName)) return new List<GameObject>();

            List<GameObject> gameObjects = new List<GameObject>();
            foreach (DictionaryTObjectChance<GameObject> chanceObj in objsObjsWithChance[listName])
            {
                if (!chanceObj.obj) continue;
                
                gameObjects.Add(chanceObj.obj);
            }
            return gameObjects;
        }

        /// <summary>
        /// Gets a random object from the list objects by the given list name.
        /// </summary>
        /// <param name="listName">The name of the type the desired objects are.</param>
        /// <returns>The randomly chosen object.</returns>
        public virtual GameObject GetRandomObjectWithChance(string listName)
        {
            HashSet<int> temp = new HashSet<int>();
            return GetRandomObjectWithChance(listName, ref temp);
        }
        /// <summary>
        /// Gets a random object from the list objects by the given list name using chance.
        /// </summary>
        /// <param name="listName">The name of the type the desired objects are.</param>
        /// <param name="invalidIndexes">Indexes of the objects not to choose.</param>
        /// <returns>The randomly chosen object.</returns>
        public virtual GameObject GetRandomObjectWithChance(string listName, ref HashSet<int> invalidIndexes)
        {
            if (objsWithChance == null || objsWithChance.Count == 0 || string.IsNullOrEmpty(listName)) return null;
            if (invalidIndexes == null) invalidIndexes = new HashSet<int>();
            if (objsObjsWithChance == null || objsObjsWithChance.Count != objsWithChance.Count) objsObjsWithChance = DictionaryNameListDictionaryGameObjectChance.ToDictionaryTObjectChance(objsWithChance);

            return (objsObjsWithChance.ContainsKey(listName)) ? ListUtility.GetRandomObjectWithChance(objsObjsWithChance[listName], ref invalidIndexes) : null;
        }

        /// <summary>
        /// Gets a random object using all the objects in the given list names.
        /// </summary>
        /// <param name="listNames">The names of the types to include in the list to choose from.</param>
        /// <returns>The randomly chosen object.</returns>
        public virtual GameObject GetRandomObjectWithChance(List<string> listNames)
        {
            string temp = "";
            return GetRandomObjectWithChance(listNames, out temp);
        }
        /// <summary>
        /// Gets a random object using all the objects in the given list names.
        /// </summary>
        /// <param name="listNames">The names of the types to include in the list to choose from.</param>
        /// <param name="chosenListName">The list name that the chosen object was in.</param>
        /// <returns>The randomly chosen object.</returns>
        public virtual GameObject GetRandomObjectWithChance(List<string> listNames, out string chosenListName)
        {
            chosenListName = "";
            if (listNames == null || listNames.Count == 0) return null;
            if (objsObjsWithChance == null || objsObjsWithChance.Count != objsWithChance.Count) objsObjsWithChance = DictionaryNameListDictionaryGameObjectChance.ToDictionaryTObjectChance(objsWithChance);

            List<DictionaryTObjectChance<GameObject>> combinedList = new List<DictionaryTObjectChance<GameObject>>();
            foreach(string listName in listNames)
            {
                if (string.IsNullOrEmpty(listName) || !objsObjsWithChance.ContainsKey(listName) || objsObjsWithChance[listName] == null || objsObjsWithChance[listName].Count == 0) continue;

                objsObjsWithChance[listName].ForEach(x => combinedList.Add(x));
            }

            GameObject chosenObj = ListUtility.GetRandomObjectWithChance(combinedList);
            foreach(KeyValuePair<string, List<DictionaryTObjectChance<GameObject>>> list in objsObjsWithChance)
            {
                foreach(DictionaryTObjectChance<GameObject> obj in list.Value)
                {
                    if(chosenObj == obj.obj)
                    {
                        chosenListName = list.Key;
                        return chosenObj;
                    }
                }
            }
            return chosenObj;
        }

        /// <summary>
        /// Clears the saved lists so that they can be repopulated.
        /// </summary>
        public override void ClearGeneratedLists()
        {
            base.ClearGeneratedLists();
            objsObjsWithChance = null;
        }
    }
}
