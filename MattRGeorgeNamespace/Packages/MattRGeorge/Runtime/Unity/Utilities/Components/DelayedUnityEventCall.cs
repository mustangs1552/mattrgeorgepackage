﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using MattRGeorge.Unity.Utilities.Static;
using MattRGeorge.Unity.Utilities.Enums;

namespace MattRGeorge.Unity.Utilities.Components
{
    public class DelayedUnityEventCall : MonoBehaviour
    {
        [Tooltip("Time before event is called.")]
        [SerializeField] protected float delay = 1;
        [Tooltip("Automatically start the timmer to call the method in the set Unity method?")]
        public UnityStartMethods autoStartOn = UnityStartMethods.None;
        public UnityEvent OnDelayedCallStart = new UnityEvent();
        public UnityEvent OnDelayedCall = new UnityEvent();

        /// <summary>
        /// Time before event is called.
        /// </summary>
        public virtual float Delay
        {
            get => delay;
            set
            {
                delay = (value < 0) ? 0 : value;
            }
        }

        protected List<Coroutine> calledCoroutines = new List<Coroutine>();

        /// <summary>
        /// Start the timer to call event.
        /// </summary>
        public virtual void StartDelayedCall()
        {
            OnDelayedCallStart?.Invoke();
            calledCoroutines.Add(StartCoroutine(CoroutineUtility.DelayedMethodCoroutine(DelayedCall, Delay)));
        }

        /// <summary>
        /// Clear any pending calls waiting for the delay.
        /// </summary>
        public virtual void ClearPendingCalls()
        {
            calledCoroutines.ForEach(x => StopCoroutine(x));
            calledCoroutines = new List<Coroutine>();
        }

        /// <summary>
        /// Call the event.
        /// </summary>
        protected virtual void DelayedCall()
        {
            OnDelayedCall?.Invoke();
            if (calledCoroutines != null && calledCoroutines.Count > 0) calledCoroutines.RemoveAt(0);
        }

        protected virtual void OnValidate()
        {
            Delay = delay;
        }

        protected virtual void Awake()
        {
            if (autoStartOn == UnityStartMethods.Awake) StartDelayedCall();
        }
        protected virtual void Start()
        {
            if (autoStartOn == UnityStartMethods.Start) StartDelayedCall();
        }
    }
}
