﻿using System.Collections.Generic;
using UnityEngine;
using MattRGeorge.Unity.Utilities.Static;

namespace MattRGeorge.Unity.Utilities.Components
{
    public class LineRendererCaster : MonoBehaviour
    {
        [Tooltip("The LineRenderer to use (will use LineRenderer on object if null).")]
        public LineRenderer lineRenderer = null;
        [Tooltip("The list of transforms to use for the points.")]
        [SerializeField] protected List<Transform> points = new List<Transform>();
        [Tooltip("Update the position of the points each frame?")]
        public bool auto = false;

        /// <summary>
        /// Show the line or not?
        /// </summary>
        public virtual bool ShowLine
        {
            get => lineRenderer.enabled;
            set => lineRenderer.enabled = value;
        }

        /// <summary>
        /// The list of transforms to use for the points.
        /// </summary>
        public virtual List<Transform> Points
        {
            get => new List<Transform>(points);
            set
            {
                if (value == null) return;

                points = ListUtility.RemoveNullEntries(value);
            }
        }

        /// <summary>
        /// Add to the list of transforms to use for the points.
        /// </summary>
        /// <param name="points">The points to add.</param>
        public virtual void AddPoints(List<Transform> points)
        {
            if (points == null || points.Count == 0) return;

            points.ForEach(x => AddPoint(x));
        }
        /// <summary>
        /// Add to the list of transforms to use for the points.
        /// </summary>
        /// <param name="point">The point to add.</param>
        public virtual void AddPoint(Transform point)
        {
            if (!point) return;

            points.Add(point);
        }

        /// <summary>
        /// Update the list points on the LineRenderer with the transforms listed.
        /// </summary>
        public virtual void UpdatePoints()
        {
            if (!lineRenderer || points == null || points.Count < 2) return;

            Vector3[] pts = new Vector3[points.Count];
            for (int i = 0; i < points.Count; i++) pts[i] = points[i].position;
            lineRenderer.positionCount = pts.Length;
            lineRenderer.SetPositions(pts);
        }

        /// <summary>
        /// Check for any missing required values.
        /// </summary>
        protected virtual void CheckMissingValues()
        {
            if (!lineRenderer) lineRenderer = GetComponent<LineRenderer>();
        }

        protected virtual void Awake()
        {
            CheckMissingValues();
        }

        protected virtual void Update()
        {
            if (auto) UpdatePoints();
        }
    }
}
