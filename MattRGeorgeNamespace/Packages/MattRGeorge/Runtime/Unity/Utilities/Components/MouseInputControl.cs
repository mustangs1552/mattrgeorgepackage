﻿using UnityEngine;
using MattRGeorge.Unity.Utilities.Helpers;

namespace MattRGeorge.Unity.Utilities.Components
{
    public class MouseInputControl : MonoBehaviour
    {
        [Tooltip("Use the mouse X/Y input from Unity's input manager instead?")]
        public bool useInputManager = true;
        [Tooltip("The name of the mouse x input in Unity's input manager.")]
        public string inputManagerMouseXName = "Mouse X";
        [Tooltip("The name of the mouse y input in Unity's input manager.")]
        public string inputManagerMouseYName = "Mouse Y";
        public Vector2UnityEvent OnUpdate = new Vector2UnityEvent();

        protected Vector2 lastMousePos = Vector2.zero;

        /// <summary>
        /// Checks the mouse position and sends the delta pos via the event.
        /// </summary>
        protected virtual void CheckInput()
        {
            if (!useInputManager)
            {
                if ((Vector2)Input.mousePosition != lastMousePos) OnUpdate?.Invoke((Vector2)Input.mousePosition - lastMousePos);
            }
            else OnUpdate?.Invoke(new Vector2(Input.GetAxis(inputManagerMouseXName), Input.GetAxis(inputManagerMouseYName)));

            lastMousePos = Input.mousePosition;
        }

        protected virtual void Awake()
        {
            lastMousePos = Input.mousePosition;
        }

        protected virtual void Update()
        {
            CheckInput();
        }
    }
}
