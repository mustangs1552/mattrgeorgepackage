﻿using UnityEngine;
using MattRGeorge.Unity.Utilities.Helpers;

namespace MattRGeorge.Unity.Utilities.Components
{
    public class Versioning : MonoBehaviour
    {
        public StringUnityEvent OnVersionReady = new StringUnityEvent();

        protected virtual void Awake()
        {
            OnVersionReady?.Invoke(Application.version);
        }
    }
}
