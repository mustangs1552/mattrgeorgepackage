﻿using System.Collections;
using UnityEngine;
using MattRGeorge.Unity.Tools.ObjectPooling;
using MattRGeorge.Unity.Utilities.Helpers;
using MattRGeorge.Unity.Utilities.Components.Helpers;

namespace MattRGeorge.Unity.Utilities.Components
{
    /// <summary>
    /// An object that is used to spawn a single object at a time and requires another object to tell it to spawn as well as giving it an object to spawn.
    /// This object also doesn't save a history of what it has spawned.
    /// </summary>
    public class SpawnObjectNode : MonoBehaviour
    {
        [Tooltip("Spawn the objects as active or in-active?")]
        public bool spawnAsActive = true;
        [Tooltip("Use the the Object Pool when spawning the objects?")]
        public bool useObjectPool = false;
        [Tooltip("Should the Object Pool store and manage the spawned objects? No effect when 'useObjectPool' is false")]
        public bool isObjectPoolManaged = true;
        [Tooltip("The delay before spawning each object.")]
        [SerializeField] protected float spawnDelay = 0;
        [Tooltip("The delay after spawning each object before calling 'OnPostSpawn()'.")]
        [SerializeField] protected float postSpawnDelay = 0;
        public GameObjectUnityEvent OnSpawn = new GameObjectUnityEvent();
        public GameObjectUnityEvent OnPostSpawn = new GameObjectUnityEvent();

        /// <summary>
        /// The delay before spawning each object.
        /// </summary>
        public virtual float SpawnDelay
        {
            get => spawnDelay;
            set
            {
                spawnDelay = (value < 0) ? 0 : value;
            }
        }
        /// <summary>
        /// The delay after spawning each object before calling 'OnPostSpawn()'.
        /// </summary>
        public virtual float PostSpawnDelay
        {
            get => postSpawnDelay;
            set
            {
                postSpawnDelay = (value < 0) ? 0 : value;
            }
        }

        /// <summary>
        /// Spawn one of the given object.
        /// </summary>
        /// <param name="obj">The object to spawn.</param>
        public virtual void SpawnObj(GameObject obj)
        {
            SpawnRequest temp = new SpawnRequest();
            SpawnObj(obj, ref temp);
        }
        /// <summary>
        /// Spawn one of the given object using a SpawnRequest to keep track of the spawn process.
        /// </summary>
        /// <param name="obj">The object to spawn.</param>
        /// <param name="request">The SpawnRequest to use.</param>
        public virtual void SpawnObj(GameObject obj, ref SpawnRequest request)
        {
            if (request != null) request.calledCoroutine = StartCoroutine(SpawnOne(obj, request));
            else StartCoroutine(SpawnOne(obj, request));
        }

        /// <summary>
        /// Checks if the given object is spawnable if spawned now.
        /// Is ran to check spawnability before every spawn attempt and this method must also include a null check to include that case.
        /// </summary>
        /// <param name="obj">The object to check.</param>
        /// <returns>True if is spawnable now.</returns>
        public virtual bool IsSpawnable(GameObject obj)
        {
            return obj;
        }

        /// <summary>
        /// Spawn one of the given object using/updating the given SpawnRequest.
        /// </summary>
        /// <param name="obj">The object to spawn.</param>
        /// <param name="request">The SpawnRequest to use.</param>
        protected virtual IEnumerator SpawnOne(GameObject obj, SpawnRequest request = null)
        {
            if (!IsSpawnable(obj))
            {
                if (request != null)
                {
                    request.failed = true;
                    request.completed = true;
                    request.OnCompletedCallback?.Invoke(request);
                }

                yield break;
            }

            if (SpawnDelay > 0) yield return new WaitForSeconds(SpawnDelay);
            if (!IsSpawnable(obj))
            {
                if (request != null)
                {
                    request.failed = true;
                    request.completed = true;
                    request.OnCompletedCallback?.Invoke(request);
                }

                yield break;
            }
            GameObject spawnedObj = null;
            if (useObjectPool && ObjectPoolManager.SINGLETON) spawnedObj = ObjectPoolManager.SINGLETON.Instantiate(obj, transform.position, transform.rotation, spawnAsActive, isObjectPoolManaged);
            else if (!useObjectPool) spawnedObj = GameObject.Instantiate(obj, transform.position, transform.rotation);
            if (spawnedObj)
            {
                spawnedObj.SetActive(spawnAsActive);
                OnSpawn?.Invoke(spawnedObj);
                if (request != null)
                {
                    request.spawnedObj = spawnedObj;
                    request.spawned = true;
                    request.OnSpawnCallback?.Invoke(request);
                }

                if (PostSpawnDelay > 0) yield return new WaitForSeconds(PostSpawnDelay);
                OnPostSpawn?.Invoke(spawnedObj);
                if (request != null)
                {
                    request.postSpawned = true;
                    request.OnPostSpawnCallback?.Invoke(request);
                    request.completed = true;
                    request.OnCompletedCallback?.Invoke(request);
                }

                yield break;
            }
            else if (request != null) request.failed = true;

            if (request != null)
            {
                request.completed = true;
                request.OnCompletedCallback?.Invoke(request);
            }
        }

        protected virtual void OnValidate()
        {
            SpawnDelay = spawnDelay;
            PostSpawnDelay = postSpawnDelay;
        }
    }
}
