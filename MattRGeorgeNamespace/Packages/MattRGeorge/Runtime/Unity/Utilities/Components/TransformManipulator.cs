﻿using UnityEngine;

namespace MattRGeorge.Unity.Utilities.Components
{
    public class TransformManipulator : MonoBehaviour
    {
        [Tooltip("The transform to manipulate. Will use own transform if null")]
        public Transform targetTransform = null;

        /// <summary>
        /// Set the position of the target transform to the given gameobject's position.
        /// </summary>
        /// <param name="obj">The gameobject's position to use.</param>
        public virtual void SetPosition(GameObject obj)
        {
            if (!obj) return;

            if (!targetTransform) transform.position = obj.transform.position;
            else targetTransform.position = obj.transform.position;
        }
        /// <summary>
        /// Set the position of the target transform to the given transform's position.
        /// </summary>
        /// <param name="trans">The transform's position to use.</param>
        public virtual void SetPosition(Transform trans)
        {
            if (!trans) return;

            if (!targetTransform) transform.position = trans.position;
            else targetTransform.position = trans.position;
        }
        /// <summary>
        /// Set the position of the target transform to the given position.
        /// </summary>
        /// <param name="pos">The position to move the target transform to.</param>
        public virtual void SetPosition(Vector3 pos)
        {
            if (pos == null) return;

            if (!targetTransform) transform.position = pos;
            else targetTransform.position = pos;
        }

        /// <summary>
        /// Set the rotation of the target transform to the given gameobject's rotation.
        /// </summary>
        /// <param name="obj">The gameobject's rotation to use.</param>
        public virtual void SetRotation(GameObject obj)
        {
            if (!obj) return;

            if (!targetTransform) transform.rotation = obj.transform.rotation;
            else targetTransform.rotation = obj.transform.rotation;
        }
        /// <summary>
        /// Set the rotation of the target transform to the given transform's rotation.
        /// </summary>
        /// <param name="trans">The transform's rotation to use.</param>
        public virtual void SetRotation(Transform trans)
        {
            if (!trans) return;

            if (!targetTransform) transform.rotation = trans.rotation;
            else targetTransform.rotation = trans.rotation;
        }
    }
}
