﻿using System.Collections;
using UnityEngine;
using MattRGeorge.Unity.Utilities.Enums;
using MattRGeorge.Unity.Utilities.Helpers;
using MattRGeorge.Unity.Tools.ObjectPooling;
using MattRGeorge.Unity.Utilities.Components.Enums;

namespace MattRGeorge.Unity.Utilities.Components
{
    public class TranslateObject : MonoBehaviour
    {
        [Tooltip("The object's movement speed.")]
        public Vector3 direction = Vector3.forward;
        [Tooltip("The time from this object starts moving to end.")]
        [SerializeField] protected float life = 10;
        [Tooltip("Move in what space?")]
        public Space space = Space.Self;
        [Tooltip("Start moving on awake/start?")]
        public UnityStartMethods startOn = UnityStartMethods.None;
        [Tooltip("What to do when end of life is reached?")]
        public EOLMode endMode = EOLMode.Destroy;
        public bool useObjPoolDestroy = false;
        public GameObjectUnityEvent OnStart = new GameObjectUnityEvent();
        public GameObjectUnityEvent OnMoved = new GameObjectUnityEvent();
        public GameObjectUnityEvent OnEnd = new GameObjectUnityEvent();

        /// <summary>
        /// The time from this object starts moving to end.
        /// </summary>
        public virtual float Life
        {
            get => life;
            set
            {
                life = (value < 0) ? 0 : value;
            }
        }

        protected Vector3 currSpeed = Vector3.zero;
        protected Coroutine eolCoroutine = null;

        /// <summary>
        /// Start moving.
        /// </summary>
        /// <param name="restartEOLTimer">Restart the EOL timer?</param>
        public virtual void StartMoving(bool restartEOLTimer = true)
        {
            currSpeed = direction;
            if (restartEOLTimer || eolCoroutine == null)
            {
                if (eolCoroutine != null) StopCoroutine(eolCoroutine);
                eolCoroutine = StartCoroutine(End());
            }
            OnStart?.Invoke(gameObject);
        }

        /// <summary>
        /// Stop moving imediatly.
        /// </summary>
        /// <param name="cancelEOLTimer">Cancel the end of life timer also?</param>
        public virtual void Stop(bool cancelEOLTimer = true)
        {
            currSpeed = Vector3.zero;
            if (cancelEOLTimer && eolCoroutine != null)
            {
                StopCoroutine(eolCoroutine);
                eolCoroutine = null;
            }
        }

        /// <summary>
        /// End this object's life by the desired mode.
        /// </summary>
        protected virtual IEnumerator End()
        {
            yield return new WaitForSeconds(Life);

            if (endMode == EOLMode.Stop) currSpeed = Vector3.zero;
            else if (endMode == EOLMode.Disable) gameObject.SetActive(false);
            else if (endMode == EOLMode.Destroy)
            {
                if (useObjPoolDestroy && ObjectPoolManager.SINGLETON) ObjectPoolManager.SINGLETON.Destroy(gameObject);
                else if (!useObjPoolDestroy) GameObject.Destroy(gameObject);
            }

            OnEnd?.Invoke(gameObject);
            eolCoroutine = null;
        }

        /// <summary>
        /// Move this object by the speed.
        /// </summary>
        protected virtual void Move()
        {
            if (currSpeed == Vector3.zero) return;

            transform.Translate(currSpeed * Time.deltaTime, space);
            OnMoved?.Invoke(gameObject);
        }

        protected virtual void OnValidate()
        {
            Life = life;
        }

        protected virtual void Awake()
        {
            if (startOn == UnityStartMethods.Awake) StartMoving();
        }
        protected virtual void Start()
        {
            if (startOn == UnityStartMethods.Start) StartMoving();
        }

        protected virtual void Update()
        {
            Move();
        }
    }
}
