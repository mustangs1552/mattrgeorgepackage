﻿using UnityEngine;
using MattRGeorge.Unity.Utilities.Helpers;
using MattRGeorge.Unity.Utilities.Components.Enums;

namespace MattRGeorge.Unity.Utilities.Components
{
    public class InputControl : MonoBehaviour
    {
        [Tooltip("Is debugging enabled?")]
        public bool isDebug = false;

        [Tooltip("The type of input to use.")]
        public InputType inputType = InputType.UnityInput;

        [Header("Unity Input")]
        [Tooltip("The name of the input in Unity's Input Manager.")]
        public string unityInputName = "";

        [Header("Key Code Input")]
        [Tooltip("The positive keycode to the desired input.")]
        public KeyCode posKeyCode = KeyCode.None;
        [Tooltip("The negative keycode to the desired input.")]
        public KeyCode negKeyCode = KeyCode.None;
        [Tooltip("The positive alternate keycode to the desired input.")]
        public KeyCode altPosKeyCode = KeyCode.None;
        [Tooltip("The negative alternate keycode to the desired input.")]
        public KeyCode altNegKeyCode = KeyCode.None;
        [Tooltip("Require left/right shift to be pressed also?")]
        public bool requireShift = false;
        [Tooltip("Require left/right CTRL to be pressed also?")]
        public bool requireCTRL = false;
        [Tooltip("Require left/right ALT to be pressed also?")]
        public bool requireALT = false;

        [Header("Events")]
        [Tooltip("Called the same frame that this input was pressed. Passes the value of the input from -1 - 1.")]
        public FloatUnityEvent OnGetDown = new FloatUnityEvent();
        [Tooltip("Called the each frame that this input is pressed.")]
        public FloatUnityEvent OnGet = new FloatUnityEvent();
        [Tooltip("Called the same frame that this input was de-pressed.")]
        public FloatUnityEvent OnGetUp = new FloatUnityEvent();

        /// <summary>
        /// This input is currently being pressed.
        /// This input is passed the OnDown state.
        /// </summary>
        public virtual bool InputDown { get; protected set; }

        protected float inputValue = 0;

        /// <summary>
        /// Check the state of the given input's Unity Input Manager input type.
        /// </summary>
        protected virtual void CheckUnityInput()
        {
            if (!string.IsNullOrEmpty(unityInputName))
            {
                inputValue = Input.GetAxis(unityInputName);
                if (inputValue != 0)
                {
                    if (!InputDown)
                    {
                        if (isDebug) Debug.Log($"Unity input for '{unityInputName}' of gameobject '{gameObject.name}' was pressed with a value of {inputValue}!");
                        OnGetDown?.Invoke(inputValue);
                        InputDown = true;
                    }
                    else
                    {
                        if (isDebug) Debug.Log($"Unity input for '{unityInputName}' of gameobject '{gameObject.name}' is still being pressed with a value of {inputValue}!");
                        OnGet?.Invoke(inputValue);
                    }
                }
                else if (InputDown)
                {
                    if (isDebug) Debug.Log($"Unity input for '{unityInputName}' of gameobject '{gameObject.name}' is no longer being pressed with a value of {inputValue}!");
                    OnGetUp?.Invoke(0);
                    InputDown = false;
                }
            }
        }
        /// <summary>
        /// Check the state of the given input's keycode input type.
        /// </summary>
        protected virtual void CheckKeyCodeInput()
        {
            if (requireShift && (!Input.GetKey(KeyCode.LeftShift) || !Input.GetKey(KeyCode.RightShift))) return;
            if (requireCTRL && (!Input.GetKey(KeyCode.LeftControl) || !Input.GetKey(KeyCode.RightControl))) return;
            if (requireALT && (!Input.GetKey(KeyCode.LeftAlt) || !Input.GetKey(KeyCode.RightAlt))) return;

            if (Input.GetKeyDown(posKeyCode) || Input.GetKeyDown(altPosKeyCode))
            {
                if (isDebug) Debug.Log($"Positive key code for gameobject '{gameObject.name}' was pressed!");
                OnGetDown?.Invoke(1);
            }
            else if (Input.GetKey(posKeyCode) || Input.GetKey(altPosKeyCode))
            {
                if (isDebug) Debug.Log($"Positive key code for gameobject '{gameObject.name}' is still being pressed!");
                OnGet?.Invoke(1);
            }
            else if (Input.GetKeyUp(posKeyCode) || Input.GetKeyUp(altPosKeyCode))
            {
                if (isDebug) Debug.Log($"Positive key code for gameobject '{gameObject.name}' is no longer being pressed!");
                OnGetUp?.Invoke(1);
            }

            if (Input.GetKeyDown(negKeyCode) || Input.GetKeyDown(altNegKeyCode))
            {
                if (isDebug) Debug.Log($"Negative key code for gameobject '{gameObject.name}' was pressed!");
                OnGetDown?.Invoke(-1);
            }
            else if (Input.GetKey(negKeyCode) || Input.GetKey(altNegKeyCode))
            {
                if (isDebug) Debug.Log($"Negative key code for gameobject '{gameObject.name}' is still being pressed!");
                OnGet?.Invoke(-1);
            }
            else if (Input.GetKeyUp(negKeyCode) || Input.GetKeyUp(altNegKeyCode))
            {
                if (isDebug) Debug.Log($"Negative key code for gameobject '{gameObject.name}' is no longer being pressed!");
                OnGetUp?.Invoke(-1);
            }
        }
        /// <summary>
        /// Check the given input depending on its target type.
        /// </summary>
        protected virtual void CheckInput()
        {
            if (inputType == InputType.UnityInput) CheckUnityInput();
            else if (inputType == InputType.KeyCode) CheckKeyCodeInput();
        }

        protected virtual void Update()
        {
            CheckInput();
        }
    }
}
