﻿using System.Collections.Generic;
using UnityEngine;
using MattRGeorge.Unity.Utilities.Static;
using MattRGeorge.Unity.Utilities.Helpers;

namespace MattRGeorge.Unity.Utilities.Components
{
    public class AssetLibrary : MonoBehaviour
    {
        [Tooltip("List of objs with a string name.")]
        [SerializeField] protected List<DictionaryNameGameObject> objs = new List<DictionaryNameGameObject>();
        [Tooltip("List of obj lists with a string name.")]
        [SerializeField] protected List<DictionaryNameListGameObject> objLists = new List<DictionaryNameListGameObject>();

        protected Dictionary<string, GameObject> objsDict = null;
        protected Dictionary<string, List<GameObject>> objListsDict = null;

        /// <summary>
        /// List of objs with a string name.
        /// </summary>
        public virtual List<DictionaryNameGameObject> Objs
        {
            get => new List<DictionaryNameGameObject>(objs);
            set
            {
                if (value == null) objs = new List<DictionaryNameGameObject>();
                else objs = ListUtility.RemoveNullEntries(value);
                ClearGeneratedLists();
            }
        }
        /// <summary>
        /// List of obj lists with a string name.
        /// </summary>
        public virtual List<DictionaryNameListGameObject> ObjLists
        {
            get => new List<DictionaryNameListGameObject>(objLists);
            set
            {
                if (value == null) objLists = new List<DictionaryNameListGameObject>();
                else objLists = ListUtility.RemoveNullEntries(value);
                ClearGeneratedLists();
            }
        }

        /// <summary>
        /// Returns the dictionary of `objLists` as one combined `List<GameObject>`.
        /// </summary>
        protected virtual List<GameObject> ObjsDictList
        {
            get
            {
                if (objsDict == null) objsDict = DictionaryNameGameObject.ToDictionary(objs);

                List<GameObject> list = new List<GameObject>();
                foreach (KeyValuePair<string, GameObject> pair in objsDict) list.Add(pair.Value);
                return list;
            }
        }

        /// <summary>
        /// Get the object by the provided name that it was assigned.
        /// </summary>
        /// <param name="name">The name assigned to the desired object.</param>
        /// <returns>The desired object.</returns>
        public virtual GameObject GetObject(string name)
        {
            if (objsDict == null) objsDict = DictionaryNameGameObject.ToDictionary(objs);
            if (string.IsNullOrEmpty(name) || !objsDict.ContainsKey(name)) return null;

            return objsDict[name];
        }

        /// <summary>
        /// Gets a random object from the `objs` list.
        /// </summary>
        /// <returns>A random object from the `objs` list.</returns>
        public virtual GameObject GetRandomObject()
        {
            HashSet<int> temp = new HashSet<int>();
            return GetRandomObject(ref temp);
        }
        /// <summary>
        /// Gets a random object from the `objs` list with the option to not allow certain objects.
        /// </summary>
        /// <param name="invalidIndexes">Indexes of the objects not to allow being chosen.</param>
        /// <returns>A random object from the `objs` list.</returns>
        public virtual GameObject GetRandomObject(ref HashSet<int> invalidIndexes)
        {
            if (invalidIndexes == null) invalidIndexes = new HashSet<int>();
            if (objsDict == null) objsDict = DictionaryNameGameObject.ToDictionary(objs);

            return ListUtility.GetValidRandomObject(ObjsDictList, ref invalidIndexes);
        }

        /// <summary>
        /// Gets a random object from any of the object lists by the names provided.
        /// </summary>
        /// <param name="listNames">The names of the lists to use.</param>
        /// <returns>A random object from any of the requested lists.</returns>
        public virtual GameObject GetRandomObjectFromObjectLists(List<string> listNames)
        {
            string temp = "";
            return GetRandomObjectFromObjectLists(listNames, out temp);
        }
        /// <summary>
        /// Gets a random object from any of the object lists by the names provided and returns the chosen list's name via `out`.
        /// </summary>
        /// <param name="listNames">The names of the lists to use.</param>
        /// <param name="chosenListName">An `out` that contains the name of the chosen list.</param>
        /// <returns>A random object from any of the requested lists.</returns>
        public virtual GameObject GetRandomObjectFromObjectLists(List<string> listNames, out string chosenListName)
        {
            chosenListName = "";
            if (listNames == null || listNames.Count == 0) return null;
            if (objListsDict == null) objListsDict = DictionaryNameListGameObject.ToDictionary(objLists);

            List<GameObject> combinedList = new List<GameObject>();
            foreach(string listName in listNames)
            {
                if (string.IsNullOrEmpty(listName) || !objListsDict.ContainsKey(listName) || objListsDict[listName] == null || objListsDict[listName].Count == 0) continue;

                objListsDict[listName].ForEach(x => combinedList.Add(x));
            }

            HashSet<int> temp = new HashSet<int>();
            GameObject chosenObj = ListUtility.GetValidRandomObject(combinedList, ref temp);
            foreach (KeyValuePair<string, List<GameObject>> list in objListsDict)
            {
                foreach (GameObject obj in list.Value)
                {
                    if (chosenObj == obj)
                    {
                        chosenListName = list.Key;
                        return chosenObj;
                    }
                }
            }
            return chosenObj;
        }

        /// <summary>
        /// Get the list of objects by the provided name that it was assigned.
        /// </summary>
        /// <param name="name">The name assinged to the desired list of objects.</param>
        /// <returns>The desired list of objects.</returns>
        public virtual List<GameObject> GetObjectLists(string name)
        {
            if (objListsDict == null) objListsDict = DictionaryNameListGameObject.ToDictionary(objLists);
            if (string.IsNullOrEmpty(name) || !objListsDict.ContainsKey(name)) return null;

            return objListsDict[name];
        }

        /// <summary>
        /// Gets a random object list from the `objLists` list.
        /// </summary>
        /// <returns>A random object list from the `objLists` list.</returns>
        public virtual List<GameObject> GetRandomObjectLists()
        {
            HashSet<int> tempHS = new HashSet<int>();
            return GetRandomObjectLists(ref tempHS);
        }
        /// <summary>
        /// Gets a random object list from the `objLists` list with the option to not allow certain lists.
        /// </summary>
        /// <param name="invalidIndexes">Indexes of the lists not to allow being chosen.</param>
        /// <returns>A random object list from the `objLists` list.</returns>
        public virtual List<GameObject> GetRandomObjectLists(ref HashSet<int> invalidIndexes)
        {
            if (objListsDict == null) objListsDict = DictionaryNameListGameObject.ToDictionary(objLists);

            List<List<GameObject>> lists = new List<List<GameObject>>();
            foreach (KeyValuePair<string, List<GameObject>> list in objListsDict) lists.Add(list.Value);
            return ListUtility.GetValidRandomObject(lists, ref invalidIndexes);
        }

        /// <summary>
        /// Clears the saved lists so that they can be repopulated.
        /// </summary>
        public virtual void ClearGeneratedLists()
        {
            objsDict = null;
            objListsDict = null;
        }
    }
}
