﻿using UnityEngine;

namespace MattRGeorge.Unity.Utilities.Components
{
    public class LookAtTransform : MonoBehaviour
    {
        [Tooltip("The target transform to look at.")]
        public Transform target = null;
        [Tooltip("Update the transform each frame?")]
        public bool auto = false;

        /// <summary>
        /// Update the transform to look at the target.
        /// </summary>
        public virtual void UpdateLookAt()
        {
            if (!target) return;

            transform.LookAt(target);
        }

        protected virtual void Update()
        {
            if (auto) UpdateLookAt();
        }
    }
}
