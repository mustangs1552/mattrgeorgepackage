﻿using System.Collections.Generic;
using UnityEngine;

namespace MattRGeorge.Unity.Utilities.Components
{
    /// <summary>
    /// An object that is placed on a trigger and monitors how many objects are currently within the bounds.
    /// </summary>
    public class ObjectPresenceMonitor : MonoBehaviour
    {
        [Tooltip("The objects to check for.")]
        [SerializeField] protected List<GameObject> objsToMonitor = new List<GameObject>();

        /// <summary>
        /// The amount of objects currently within the bounds.
        /// </summary>
        public virtual int ObjCount => objsPresent.Count;

        protected List<GameObject> objsPresent = new List<GameObject>();

        /// <summary>
        /// Check to see if the object entering the trigger is in the list of monitored objects and isn't already present and increment current count.
        /// </summary>
        /// <param name="obj">Triggered object (Not used).</param>
        /// <param name="otherObj">Triggering object.</param>
        public virtual void CheckObjectIn(GameObject obj, Collider otherObj)
        {
            if (!otherObj) return;

            if (objsToMonitor.Contains(otherObj.gameObject) && !objsPresent.Contains(otherObj.gameObject)) objsPresent.Add(otherObj.gameObject);
        }
        /// <summary>
        /// Check to see if the object leaving the trigger is in the list of monitored objects and is present and decrement current count.
        /// </summary>
        /// <param name="obj">Triggered object (Not used).</param>
        /// <param name="otherObj">Triggering object.</param>
        public virtual void CheckObjectOut(GameObject obj, Collider otherObj)
        {
            if (!otherObj) return;

            if (objsToMonitor.Contains(otherObj.gameObject) && objsPresent.Contains(otherObj.gameObject)) objsPresent.Remove(otherObj.gameObject);
        }
    }
}