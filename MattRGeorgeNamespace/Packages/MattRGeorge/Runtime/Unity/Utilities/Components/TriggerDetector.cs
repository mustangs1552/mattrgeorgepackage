﻿using UnityEngine;
using MattRGeorge.Unity.Utilities.Helpers;

namespace MattRGeorge.Unity.Utilities.Components
{
    public class TriggerDetector : MonoBehaviour
    {
        [Tooltip("Is debugging enabled?")]
        public bool isDebug = false;
        public GameObjectColliderUnityEvent OnEnter = new GameObjectColliderUnityEvent();
        public GameObjectColliderUnityEvent OnStay = new GameObjectColliderUnityEvent();
        public GameObjectColliderUnityEvent OnExit = new GameObjectColliderUnityEvent();

        protected virtual void OnTriggerEnter(Collider other)
        {
            if (isDebug) Debug.Log($"{gameObject.name} entered a collision with {other.gameObject.name}!");
            OnEnter?.Invoke(gameObject, other);
        }
        protected virtual void OnTriggerStay(Collider other)
        {
            if (isDebug) Debug.Log($"{gameObject.name} is still colliding with {other.gameObject.name}!");
            OnStay?.Invoke(gameObject, other);
        }
        protected virtual void OnTriggerExit(Collider other)
        {
            if (isDebug) Debug.Log($"{gameObject.name} exited a collision with {other.gameObject.name}!");
            OnExit?.Invoke(gameObject, other);
        }
    }
}
