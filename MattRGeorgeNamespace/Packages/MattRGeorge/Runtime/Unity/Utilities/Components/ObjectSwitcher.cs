﻿using System.Collections.Generic;
using UnityEngine;
using MattRGeorge.Unity.Utilities.Static;
using MattRGeorge.Unity.Utilities.Helpers;

namespace MattRGeorge.Unity.Utilities.Components
{
    public class ObjectSwitcher : MonoBehaviour
    {
        [Tooltip("The objects that can be switched between with names used to select them.")]
        [SerializeField] protected List<DictionaryNameGameObject> objs = new List<DictionaryNameGameObject>();

        /// <summary>
        /// The objects that can be switched between with names used to select them.
        /// </summary>
        public virtual List<DictionaryNameGameObject> Objs
        {
            get => new List<DictionaryNameGameObject>(objs);
            set
            {
                if (value == null) objs = new List<DictionaryNameGameObject>();
                else objs = ListUtility.RemoveNullEntries(value);
            }
        }

        /// <summary>
        /// Switch to the first available object in the list.
        /// </summary>
        public virtual void SwitchToFirstAvailableObject()
        {
            if (objs == null || objs.Count == 0) return;

            bool objWasFound = false;
            foreach (DictionaryNameGameObject obj in objs)
            {
                if (obj != null && obj.obj)
                {
                    if (!objWasFound)
                    {
                        obj.obj.SetActive(true);
                        objWasFound = true;
                    }
                    else obj.obj.SetActive(false);
                }
            }
        }

        /// <summary>
        /// Switch to the object by the selectable name.
        /// Will switch to the first object in list if object wasn't found.
        /// </summary>
        /// <param name="name">The name that represents the desired object.</param>
        public virtual void SwitchObjects(string name)
        {
            if (string.IsNullOrEmpty(name) || objs == null || objs.Count == 0) return;

            bool objWasFound = false;
            GameObject defaultObj = null;
            foreach (DictionaryNameGameObject obj in objs)
            {
                if (obj == null || !obj.obj) return;
                if (defaultObj == null) defaultObj = obj.obj;

                if (!string.IsNullOrEmpty(obj.name))
                {
                    if (obj.name == name)
                    {
                        obj.obj.SetActive(true);
                        objWasFound = true;
                    }
                    else obj.obj.SetActive(false);
                }
                else obj.obj.SetActive(false);
            }

            if (!objWasFound && defaultObj) defaultObj.SetActive(true);
        }

        protected virtual void Awake()
        {
            SwitchToFirstAvailableObject();
        }
    }
}
