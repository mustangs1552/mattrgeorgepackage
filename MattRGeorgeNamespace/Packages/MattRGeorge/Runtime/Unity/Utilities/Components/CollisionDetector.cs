﻿using UnityEngine;
using MattRGeorge.Unity.Utilities.Helpers;

namespace MattRGeorge.Unity.Utilities.Components
{
    public class CollisionDetector : MonoBehaviour
    {
        [Tooltip("Is debugging enabled?")]
        public bool isDebug = false;
        [Tooltip("Draws a line back to last known position to check for missed collisions for fast moving objects.")]
        [SerializeField] protected bool enableAutoBacktracing = true;
        [Tooltip("The layer mask used by the Auto Backtracing linecast.")]
        public LayerMask backtraceLayerMask = ~0;
        public GameObjectCollisionUnityEvent OnEnter = new GameObjectCollisionUnityEvent();
        public GameObjectCollisionUnityEvent OnStay = new GameObjectCollisionUnityEvent();
        public GameObjectCollisionUnityEvent OnExit = new GameObjectCollisionUnityEvent();
        public GameObjectUnityEvent OnBacktraceHit = new GameObjectUnityEvent();

        /// <summary>
        /// Draws a line back to last known position to check for missed collisions for fast moving objects.
        /// </summary>
        public virtual bool EnableAutoBacktracing
        {
            get => enableAutoBacktracing;
            set
            {
                if (value) lastPos = transform.position;
                enableAutoBacktracing = value;
            }
        }

        protected Vector3 lastPos = Vector3.zero;

        /// <summary>
        /// Check a linecast for objects that were passed through and missed collisions.
        /// </summary>
        public virtual void CheckBacktracing()
        {
            if (lastPos == transform.position) return;

            RaycastHit hit = new RaycastHit();
            if (Physics.Linecast(lastPos, transform.position, out hit, backtraceLayerMask))
            {
                if (hit.collider.isTrigger) lastPos = transform.position;
                else
                {
                    if (isDebug) Debug.LogError($"{gameObject.name} collided with {hit.collider.gameObject.name} via backtrace!");
                    OnBacktraceHit?.Invoke(hit.collider.gameObject);
                }
            }
            else lastPos = transform.position;
        }

        protected virtual void OnCollisionEnter(Collision collision)
        {
            if (isDebug) Debug.Log($"{gameObject.name} entered a collision with {collision.gameObject.name}!");
            OnEnter?.Invoke(gameObject, collision);
        }
        protected virtual void OnCollisionStay(Collision collision)
        {
            if (isDebug) Debug.Log($"{gameObject.name} is still colliding with {collision.gameObject.name}!");
            OnStay?.Invoke(gameObject, collision);
        }
        protected virtual void OnCollisionExit(Collision collision)
        {
            if (isDebug) Debug.Log($"{gameObject.name} exited a collision with {collision.gameObject.name}!");
            OnExit?.Invoke(gameObject, collision);
        }

        protected virtual void OnEnable()
        {
            lastPos = transform.position;
        }

        protected virtual void Awake()
        {
            lastPos = transform.position;
        }

        protected virtual void FixedUpdate()
        {
            if (EnableAutoBacktracing) CheckBacktracing();
        }
    }
}
