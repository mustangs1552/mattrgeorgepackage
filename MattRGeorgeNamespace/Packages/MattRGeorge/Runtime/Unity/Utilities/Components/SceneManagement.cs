﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace MattRGeorge.Unity.Utilities.Components
{
    /// <summary>
    /// Component with way to use the scene manager from the inspector.
    /// </summary>
    public class SceneManagement : MonoBehaviour
    {
        /// <summary>
        /// Uses `SceneManager.LoadScene()` to load the scene at the given scene index in the build settings.
        /// </summary>
        /// <param name="sceneIndex">The scene index in the build settings.</param>
        public virtual void LoadScene(int sceneIndex)
        {
            if (sceneIndex < 0 || sceneIndex >= SceneManager.sceneCountInBuildSettings) return;

            SceneManager.LoadScene(sceneIndex);
        }
        /// <summary>
        /// Uses `SceneManager.LoadScene()` to load the scene given name in the build settings.
        /// </summary>
        /// <param name="sceneName">The name of the desired scene.</param>
        public virtual void LoadScene(string sceneName)
        {
            if (string.IsNullOrEmpty(sceneName)) return;

            SceneManager.LoadScene(sceneName);
        }

        /// <summary>
        /// Uses `SceneManager.LoadSceneAsync()` to load the scene at the given scene index in the build settings.
        /// </summary>
        /// <param name="sceneIndex">The scene index in the build settings.</param>
        public virtual void LoadSceneAsync(int sceneIndex)
        {
            if (sceneIndex < 0 || sceneIndex >= SceneManager.sceneCountInBuildSettings) return;

            SceneManager.LoadSceneAsync(sceneIndex);
        }
        /// <summary>
        /// Uses `SceneManager.LoadSceneAsync()` to load the scene given name in the build settings.
        /// </summary>
        /// <param name="sceneName">The name of the desired scene.</param>
        public virtual void LoadSceneAsync(string sceneName)
        {
            if (string.IsNullOrEmpty(sceneName)) return;

            SceneManager.LoadSceneAsync(sceneName);
        }
    }
}
