﻿using UnityEngine;
using UnityEngine.Events;
using MattRGeorge.Unity.Utilities.Enums;

namespace MattRGeorge.Unity.Utilities.Components
{
    /// <summary>
    /// A simple timer that supports both counting down and counting up.
    /// </summary>
    public class Timer : MonoBehaviour
    {
        [Header("The countdown amount in seconds.")]
        [SerializeField] protected float countdownTimer = 10;
        [Tooltip("Start timer on awake/start?")]
        public UnityStartMethods startOn = UnityStartMethods.None;
        public UnityEvent OnTimeUp = new UnityEvent();

        /// <summary>
        /// The countdown amount in seconds.
        /// </summary>
        public virtual float CountdownTimer
        {
            get => countdownTimer;
            set
            {
                countdownTimer = (value < 0) ? 0 : value;
            }
        }
        /// <summary>
        /// The time remaining in the count down.
        /// </summary>
        public virtual float TimeRemaining
        {
            get
            {
                return (isCounting && currTimeLength > 0) ? currTimeLength - (Time.time - startTime) : 0;
            }
        }
        /// <summary>
        /// The amount of time that has passed since timer started counting up/down.
        /// </summary>
        public virtual float TimePassed
        {
            get
            {
                return (isCounting) ? Time.time - startTime : 0;
            }
        }
        
        protected bool isCounting = false;
        protected float currTimeLength = 0;
        protected float startTime = 0;

        /// <summary>
        /// Start the timer counting down.
        /// </summary>
        /// <param name="time">The time to count down from.</param>
        /// <returns>return true if successfully started.</returns>
        public virtual bool StartCountdown(float time = -1)
        {
            if (time < 0) time = CountdownTimer;

            if (!isCounting)
            {
                currTimeLength = time;
                startTime = Time.time;
                isCounting = true;
                return true;
            }

            return false;
        }
        /// <summary>
        /// Start the timer counting up.
        /// </summary>
        /// <returns>True if successfully started.</returns>
        public virtual bool StartCountingUp()
        {
            if (!isCounting)
            {
                startTime = Time.time;
                isCounting = true;
                return true;
            }

            return false;
        }

        /// <summary>
        /// Stop the timer counting up/down.
        /// </summary>
        /// <returns>The time that has passed since it started.</returns>
        public virtual float CancelCounting()
        {
            float timePassed = TimePassed;
            isCounting = false;
            currTimeLength = 0;
            return timePassed;
        }

        /// <summary>
        /// Check if the timer has finished counting down.
        /// </summary>
        protected virtual void CheckTime()
        {
            if (isCounting && currTimeLength > 0 && TimeRemaining <= 0)
            {
                currTimeLength = 0;
                isCounting = false;

                OnTimeUp?.Invoke();
            }
        }

        protected virtual void OnValidate()
        {
            CountdownTimer = countdownTimer;
        }

        protected virtual void Awake()
        {
            if (startOn == UnityStartMethods.Awake) StartCountdown(CountdownTimer);
        }
        protected virtual void Start()
        {
            if (startOn == UnityStartMethods.Start) StartCountdown(CountdownTimer);
        }

        protected virtual void Update()
        {
            CheckTime();
        }
    }
}