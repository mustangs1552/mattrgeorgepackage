﻿using UnityEngine;
using UnityEngine.Events;

namespace MattRGeorge.Unity.Utilities.Components
{
    public class UnityEvents : MonoBehaviour
    {
        public UnityEvent OnAwake = new UnityEvent();
        public UnityEvent OnStart = new UnityEvent();
        public UnityEvent OnUpdate = new UnityEvent();
        public UnityEvent OnFixedUpdate = new UnityEvent();

        protected virtual void Awake()
        {
            OnAwake?.Invoke();
        }
        protected virtual void Start()
        {
            OnStart?.Invoke();
        }

        protected virtual void Update()
        {
            OnUpdate?.Invoke();
        }
        protected virtual void FixedUpdate()
        {
            OnFixedUpdate?.Invoke();
        }
    }
}
