﻿using UnityEngine;

namespace MattRGeorge.Unity.Utilities.Components
{
    public class FollowTransform : MonoBehaviour
    {
        [Tooltip("The target transform to follow.")]
        public Transform target = null;
        [Tooltip("Update the transform each frame?")]
        public bool auto = false;
        [Header("Position")]
        [Tooltip("Follow via positional on x-axis?")]
        public bool followPostionX = true;
        [Tooltip("Follow via positional on y-axis?")]
        public bool followPostionY = true;
        [Tooltip("Follow via positional on z-axis?")]
        public bool followPostionZ = true;
        [Header("Rotation")]
        [Tooltip("Follow via rotational on x-axis?")]
        public bool followRotationX = true;
        [Tooltip("Follow via rotational on y-axis?")]
        public bool followRotationY = true;
        [Tooltip("Follow via rotational on z-axis?")]
        public bool followRotationZ = true;

        protected float currPosX = 0;
        protected float currPosY = 0;
        protected float currPosZ = 0;
        protected float currRotX = 0;
        protected float currRotY = 0;
        protected float currRotZ = 0;

        /// <summary>
        /// Update the position of this object.
        /// </summary>
        public virtual void UpdatePosition()
        {
            if (!target) return;

            currPosX = (followPostionX) ? target.position.x : transform.position.x;
            currPosY = (followPostionY) ? target.position.y : transform.position.y;
            currPosZ = (followPostionZ) ? target.position.z : transform.position.z;
            transform.position = new Vector3(currPosX, currPosY, currPosZ);
        }
        /// <summary>
        /// Update the rotation of this object.
        /// </summary>
        public virtual void UpdateRotation()
        {
            if (!target) return;

            currRotX = (followRotationX) ? target.eulerAngles.x : transform.eulerAngles.x;
            currRotY = (followRotationY) ? target.eulerAngles.y : transform.eulerAngles.y;
            currRotZ = (followRotationZ) ? target.eulerAngles.z : transform.eulerAngles.z;
            transform.eulerAngles = new Vector3(currRotX, currRotY, currRotZ);
        }
        /// <summary>
        /// Update this object's transform.
        /// </summary>
        public virtual void UpdateTransform()
        {
            UpdatePosition();
            UpdateRotation();
        }

        protected virtual void Update()
        {
            if(auto) UpdateTransform();
        }
    }
}
