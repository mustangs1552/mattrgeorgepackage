﻿using UnityEngine;
using MattRGeorge.Unity.Utilities.Helpers;
using MattRGeorge.Unity.Utilities.Static;

namespace MattRGeorge.Unity.Utilities.Components
{
    public class RaycasterDetector : MonoBehaviour
    {
        [Tooltip("Is debugging enabled?")]
        public bool isDebug = false;
        [Tooltip("The range of the ray from its start point.")]
        [SerializeField] protected float range = 10;
        [Tooltip("The starting offset of the ray along this objects z-axis.")]
        public float startOffset = 0;
        [Tooltip("The layer mask to use.")]
        public LayerMask layerMask = ~0;
        [Tooltip("Automatically check the raycast by the frequency?")]
        [SerializeField] protected bool auto = false;
        [Tooltip("The frequency in seconds to check the raycast. Setting this to 0 checks every frame.")]
        [SerializeField] protected float frequency = 1;
        [Header("Events")]
        public RaycastHitUnityEvent OnHitStart = new RaycastHitUnityEvent();
        public RaycastHitUnityEvent OnHit = new RaycastHitUnityEvent();
        public GameObjectUnityEvent OnHitEnd = new GameObjectUnityEvent();

        /// <summary>
        /// The range of the ray from its start point.
        /// </summary>
        public virtual float Range
        {
            get => range;
            set
            {
                range = (value > 0) ? value : 0;
            }
        }
        /// <summary>
        /// Automatically check the raycast by the frequency?
        /// </summary>
        public virtual bool Auto
        {
            get => auto;
            set
            {
                auto = value;
                ResetCheckRaycast();
            }
        }
        /// <summary>
        /// The frequency in seconds to check the raycast.
        /// Setting this to 0 checks every frame.
        /// </summary>
        public virtual float Frequency
        {
            get => frequency;
            set
            {
                frequency = (value > 0) ? value : 0;
                ResetCheckRaycast();
            }
        }

        protected GameObject lastHitObj = null;
        protected Coroutine autoCoroutine = null;

        /// <summary>
        /// Checks the raycast to see if anything was hit.
        /// </summary>
        /// <returns>True if a hit was detected.</returns>
        public virtual bool CheckRaycast()
        {
            RaycastHit temp = new RaycastHit();
            return CheckRaycast(out temp);
        }
        /// <summary>
        /// Checks the raycast to see if anything was hit.
        /// </summary>
        /// <param name="hit">The resulting hit.</param>
        /// <returns>True if a hit was detected.</returns>
        public virtual bool CheckRaycast(out RaycastHit hit)
        {
            hit = new RaycastHit();
            return Physics.Raycast(transform.position + transform.forward * startOffset, transform.forward, out hit, Range, layerMask);
        }

        /// <summary>
        /// The auto mode for checking raycast.
        /// This calls bool CheckRaycast() to do the check and then will call the proper event depending on the result.
        /// </summary>
        protected virtual bool CheckRaycastAuto()
        {
            RaycastHit hit = new RaycastHit();
            if (CheckRaycast(out hit))
            {
                if (!lastHitObj)
                {
                    if (isDebug) Debug.Log($"Raycast hit {hit.collider.gameObject}.");
                    lastHitObj = hit.collider.gameObject;
                    OnHitStart?.Invoke(hit);
                }
                else if (hit.collider.gameObject == lastHitObj)
                {
                    if (isDebug) Debug.Log($"Raycast still hitting {hit.collider.gameObject}.");
                    OnHit?.Invoke(hit);
                }
                else
                {
                    if (isDebug) Debug.Log($"Raycast no longer hitting {lastHitObj} but, is now hitting {hit.collider.gameObject}.");
                    OnHitEnd?.Invoke(lastHitObj);
                    OnHitStart?.Invoke(hit);
                    lastHitObj = hit.collider.gameObject;
                }
            }
            else if (lastHitObj)
            {
                if (isDebug) Debug.Log($"Raycast no longer hitting {lastHitObj}.");
                OnHitEnd?.Invoke(lastHitObj);
                lastHitObj = null;
            }

            return true;
        }

        /// <summary>
        /// Start/update the repeating `CheckRaycastAuto()` method or disable it..
        /// </summary>
        protected virtual void ResetCheckRaycast()
        {
            if (Frequency <= 0 || !Auto)
            {
                StopAutoCoroutine();
                return;
            }

            StopAutoCoroutine();
            autoCoroutine = StartCoroutine(CoroutineUtility.WhileCoroutine(CheckRaycastAuto, null, 0, Frequency));
        }
        /// <summary>
        /// Stop the repeating `CheckRaycastAuto()` coroutine.
        /// </summary>
        protected virtual void StopAutoCoroutine()
        {
            if (autoCoroutine == null) return;

            StopCoroutine(autoCoroutine);
            autoCoroutine = null;
        }

        protected virtual void OnValidate()
        {
            Range = range;
            Frequency = frequency;
        }

        protected virtual void Awake()
        {
            ResetCheckRaycast();
        }
        protected virtual void Update()
        {
            if (Frequency == 0 && autoCoroutine == null) CheckRaycastAuto();
        }

        protected virtual void OnDrawGizmos()
        {
            if (!isDebug) return;

            Gizmos.color = Color.green;
            Gizmos.DrawRay(transform.position + transform.forward * startOffset, transform.forward * Range);
        }
    }
}
