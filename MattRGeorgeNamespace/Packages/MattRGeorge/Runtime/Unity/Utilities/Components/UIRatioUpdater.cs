﻿using UnityEngine;

namespace MattRGeorge.Unity.Utilities.Components
{
    /// <summary>
    /// Updates the width or height of the attached rect transform to match given ratio.
    /// </summary>
    [RequireComponent(typeof(RectTransform))]
    public class UIRatioUpdater : MonoBehaviour
    {
        [Tooltip("The ratio desired.")]
        [SerializeField] protected float ratio = 1;

        /// <summary>
        /// The ratio desired.
        /// </summary>
        public virtual float Ratio
        {
            get => ratio;
            set
            {
                ratio = (value < 1) ? 1 : value;
            }
        }

        protected RectTransform trans = null;

        /// <summary>
        /// Re-size the RectTransform to the desired ratio.
        /// </summary>
        /// <param name="updateWidth">Re-size by the width.</param>
        protected virtual void ResizeToRatio(bool updateWidth)
        {
            if (!trans) return;

            if(!updateWidth) trans.sizeDelta = new Vector2(0, (trans.rect.width / Ratio - trans.rect.height) / 2);
            else trans.sizeDelta = new Vector2((trans.rect.height / Ratio - trans.rect.width) / 2, 0);
        }
        /// <summary>
        /// Check to see to the current ratio is the same as the desired one.
        /// </summary>
        protected virtual void CheckRatio()
        {
            if (!trans) return;

            bool widthLarger = trans.rect.width > trans.rect.height;
            float currRatio = (widthLarger) ? trans.rect.width / trans.rect.height : trans.rect.height / trans.rect.width;

            if (currRatio != Ratio) ResizeToRatio(widthLarger);
        }

        /// <summary>
        /// Initial setup.
        /// </summary>
        protected virtual void Setup()
        {
            trans = GetComponent<RectTransform>();
        }

        protected virtual void OnValidate()
        {
            Ratio = ratio;
        }

        protected virtual void Awake()
        {
            Setup();
            CheckRatio();
        }
    }
}
