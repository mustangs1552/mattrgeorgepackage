﻿using System.Collections.Generic;
using UnityEngine;
using MattRGeorge.General.Utilities.Enums;
using MattRGeorge.Unity.Utilities.Helpers;
using MattRGeorge.Unity.Utilities.Static;

namespace MattRGeorge.Unity.Utilities.Components
{
    public class TranslateObjectViaNodes : TranslateObject
    {
        [Tooltip("The nodes to move between.")]
        [SerializeField] protected List<Transform> nodes = new List<Transform>();
        [Tooltip("The way the next node is chosen.")]
        public ListRandomChoiceMode nodeChoiceMode = ListRandomChoiceMode.AllSequentially;
        public GameObjectTransformUnityEvent OnNodeReached = new GameObjectTransformUnityEvent();

        /// <summary>
        /// The nodes to move between.
        /// </summary>
        public virtual List<Transform> Nodes
        {
            get => new List<Transform>(nodes);
            set
            {
                if (value == null) nodes = new List<Transform>();
                else nodes = ListUtility.RemoveNullEntries(value);
            }
        }

        protected Transform nextNode = null;
        protected HashSet<int> usedIndexes = new HashSet<int>();

        public override void StartMoving(bool restartEOLTimer = true)
        {
            base.StartMoving(restartEOLTimer);

            ChooseNode();
        }
        public override void Stop(bool cancelEOLTimer = true)
        {
            base.Stop(cancelEOLTimer);

            nextNode = null;
        }

        /// <summary>
        /// Choose the next node to move to.
        /// </summary>
        protected virtual void ChooseNode()
        {
            if (nodes == null || nodes.Count == 0) return;

            nextNode = null;
            while (!nextNode) nextNode = ListUtility.GetObjectByMode(nodeChoiceMode, nodes, ref usedIndexes);
            CheckDirectionValue();
            transform.LookAt(nextNode.transform);
        }

        /// <summary>
        /// Check to see if the next node was reached and choose a new node.
        /// </summary>
        protected virtual void CheckNodeDist()
        {
            if (!nextNode) return;

            if(Vector3.Distance(transform.position, nextNode.position) <= .1f)
            {
                OnNodeReached?.Invoke(gameObject, nextNode);

                ChooseNode();
            }
        }

        /// <summary>
        /// Check that the direction is going down the z-axis and not moving on anyother axis.
        /// </summary>
        protected virtual void CheckDirectionValue()
        {
            if (direction.x != 0 || direction.y != 0) direction = Vector3.forward * direction.z;
        }

        protected override void OnValidate()
        {
            base.OnValidate();

            CheckDirectionValue();
        }

        protected override void Update()
        {
            base.Update();

            CheckNodeDist();
        }
    }
}
