﻿namespace MattRGeorge.Unity.Utilities.Components.Enums
{
    /// <summary>
    /// End of Life modes for what to do after an object is at the end of its life.
    /// </summary>
    public enum EOLMode
    {
        Nothing,
        Stop,
        Disable,
        Destroy
    }
}
