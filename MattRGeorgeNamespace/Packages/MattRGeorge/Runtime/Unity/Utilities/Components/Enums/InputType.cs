﻿namespace MattRGeorge.Unity.Utilities.Components.Enums
{
    /// <summary>
    /// Types of input systems.
    /// </summary>
    public enum InputType
    {
        UnityInput,
        KeyCode
    }
}
