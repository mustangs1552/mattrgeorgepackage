﻿using UnityEngine;
using MattRGeorge.Unity.Utilities.Static;
using MattRGeorge.Unity.Utilities.Enums;

namespace MattRGeorge.Unity.Utilities.Components
{
    public class ApplyForceToObject : MonoBehaviour
    {
        [Tooltip("The force and direction to apply.")]
        public Vector3 force = new Vector3(0, 0, 10);
        [Tooltip("The force mode to use.")]
        public ForceMode forceMode = ForceMode.Impulse;
        [Tooltip("Add force locally?")]
        public bool addForceLocally = true;
        [Tooltip("The Rigidbody to apply the force to.")]
        public Rigidbody rBody = null;
        [Tooltip("Continuously apply force overtime?")]
        public bool applyContinuously = false;
        [Tooltip("Frequency to continuously apply force.")]
        public float continuouslyFreqeuncy = 1;
        [Tooltip("Apply force on awake/start?")]
        [SerializeField] protected UnityStartMethods startOn = UnityStartMethods.None;

        /// <summary>
        /// Apply a force via the settings and fire off another call to this method if continuous force is enabled.
        /// </summary>
        public virtual void ApplyForce()
        {
            ApplyForce(1);
        }
        /// <summary>
        /// Apply a force via the settings and fire off another call (doesn't include multiplier) to this method if continuous force is enabled.
        /// </summary>
        /// <param name="multiplier">Multiplier to apply to the force value.</param>
        public virtual void ApplyForce(float multiplier)
        {
            if (!rBody) return;

            if (addForceLocally) rBody.AddRelativeForce(force * multiplier, forceMode);
            else rBody.AddForce(force, forceMode);

            if (applyContinuously) StartCoroutine(CoroutineUtility.DelayedMethodCoroutine(ApplyForce, continuouslyFreqeuncy));
        }

        /// <summary>
        /// Check for missing values from the user.
        /// </summary>
        protected virtual void CheckMissingValues()
        {
            if (!rBody) rBody = GetComponent<Rigidbody>();
        }

        protected virtual void OnValidate()
        {
            if (continuouslyFreqeuncy < 0) continuouslyFreqeuncy = 0;
        }

        protected virtual void Awake()
        {
            CheckMissingValues();
            if (startOn == UnityStartMethods.Awake) ApplyForce();
        }
        protected virtual void Start()
        {
            if (startOn == UnityStartMethods.Start) ApplyForce();
        }
    }
}
