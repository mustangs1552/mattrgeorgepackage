﻿using UnityEngine;

namespace MattRGeorge.Unity.Utilities.Components
{
    /// <summary>
    /// Methods that can be used from an event in the inspector to call Debug.Log...() methods.
    /// </summary>
    public class Logger : MonoBehaviour
    {
        /// <summary>
        /// Log a message via Debug.Log().
        /// </summary>
        /// <param name="msg">The message to log.</param>
        public virtual void LogMessage(string msg)
        {
            if (string.IsNullOrEmpty(msg)) return;

            Debug.Log(msg);
        }
        /// <summary>
        /// Log a warning message via Debug.LogWarning().
        /// </summary>
        /// <param name="msg">The message to log.</param>
        public virtual void LogWarningMessage(string msg)
        {
            if (string.IsNullOrEmpty(msg)) return;

            Debug.LogWarning(msg);
        }
        /// <summary>
        /// Log an error message via Debug.LogError().
        /// </summary>
        /// <param name="msg">The message to log.</param>
        public virtual void LogErrorMessage(string msg)
        {
            if (string.IsNullOrEmpty(msg)) return;

            Debug.LogError(msg);
        }
    }
}
