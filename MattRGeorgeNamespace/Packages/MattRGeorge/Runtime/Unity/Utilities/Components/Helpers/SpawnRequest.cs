﻿using System;
using UnityEngine;

namespace MattRGeorge.Unity.Utilities.Components.Helpers
{
    /// <summary>
    /// A request to spawn an object that has information for when making the spawn as well as information for after the spawn is made and current status.
    /// </summary>
    public class SpawnRequest
    {
        public Action<SpawnRequest> OnSpawnCallback = null;
        public Action<SpawnRequest> OnPostSpawnCallback = null;
        public Action<SpawnRequest> OnCompletedCallback = null;

        public GameObject spawnedObj = null;
        public Coroutine calledCoroutine = null;
        public bool spawned = false;
        public bool postSpawned = false;
        public bool failed = false;
        public bool completed = false;
    }
}
