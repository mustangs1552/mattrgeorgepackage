﻿using NUnit.Framework;
using MattRGeorge.General.Utilities;

namespace MattRGeorge.Tests.General.Utilities
{
    public class SemanticVersionTests
    {
        SemanticVersion ver = new SemanticVersion();
        SemanticVersion ver2 = new SemanticVersion();
        string strVer = "";

        [Test]
        public void Instantiate_Tests()
        {
            // Direct values
            CheckValues(ver, 0, 0, 0, "", "");

            ver = new SemanticVersion(8, 2, 15, "preview", "beta");
            CheckValues(ver, 8, 2, 15, "preview", "beta");

            ver = new SemanticVersion(8, 2, 15, "preview");
            CheckValues(ver, 8, 2, 15, "preview", "");

            ver = new SemanticVersion(8, 2, 15, null, "beta");
            CheckValues(ver, 8, 2, 15, "", "beta");

            // From string
            ver = new SemanticVersion("8.2.15-preview+beta");
            CheckValues(ver, 8, 2, 15, "preview", "beta");

            ver = new SemanticVersion("8.2.15-preview");
            CheckValues(ver, 8, 2, 15, "preview", "");

            ver = new SemanticVersion("8.2.15+beta");
            CheckValues(ver, 8, 2, 15, "", "beta");

            ver = new SemanticVersion("8.2.15");
            CheckValues(ver, 8, 2, 15, "", "");
        }
        [Test]
        public void Instantiate_InvalidInput_Tests()
        {
            ver = new SemanticVersion(-8, -2, -15, null, null);
            CheckValues(ver, 0, 0, 0, "", "");

            ver = new SemanticVersion("-8.-2.-15-+");
            CheckValues(ver, 0, 0, 0, "", "");
        }

        [Test]
        public void ToString_Tests()
        {
            ver = new SemanticVersion("8.2.15-preview+beta");
            strVer = ver.ToString();
            Assert.IsNotNull(strVer);
            Assert.AreEqual("8.2.15-preview+beta", strVer);

            ver = new SemanticVersion("8.2.15-preview");
            strVer = ver.ToString();
            Assert.IsNotNull(strVer);
            Assert.AreEqual("8.2.15-preview", strVer);

            ver = new SemanticVersion("8.2.15+beta");
            strVer = ver.ToString();
            Assert.IsNotNull(strVer);
            Assert.AreEqual("8.2.15+beta", strVer);

            ver = new SemanticVersion("8.2.15");
            strVer = ver.ToString();
            Assert.IsNotNull(strVer);
            Assert.AreEqual("8.2.15", strVer);
        }
        [Test]
        public void ToString_InvalidInput_Tests()
        {
            ver = new SemanticVersion(-8, -2, -15, null, null);
            strVer = ver.ToString();
            Assert.IsNotNull(strVer);
            Assert.AreEqual("0.0.0", strVer);

            ver = new SemanticVersion("-8.-2.-15-+");
            strVer = ver.ToString();
            Assert.IsNotNull(strVer);
            Assert.AreEqual("0.0.0", strVer);
        }

        [Test]
        public void SameAsOp_Tests()
        {
            #region Are same as
            Assert.IsTrue(ver == ver2);

            ver = new SemanticVersion("8.2.15");
            ver2 = new SemanticVersion("8.2.15");
            Assert.IsTrue(ver == ver2);

            ver = new SemanticVersion("8.2.15-preview");
            ver2 = new SemanticVersion("8.2.15-preview");
            Assert.IsTrue(ver == ver2);

            ver = new SemanticVersion("8.2.15-beta");
            ver2 = new SemanticVersion("8.2.15-beta");
            Assert.IsTrue(ver == ver2);

            ver = new SemanticVersion("8.2.15-preview+beta");
            ver2 = new SemanticVersion("8.2.15-preview+beta");
            Assert.IsTrue(ver == ver2);
            #endregion

            #region Are not same as
            ver = new SemanticVersion("9.2.15-preview+beta");
            ver2 = new SemanticVersion("8.2.15-preview+beta");
            Assert.IsFalse(ver == ver2);

            ver = new SemanticVersion("8.3.15-preview+beta");
            ver2 = new SemanticVersion("8.2.15-preview+beta");
            Assert.IsFalse(ver == ver2);

            ver = new SemanticVersion("8.2.20-preview+beta");
            ver2 = new SemanticVersion("8.2.15-preview+beta");
            Assert.IsFalse(ver == ver2);

            ver = new SemanticVersion("8.2.15-preRelease+beta");
            ver2 = new SemanticVersion("8.2.15-preview+beta");
            Assert.IsFalse(ver == ver2);

            ver = new SemanticVersion("8.2.15-preview+alpha");
            ver2 = new SemanticVersion("8.2.15-preview+beta");
            Assert.IsFalse(ver == ver2);

            ver = new SemanticVersion("8.2.15+beta");
            ver2 = new SemanticVersion("8.2.15-preview+beta");
            Assert.IsFalse(ver == ver2);

            ver = new SemanticVersion("8.2.15-preview");
            ver2 = new SemanticVersion("8.2.15-preview+beta");
            Assert.IsFalse(ver == ver2);

            ver = new SemanticVersion("8.2.15");
            ver2 = new SemanticVersion("8.2.15-preview+beta");
            Assert.IsFalse(ver == ver2);

            ver = new SemanticVersion("9.2.15");
            ver2 = new SemanticVersion("8.2.15");
            Assert.IsFalse(ver == ver2);

            ver = new SemanticVersion("8.3.15");
            ver2 = new SemanticVersion("8.2.15");
            Assert.IsFalse(ver == ver2);

            ver = new SemanticVersion("8.2.20");
            ver2 = new SemanticVersion("8.2.15");
            Assert.IsFalse(ver == ver2);
            #endregion
        }
        [Test]
        public void NotSameAsOp_Tests()
        {
            // Are same as
            Assert.IsFalse(ver != ver2);

            ver = new SemanticVersion("8.2.15");
            ver2 = new SemanticVersion("8.2.15");
            Assert.IsFalse(ver != ver2);

            ver = new SemanticVersion("8.2.15-preview");
            ver2 = new SemanticVersion("8.2.15-preview");
            Assert.IsFalse(ver != ver2);

            ver = new SemanticVersion("8.2.15-beta");
            ver2 = new SemanticVersion("8.2.15-beta");
            Assert.IsFalse(ver != ver2);

            ver = new SemanticVersion("8.2.15-preview+beta");
            ver2 = new SemanticVersion("8.2.15-preview+beta");
            Assert.IsFalse(ver != ver2);

            // Are not same as
            ver = new SemanticVersion("9.2.15-preview+beta");
            ver2 = new SemanticVersion("8.2.15-preview+beta");
            Assert.IsTrue(ver != ver2);

            ver = new SemanticVersion("8.3.15-preview+beta");
            ver2 = new SemanticVersion("8.2.15-preview+beta");
            Assert.IsTrue(ver != ver2);

            ver = new SemanticVersion("8.2.20-preview+beta");
            ver2 = new SemanticVersion("8.2.15-preview+beta");
            Assert.IsTrue(ver != ver2);

            ver = new SemanticVersion("8.2.15-preRelease+beta");
            ver2 = new SemanticVersion("8.2.15-preview+beta");
            Assert.IsTrue(ver != ver2);

            ver = new SemanticVersion("8.2.15-preview+alpha");
            ver2 = new SemanticVersion("8.2.15-preview+beta");
            Assert.IsTrue(ver != ver2);

            ver = new SemanticVersion("8.2.15+beta");
            ver2 = new SemanticVersion("8.2.15-preview+beta");
            Assert.IsTrue(ver != ver2);

            ver = new SemanticVersion("8.2.15-preview");
            ver2 = new SemanticVersion("8.2.15-preview+beta");
            Assert.IsTrue(ver != ver2);

            ver = new SemanticVersion("8.2.15");
            ver2 = new SemanticVersion("8.2.15-preview+beta");
            Assert.IsTrue(ver != ver2);

            ver = new SemanticVersion("9.2.15");
            ver2 = new SemanticVersion("8.2.15");
            Assert.IsTrue(ver != ver2);

            ver = new SemanticVersion("8.3.15");
            ver2 = new SemanticVersion("8.2.15");
            Assert.IsTrue(ver != ver2);

            ver = new SemanticVersion("8.2.20");
            ver2 = new SemanticVersion("8.2.15");
            Assert.IsTrue(ver != ver2);
        }
        [Test]
        public void GreaterThanOp_Tests()
        {
            #region Is not the same
            Assert.IsFalse(ver > ver2);
            Assert.IsFalse(ver2 > ver);

            ver = new SemanticVersion("8.2.15");
            ver2 = new SemanticVersion("8.2.15");
            Assert.IsFalse(ver > ver2);
            Assert.IsFalse(ver2 > ver);

            ver = new SemanticVersion("8.2.15-preview");
            ver2 = new SemanticVersion("8.2.15-preview");
            Assert.IsFalse(ver > ver2);
            Assert.IsFalse(ver2 > ver);

            ver = new SemanticVersion("8.2.15-beta");
            ver2 = new SemanticVersion("8.2.15-beta");
            Assert.IsFalse(ver > ver2);
            Assert.IsFalse(ver2 > ver);

            ver = new SemanticVersion("8.2.15-preview+beta");
            ver2 = new SemanticVersion("8.2.15-preview+beta");
            Assert.IsFalse(ver > ver2);
            Assert.IsFalse(ver2 > ver);
            #endregion

            #region Is greater than
            ver = new SemanticVersion("9.2.15");
            ver2 = new SemanticVersion("8.2.15");
            Assert.IsTrue(ver > ver2);
            Assert.IsFalse(ver2 > ver);

            ver = new SemanticVersion("8.3.15");
            ver2 = new SemanticVersion("8.2.15");
            Assert.IsTrue(ver > ver2);
            Assert.IsFalse(ver2 > ver);

            ver = new SemanticVersion("8.2.16");
            ver2 = new SemanticVersion("8.2.15");
            Assert.IsTrue(ver > ver2);
            Assert.IsFalse(ver2 > ver);

            ver = new SemanticVersion("9.2.15-preview+beta");
            ver2 = new SemanticVersion("8.2.15-preview+beta");
            Assert.IsTrue(ver > ver2);
            Assert.IsFalse(ver2 > ver);

            ver = new SemanticVersion("9.2.15+beta");
            ver2 = new SemanticVersion("8.2.15+beta");
            Assert.IsTrue(ver > ver2);
            Assert.IsFalse(ver2 > ver);

            ver = new SemanticVersion("9.2.15-preview");
            ver2 = new SemanticVersion("8.2.15-preview");
            Assert.IsTrue(ver > ver2);
            Assert.IsFalse(ver2 > ver);

            ver = new SemanticVersion("8.3.15-preview+beta");
            ver2 = new SemanticVersion("8.2.15-preview+beta");
            Assert.IsTrue(ver > ver2);
            Assert.IsFalse(ver2 > ver);

            ver = new SemanticVersion("8.3.15+beta");
            ver2 = new SemanticVersion("8.2.15+beta");
            Assert.IsTrue(ver > ver2);
            Assert.IsFalse(ver2 > ver);

            ver = new SemanticVersion("8.3.15-preview");
            ver2 = new SemanticVersion("8.2.15-preview");
            Assert.IsTrue(ver > ver2);
            Assert.IsFalse(ver2 > ver);

            ver = new SemanticVersion("8.2.16-preview+beta");
            ver2 = new SemanticVersion("8.2.15-preview+beta");
            Assert.IsTrue(ver > ver2);
            Assert.IsFalse(ver2 > ver);

            ver = new SemanticVersion("8.2.16+beta");
            ver2 = new SemanticVersion("8.2.15+beta");
            Assert.IsTrue(ver > ver2);
            Assert.IsFalse(ver2 > ver);

            ver = new SemanticVersion("8.2.16-preview");
            ver2 = new SemanticVersion("8.2.15-preview");
            Assert.IsTrue(ver > ver2);
            Assert.IsFalse(ver2 > ver);
            #endregion

            #region Ignore pre-release and build
            ver = new SemanticVersion("8.2.15-preRelease+beta");
            ver2 = new SemanticVersion("8.2.15-preview+beta");
            Assert.IsFalse(ver > ver2);
            Assert.IsFalse(ver2 > ver);

            ver = new SemanticVersion("8.2.15-preview+alpha");
            ver2 = new SemanticVersion("8.2.15-preview+beta");
            Assert.IsFalse(ver > ver2);
            Assert.IsFalse(ver2 > ver);

            ver = new SemanticVersion("8.2.15+beta");
            ver2 = new SemanticVersion("8.2.15-preview+beta");
            Assert.IsFalse(ver > ver2);
            Assert.IsFalse(ver2 > ver);

            ver = new SemanticVersion("8.2.15-preview");
            ver2 = new SemanticVersion("8.2.15-preview+beta");
            Assert.IsFalse(ver > ver2);
            Assert.IsFalse(ver2 > ver);
            #endregion
        }
        [Test]
        public void LessThanOp_Tests()
        {
            #region Is not the same
            Assert.IsFalse(ver < ver2);
            Assert.IsFalse(ver2 < ver);

            ver = new SemanticVersion("8.2.15");
            ver2 = new SemanticVersion("8.2.15");
            Assert.IsFalse(ver < ver2);
            Assert.IsFalse(ver2 < ver);

            ver = new SemanticVersion("8.2.15-preview");
            ver2 = new SemanticVersion("8.2.15-preview");
            Assert.IsFalse(ver < ver2);
            Assert.IsFalse(ver2 < ver);

            ver = new SemanticVersion("8.2.15-beta");
            ver2 = new SemanticVersion("8.2.15-beta");
            Assert.IsFalse(ver < ver2);
            Assert.IsFalse(ver2 < ver);

            ver = new SemanticVersion("8.2.15-preview+beta");
            ver2 = new SemanticVersion("8.2.15-preview+beta");
            Assert.IsFalse(ver < ver2);
            Assert.IsFalse(ver2 < ver);
            #endregion

            #region Is greater than
            ver = new SemanticVersion("9.2.15");
            ver2 = new SemanticVersion("8.2.15");
            Assert.IsFalse(ver < ver2);
            Assert.IsTrue(ver2 < ver);

            ver = new SemanticVersion("8.3.15");
            ver2 = new SemanticVersion("8.2.15");
            Assert.IsFalse(ver < ver2);
            Assert.IsTrue(ver2 < ver);

            ver = new SemanticVersion("8.2.16");
            ver2 = new SemanticVersion("8.2.15");
            Assert.IsFalse(ver < ver2);
            Assert.IsTrue(ver2 < ver);

            ver = new SemanticVersion("9.2.15-preview+beta");
            ver2 = new SemanticVersion("8.2.15-preview+beta");
            Assert.IsFalse(ver < ver2);
            Assert.IsTrue(ver2 < ver);

            ver = new SemanticVersion("9.2.15+beta");
            ver2 = new SemanticVersion("8.2.15+beta");
            Assert.IsFalse(ver < ver2);
            Assert.IsTrue(ver2 < ver);

            ver = new SemanticVersion("9.2.15-preview");
            ver2 = new SemanticVersion("8.2.15-preview");
            Assert.IsFalse(ver < ver2);
            Assert.IsTrue(ver2 < ver);

            ver = new SemanticVersion("8.3.15-preview+beta");
            ver2 = new SemanticVersion("8.2.15-preview+beta");
            Assert.IsFalse(ver < ver2);
            Assert.IsTrue(ver2 < ver);

            ver = new SemanticVersion("8.3.15+beta");
            ver2 = new SemanticVersion("8.2.15+beta");
            Assert.IsFalse(ver < ver2);
            Assert.IsTrue(ver2 < ver);

            ver = new SemanticVersion("8.3.15-preview");
            ver2 = new SemanticVersion("8.2.15-preview");
            Assert.IsFalse(ver < ver2);
            Assert.IsTrue(ver2 < ver);

            ver = new SemanticVersion("8.2.16-preview+beta");
            ver2 = new SemanticVersion("8.2.15-preview+beta");
            Assert.IsFalse(ver < ver2);
            Assert.IsTrue(ver2 < ver);

            ver = new SemanticVersion("8.2.16+beta");
            ver2 = new SemanticVersion("8.2.15+beta");
            Assert.IsFalse(ver < ver2);
            Assert.IsTrue(ver2 < ver);

            ver = new SemanticVersion("8.2.16-preview");
            ver2 = new SemanticVersion("8.2.15-preview");
            Assert.IsFalse(ver < ver2);
            Assert.IsTrue(ver2 < ver);
            #endregion

            #region Ignore pre-release and build
            ver = new SemanticVersion("8.2.15-preRelease+beta");
            ver2 = new SemanticVersion("8.2.15-preview+beta");
            Assert.IsFalse(ver < ver2);
            Assert.IsFalse(ver2 < ver);

            ver = new SemanticVersion("8.2.15-preview+alpha");
            ver2 = new SemanticVersion("8.2.15-preview+beta");
            Assert.IsFalse(ver < ver2);
            Assert.IsFalse(ver2 < ver);

            ver = new SemanticVersion("8.2.15+beta");
            ver2 = new SemanticVersion("8.2.15-preview+beta");
            Assert.IsFalse(ver < ver2);
            Assert.IsFalse(ver2 < ver);

            ver = new SemanticVersion("8.2.15-preview");
            ver2 = new SemanticVersion("8.2.15-preview+beta");
            Assert.IsFalse(ver < ver2);
            Assert.IsFalse(ver2 < ver);
            #endregion
        }
        [Test]
        public void GreaterThanOrEqualOp_Tests()
        {
            #region Is the same
            Assert.IsTrue(ver >= ver2);
            Assert.IsTrue(ver2 >= ver);

            ver = new SemanticVersion("8.2.15");
            ver2 = new SemanticVersion("8.2.15");
            Assert.IsTrue(ver >= ver2);
            Assert.IsTrue(ver2 >= ver);

            ver = new SemanticVersion("8.2.15-preview");
            ver2 = new SemanticVersion("8.2.15-preview");
            Assert.IsTrue(ver >= ver2);
            Assert.IsTrue(ver2 >= ver);

            ver = new SemanticVersion("8.2.15-beta");
            ver2 = new SemanticVersion("8.2.15-beta");
            Assert.IsTrue(ver >= ver2);
            Assert.IsTrue(ver2 >= ver);

            ver = new SemanticVersion("8.2.15-preview+beta");
            ver2 = new SemanticVersion("8.2.15-preview+beta");
            Assert.IsTrue(ver >= ver2);
            Assert.IsTrue(ver2 >= ver);
            #endregion

            #region Is greater than
            ver = new SemanticVersion("9.2.15");
            ver2 = new SemanticVersion("8.2.15");
            Assert.IsTrue(ver >= ver2);
            Assert.IsFalse(ver2 >= ver);

            ver = new SemanticVersion("8.3.15");
            ver2 = new SemanticVersion("8.2.15");
            Assert.IsTrue(ver >= ver2);
            Assert.IsFalse(ver2 >= ver);

            ver = new SemanticVersion("8.2.16");
            ver2 = new SemanticVersion("8.2.15");
            Assert.IsTrue(ver >= ver2);
            Assert.IsFalse(ver2 >= ver);

            ver = new SemanticVersion("9.2.15-preview+beta");
            ver2 = new SemanticVersion("8.2.15-preview+beta");
            Assert.IsTrue(ver >= ver2);
            Assert.IsFalse(ver2 >= ver);

            ver = new SemanticVersion("9.2.15+beta");
            ver2 = new SemanticVersion("8.2.15+beta");
            Assert.IsTrue(ver >= ver2);
            Assert.IsFalse(ver2 >= ver);

            ver = new SemanticVersion("9.2.15-preview");
            ver2 = new SemanticVersion("8.2.15-preview");
            Assert.IsTrue(ver >= ver2);
            Assert.IsFalse(ver2 >= ver);

            ver = new SemanticVersion("8.3.15-preview+beta");
            ver2 = new SemanticVersion("8.2.15-preview+beta");
            Assert.IsTrue(ver >= ver2);
            Assert.IsFalse(ver2 >= ver);

            ver = new SemanticVersion("8.3.15+beta");
            ver2 = new SemanticVersion("8.2.15+beta");
            Assert.IsTrue(ver >= ver2);
            Assert.IsFalse(ver2 >= ver);

            ver = new SemanticVersion("8.3.15-preview");
            ver2 = new SemanticVersion("8.2.15-preview");
            Assert.IsTrue(ver >= ver2);
            Assert.IsFalse(ver2 >= ver);

            ver = new SemanticVersion("8.2.16-preview+beta");
            ver2 = new SemanticVersion("8.2.15-preview+beta");
            Assert.IsTrue(ver >= ver2);
            Assert.IsFalse(ver2 >= ver);

            ver = new SemanticVersion("8.2.16+beta");
            ver2 = new SemanticVersion("8.2.15+beta");
            Assert.IsTrue(ver >= ver2);
            Assert.IsFalse(ver2 >= ver);

            ver = new SemanticVersion("8.2.16-preview");
            ver2 = new SemanticVersion("8.2.15-preview");
            Assert.IsTrue(ver >= ver2);
            Assert.IsFalse(ver2 >= ver);
            #endregion

            #region Ignore pre-release and build
            ver = new SemanticVersion("8.2.15-preRelease+beta");
            ver2 = new SemanticVersion("8.2.15-preview+beta");
            Assert.IsFalse(ver >= ver2);
            Assert.IsFalse(ver2 >= ver);

            ver = new SemanticVersion("8.2.15-preview+alpha");
            ver2 = new SemanticVersion("8.2.15-preview+beta");
            Assert.IsFalse(ver >= ver2);
            Assert.IsFalse(ver2 >= ver);

            ver = new SemanticVersion("8.2.15+beta");
            ver2 = new SemanticVersion("8.2.15-preview+beta");
            Assert.IsFalse(ver >= ver2);
            Assert.IsFalse(ver2 >= ver);

            ver = new SemanticVersion("8.2.15-preview");
            ver2 = new SemanticVersion("8.2.15-preview+beta");
            Assert.IsFalse(ver >= ver2);
            Assert.IsFalse(ver2 >= ver);
            #endregion
        }
        [Test]
        public void LessThanOrEqualOp_Tests()
        {
            #region Is not the same
            Assert.IsTrue(ver <= ver2);
            Assert.IsTrue(ver2 <= ver);

            ver = new SemanticVersion("8.2.15");
            ver2 = new SemanticVersion("8.2.15");
            Assert.IsTrue(ver <= ver2);
            Assert.IsTrue(ver2 <= ver);

            ver = new SemanticVersion("8.2.15-preview");
            ver2 = new SemanticVersion("8.2.15-preview");
            Assert.IsTrue(ver <= ver2);
            Assert.IsTrue(ver2 <= ver);

            ver = new SemanticVersion("8.2.15-beta");
            ver2 = new SemanticVersion("8.2.15-beta");
            Assert.IsTrue(ver <= ver2);
            Assert.IsTrue(ver2 <= ver);

            ver = new SemanticVersion("8.2.15-preview+beta");
            ver2 = new SemanticVersion("8.2.15-preview+beta");
            Assert.IsTrue(ver <= ver2);
            Assert.IsTrue(ver2 <= ver);
            #endregion

            #region Is greater than
            ver = new SemanticVersion("9.2.15");
            ver2 = new SemanticVersion("8.2.15");
            Assert.IsFalse(ver <= ver2);
            Assert.IsTrue(ver2 <= ver);

            ver = new SemanticVersion("8.3.15");
            ver2 = new SemanticVersion("8.2.15");
            Assert.IsFalse(ver <= ver2);
            Assert.IsTrue(ver2 <= ver);

            ver = new SemanticVersion("8.2.16");
            ver2 = new SemanticVersion("8.2.15");
            Assert.IsFalse(ver <= ver2);
            Assert.IsTrue(ver2 <= ver);

            ver = new SemanticVersion("9.2.15-preview+beta");
            ver2 = new SemanticVersion("8.2.15-preview+beta");
            Assert.IsFalse(ver <= ver2);
            Assert.IsTrue(ver2 <= ver);

            ver = new SemanticVersion("9.2.15+beta");
            ver2 = new SemanticVersion("8.2.15+beta");
            Assert.IsFalse(ver <= ver2);
            Assert.IsTrue(ver2 <= ver);

            ver = new SemanticVersion("9.2.15-preview");
            ver2 = new SemanticVersion("8.2.15-preview");
            Assert.IsFalse(ver <= ver2);
            Assert.IsTrue(ver2 <= ver);

            ver = new SemanticVersion("8.3.15-preview+beta");
            ver2 = new SemanticVersion("8.2.15-preview+beta");
            Assert.IsFalse(ver <= ver2);
            Assert.IsTrue(ver2 <= ver);

            ver = new SemanticVersion("8.3.15+beta");
            ver2 = new SemanticVersion("8.2.15+beta");
            Assert.IsFalse(ver <= ver2);
            Assert.IsTrue(ver2 <= ver);

            ver = new SemanticVersion("8.3.15-preview");
            ver2 = new SemanticVersion("8.2.15-preview");
            Assert.IsFalse(ver <= ver2);
            Assert.IsTrue(ver2 <= ver);

            ver = new SemanticVersion("8.2.16-preview+beta");
            ver2 = new SemanticVersion("8.2.15-preview+beta");
            Assert.IsFalse(ver <= ver2);
            Assert.IsTrue(ver2 <= ver);

            ver = new SemanticVersion("8.2.16+beta");
            ver2 = new SemanticVersion("8.2.15+beta");
            Assert.IsFalse(ver <= ver2);
            Assert.IsTrue(ver2 <= ver);

            ver = new SemanticVersion("8.2.16-preview");
            ver2 = new SemanticVersion("8.2.15-preview");
            Assert.IsFalse(ver <= ver2);
            Assert.IsTrue(ver2 <= ver);
            #endregion

            #region Ignore pre-release and build
            ver = new SemanticVersion("8.2.15-preRelease+beta");
            ver2 = new SemanticVersion("8.2.15-preview+beta");
            Assert.IsFalse(ver <= ver2);
            Assert.IsFalse(ver2 <= ver);

            ver = new SemanticVersion("8.2.15-preview+alpha");
            ver2 = new SemanticVersion("8.2.15-preview+beta");
            Assert.IsFalse(ver <= ver2);
            Assert.IsFalse(ver2 <= ver);

            ver = new SemanticVersion("8.2.15+beta");
            ver2 = new SemanticVersion("8.2.15-preview+beta");
            Assert.IsFalse(ver <= ver2);
            Assert.IsFalse(ver2 <= ver);

            ver = new SemanticVersion("8.2.15-preview");
            ver2 = new SemanticVersion("8.2.15-preview+beta");
            Assert.IsFalse(ver <= ver2);
            Assert.IsFalse(ver2 <= ver);
            #endregion
        }

        [SetUp]
        public void Setup()
        {
            ver = new SemanticVersion();
            ver2 = new SemanticVersion();
            strVer = "";
        }

        private void CheckValues(SemanticVersion ver, int major, int minor, int patch, string preRelease, string build)
        {
            Assert.IsNotNull(ver);
            Assert.IsNotNull(ver.preRelease);
            Assert.IsNotNull(ver.build);

            Assert.AreEqual(major, ver.major);
            Assert.AreEqual(minor, ver.minor);
            Assert.AreEqual(patch, ver.patch);
            Assert.AreEqual(preRelease, ver.preRelease);
            Assert.AreEqual(build, ver.build);
        }
    }
}
