﻿using System;
using System.Collections.Generic;
using NUnit.Framework;
using MattRGeorge.General.Utilities.Static;

namespace MattRGeorge.Tests.General.Utilities.Static
{
    public class FileAccessUtilityTests
    {
        [Serializable]
        private class FileAccessUtilityTestClass
        {
            public int num = 2021;
            public float dec = -2.021f;
            public string text = "Hello World!";
            public char[] charArray = new char[3] { '1', '2', '3' };
            public List<char> charList = new List<char>() { 'a', 'b', 'c' };

            public FileAccessUtilityTestClass() { }
            public FileAccessUtilityTestClass(int num, float dec, string text, char[] charArray, List<char> charList)
            {
                this.num = num;
                this.dec = dec;
                this.text = text;
                this.charArray = charArray;
                this.charList = charList;
            }
        }

        private const string TEST_FILE_PATH = @"C:\Repos\Online\Tools\MattRGeorge\MattRGeorgeNamespace\Packages\MattRGeorge\Tests\General\Utilities\Static";
        private const string TEST_FILE = "fileAccessUtility_TestFile.txt";
        private FileAccessUtilityTestClass testClass = null;
        private FileAccessUtilityTestClass testClassLoaded = null;
        private bool success = false;

        [Test]
        public void DoesDirectoryOrFileExist_Tests()
        {
            Assert.IsTrue(FileAccessUtility.DoesDirectoryOrFileExist(TEST_FILE_PATH));
            Assert.IsTrue(FileAccessUtility.DoesDirectoryOrFileExist($@"{TEST_FILE_PATH}\FileAccessUtilityTests.cs"));

            Assert.IsFalse(FileAccessUtility.DoesDirectoryOrFileExist($"{TEST_FILE_PATH}WRONG"));
            Assert.IsFalse(FileAccessUtility.DoesDirectoryOrFileExist($@"{TEST_FILE_PATH}\FileAccessWRONGUtilityTests.cs"));
        }
        [Test]
        public void DoesDirectoryOrFileExist_InvalidInput_Tests()
        {
            Assert.IsFalse(FileAccessUtility.DoesDirectoryOrFileExist(null));
            Assert.IsFalse(FileAccessUtility.DoesDirectoryOrFileExist(""));
            Assert.IsFalse(FileAccessUtility.DoesDirectoryOrFileExist("I am a file path!"));
        }

        [Test]
        public void SaveToBinaryFile_LoadFromBinaryFile_Tests()
        {
            success = FileAccessUtility.SaveToBinaryFile(TEST_FILE_PATH, TEST_FILE, testClass);
            Assert.IsTrue(success);
            testClassLoaded = FileAccessUtility.LoadFromBinaryFile<FileAccessUtilityTestClass>(TEST_FILE_PATH, TEST_FILE);
            CheckTestClassValue(testClassLoaded, 2021, -2.021f, "Hello World!", new char[3] { '1', '2', '3'}, new List<char>() { 'a', 'b', 'c'});
        }
        [Test]
        public void SaveToBinaryFile_LoadFromBinaryFile_InvalidInput_Tests()
        {
            success = FileAccessUtility.SaveToBinaryFile("", TEST_FILE, testClass);
            Assert.IsFalse(success);
            success = FileAccessUtility.SaveToBinaryFile(null, TEST_FILE, testClass);
            Assert.IsFalse(success);

            success = FileAccessUtility.SaveToBinaryFile(TEST_FILE_PATH, "", testClass);
            Assert.IsFalse(success);
            success = FileAccessUtility.SaveToBinaryFile(TEST_FILE_PATH, null, testClass);
            Assert.IsFalse(success);

            success = FileAccessUtility.SaveToBinaryFile<FileAccessUtilityTestClass>(TEST_FILE_PATH, TEST_FILE, null);
            Assert.IsFalse(success);
        }

        [SetUp]
        public void Setup()
        {
            testClass = new FileAccessUtilityTestClass();
            testClassLoaded = null;
        }

        private void CheckTestClassValue(FileAccessUtilityTestClass testClass, int num, float dec, string text, char[] charArray, List<char> charList)
        {
            Assert.IsNotNull(testClass);
            Assert.AreEqual(num, testClass.num);
            Assert.AreEqual(dec, testClass.dec);
            Assert.AreEqual(text, testClass.text);
            Assert.IsNotNull(testClass.charArray);
            CollectionAssert.AreEqual(charArray, testClass.charArray);
            Assert.IsNotNull(testClass.charList);
            CollectionAssert.AreEqual(charList, testClass.charList);
        }
    }
}
