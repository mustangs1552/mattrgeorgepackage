﻿using NUnit.Framework;
using MattRGeorge.General.Utilities.Static;

namespace MattRGeorge.Tests.General.Utilities.Static
{
    public class MathUtilityTests
    {
        [Test]
        public void AreAlmostEqual_Tests()
        {
            Assert.IsFalse(MathUtility.AreAlmostEqual(3, 1, .5f));
            Assert.IsFalse(MathUtility.AreAlmostEqual(1.51f, 1, .5f));
            Assert.IsTrue(MathUtility.AreAlmostEqual(1.5f, 1, .5f));
            Assert.IsTrue(MathUtility.AreAlmostEqual(1, 1, .5f));
            Assert.IsTrue(MathUtility.AreAlmostEqual(.5f, 1, .5f));
            Assert.IsFalse(MathUtility.AreAlmostEqual(.49f, 1, .5f));
            Assert.IsFalse(MathUtility.AreAlmostEqual(-1, 1, .5f));
        }
    }
}
