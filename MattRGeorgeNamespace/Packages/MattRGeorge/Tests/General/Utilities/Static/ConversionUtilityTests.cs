﻿using System;
using NUnit.Framework;
using MattRGeorge.General.Utilities.Static;

namespace MattRGeorge.Tests.General.Utilities.Static
{
    public class ConversionUtilityTests
    {
        private Type type = null;
        bool result = false;
        object resultObj = null;

        [Test]
        public void TryConvertValue_Tests()
        {
            resultObj = ConversionUtility.TryConvertValue("0", out type);
            Assert.IsNotNull(resultObj);
            Assert.AreEqual(0, (byte)resultObj);
            Assert.AreEqual(typeof(byte), type);
            resultObj = ConversionUtility.TryConvertValue("255", out type);
            Assert.IsNotNull(resultObj);
            Assert.AreEqual(255, (byte)resultObj);
            Assert.AreEqual(typeof(byte), type);

            resultObj = ConversionUtility.TryConvertValue("-128", out type);
            Assert.IsNotNull(resultObj);
            Assert.AreEqual(-128, (sbyte)resultObj);
            Assert.AreEqual(typeof(sbyte), type);
            resultObj = ConversionUtility.TryConvertValue("-1", out type);
            Assert.IsNotNull(resultObj);
            Assert.AreEqual(-1, (sbyte)resultObj);
            Assert.AreEqual(typeof(sbyte), type);

            resultObj = ConversionUtility.TryConvertValue("-32768", out type);
            Assert.IsNotNull(resultObj);
            Assert.AreEqual(-32768, (short)resultObj);
            Assert.AreEqual(typeof(short), type);
            resultObj = ConversionUtility.TryConvertValue("-129", out type);
            Assert.IsNotNull(resultObj);
            Assert.AreEqual(-129, (short)resultObj);
            Assert.AreEqual(typeof(short), type);
            resultObj = ConversionUtility.TryConvertValue("256", out type);
            Assert.IsNotNull(resultObj);
            Assert.AreEqual(256, (short)resultObj);
            Assert.AreEqual(typeof(short), type);
            resultObj = ConversionUtility.TryConvertValue("32767", out type);
            Assert.IsNotNull(resultObj);
            Assert.AreEqual(32767, (short)resultObj);
            Assert.AreEqual(typeof(short), type);

            resultObj = ConversionUtility.TryConvertValue("32768", out type);
            Assert.IsNotNull(resultObj);
            Assert.AreEqual(32768, (ushort)resultObj);
            Assert.AreEqual(typeof(ushort), type);
            resultObj = ConversionUtility.TryConvertValue("65535", out type);
            Assert.IsNotNull(resultObj);
            Assert.AreEqual(65535, (ushort)resultObj);
            Assert.AreEqual(typeof(ushort), type);

            resultObj = ConversionUtility.TryConvertValue("-2147483648", out type);
            Assert.IsNotNull(resultObj);
            Assert.AreEqual(-2147483648, (int)resultObj);
            Assert.AreEqual(typeof(int), type);
            resultObj = ConversionUtility.TryConvertValue("-32769", out type);
            Assert.IsNotNull(resultObj);
            Assert.AreEqual(-32769, (int)resultObj);
            Assert.AreEqual(typeof(int), type);
            resultObj = ConversionUtility.TryConvertValue("65536", out type);
            Assert.IsNotNull(resultObj);
            Assert.AreEqual(65536, (int)resultObj);
            Assert.AreEqual(typeof(int), type);
            resultObj = ConversionUtility.TryConvertValue("2147483647", out type);
            Assert.IsNotNull(resultObj);
            Assert.AreEqual(2147483647, (int)resultObj);
            Assert.AreEqual(typeof(int), type);

            resultObj = ConversionUtility.TryConvertValue("2147483649", out type);
            Assert.IsNotNull(resultObj);
            Assert.AreEqual(2147483649, (uint)resultObj);
            Assert.AreEqual(typeof(uint), type);
            resultObj = ConversionUtility.TryConvertValue("4294967295", out type);
            Assert.IsNotNull(resultObj);
            Assert.AreEqual(4294967295, (uint)resultObj);
            Assert.AreEqual(typeof(uint), type);

            resultObj = ConversionUtility.TryConvertValue("-9223372036854775808", out type);
            Assert.IsNotNull(resultObj);
            Assert.AreEqual(-9223372036854775808, (long)resultObj);
            Assert.AreEqual(typeof(long), type);
            resultObj = ConversionUtility.TryConvertValue("-2147483649", out type);
            Assert.IsNotNull(resultObj);
            Assert.AreEqual(-2147483649, (long)resultObj);
            Assert.AreEqual(typeof(long), type);
            resultObj = ConversionUtility.TryConvertValue("4294967296", out type);
            Assert.IsNotNull(resultObj);
            Assert.AreEqual(4294967296, (long)resultObj);
            Assert.AreEqual(typeof(long), type);
            resultObj = ConversionUtility.TryConvertValue("9223372036854775807", out type);
            Assert.IsNotNull(resultObj);
            Assert.AreEqual(9223372036854775807, (long)resultObj);
            Assert.AreEqual(typeof(long), type);

            resultObj = ConversionUtility.TryConvertValue("9223372036854775808", out type);
            Assert.IsNotNull(resultObj);
            Assert.AreEqual(9223372036854775808, (ulong)resultObj);
            Assert.AreEqual(typeof(ulong), type);
            resultObj = ConversionUtility.TryConvertValue("18446744073709551615", out type);
            Assert.IsNotNull(resultObj);
            Assert.AreEqual(18446744073709551615, (ulong)resultObj);
            Assert.AreEqual(typeof(ulong), type);

            resultObj = ConversionUtility.TryConvertValue("-3.402823e38", out type);
            Assert.IsNotNull(resultObj);
            Assert.AreEqual(-3.402823e38f, (float)resultObj);
            Assert.AreEqual(typeof(float), type);
            resultObj = ConversionUtility.TryConvertValue("-.000000003402823", out type);
            Assert.IsNotNull(resultObj);
            Assert.AreEqual(-.000000003402823f, (float)resultObj);
            Assert.AreEqual(typeof(float), type);
            resultObj = ConversionUtility.TryConvertValue("-0.000000003402823", out type);
            Assert.IsNotNull(resultObj);
            Assert.AreEqual(-0.000000003402823f, (float)resultObj);
            Assert.AreEqual(typeof(float), type);
            resultObj = ConversionUtility.TryConvertValue(".000000003402823", out type);
            Assert.IsNotNull(resultObj);
            Assert.AreEqual(.000000003402823f, (float)resultObj);
            Assert.AreEqual(typeof(float), type);
            resultObj = ConversionUtility.TryConvertValue("0.000000003402823", out type);
            Assert.IsNotNull(resultObj);
            Assert.AreEqual(0.000000003402823f, (float)resultObj);
            Assert.AreEqual(typeof(float), type);
            resultObj = ConversionUtility.TryConvertValue("3.402823e38", out type);
            Assert.IsNotNull(resultObj);
            Assert.AreEqual(3.402823e38f, (float)resultObj);
            Assert.AreEqual(typeof(float), type);

            // Had issues when testing double's max values, had to go one value less.
            resultObj = ConversionUtility.TryConvertValue("-1.79769313486231e308", out type);
            Assert.IsNotNull(resultObj);
            Assert.AreEqual(-1.79769313486231e308d, (double)resultObj);
            Assert.AreEqual(typeof(double), type);
            resultObj = ConversionUtility.TryConvertValue("-3.402824e38", out type);
            Assert.IsNotNull(resultObj);
            Assert.AreEqual(-3.402824e38d, (double)resultObj);
            Assert.AreEqual(typeof(double), type);
            resultObj = ConversionUtility.TryConvertValue("3.402824e38", out type);
            Assert.IsNotNull(resultObj);
            Assert.AreEqual(3.402824e38d, (double)resultObj);
            Assert.AreEqual(typeof(double), type);
            resultObj = ConversionUtility.TryConvertValue("1.79769313486231e308", out type);
            Assert.IsNotNull(resultObj);
            Assert.AreEqual(1.79769313486231e308d, (double)resultObj);
            Assert.AreEqual(typeof(double), type);

            // Had issues testing more than double

            resultObj = ConversionUtility.TryConvertValue("True", out type);
            Assert.IsNotNull(resultObj);
            Assert.AreEqual(true, (bool)resultObj);
            Assert.AreEqual(typeof(bool), type);
            resultObj = ConversionUtility.TryConvertValue("true", out type);
            Assert.IsNotNull(resultObj);
            Assert.AreEqual(true, (bool)resultObj);
            Assert.AreEqual(typeof(bool), type);
            resultObj = ConversionUtility.TryConvertValue("TRUE", out type);
            Assert.IsNotNull(resultObj);
            Assert.AreEqual(true, (bool)resultObj);
            Assert.AreEqual(typeof(bool), type);
            resultObj = ConversionUtility.TryConvertValue("False", out type);
            Assert.IsNotNull(resultObj);
            Assert.AreEqual(false, (bool)resultObj);
            Assert.AreEqual(typeof(bool), type);
            resultObj = ConversionUtility.TryConvertValue("false", out type);
            Assert.IsNotNull(resultObj);
            Assert.AreEqual(false, (bool)resultObj);
            Assert.AreEqual(typeof(bool), type);
            resultObj = ConversionUtility.TryConvertValue("FALSE", out type);
            Assert.IsNotNull(resultObj);
            Assert.AreEqual(false, (bool)resultObj);
            Assert.AreEqual(typeof(bool), type);
        }
        [Test]
        public void TryConvertValue_InvalidInput_Tests()
        {
            resultObj = ConversionUtility.TryConvertValue("", out type);
            Assert.IsNotNull(resultObj);
            Assert.AreEqual("", (string)resultObj);
            Assert.IsNull(type);
            resultObj = ConversionUtility.TryConvertValue(null, out type);
            Assert.IsNull(resultObj);
            Assert.IsNull(type);

            resultObj = ConversionUtility.TryConvertValue("int", out type);
            Assert.IsNotNull(resultObj);
            Assert.AreEqual("int", (string)resultObj);
            Assert.AreEqual(typeof(string), type);
            resultObj = ConversionUtility.TryConvertValue("1two3four", out type);
            Assert.IsNotNull(resultObj);
            Assert.AreEqual("1two3four", (string)resultObj);
            Assert.AreEqual(typeof(string), type);
            resultObj = ConversionUtility.TryConvertValue("Ol23456", out type);
            Assert.IsNotNull(resultObj);
            Assert.AreEqual("Ol23456", (string)resultObj);
            Assert.AreEqual(typeof(string), type);
            resultObj = ConversionUtility.TryConvertValue("2.356.567", out type);
            Assert.IsNotNull(resultObj);
            Assert.AreEqual("2.356.567", (string)resultObj);
            Assert.AreEqual(typeof(string), type);
        }

        [Test]
        public void CanConvertToPrimitiveType_Tests()
        {
            result = ConversionUtility.CanConvertToPrimitiveType("0", out type);
            Assert.IsTrue(result);
            Assert.AreEqual(typeof(byte), type);
            result = ConversionUtility.CanConvertToPrimitiveType("255", out type);
            Assert.IsTrue(result);
            Assert.AreEqual(typeof(byte), type);

            result = ConversionUtility.CanConvertToPrimitiveType("-128", out type);
            Assert.IsTrue(result);
            Assert.AreEqual(typeof(sbyte), type);
            result = ConversionUtility.CanConvertToPrimitiveType("-1", out type);
            Assert.IsTrue(result);
            Assert.AreEqual(typeof(sbyte), type);

            result = ConversionUtility.CanConvertToPrimitiveType("-32768", out type);
            Assert.IsTrue(result);
            Assert.AreEqual(typeof(short), type);
            result = ConversionUtility.CanConvertToPrimitiveType("-129", out type);
            Assert.IsTrue(result);
            Assert.AreEqual(typeof(short), type);
            result = ConversionUtility.CanConvertToPrimitiveType("256", out type);
            Assert.IsTrue(result);
            Assert.AreEqual(typeof(short), type);
            result = ConversionUtility.CanConvertToPrimitiveType("32767", out type);
            Assert.IsTrue(result);
            Assert.AreEqual(typeof(short), type);

            result = ConversionUtility.CanConvertToPrimitiveType("32768", out type);
            Assert.IsTrue(result);
            Assert.AreEqual(typeof(ushort), type);
            result = ConversionUtility.CanConvertToPrimitiveType("65535", out type);
            Assert.IsTrue(result);
            Assert.AreEqual(typeof(ushort), type);

            result = ConversionUtility.CanConvertToPrimitiveType("-2147483648", out type);
            Assert.IsTrue(result);
            Assert.AreEqual(typeof(int), type);
            result = ConversionUtility.CanConvertToPrimitiveType("-32769", out type);
            Assert.IsTrue(result);
            Assert.AreEqual(typeof(int), type);
            result = ConversionUtility.CanConvertToPrimitiveType("65536", out type);
            Assert.IsTrue(result);
            Assert.AreEqual(typeof(int), type);
            result = ConversionUtility.CanConvertToPrimitiveType("2147483647", out type);
            Assert.IsTrue(result);
            Assert.AreEqual(typeof(int), type);

            result = ConversionUtility.CanConvertToPrimitiveType("2147483649", out type);
            Assert.IsTrue(result);
            Assert.AreEqual(typeof(uint), type);
            result = ConversionUtility.CanConvertToPrimitiveType("4294967295", out type);
            Assert.IsTrue(result);
            Assert.AreEqual(typeof(uint), type);

            result = ConversionUtility.CanConvertToPrimitiveType("-9223372036854775808", out type);
            Assert.IsTrue(result);
            Assert.AreEqual(typeof(long), type);
            result = ConversionUtility.CanConvertToPrimitiveType("-2147483649", out type);
            Assert.IsTrue(result);
            Assert.AreEqual(typeof(long), type);
            result = ConversionUtility.CanConvertToPrimitiveType("4294967296", out type);
            Assert.IsTrue(result);
            Assert.AreEqual(typeof(long), type);
            result = ConversionUtility.CanConvertToPrimitiveType("9223372036854775807", out type);
            Assert.IsTrue(result);
            Assert.AreEqual(typeof(long), type);

            result = ConversionUtility.CanConvertToPrimitiveType("9223372036854775808", out type);
            Assert.IsTrue(result);
            Assert.AreEqual(typeof(ulong), type);
            result = ConversionUtility.CanConvertToPrimitiveType("18446744073709551615", out type);
            Assert.IsTrue(result);
            Assert.AreEqual(typeof(ulong), type);

            result = ConversionUtility.CanConvertToPrimitiveType("-3.402823e38", out type);
            Assert.IsTrue(result);
            Assert.AreEqual(typeof(float), type);
            result = ConversionUtility.CanConvertToPrimitiveType("-.000000003402823", out type);
            Assert.IsTrue(result);
            Assert.AreEqual(typeof(float), type);
            result = ConversionUtility.CanConvertToPrimitiveType("-0.000000003402823", out type);
            Assert.IsTrue(result);
            Assert.AreEqual(typeof(float), type);
            result = ConversionUtility.CanConvertToPrimitiveType(".000000003402823", out type);
            Assert.IsTrue(result);
            Assert.AreEqual(typeof(float), type);
            result = ConversionUtility.CanConvertToPrimitiveType("0.000000003402823", out type);
            Assert.IsTrue(result);
            Assert.AreEqual(typeof(float), type);
            result = ConversionUtility.CanConvertToPrimitiveType("3.402823e38", out type);
            Assert.IsTrue(result);
            Assert.AreEqual(typeof(float), type);

            // Had issues when testing double's max values, had to go one value less.
            result = ConversionUtility.CanConvertToPrimitiveType("-1.79769313486231e308", out type);
            Assert.IsTrue(result);
            Assert.AreEqual(typeof(double), type);
            result = ConversionUtility.CanConvertToPrimitiveType("-3.402824e38", out type);
            Assert.IsTrue(result);
            Assert.AreEqual(typeof(double), type);
            result = ConversionUtility.CanConvertToPrimitiveType("3.402824e38", out type);
            Assert.IsTrue(result);
            Assert.AreEqual(typeof(double), type);
            result = ConversionUtility.CanConvertToPrimitiveType("1.79769313486231e308", out type);
            Assert.IsTrue(result);
            Assert.AreEqual(typeof(double), type);

            // Had issues testing more than double

            result = ConversionUtility.CanConvertToPrimitiveType("True", out type);
            Assert.IsTrue(result);
            Assert.AreEqual(typeof(bool), type);
            result = ConversionUtility.CanConvertToPrimitiveType("true", out type);
            Assert.IsTrue(result);
            Assert.AreEqual(typeof(bool), type);
            result = ConversionUtility.CanConvertToPrimitiveType("TRUE", out type);
            Assert.IsTrue(result);
            Assert.AreEqual(typeof(bool), type);
            result = ConversionUtility.CanConvertToPrimitiveType("False", out type);
            Assert.IsTrue(result);
            Assert.AreEqual(typeof(bool), type);
            result = ConversionUtility.CanConvertToPrimitiveType("false", out type);
            Assert.IsTrue(result);
            Assert.AreEqual(typeof(bool), type);
            result = ConversionUtility.CanConvertToPrimitiveType("FALSE", out type);
            Assert.IsTrue(result);
            Assert.AreEqual(typeof(bool), type);
        }
        [Test]
        public void CanConvertToPrimitiveType_InvalidInput_Tests()
        {
            result = ConversionUtility.CanConvertToPrimitiveType("", out type);
            Assert.IsFalse(result);
            Assert.IsNull(type);
            result = ConversionUtility.CanConvertToPrimitiveType(null, out type);
            Assert.IsFalse(result);
            Assert.IsNull(type);

            result = ConversionUtility.CanConvertToPrimitiveType("int", out type);
            Assert.IsFalse(result);
            Assert.AreEqual(typeof(string), type);
            result = ConversionUtility.CanConvertToPrimitiveType("1two3four", out type);
            Assert.IsFalse(result);
            Assert.AreEqual(typeof(string), type);
            result = ConversionUtility.CanConvertToPrimitiveType("Ol23456", out type);
            Assert.IsFalse(result);
            Assert.AreEqual(typeof(string), type);
            result = ConversionUtility.CanConvertToPrimitiveType("2.356.567", out type);
            Assert.IsFalse(result);
            Assert.AreEqual(typeof(string), type);
        }
    }
}
