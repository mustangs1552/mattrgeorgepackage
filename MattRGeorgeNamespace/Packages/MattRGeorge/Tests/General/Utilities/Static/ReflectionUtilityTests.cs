﻿using NUnit.Framework;
using MattRGeorge.General.Utilities.Static;

namespace MattRGeorge.Tests.General.Utilities.Static
{
    public class ReflectionUtilityTests
    {
        private class TestClass
        {
            private int num = 5;
            private string str = "test";

            public int Num { get { return num; } }
            public string Str { get { return str; } }
        }
        TestClass testClass = new TestClass();

        [Test]
        public void GetInstanceField_Tests()
        {
            Assert.AreEqual(5, ReflectionUtility.GetInstanceField(typeof(TestClass), testClass, "num"));
            Assert.AreEqual("test", ReflectionUtility.GetInstanceField(typeof(TestClass), testClass, "str"));
        }
        [Test]
        public void GetInstanceField_InvalidInput_Tests()
        {
            Assert.IsNull(ReflectionUtility.GetInstanceField(null, testClass, "num"));
            Assert.IsNull(ReflectionUtility.GetInstanceField(null, testClass, "num"));
            Assert.IsNull(ReflectionUtility.GetInstanceField(typeof(TestClass), null, "num"));
            Assert.IsNull(ReflectionUtility.GetInstanceField(typeof(TestClass), testClass, ""));
            Assert.IsNull(ReflectionUtility.GetInstanceField(typeof(TestClass), testClass, null));
        }

        [Test]
        public void SetInstanceField_Tests()
        {
            ReflectionUtility.SetInstanceField(typeof(TestClass), testClass, "num", 10);
            Assert.AreEqual(10, testClass.Num);
            ReflectionUtility.SetInstanceField(typeof(TestClass), testClass, "num", -5);
            Assert.AreEqual(-5, testClass.Num);

            ReflectionUtility.SetInstanceField(typeof(TestClass), testClass, "str", "changed text");
            Assert.AreEqual("changed text", testClass.Str);
            ReflectionUtility.SetInstanceField(typeof(TestClass), testClass, "str", "changed again");
            Assert.AreEqual("changed again", testClass.Str);
            ReflectionUtility.SetInstanceField(typeof(TestClass), testClass, "str", "");
            Assert.AreEqual("", testClass.Str);
            ReflectionUtility.SetInstanceField(typeof(TestClass), testClass, "str", null);
            Assert.AreEqual(null, testClass.Str);
        }

        [SetUp]
        public void Setup()
        {
            testClass = new TestClass();
        }
    }
}
