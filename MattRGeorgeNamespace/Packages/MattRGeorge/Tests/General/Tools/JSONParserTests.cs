﻿using System.Collections.Generic;
using NUnit.Framework;
using MattRGeorge.General.Tools;

namespace MattRGeorge.Tests.General.Tools
{
    public class JSONParserTests
    {
        [Test]
        public void FirstLevelSimple_Test()
        {
            string json = "{ \"first_name\" : \"Sammy\", \"last_name\": \"Shark\",  \"online\" : true }";

            #region Using JSONObject
            JSONObject result = JSONParser.ParseJSON(json);
            CheckObjectNullValues(result);

            Assert.AreEqual(3, result.values.Count);
            Assert.IsTrue(result.values.ContainsKey("first_name"));
            Assert.AreEqual("Sammy", result.values["first_name"]);
            Assert.AreEqual("Shark", result.values["last_name"]);
            Assert.AreEqual("true", result.values["online"]);

            Assert.AreEqual(0, result.jsonObjects.Count);

            Assert.AreEqual(0, result.objArrays.Count);
            #endregion

            #region Using Mapping
            FirstLevelSimple obj = JSONParser.ParseJSON(json, FirstLevelSimplerMapper);
            Assert.IsNotNull(obj);
            Assert.AreEqual("Sammy", obj.first_name);
            Assert.AreEqual("Shark", obj.last_name);
            Assert.AreEqual(true, obj.online);
            #endregion
        }
        [Test]
        public void FirstLevelSimpleToJSON_Test()
        {
            string json = "{ \"first_name\" : \"Sammy\", \"last_name\": \"Shark\",  \"online\" : true }";
            CheckToJSON(json);
        }
        [Test]
        public void FirstLevelManyObjects_Test()
        {
            string json = "{\"sammy\" : { \"username\"  : \"SammyShark\", \"location\"  : \"Indian Ocean\", \"online\"    : true, \"followers\" : 987 }, \"jesse\" : { \"username\"  : \"JesseOctopus\",    \"location\"  : \"Pacific Ocean\",    \"online\"    : false,    \"followers\" : 432  },\"drew\" : { \"username\"  : \"DrewSquid\", \"location\"  : \"Atlantic Ocean\", \"online\"    : false, \"followers\" : 321 },\"jamie\" : { \"username\"  : \"JamieMantisShrimp\", \"location\"  : \"Pacific Ocean\", \"online\"    : true, \"followers\" : 654 } }";

            #region Using JSONObject
            JSONObject result = JSONParser.ParseJSON(json);
            CheckObjectNullValues(result);

            Assert.AreEqual(0, result.values.Count);

            Assert.AreEqual(4, result.jsonObjects.Count);
            Assert.IsTrue(result.jsonObjects.ContainsKey("sammy"));
            Assert.IsTrue(result.jsonObjects["sammy"].values.ContainsKey("username"));
            Assert.AreEqual("SammyShark", result.jsonObjects["sammy"].values["username"]);
            Assert.IsTrue(result.jsonObjects["sammy"].values.ContainsKey("location"));
            Assert.AreEqual("Indian Ocean", result.jsonObjects["sammy"].values["location"]);
            Assert.IsTrue(result.jsonObjects["sammy"].values.ContainsKey("online"));
            Assert.AreEqual("true", result.jsonObjects["sammy"].values["online"]);
            Assert.IsTrue(result.jsonObjects["sammy"].values.ContainsKey("followers"));
            Assert.AreEqual("987", result.jsonObjects["sammy"].values["followers"]);

            Assert.IsTrue(result.jsonObjects.ContainsKey("jesse"));
            Assert.IsTrue(result.jsonObjects["jesse"].values.ContainsKey("username"));
            Assert.AreEqual("JesseOctopus", result.jsonObjects["jesse"].values["username"]);
            Assert.IsTrue(result.jsonObjects["jesse"].values.ContainsKey("location"));
            Assert.AreEqual("Pacific Ocean", result.jsonObjects["jesse"].values["location"]);
            Assert.IsTrue(result.jsonObjects["jesse"].values.ContainsKey("online"));
            Assert.AreEqual("false", result.jsonObjects["jesse"].values["online"]);
            Assert.IsTrue(result.jsonObjects["jesse"].values.ContainsKey("followers"));
            Assert.AreEqual("432", result.jsonObjects["jesse"].values["followers"]);

            Assert.IsTrue(result.jsonObjects.ContainsKey("drew"));
            Assert.IsTrue(result.jsonObjects["drew"].values.ContainsKey("username"));
            Assert.AreEqual("DrewSquid", result.jsonObjects["drew"].values["username"]);
            Assert.IsTrue(result.jsonObjects["drew"].values.ContainsKey("location"));
            Assert.AreEqual("Atlantic Ocean", result.jsonObjects["drew"].values["location"]);
            Assert.IsTrue(result.jsonObjects["drew"].values.ContainsKey("online"));
            Assert.AreEqual("false", result.jsonObjects["drew"].values["online"]);
            Assert.IsTrue(result.jsonObjects["drew"].values.ContainsKey("followers"));
            Assert.AreEqual("321", result.jsonObjects["drew"].values["followers"]);

            Assert.IsTrue(result.jsonObjects.ContainsKey("jamie"));
            Assert.IsTrue(result.jsonObjects["jamie"].values.ContainsKey("username"));
            Assert.AreEqual("JamieMantisShrimp", result.jsonObjects["jamie"].values["username"]);
            Assert.IsTrue(result.jsonObjects["jamie"].values.ContainsKey("location"));
            Assert.AreEqual("Pacific Ocean", result.jsonObjects["jamie"].values["location"]);
            Assert.IsTrue(result.jsonObjects["jamie"].values.ContainsKey("online"));
            Assert.AreEqual("true", result.jsonObjects["jamie"].values["online"]);
            Assert.IsTrue(result.jsonObjects["jamie"].values.ContainsKey("followers"));
            Assert.AreEqual("654", result.jsonObjects["jamie"].values["followers"]);

            Assert.AreEqual(0, result.objArrays.Count);
            #endregion

            #region Using Mapping
            Dictionary<string, FirstLevelManyObjects> objs = JSONParser.ParseJSON(json, FirstLevelManyObjectsMapper);
            Assert.IsNotNull(objs);
            Assert.AreEqual(4, objs.Count);

            Assert.IsTrue(objs.ContainsKey("sammy"));
            Assert.AreEqual("SammyShark", objs["sammy"].username);
            Assert.AreEqual("Indian Ocean", objs["sammy"].location);
            Assert.AreEqual(true, objs["sammy"].online);
            Assert.AreEqual(987, objs["sammy"].followers);

            Assert.IsTrue(objs.ContainsKey("jesse"));
            Assert.AreEqual("JesseOctopus", objs["jesse"].username);
            Assert.AreEqual("Pacific Ocean", objs["jesse"].location);
            Assert.AreEqual(false, objs["jesse"].online);
            Assert.AreEqual(432, objs["jesse"].followers);

            Assert.IsTrue(objs.ContainsKey("drew"));
            Assert.AreEqual("DrewSquid", objs["drew"].username);
            Assert.AreEqual("Atlantic Ocean", objs["drew"].location);
            Assert.AreEqual(false, objs["drew"].online);
            Assert.AreEqual(321, objs["drew"].followers);

            Assert.IsTrue(objs.ContainsKey("jamie"));
            Assert.AreEqual("JamieMantisShrimp", objs["jamie"].username);
            Assert.AreEqual("Pacific Ocean", objs["jamie"].location);
            Assert.AreEqual(true, objs["jamie"].online);
            Assert.AreEqual(654, objs["jamie"].followers);
            #endregion
        }
        [Test]
        public void FirstLevelManyObjectsToJSON_Test()
        {
            string json = "{\"sammy\" : { \"username\"  : \"SammyShark\", \"location\"  : \"Indian Ocean\", \"online\"    : true, \"followers\" : 987 }, \"jesse\" : { \"username\"  : \"JesseOctopus\",    \"location\"  : \"Pacific Ocean\",    \"online\"    : false,    \"followers\" : 432  },\"drew\" : { \"username\"  : \"DrewSquid\", \"location\"  : \"Atlantic Ocean\", \"online\"    : false, \"followers\" : 321 },\"jamie\" : { \"username\"  : \"JamieMantisShrimp\", \"location\"  : \"Pacific Ocean\", \"online\"    : true, \"followers\" : 654 } }";
            CheckToJSON(json);
        }
        [Test]
        public void FirstLevelArray_Test()
        {
            string json = "{[  { \"description\" : \"work\", \"URL\" : \"https://www.digitalocean.com/\" }, { \"description\" : \"tutorials\", \"URL\" : \"https://www.digitalocean.com/community/tutorials\" } ]}";

            #region Using JSONObject
            JSONObject result = JSONParser.ParseJSON(json);
            CheckObjectNullValues(result);

            Assert.AreEqual(0, result.values.Count);

            Assert.AreEqual(0, result.jsonObjects.Count);

            Assert.AreEqual(1, result.objArrays.Count);
            Assert.IsTrue(result.objArrays.ContainsKey("ROOT_ARRAY"));
            Assert.AreEqual(2, result.objArrays["ROOT_ARRAY"].Count);
            Assert.AreEqual("work", result.objArrays["ROOT_ARRAY"][0].values["description"]);
            Assert.AreEqual("https://www.digitalocean.com/", result.objArrays["ROOT_ARRAY"][0].values["URL"]);
            Assert.AreEqual("tutorials", result.objArrays["ROOT_ARRAY"][1].values["description"]);
            Assert.AreEqual("https://www.digitalocean.com/community/tutorials", result.objArrays["ROOT_ARRAY"][1].values["URL"]);
            #endregion

            #region Using Mapping
            List<FirstLevelArray> objs = JSONParser.ParseJSON(json, FirstLevelArrayMapper);
            Assert.IsNotNull(objs);
            Assert.AreEqual(2, objs.Count);

            Assert.AreEqual("work", objs[0].description);
            Assert.AreEqual("https://www.digitalocean.com/", objs[0].url);

            Assert.AreEqual("tutorials", objs[1].description);
            Assert.AreEqual("https://www.digitalocean.com/community/tutorials", objs[1].url);
            #endregion
        }
        [Test]
        public void FirstLevelArrayToJSON_Test()
        {
            string json = "{[  { \"description\" : \"work\", \"URL\" : \"https://www.digitalocean.com/\" }, { \"description\" : \"tutorials\", \"URL\" : \"https://www.digitalocean.com/community/tutorials\" } ]}";
            CheckToJSON(json);
        }

        [Test]
        public void ComplexObject_Test()
        {
            string json = "{ \"id\": \"0001\", \"type\": \"donut\", \"name\": \"Cake\", \"ppu\": 0.55, \"batters\": { \"batter\": [ { \"id\": \"1001\", \"type\": \"Regular\" }, { \"id\": \"1002\", \"type\": \"Chocolate\" }, { \"id\": \"1003\", \"type\": \"Blueberry\" }, { \"id\": \"1004\", \"type\": \"Devil's Food\" } ] }, \"topping\": [ { \"id\": \"5001\", \"type\": \"None\" }, { \"id\": \"5002\", \"type\": \"Glazed\" }, { \"id\": \"5005\", \"type\": \"Sugar\" }, { \"id\": \"5007\", \"type\": \"Powdered Sugar\" }, { \"id\": \"5006\", \"type\": \"Chocolate with Sprinkles\" }, { \"id\": \"5003\", \"type\": \"Chocolate\" }, { \"id\": \"5004\", \"type\": \"Maple\" } ] }";

            #region Using JSONObject
            JSONObject result = JSONParser.ParseJSON(json);
            CheckObjectNullValues(result);

            Assert.AreEqual(4, result.values.Count);

            Assert.AreEqual(1, result.jsonObjects.Count);
            Assert.IsTrue(result.jsonObjects.ContainsKey("batters"));
            Assert.IsTrue(result.jsonObjects["batters"].objArrays.ContainsKey("batter"));
            Assert.AreEqual(4, result.jsonObjects["batters"].objArrays["batter"].Count);

            Assert.AreEqual(1, result.objArrays.Count);
            Assert.IsFalse(result.objArrays.ContainsKey("ROOT_ARRAY"));
            Assert.IsTrue(result.objArrays.ContainsKey("topping"));
            Assert.AreEqual(7, result.objArrays["topping"].Count);
            #endregion

            #region Using Mapping
            ComplexObject obj = JSONParser.ParseJSON(json, ComplexObjectMapper);
            Assert.IsNotNull(obj);

            Assert.AreEqual(0001, obj.id);
            Assert.AreEqual("donut", obj.type);
            Assert.AreEqual("Cake", obj.name);
            Assert.AreEqual(.55, obj.ppu);

            Assert.IsNotNull(obj.batters);
            Assert.AreEqual(4, obj.batters.Count);
            Assert.AreEqual(1001, obj.batters[0].id);
            Assert.AreEqual("Regular", obj.batters[0].type);
            Assert.AreEqual(1002, obj.batters[1].id);
            Assert.AreEqual("Chocolate", obj.batters[1].type);
            Assert.AreEqual(1003, obj.batters[2].id);
            Assert.AreEqual("Blueberry", obj.batters[2].type);
            Assert.AreEqual(1004, obj.batters[3].id);
            Assert.AreEqual("Devil's Food", obj.batters[3].type);

            Assert.IsNotNull(obj.topping);
            Assert.AreEqual(7, obj.topping.Count);
            Assert.AreEqual(5001, obj.topping[0].id);
            Assert.AreEqual("None", obj.topping[0].type);
            Assert.AreEqual(5002, obj.topping[1].id);
            Assert.AreEqual("Glazed", obj.topping[1].type);
            Assert.AreEqual(5005, obj.topping[2].id);
            Assert.AreEqual("Sugar", obj.topping[2].type);
            Assert.AreEqual(5007, obj.topping[3].id);
            Assert.AreEqual("Powdered Sugar", obj.topping[3].type);
            Assert.AreEqual(5006, obj.topping[4].id);
            Assert.AreEqual("Chocolate with Sprinkles", obj.topping[4].type);
            Assert.AreEqual(5003, obj.topping[5].id);
            Assert.AreEqual("Chocolate", obj.topping[5].type);
            Assert.AreEqual(5004, obj.topping[6].id);
            Assert.AreEqual("Maple", obj.topping[6].type);
            #endregion
        }
        [Test]
        public void ComplexObjectToJSON_Test()
        {
            //string json = "{ \"id\": \"0001\", \"type\": \"donut\", \"name\": \"Cake\", \"ppu\": 0.55, \"batters\": { \"batter\": [ { \"id\": \"1001\", \"type\": \"Regular\" }, { \"id\": \"1002\", \"type\": \"Chocolate\" }, { \"id\": \"1003\", \"type\": \"Blueberry\" }, { \"id\": \"1004\", \"type\": \"Devil's Food\" } ] }, \"topping\": [ { \"id\": \"5001\", \"type\": \"None\" }, { \"id\": \"5002\", \"type\": \"Glazed\" }, { \"id\": \"5005\", \"type\": \"Sugar\" }, { \"id\": \"5007\", \"type\": \"Powdered Sugar\" }, { \"id\": \"5006\", \"type\": \"Chocolate with Sprinkles\" }, { \"id\": \"5003\", \"type\": \"Chocolate\" }, { \"id\": \"5004\", \"type\": \"Maple\" } ] }";
            string json = "{ \"id\": 1, \"type\": \"donut\", \"name\": \"Cake\", \"ppu\": 0.55, \"batters\": { \"batter\": [ { \"id\": 1001, \"type\": \"Regular\" }, { \"id\": 1002, \"type\": \"Chocolate\" }, { \"id\": 1003, \"type\": \"Blueberry\" }, { \"id\": 1004, \"type\": \"Devil's Food\" } ] }, \"topping\": [ { \"id\": 5001, \"type\": \"None\" }, { \"id\": 5002, \"type\": \"Glazed\" }, { \"id\": 5005, \"type\": \"Sugar\" }, { \"id\": 5007, \"type\": \"Powdered Sugar\" }, { \"id\": 5006, \"type\": \"Chocolate with Sprinkles\" }, { \"id\": 5003, \"type\": \"Chocolate\" }, { \"id\": 5004, \"type\": \"Maple\" } ] }";
            CheckToJSON(json);
        }

        [Test]
        public void SingleStringValue_Test()
        {
            JSONObject result = JSONParser.ParseJSON("\"Value\"");
            CheckObjectNullValues(result);

            Assert.AreEqual(1, result.values.Count);
            Assert.IsTrue(result.values.ContainsKey("SINGLE_VALUE"));
            Assert.AreEqual("Value", result.values["SINGLE_VALUE"]);
            Assert.AreEqual("Value", result.GetSingleStringValue());

            Assert.AreEqual(0, result.jsonObjects.Count);

            Assert.AreEqual(0, result.objArrays.Count);
        }
        [Test]
        public void SingleStringValueToJSON_Test()
        {
            JSONObject result = JSONParser.ParseJSON("\"Value\"");
            CheckToJSON("\"Value\"");
        }
        [Test]
        public void SingleIntValue_Test()
        {
            JSONObject result = JSONParser.ParseJSON("1025");
            CheckObjectNullValues(result);

            Assert.AreEqual(1, result.values.Count);
            Assert.IsTrue(result.values.ContainsKey("SINGLE_VALUE"));
            Assert.AreEqual("1025", result.values["SINGLE_VALUE"]);
            Assert.AreEqual(1025, result.GetSingleValue());

            Assert.AreEqual(0, result.jsonObjects.Count);

            Assert.AreEqual(0, result.objArrays.Count);
        }
        [Test]
        public void SingleIntValueToJSON_Test()
        {
            JSONObject result = JSONParser.ParseJSON("1025");
            CheckToJSON("1025");
        }

        [Test]
        public void ArrayOfStringValues_Test()
        {
            JSONObject result = JSONParser.ParseJSON("[\"Value1\", \"Value2\", \"Value3\"]");
            CheckObjectNullValues(result);

            Assert.AreEqual(0, result.values.Count);

            Assert.AreEqual(0, result.jsonObjects.Count);

            Assert.AreEqual(1, result.valueArrays.Count);
            Assert.IsTrue(result.valueArrays.ContainsKey("ROOT_ARRAY"));
            Assert.AreEqual(3, result.valueArrays["ROOT_ARRAY"].Count);
            Assert.AreEqual("Value1", result.valueArrays["ROOT_ARRAY"][0]);
            Assert.AreEqual("Value1", result.GetStringRootArray()[0]);
            Assert.AreEqual("Value2", result.valueArrays["ROOT_ARRAY"][1]);
            Assert.AreEqual("Value2", result.GetStringRootArray()[1]);
            Assert.AreEqual("Value3", result.valueArrays["ROOT_ARRAY"][2]);
            Assert.AreEqual("Value3", result.GetStringRootArray()[2]);
        }
        [Test]
        public void ArrayOfStringValuesToJSON_Test()
        {
            JSONObject result = JSONParser.ParseJSON("[\"Value1\", \"Value2\", \"Value3\"]");
            CheckToJSON("[\"Value1\", \"Value2\", \"Value3\"]");
        }
        [Test]
        public void ArrayOfNumberValues_Test()
        {
            JSONObject result = JSONParser.ParseJSON("[100, 3.5, 5]");
            CheckObjectNullValues(result);

            Assert.AreEqual(0, result.values.Count);

            Assert.AreEqual(0, result.jsonObjects.Count);

            Assert.AreEqual(1, result.valueArrays.Count);
            Assert.IsTrue(result.valueArrays.ContainsKey("ROOT_ARRAY"));
            Assert.AreEqual(3, result.valueArrays["ROOT_ARRAY"].Count);
            Assert.AreEqual("100", result.valueArrays["ROOT_ARRAY"][0]);
            Assert.AreEqual(100, result.GetValueRootArray()[0]);
            Assert.AreEqual("3.5", result.valueArrays["ROOT_ARRAY"][1]);
            Assert.AreEqual(3.5f, result.GetValueRootArray()[1]);
            Assert.AreEqual(3.5d, result.GetValueRootArray()[1]);
            Assert.AreEqual(3.5m, result.GetValueRootArray()[1]);
            Assert.AreEqual("5", result.valueArrays["ROOT_ARRAY"][2]);
            Assert.AreEqual(5, result.GetValueRootArray()[2]);
        }
        [Test]
        public void ArrayOfNumberValuesToJSON_Test()
        {
            JSONObject result = JSONParser.ParseJSON("[100, 3.5, 5]");
            CheckToJSON("[100, 3.5, 5]");
        }
        [Test]
        public void ArrayOfMixedValues_Test()
        {
            JSONObject result = JSONParser.ParseJSON("[\"One\", 100, 3.5, 5, true, \"false\"]");
            CheckObjectNullValues(result);

            Assert.AreEqual(0, result.values.Count);

            Assert.AreEqual(0, result.jsonObjects.Count);

            Assert.AreEqual(1, result.valueArrays.Count);
            Assert.IsTrue(result.valueArrays.ContainsKey("ROOT_ARRAY"));
            Assert.AreEqual(6, result.valueArrays["ROOT_ARRAY"].Count);
            Assert.AreEqual("One", result.valueArrays["ROOT_ARRAY"][0]);
            Assert.AreEqual("One", result.GetValueRootArray()[0]);
            Assert.AreEqual("100", result.valueArrays["ROOT_ARRAY"][1]);
            Assert.AreEqual(100, result.GetValueRootArray()[1]);
            Assert.AreEqual("3.5", result.valueArrays["ROOT_ARRAY"][2]);
            Assert.AreEqual(3.5f, result.GetValueRootArray()[2]);
            Assert.AreEqual(3.5d, result.GetValueRootArray()[2]);
            Assert.AreEqual(3.5m, result.GetValueRootArray()[2]);
            Assert.AreEqual("5", result.valueArrays["ROOT_ARRAY"][3]);
            Assert.AreEqual(5, result.GetValueRootArray()[3]);
            Assert.AreEqual("true", result.valueArrays["ROOT_ARRAY"][4]);
            Assert.AreEqual(true, result.GetValueRootArray()[4]);
            Assert.AreEqual("false", result.valueArrays["ROOT_ARRAY"][5]);
            Assert.AreEqual(false, result.GetValueRootArray()[5]);
        }
        [Test]
        public void ArrayOfMixedValuesToJSON_Test()
        {
            JSONObject result = JSONParser.ParseJSON("[\"One\", 100, 3.5, 5, true, \"false\"]");
            CheckToJSON("[\"One\", 100, 3.5, 5, true, false]");
        }

        [Test]
        public void PackageJSON_Test()
        {
            string json = "{\"name\": \"com.mattrgeorge.mattrgeorge\",\"version\": \"1.4.11\",\"displayName\": \"MattRGeorge\",\"description\": \"General files that can be used accross multiple projects.\",\"keywords\": [],\"author\": {\"name\": \"Matthew George\",\"email\": \"Matthew.George1552@gmail.com\",\"url\": \"https://www.mattrgeorge.com\"},\"type\": \"library\",\"samples\": [{\"displayName\": \"Prefabs\",\"path\": \"Samples~/Prefabs\"}]}";

            JSONObject result = JSONParser.ParseJSON(json);
            CheckObjectNullValues(result);

            Assert.AreEqual(5, result.values.Count);
            Assert.IsTrue(result.values.ContainsKey("name"));
            Assert.AreEqual("com.mattrgeorge.mattrgeorge", result.values["name"]);
            Assert.IsTrue(result.values.ContainsKey("version"));
            Assert.AreEqual("1.4.11", result.values["version"]);
            Assert.IsTrue(result.values.ContainsKey("displayName"));
            Assert.AreEqual("MattRGeorge", result.values["displayName"]);
            Assert.IsTrue(result.values.ContainsKey("description"));
            Assert.AreEqual("General files that can be used accross multiple projects.", result.values["description"]);
            Assert.IsTrue(result.values.ContainsKey("type"));
            Assert.AreEqual("library", result.values["type"]);

            Assert.AreEqual(1, result.valueArrays.Count);
            Assert.IsTrue(result.valueArrays.ContainsKey("keywords"));
            Assert.IsNotNull(result.valueArrays["keywords"]);
            Assert.AreEqual(0, result.valueArrays["keywords"].Count);

            Assert.AreEqual(1, result.jsonObjects.Count);
            Assert.IsTrue(result.jsonObjects.ContainsKey("author"));
            Assert.AreEqual(3, result.jsonObjects["author"].values.Count);
            Assert.IsTrue(result.jsonObjects["author"].values.ContainsKey("name"));
            Assert.AreEqual("Matthew George", result.jsonObjects["author"].values["name"]);
            Assert.IsTrue(result.jsonObjects["author"].values.ContainsKey("email"));
            Assert.AreEqual("Matthew.George1552@gmail.com", result.jsonObjects["author"].values["email"]);
            Assert.IsTrue(result.jsonObjects["author"].values.ContainsKey("url"));
            Assert.AreEqual("https://www.mattrgeorge.com", result.jsonObjects["author"].values["url"]);

            Assert.AreEqual(1, result.objArrays.Count);
            Assert.IsTrue(result.objArrays.ContainsKey("samples"));
            Assert.AreEqual(1, result.objArrays["samples"].Count);
            Assert.IsTrue(result.objArrays["samples"][0].values.ContainsKey("displayName"));
            Assert.AreEqual("Prefabs", result.objArrays["samples"][0].values["displayName"]);
            Assert.IsTrue(result.objArrays["samples"][0].values.ContainsKey("path"));
            Assert.AreEqual("Samples~/Prefabs", result.objArrays["samples"][0].values["path"]);
        }
        [Test]
        public void PackageJSONToJSON_Test()
        {
            string json = "{\"name\": \"com.mattrgeorge.mattrgeorge\",\"version\": \"1.4.11\",\"displayName\": \"MattRGeorge\",\"description\": \"General files that can be used accross multiple projects.\",\"type\": \"library\",\"keywords\": [],\"author\": {\"name\": \"Matthew George\",\"email\": \"Matthew.George1552@gmail.com\",\"url\": \"https://www.mattrgeorge.com\"},\"samples\": [{\"displayName\": \"Prefabs\",\"path\": \"Samples~/Prefabs\"}]}";
            CheckToJSON(json);
        }

        private void CheckObjectNullValues(JSONObject obj)
        {
            Assert.IsNotNull(obj);
            Assert.IsNotNull(obj.values);
            Assert.IsNotNull(obj.jsonObjects);
            Assert.IsNotNull(obj.objArrays);

            foreach (KeyValuePair<string, string> pair in obj.values)
            {
                Assert.IsNotNull(pair.Key);
                Assert.AreNotEqual("", pair.Key);
                Assert.IsNotNull(pair.Value);
            }
            foreach (KeyValuePair<string, JSONObject> pair in obj.jsonObjects)
            {
                Assert.IsNotNull(pair.Key);
                Assert.AreNotEqual("", pair.Key);
                Assert.IsNotNull(pair.Value);
                CheckObjectNullValues(pair.Value);
            }
            foreach (KeyValuePair<string, List<JSONObject>> pair in obj.objArrays)
            {
                Assert.IsNotNull(pair.Key);
                Assert.AreNotEqual("", pair.Key);
                Assert.IsNotNull(pair.Value);
                foreach (JSONObject jsonObj in pair.Value) CheckObjectNullValues(jsonObj);
            }
        }

        private void CheckToJSON(string originalJSON)
        {
            JSONObject result = JSONParser.ParseJSON(originalJSON);

            originalJSON = originalJSON.Replace(" ", "").ToLower();
            string revertedJSON = result.ToJSON().Replace(" ", "").ToLower();
            Assert.IsNotNull(revertedJSON);
            Assert.AreEqual(originalJSON, revertedJSON);
        }

        #region Models and Mappers
        private class FirstLevelSimple
        {
            public string first_name = "";
            public string last_name = "";
            public bool online = false;
        }
        private FirstLevelSimple FirstLevelSimplerMapper(JSONObject parsedObj)
        {
            FirstLevelSimple obj = new FirstLevelSimple()
            {
                first_name = parsedObj.values["first_name"],
                last_name = parsedObj.values["last_name"],
                online = bool.Parse(parsedObj.values["online"])
            };

            return obj;
        }

        private class FirstLevelManyObjects
        {
            public string username = "";
            public string location = "";
            public bool online = false;
            public int followers = 0;
        }
        private Dictionary<string, FirstLevelManyObjects> FirstLevelManyObjectsMapper(JSONObject parsedObj)
        {
            Dictionary<string, FirstLevelManyObjects> objs = new Dictionary<string, FirstLevelManyObjects>();
            foreach (KeyValuePair<string, JSONObject> pair in parsedObj.jsonObjects)
            {
                objs.Add(pair.Key, new FirstLevelManyObjects()
                {
                    username = pair.Value.values["username"],
                    location = pair.Value.values["location"],
                    online = bool.Parse(pair.Value.values["online"]),
                    followers = int.Parse(pair.Value.values["followers"]),
                });
            }

            return objs;
        }

        private class FirstLevelArray
        {
            public string description = "";
            public string url = "";
        }
        private List<FirstLevelArray> FirstLevelArrayMapper(JSONObject parsedObj)
        {
            List<FirstLevelArray> objs = new List<FirstLevelArray>();
            foreach (JSONObject jsonObj in parsedObj.objArrays["ROOT_ARRAY"])
            {
                objs.Add(new FirstLevelArray()
                {
                    description = jsonObj.values["description"],
                    url = jsonObj.values["URL"]
                });
            }

            return objs;
        }

        private class ComplexObjSubObj
        {
            public int id = 0;
            public string type = "";
        }
        private class ComplexObject
        {
            public int id = 0;
            public string type = "";
            public string name = "cake";
            public double ppu = 0;
            public List<ComplexObjSubObj> batters = new List<ComplexObjSubObj>();
            public List<ComplexObjSubObj> topping = new List<ComplexObjSubObj>();
        }
        private ComplexObject ComplexObjectMapper(JSONObject parsedObj)
        {
            ComplexObject obj = new ComplexObject();

            obj.id = int.Parse(parsedObj.values["id"]);
            obj.type = parsedObj.values["type"];
            obj.name = parsedObj.values["name"];
            obj.ppu = double.Parse(parsedObj.values["ppu"]);
            foreach (JSONObject jsonObj in parsedObj.jsonObjects["batters"].objArrays["batter"])
            {
                obj.batters.Add(new ComplexObjSubObj()
                {
                    id = int.Parse(jsonObj.values["id"]),
                    type = jsonObj.values["type"]
                });
            }
            foreach (JSONObject jsonObj in parsedObj.objArrays["topping"])
            {
                obj.topping.Add(new ComplexObjSubObj()
                {
                    id = int.Parse(jsonObj.values["id"]),
                    type = jsonObj.values["type"]
                });
            }

            return obj;
        }
        #endregion
    }
}
