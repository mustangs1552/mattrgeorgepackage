﻿using System.Collections.Generic;
using UnityEngine;
using NUnit.Framework;
using MattRGeorge.Unity.Tools.Stats;

namespace MattRGeorge.Tests.Unity.Tools.Stats
{
    public class HealthTests
    {
        private Health healthObj = null;
        private List<HealthPiece> healthPieceObjs = new List<HealthPiece>();

        #region Health Tests
        [Test]
        public void TakeDamage_Tests()
        {
            healthObj.TakeDamage(50);
            CheckHealth(50, 100, 0, 1, 1, true, healthPieceObjs);
            healthObj.TakeDamage(75);
            CheckHealth(50, 100, 0, 1, 1, true, healthPieceObjs);
        }
        [Test]
        public void DamageMultiplier_Tests()
        {
            healthObj.damageMultiplier.CurrAmount = 2;

            healthObj.TakeDamage(30);
            CheckHealth(40, 100, 0, 1, 2, true, healthPieceObjs);
            healthObj.TakeDamage(75);
            CheckHealth(40, 100, 0, 1, 2, true, healthPieceObjs);
        }

        [Test]
        public void Heal_Tests()
        {
            healthObj.TakeDamage(50);

            healthObj.Heal(25);
            CheckHealth(75, 100, 0, 1, 1, true, healthPieceObjs);
            healthObj.Heal(75);
            CheckHealth(75, 100, 0, 1, 1, true, healthPieceObjs);
        }

        [Test]
        public void KillRevive_Tests()
        {
            healthObj.Kill();
            CheckHealth(0, 100, 0, 1, 1, true, healthPieceObjs);
            healthObj.Revive(false);
            CheckHealth(100, 100, 0, 1, 1, true, healthPieceObjs);

            healthObj.damageMultiplier.CurrAmount = 2;
            healthObj.Kill();
            CheckHealth(0, 100, 0, 1, 2, true, healthPieceObjs);
            healthObj.Revive(false);
            CheckHealth(100, 100, 0, 1, 2, true, healthPieceObjs);
            healthObj.Kill();
            healthObj.Revive(true);
            CheckHealth(100, 100, 0, 1, 1, true, healthPieceObjs);
        }

        [Test]
        public void ResetStats_Tests()
        {
            healthObj.damageMultiplier.CurrAmount = 2;
            healthObj.TakeDamage(30);

            healthObj.ResetStats();
            CheckHealth(100, 100, 0, 1, 1, true, healthPieceObjs);
        }

        [Test]
        public void ChangeHealthPieces_Tests()
        {
            healthPieceObjs.Add(healthObj.gameObject.AddComponent<HealthPiece>());
            healthObj.HealthPieces = healthPieceObjs;
            CheckHealth(100, 100, 0, 1, 1, true, healthPieceObjs);

            healthPieceObjs.RemoveAt(1);
            healthObj.HealthPieces = healthPieceObjs;
            CheckHealth(100, 100, 0, 1, 1, true, healthPieceObjs);

            healthPieceObjs = null;
            healthObj.HealthPieces = healthPieceObjs;
            CheckHealth(100, 100, 0, 1, 1, true, healthPieceObjs);
        }

        private void CheckHealth(float currHealth, float currMaxHealth, float currHealthUpdate, float passiveHealRate, float dmgMulti, bool updatingHealth, List<HealthPiece> healthPieces)
        {
            Assert.AreEqual(currHealth, healthObj.CurrHealth);
            Assert.AreEqual(currMaxHealth, healthObj.CurrMaxHealth);
            Assert.AreEqual(currHealthUpdate, healthObj.CurrHealthUpdateAmount);
            Assert.AreEqual(passiveHealRate, healthObj.PassiveHealRate);
            Assert.AreEqual(dmgMulti, healthObj.damageMultiplier.CurrAmount);
            Assert.AreEqual(updatingHealth, healthObj.UpdatingHealth);

            List<HealthPiece> healthObjHealthPieces = healthObj.HealthPieces;
            Assert.IsNotNull(healthObjHealthPieces);
            if (healthPieces == null) Assert.AreEqual(0, healthObjHealthPieces.Count);
            else
            {
                Assert.AreEqual(healthPieces.Count, healthObjHealthPieces.Count);
                for (int i = 0; i < healthPieces.Count; i++) Assert.AreEqual(healthPieces[i], healthObjHealthPieces[i]);
            }
        }
        #endregion

        #region Health Piece Tests
        [Test]
        public void HPTakeDamage_Tests()
        {
            healthPieceObjs[0].TakeDamage(50);
            CheckHealth(50, 100, 0, 1, 1, true, healthPieceObjs);
            healthPieceObjs[0].TakeDamage(75);
            CheckHealth(50, 100, 0, 1, 1, true, healthPieceObjs);

            CheckHealthPieces(1, 0);
        }
        [Test]
        public void HPDamageMultiplier_Tests()
        {
            healthPieceObjs[0].damageMultiplier.CurrAmount = 2;

            healthPieceObjs[0].TakeDamage(30);
            CheckHealth(40, 100, 0, 1, 1, true, healthPieceObjs);
            healthPieceObjs[0].TakeDamage(75);
            CheckHealth(40, 100, 0, 1, 1, true, healthPieceObjs);

            CheckHealthPieces(2, 0);
        }

        [Test]
        public void HPHeal_Tests()
        {
            healthObj.TakeDamage(50);

            healthPieceObjs[0].Heal(25);
            CheckHealth(75, 100, 0, 1, 1, true, healthPieceObjs);
            healthPieceObjs[0].Heal(75);
            CheckHealth(75, 100, 0, 1, 1, true, healthPieceObjs);

            CheckHealthPieces(1, 0);
        }

        private void CheckHealthPieces(float dmgMulti, int healthPieceI)
        {
            Assert.AreEqual(dmgMulti, healthPieceObjs[healthPieceI].damageMultiplier.CurrAmount);
        }
        #endregion

        [SetUp]
        public void TestSetup()
        {
            GameObject obj = new GameObject("HealthObj");
            healthObj = obj.AddComponent<Health>();
            healthPieceObjs = new List<HealthPiece>();
            healthPieceObjs.Add(obj.AddComponent<HealthPiece>());
            healthObj.HealthPieces = healthPieceObjs;

            healthObj.ResetStats();
        }
    }
}
