﻿using NUnit.Framework;
using MattRGeorge.Unity.Tools.Stats;

namespace MattRGeorge.Tests.Unity.Tools.Stats
{
    public class StatsTests
    {
        private Stat stat = null;
        private StatGauge statGauge = null;
        private DynamicStatGauge dynamicStatGauge = null;
        private RangeStat rangeStat = null;

        private bool callbackCalled = false;

        private void CheckStat(float amount)
        {
            Assert.AreEqual(amount, stat.CurrAmount, $"Stat: Current amount was wrong!");
        }
        private void StatOnChanged(float amount)
        {
            CheckStat(0);
            CheckStat(amount);
            callbackCalled = true;
        }
        [Test]
        public void Stat_Tests()
        {
            // Created
            stat = new Stat(10);
            CheckStat(10);

            // Adjust amount
            stat.CurrAmount -= 5;
            CheckStat(5);
            stat.CurrAmount += 3;
            CheckStat(8);

            // On changed callback
            stat.OnChanged.AddListener(StatOnChanged);
            callbackCalled = false;
            stat.CurrAmount = 0;
            Assert.IsTrue(callbackCalled, "Stat: OnChanged() not called!");
            stat.OnChanged.RemoveListener(StatOnChanged);

            // Reseting stat
            stat.StatReset();
            CheckStat(10);
        }

        private void CheckStatGauge(float amount, float maxAmount)
        {
            Assert.AreEqual(amount, statGauge.CurrAmount, $"Stat Gauge: Current amount was wrong!");
            Assert.AreEqual(maxAmount, statGauge.CurrMaxAmount, $"Stat Gauge: Current max amount was wrong!");
        }
        private void StatGaugeOnChanged(float amount)
        {
            CheckStatGauge(7, 10);
            CheckStatGauge(amount, 10);
            callbackCalled = true;
        }
        private void StatGaugeOnMinReached(float amount)
        {
            CheckStatGauge(0, 10);
            CheckStatGauge(amount, 10);
            callbackCalled = true;
        }
        private void StatGaugeOnMaxReached(float amount)
        {
            CheckStatGauge(10, 10);
            CheckStatGauge(amount, 10);
            callbackCalled = true;
        }
        [Test]
        public void StatGauge_Tests()
        {
            // Created
            statGauge = new StatGauge(10, 10, true);
            CheckStatGauge(10, 10);

            // Adjust amount
            statGauge.CurrAmount -= 5;
            CheckStatGauge(5, 10);
            statGauge.CurrAmount += 3;
            CheckStatGauge(8, 10);

            // Exceeded boundries their callbacks
            statGauge.OnMinReached.AddListener(StatGaugeOnMinReached);
            statGauge.OnMaxReached.AddListener(StatGaugeOnMaxReached);
            callbackCalled = false;
            statGauge.CurrAmount -= 10;
            Assert.IsTrue(callbackCalled, "Stat Gauge: OnMinReached() not called!");
            callbackCalled = false;
            statGauge.CurrAmount += 12;
            Assert.IsTrue(callbackCalled, "Stat Gauge: OnMaxReached() not called!");
            statGauge.OnMinReached.RemoveListener(StatGaugeOnMinReached);
            statGauge.OnMaxReached.RemoveListener(StatGaugeOnMaxReached);

            // On changed callback
            statGauge.OnChanged.AddListener(StatGaugeOnChanged);
            callbackCalled = false;
            statGauge.CurrAmount = 7;
            Assert.IsTrue(callbackCalled, "Stat Gauge: OnChanged() not called!");
            statGauge.OnChanged.RemoveListener(StatGaugeOnChanged);

            // Update with max amount option on
            statGauge.CurrMaxAmount = 20;
            CheckStatGauge(14, 20);
            statGauge.CurrMaxAmount = 5;
            CheckStatGauge(3.5f, 5);
            statGauge.CurrMaxAmount = -5;
            CheckStatGauge(3.5f, 5);

            // Reseting stat
            statGauge.StatReset();
            CheckStatGauge(10, 10);

            // Update with max amount option off
            statGauge = new StatGauge(10, 10, false);
            CheckStatGauge(10, 10);
            statGauge.CurrMaxAmount = 20;
            CheckStatGauge(10, 20);
            statGauge.CurrMaxAmount = 5;
            CheckStatGauge(5, 5);

            // Assigning value via percentage
            statGauge.SetAmount(.5f);
            CheckStatGauge(2.5f, 5);
            statGauge.SetAmount(1);
            CheckStatGauge(5, 5);
            statGauge.SetAmount(0);
            CheckStatGauge(0, 5);
            statGauge.SetAmount(.31f);
            CheckStatGauge(1.55f, 5);
            statGauge.SetAmount(-.5f);
            CheckStatGauge(1.55f, 5);
            statGauge.SetAmount(1.5f);
            CheckStatGauge(1.55f, 5);
        }

        private void CheckDynamicStatGauge(float amount, float maxAmount)
        {
            Assert.AreEqual(amount, dynamicStatGauge.CurrAmount, $"Dynamic Stat Gauge: Current amount was wrong!");
            Assert.AreEqual(maxAmount, dynamicStatGauge.CurrMaxAmount, $"Dynamic Stat Gauge: Current max amount was wrong!");
        }
        private void DyanamicStatGaugeOnChanged(float amount)
        {
            CheckDynamicStatGauge(7, 10);
            CheckDynamicStatGauge(amount, 10);
            callbackCalled = true;
        }
        private void DyanamicStatGaugeOnMinReached(float amount)
        {
            CheckDynamicStatGauge(0, 10);
            CheckDynamicStatGauge(amount, 10);
            callbackCalled = true;
        }
        private void DyanamicStatGaugeOnMaxReached(float amount)
        {
            CheckDynamicStatGauge(10, 10);
            CheckDynamicStatGauge(amount, 10);
            callbackCalled = true;
        }
        [Test]
        public void DynamicStatGauge_Tests()
        {
            // Created
            dynamicStatGauge = new DynamicStatGauge(10, 10, true, 0);
            CheckDynamicStatGauge(10, 10);

            // Adjust amount
            dynamicStatGauge.CurrAmount -= 5;
            CheckDynamicStatGauge(5, 10);
            dynamicStatGauge.CurrAmount += 3;
            CheckDynamicStatGauge(8, 10);

            // Exceeded boundries their callbacks
            dynamicStatGauge.OnMinReached.AddListener(DyanamicStatGaugeOnMinReached);
            dynamicStatGauge.OnMaxReached.AddListener(DyanamicStatGaugeOnMaxReached);
            callbackCalled = false;
            dynamicStatGauge.CurrAmount -= 10;
            Assert.IsTrue(callbackCalled, "Dynamic Stat Gauge: OnMinReached() not called!");
            callbackCalled = false;
            dynamicStatGauge.CurrAmount += 12;
            Assert.IsTrue(callbackCalled, "Dynamic Stat Gauge: OnMaxReached() not called!");
            dynamicStatGauge.OnMinReached.RemoveListener(DyanamicStatGaugeOnMinReached);
            dynamicStatGauge.OnMaxReached.RemoveListener(DyanamicStatGaugeOnMaxReached);

            // On changed callback
            dynamicStatGauge.OnChanged.AddListener(DyanamicStatGaugeOnChanged);
            callbackCalled = false;
            dynamicStatGauge.CurrAmount = 7;
            Assert.IsTrue(callbackCalled, "Dynamic Stat Gauge: OnChanged() not called!");
            dynamicStatGauge.OnChanged.RemoveListener(DyanamicStatGaugeOnChanged);

            // Update with max amount option on
            dynamicStatGauge.CurrMaxAmount = 20;
            CheckDynamicStatGauge(14, 20);
            dynamicStatGauge.CurrMaxAmount = 5;
            CheckDynamicStatGauge(3.5f, 5);
            dynamicStatGauge.CurrMaxAmount = -5;
            CheckDynamicStatGauge(3.5f, 5);

            // Reseting stat
            dynamicStatGauge.StatReset();
            CheckDynamicStatGauge(10, 10);

            // Update with max amount option off
            dynamicStatGauge = new DynamicStatGauge(10, 10, false, 0);
            CheckDynamicStatGauge(10, 10);
            dynamicStatGauge.CurrMaxAmount = 20;
            CheckDynamicStatGauge(10, 20);
            dynamicStatGauge.CurrMaxAmount = 5;
            CheckDynamicStatGauge(5, 5);

            // Assigning value via percentage
            dynamicStatGauge.SetAmount(.5f);
            CheckDynamicStatGauge(2.5f, 5);
            dynamicStatGauge.SetAmount(1);
            CheckDynamicStatGauge(5, 5);
            dynamicStatGauge.SetAmount(0);
            CheckDynamicStatGauge(0, 5);
            dynamicStatGauge.SetAmount(.31f);
            CheckDynamicStatGauge(1.55f, 5);
            dynamicStatGauge.SetAmount(-.5f);
            CheckDynamicStatGauge(1.55f, 5);
            dynamicStatGauge.SetAmount(1.5f);
            CheckDynamicStatGauge(1.55f, 5);

            // Amount updating
            dynamicStatGauge.SetAmount(1);
            dynamicStatGauge.CurrUpdateAmount = -1;
            CheckDynamicStatGauge(5, 5);
            dynamicStatGauge.StatUpdate();
            CheckDynamicStatGauge(4, 5);
            dynamicStatGauge.StatUpdate();
            CheckDynamicStatGauge(3, 5);
            dynamicStatGauge.CurrUpdateAmount = 1;
            CheckDynamicStatGauge(3, 5);
            dynamicStatGauge.StatUpdate();
            CheckDynamicStatGauge(4, 5);
            dynamicStatGauge.StatUpdate();
            CheckDynamicStatGauge(5, 5);
            dynamicStatGauge.CurrUpdateAmount = 0;
            CheckDynamicStatGauge(5, 5);
            dynamicStatGauge.StatUpdate();
            CheckDynamicStatGauge(5, 5);
            dynamicStatGauge.CurrUpdateAmount = -3;
            dynamicStatGauge.StatUpdate();
            dynamicStatGauge.StatUpdate();
            CheckDynamicStatGauge(0, 5);
            dynamicStatGauge.CurrUpdateAmount = 3;
            dynamicStatGauge.StatUpdate();
            dynamicStatGauge.StatUpdate();
            CheckDynamicStatGauge(5, 5);
        }

        private void CheckRangeStat(float amount, float maxAmount, float minAmount)
        {
            Assert.AreEqual(amount, rangeStat.CurrAmount, $"Range Stat Gauge: Current amount was wrong!");
            Assert.AreEqual(maxAmount, rangeStat.CurrMaxAmount, $"Range Stat Gauge: Current max amount was wrong!");
            Assert.AreEqual(minAmount, rangeStat.CurrMinAmount, $"Range Stat Gauge: Current min amount was wrong!");
        }
        private void RangeStatOnChanged(float amount)
        {
            CheckRangeStat(7, 10, 0);
            CheckRangeStat(amount, 10, 0);
            callbackCalled = true;
        }
        private void RangeStatOnMinReached(float amount)
        {
            CheckRangeStat(0, 10, 0);
            CheckRangeStat(amount, 10, 0);
            callbackCalled = true;
        }
        private void RangeStatOnMaxReached(float amount)
        {
            CheckRangeStat(10, 10, 0);
            CheckRangeStat(amount, 10, 0);
            callbackCalled = true;
        }
        [Test]
        public void RangeStat_Tests()
        {
            // Created
            rangeStat = new RangeStat(10, 10, true, 0);
            CheckRangeStat(10, 10, 0);

            // Adjust amount
            rangeStat.CurrAmount -= 5;
            CheckRangeStat(5, 10, 0);
            rangeStat.CurrAmount += 3;
            CheckRangeStat(8, 10, 0);

            // Exceeded boundries their callbacks
            rangeStat.OnMinReached.AddListener(RangeStatOnMinReached);
            rangeStat.OnMaxReached.AddListener(RangeStatOnMaxReached);
            callbackCalled = false;
            rangeStat.CurrAmount -= 10;
            Assert.IsTrue(callbackCalled, "Range Stat Gauge: OnMinReached() not called!");
            callbackCalled = false;
            rangeStat.CurrAmount += 12;
            Assert.IsTrue(callbackCalled, "Range Stat Gauge: OnMaxReached() not called!");
            rangeStat.OnMinReached.RemoveListener(RangeStatOnMinReached);
            rangeStat.OnMaxReached.RemoveListener(RangeStatOnMaxReached);

            // On changed callback
            rangeStat.OnChanged.AddListener(RangeStatOnChanged);
            callbackCalled = false;
            rangeStat.CurrAmount = 7;
            Assert.IsTrue(callbackCalled, "Rannge Stat Gauge: OnChanged() not called!");
            rangeStat.OnChanged.RemoveListener(RangeStatOnChanged);

            // Update with max amount option on
            rangeStat.CurrMaxAmount = 20;
            CheckRangeStat(14, 20, 0);
            rangeStat.CurrMaxAmount = 5;
            CheckRangeStat(3.5f, 5, 0);
            rangeStat.CurrMaxAmount = -5;
            CheckRangeStat(3.5f, 5, 0);
            rangeStat.CurrMinAmount = 1;
            CheckRangeStat(3.8f, 5, 1);
            rangeStat.CurrMinAmount = 3;
            CheckRangeStat(4.9f, 5, 3);
            rangeStat.CurrMinAmount = 8;
            CheckRangeStat(4.9f, 5, 3);

            // Reseting stat
            rangeStat.StatReset();
            CheckRangeStat(10, 10, 0);

            // Update with max amount option off
            rangeStat = new RangeStat(10, 10, false, 0);
            CheckRangeStat(10, 10, 0);
            rangeStat.CurrMaxAmount = 20;
            CheckRangeStat(10, 20, 0);
            rangeStat.CurrMaxAmount = 5;
            CheckRangeStat(5, 5, 0);
            rangeStat.CurrMaxAmount = -5;
            CheckRangeStat(5, 5, 0);
            rangeStat.CurrMinAmount = 1;
            CheckRangeStat(5, 5, 1);
            rangeStat.CurrMinAmount = 3;
            CheckRangeStat(5, 5, 3);
            rangeStat.CurrMinAmount = 8;
            CheckRangeStat(5, 5, 3);
            rangeStat.CurrMaxAmount = 20;
            rangeStat.CurrMinAmount = 8;
            CheckRangeStat(8, 20, 8);

            // Assigning value via percentage
            rangeStat.SetAmount(.5f);
            CheckRangeStat(14, 20, 8);
            rangeStat.SetAmount(1);
            CheckRangeStat(20, 20, 8);
            rangeStat.SetAmount(0);
            CheckRangeStat(8, 20, 8);
            rangeStat.SetAmount(.31f);
            CheckRangeStat(11.72f, 20, 8);
            rangeStat.SetAmount(-.5f);
            CheckRangeStat(11.72f, 20, 8);
            rangeStat.SetAmount(1.5f);
            CheckRangeStat(11.72f, 20, 8);
        }
    }
}
