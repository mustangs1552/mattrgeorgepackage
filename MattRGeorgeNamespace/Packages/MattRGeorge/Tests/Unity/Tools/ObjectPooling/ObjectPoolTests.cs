﻿using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using MattRGeorge.Unity.Tools.ObjectPooling;

namespace MattRGeorge.Tests.Unity.Tools.ObjectPooling
{
    public class ObjectPoolTests
    {
        private ObjectPool objPool = null;
        private GameObject obj = null;
        private GameObject obj2 = null;
        private GameObject obj3 = null;
        private GameObject objCopy = null;
        private GameObject obj2Copy = null;
        private GameObject obj3Copy = null;
        private GameObject dontAddObj = null;
        private string tagName = "Player";
        private List<GameObject> returnedObjs = new List<GameObject>();
        private List<Transform> returnedTrans = new List<Transform>();
        private GameObject returnedObj = null;

        [Test]
        public void AllGameObjects_Tests()
        {
            returnedObjs = objPool.AllGameObjects;
            Assert.IsNotNull(returnedObjs);
            CollectionAssert.IsEmpty(returnedObjs);

            AddObjects();

            returnedObjs = objPool.AllGameObjects;
            Assert.IsNotNull(returnedObjs);
            CollectionAssert.IsNotEmpty(returnedObjs);
            Assert.AreEqual(6, returnedObjs.Count);
            for (int i = 0; i < returnedObjs.Count; i++)
            {
                Assert.IsTrue(returnedObjs[i]);

                switch (i)
                {
                    case 0:
                        Assert.AreEqual(obj, returnedObjs[i]);
                        break;
                    case 1:
                        Assert.AreEqual(objCopy, returnedObjs[i]);
                        break;
                    case 2:
                        Assert.AreEqual(obj2, returnedObjs[i]);
                        break;
                    case 3:
                        Assert.AreEqual(obj2Copy, returnedObjs[i]);
                        break;
                    case 4:
                        Assert.AreEqual(obj3, returnedObjs[i]);
                        break;
                    case 5:
                        Assert.AreEqual(obj3Copy, returnedObjs[i]);
                        break;
                }
            }
        }
        [Test]
        public void AllTransforms_Tests()
        {
            returnedTrans = objPool.AllTransforms;
            Assert.IsNotNull(returnedTrans);
            CollectionAssert.IsEmpty(returnedTrans);

            AddObjects();

            returnedTrans = objPool.AllTransforms;
            Assert.IsNotNull(returnedTrans);
            CollectionAssert.IsNotEmpty(returnedTrans);
            Assert.AreEqual(6, returnedTrans.Count);
            for (int i = 0; i < returnedTrans.Count; i++)
            {
                Assert.IsTrue(returnedTrans[i]);

                switch (i)
                {
                    case 0:
                        Assert.AreEqual(obj.transform, returnedTrans[i]);
                        break;
                    case 1:
                        Assert.AreEqual(objCopy.transform, returnedTrans[i]);
                        break;
                    case 2:
                        Assert.AreEqual(obj2.transform, returnedTrans[i]);
                        break;
                    case 3:
                        Assert.AreEqual(obj2Copy.transform, returnedTrans[i]);
                        break;
                    case 4:
                        Assert.AreEqual(obj3.transform, returnedTrans[i]);
                        break;
                    case 5:
                        Assert.AreEqual(obj3Copy.transform, returnedTrans[i]);
                        break;
                }
            }
        }

        [Test]
        public void AddObject_Tests()
        {
            AddObjects();

            Assert.AreEqual(3, objPool.transform.childCount);
            Assert.AreEqual($"{obj.name.ToUpper()}_ANCHOR", objPool.transform.GetChild(0).name);
            Assert.AreEqual($"{obj2.name.ToUpper()}_ANCHOR", objPool.transform.GetChild(1).name);
            Assert.AreEqual($"{obj3.name.ToUpper()}_ANCHOR", objPool.transform.GetChild(2).name);
            Assert.AreEqual(2, objPool.transform.GetChild(0).childCount);
            Assert.AreEqual(2, objPool.transform.GetChild(1).childCount);
            Assert.AreEqual(2, objPool.transform.GetChild(2).childCount);
            Assert.AreEqual(obj, objPool.transform.GetChild(0).GetChild(0).gameObject);
            Assert.AreEqual(objCopy, objPool.transform.GetChild(0).GetChild(1).gameObject);
            Assert.AreEqual(obj2, objPool.transform.GetChild(1).GetChild(0).gameObject);
            Assert.AreEqual(obj2Copy, objPool.transform.GetChild(1).GetChild(1).gameObject);
            Assert.AreEqual(obj3, objPool.transform.GetChild(2).GetChild(0).gameObject);
            Assert.AreEqual(obj3Copy, objPool.transform.GetChild(2).GetChild(1).gameObject);
        }
        [Test]
        public void AddObject_InvalidInput_Tests()
        {
            Assert.IsFalse(objPool.AddObject(null));
        }

        [Test]
        public void RemoveObject_Tests()
        {
            returnedObj = objPool.RemoveObject(obj);
            Assert.IsNull(returnedObj);

            AddObjects();

            returnedObj = objPool.RemoveObject(obj);
            Assert.IsTrue(returnedObj);
            Assert.AreEqual(obj, returnedObj);
            Assert.AreEqual(3, objPool.transform.childCount);
            Assert.AreEqual($"{obj.name.ToUpper()}_ANCHOR", objPool.transform.GetChild(0).name);
            Assert.AreEqual(1, objPool.transform.GetChild(0).childCount);
            Assert.AreEqual(objCopy, objPool.transform.GetChild(0).GetChild(0).gameObject);

            returnedObj = objPool.RemoveObject(obj);
            Assert.IsNull(returnedObj);
            Assert.AreEqual(3, objPool.transform.childCount);
            Assert.AreEqual($"{obj.name.ToUpper()}_ANCHOR", objPool.transform.GetChild(0).name);
            Assert.AreEqual(1, objPool.transform.GetChild(0).childCount);
            Assert.AreEqual(objCopy, objPool.transform.GetChild(0).GetChild(0).gameObject);

            returnedObj = objPool.RemoveObject(dontAddObj);
            Assert.IsNull(returnedObj);
        }
        [Test]
        public void RemoveObject_InvalidInput_Tests()
        {
            returnedObj = objPool.RemoveObject(null);
            Assert.IsNull(returnedObj);
        }

        [Test]
        public void GetPool_Tests()
        {
            returnedObjs = objPool.GetPool(obj);
            Assert.IsNotNull(returnedObjs);
            CollectionAssert.IsEmpty(returnedObjs);

            AddObjects();

            returnedObjs = objPool.GetPool(obj);
            Assert.IsNotNull(returnedObjs);
            Assert.AreEqual(2, returnedObjs.Count);
            Assert.AreEqual(obj, returnedObjs[0]);
            Assert.AreEqual(objCopy, returnedObjs[1]);

            returnedObjs = objPool.GetPool(objCopy);
            Assert.IsNotNull(returnedObjs);
            Assert.AreEqual(2, returnedObjs.Count);
            Assert.AreEqual(obj, returnedObjs[0]);
            Assert.AreEqual(objCopy, returnedObjs[1]);

            returnedObjs = objPool.GetPool(dontAddObj);
            Assert.IsNotNull(returnedObjs);
            CollectionAssert.IsEmpty(returnedObjs);
        }
        [Test]
        public void GetPool_InvalidInput_Tests()
        {
            returnedObjs = objPool.GetPool(null);
            Assert.IsNotNull(returnedObjs);
            CollectionAssert.IsEmpty(returnedObjs);
        }

        [Test]
        public void CheckIfExists_Tests()
        {
            Assert.IsFalse(objPool.CheckIfExist(obj));

            AddObjects();

            Assert.IsTrue(objPool.CheckIfExist(obj));
            Assert.IsTrue(objPool.CheckIfExist(obj2));
            Assert.IsTrue(objPool.CheckIfExist(obj3));
            Assert.IsTrue(objPool.CheckIfExist(objCopy));
            Assert.IsTrue(objPool.CheckIfExist(obj2Copy));
            Assert.IsTrue(objPool.CheckIfExist(obj3Copy));

            Assert.IsFalse(objPool.CheckIfExist(dontAddObj));
        }
        [Test]
        public void CheckIfExists_InvalidInput_Tests()
        {
            Assert.IsFalse(objPool.CheckIfExist(null));
        }

        [Test]
        public void GetObjectComponents_Tests()
        {
            returnedTrans = objPool.GetObjectComponents<Transform>();
            Assert.IsNotNull(returnedTrans);
            CollectionAssert.IsEmpty(returnedTrans);

            AddObjects();

            returnedTrans = objPool.GetObjectComponents<Transform>();
            Assert.IsNotNull(returnedTrans);
            CollectionAssert.IsNotEmpty(returnedTrans);
            Assert.AreEqual(6, returnedTrans.Count);
            for (int i = 0; i < returnedTrans.Count; i++)
            {
                Assert.IsTrue(returnedTrans[i]);

                switch (i)
                {
                    case 0:
                        Assert.AreEqual(obj.transform, returnedTrans[i]);
                        break;
                    case 1:
                        Assert.AreEqual(objCopy.transform, returnedTrans[i]);
                        break;
                    case 2:
                        Assert.AreEqual(obj2.transform, returnedTrans[i]);
                        break;
                    case 3:
                        Assert.AreEqual(obj2Copy.transform, returnedTrans[i]);
                        break;
                    case 4:
                        Assert.AreEqual(obj3.transform, returnedTrans[i]);
                        break;
                    case 5:
                        Assert.AreEqual(obj3Copy.transform, returnedTrans[i]);
                        break;
                }
            }
        }

        [Test]
        public void GetObjectsWithTag_Tests()
        {
            returnedObjs = objPool.GetObjectsWithTag("Untagged");
            Assert.IsNotNull(returnedObjs);
            CollectionAssert.IsEmpty(returnedObjs);

            AddObjects();

            returnedObjs = objPool.GetObjectsWithTag("Untagged");
            Assert.IsNotNull(returnedObjs);
            CollectionAssert.IsNotEmpty(returnedObjs);
            Assert.AreEqual(4, returnedObjs.Count);
            for (int i = 0; i < returnedObjs.Count; i++)
            {
                Assert.IsTrue(returnedObjs[i]);

                switch (i)
                {
                    case 0:
                        Assert.AreEqual(obj, returnedObjs[i]);
                        break;
                    case 1:
                        Assert.AreEqual(objCopy, returnedObjs[i]);
                        break;
                    case 2:
                        Assert.AreEqual(obj3, returnedObjs[i]);
                        break;
                    case 3:
                        Assert.AreEqual(obj3Copy, returnedObjs[i]);
                        break;
                }
            }

            returnedObjs = objPool.GetObjectsWithTag(tagName);
            Assert.IsNotNull(returnedObjs);
            CollectionAssert.IsNotEmpty(returnedObjs);
            Assert.AreEqual(2, returnedObjs.Count);
            for (int i = 0; i < returnedObjs.Count; i++)
            {
                Assert.IsTrue(returnedObjs[i]);

                switch (i)
                {
                    case 0:
                        Assert.AreEqual(obj2, returnedObjs[i]);
                        break;
                    case 1:
                        Assert.AreEqual(obj2Copy, returnedObjs[i]);
                        break;
                }
            }
        }
        [Test]
        public void GetObjectsWithTag_InvalidInput_Tests()
        {
            returnedObjs = objPool.GetObjectsWithTag("");
            Assert.IsNotNull(returnedObjs);
            CollectionAssert.IsEmpty(returnedObjs);

            returnedObjs = objPool.GetObjectsWithTag(null);
            Assert.IsNotNull(returnedObjs);
            CollectionAssert.IsEmpty(returnedObjs);

            returnedObjs = objPool.GetObjectsWithTag("Tag Doesn't Exist");
            Assert.IsNotNull(returnedObjs);
            CollectionAssert.IsEmpty(returnedObjs);
        }

        [Test]
        public void GetTransformsWithTag_Tests()
        {
            returnedTrans = objPool.GetTransformsWithTag("Untagged");
            Assert.IsNotNull(returnedTrans);
            CollectionAssert.IsEmpty(returnedTrans);

            AddObjects();

            returnedTrans = objPool.GetTransformsWithTag("Untagged");
            Assert.IsNotNull(returnedTrans);
            CollectionAssert.IsNotEmpty(returnedTrans);
            Assert.AreEqual(4, returnedTrans.Count);
            for (int i = 0; i < returnedTrans.Count; i++)
            {
                Assert.IsTrue(returnedTrans[i]);

                switch (i)
                {
                    case 0:
                        Assert.AreEqual(obj.transform, returnedTrans[i]);
                        break;
                    case 1:
                        Assert.AreEqual(objCopy.transform, returnedTrans[i]);
                        break;
                    case 2:
                        Assert.AreEqual(obj3.transform, returnedTrans[i]);
                        break;
                    case 3:
                        Assert.AreEqual(obj3Copy.transform, returnedTrans[i]);
                        break;
                }
            }

            returnedTrans = objPool.GetTransformsWithTag(tagName);
            Assert.IsNotNull(returnedTrans);
            CollectionAssert.IsNotEmpty(returnedTrans);
            Assert.AreEqual(2, returnedTrans.Count);
            for (int i = 0; i < returnedTrans.Count; i++)
            {
                Assert.IsTrue(returnedTrans[i]);

                switch (i)
                {
                    case 0:
                        Assert.AreEqual(obj2.transform, returnedTrans[i]);
                        break;
                    case 1:
                        Assert.AreEqual(obj2Copy.transform, returnedTrans[i]);
                        break;
                }
            }
        }
        [Test]
        public void GetTransformsWithTag_InvalidInput_Tests()
        {
            returnedTrans = objPool.GetTransformsWithTag("");
            Assert.IsNotNull(returnedTrans);
            CollectionAssert.IsEmpty(returnedTrans);

            returnedTrans = objPool.GetTransformsWithTag(null);
            Assert.IsNotNull(returnedTrans);
            CollectionAssert.IsEmpty(returnedTrans);

            returnedTrans = objPool.GetTransformsWithTag("Tag Doesn't Exist");
            Assert.IsNotNull(returnedTrans);
            CollectionAssert.IsEmpty(returnedTrans);
        }

        [Test]
        public void SetObjectsActive_Tests()
        {
            objPool.SetObjectsActive(false);

            AddObjects();

            objPool.SetObjectsActive(false);
            Assert.IsFalse(obj.activeInHierarchy);
            Assert.IsFalse(obj2.activeInHierarchy);
            Assert.IsFalse(obj3.activeInHierarchy);
            Assert.IsFalse(objCopy.activeInHierarchy);
            Assert.IsFalse(obj2Copy.activeInHierarchy);
            Assert.IsFalse(obj3Copy.activeInHierarchy);

            objPool.SetObjectsActive(true);
            Assert.IsTrue(obj.activeInHierarchy);
            Assert.IsTrue(obj2.activeInHierarchy);
            Assert.IsTrue(obj3.activeInHierarchy);
            Assert.IsTrue(objCopy.activeInHierarchy);
            Assert.IsTrue(obj2Copy.activeInHierarchy);
            Assert.IsTrue(obj3Copy.activeInHierarchy);

            objPool.SetObjectsActive(false, "Obj2");
            Assert.IsTrue(obj.activeInHierarchy);
            Assert.IsFalse(obj2.activeInHierarchy);
            Assert.IsTrue(obj3.activeInHierarchy);
            Assert.IsTrue(objCopy.activeInHierarchy);
            Assert.IsFalse(obj2Copy.activeInHierarchy);
            Assert.IsTrue(obj3Copy.activeInHierarchy);
        }
        [Test]
        public void SetObjectsActive_InvalidInput_Tests()
        {
            AddObjects();

            objPool.SetObjectsActive(false, null);
            Assert.IsFalse(obj.activeInHierarchy);
            Assert.IsFalse(obj2.activeInHierarchy);
            Assert.IsFalse(obj3.activeInHierarchy);
            Assert.IsFalse(objCopy.activeInHierarchy);
            Assert.IsFalse(obj2Copy.activeInHierarchy);
            Assert.IsFalse(obj3Copy.activeInHierarchy);

            objPool.SetObjectsActive(true, "");
            Assert.IsTrue(obj.activeInHierarchy);
            Assert.IsTrue(obj2.activeInHierarchy);
            Assert.IsTrue(obj3.activeInHierarchy);
            Assert.IsTrue(objCopy.activeInHierarchy);
            Assert.IsTrue(obj2Copy.activeInHierarchy);
            Assert.IsTrue(obj3Copy.activeInHierarchy);

            objPool.SetObjectsActive(false, "DontAddObj");
            Assert.IsTrue(obj.activeInHierarchy);
            Assert.IsTrue(obj2.activeInHierarchy);
            Assert.IsTrue(obj3.activeInHierarchy);
            Assert.IsTrue(objCopy.activeInHierarchy);
            Assert.IsTrue(obj2Copy.activeInHierarchy);
            Assert.IsTrue(obj3Copy.activeInHierarchy);
        }

        [SetUp]
        public void Setup()
        {
            objPool = new GameObject("ObjPool").AddComponent<ObjectPool>();

            obj = new GameObject("Obj");
            obj2 = new GameObject("Obj2");
            obj2.tag = tagName;
            obj3 = new GameObject("Obj3");
            objCopy = GameObject.Instantiate(obj);
            obj2Copy = GameObject.Instantiate(obj2);
            obj3Copy = GameObject.Instantiate(obj3);

            dontAddObj = new GameObject("DontAddObj");
        }
        [TearDown]
        public void TearDown()
        {
            if (objPool) GameObject.DestroyImmediate(objPool.gameObject);
            objPool = null;

            if (obj) GameObject.DestroyImmediate(obj);
            obj = null;
            if (obj2) GameObject.DestroyImmediate(obj2);
            obj2 = null;
            if (obj3) GameObject.DestroyImmediate(obj3);
            obj3 = null;
            if (objCopy) GameObject.DestroyImmediate(objCopy);
            objCopy = null;
            if (obj2Copy) GameObject.DestroyImmediate(obj2Copy);
            obj2Copy = null;
            if (obj3Copy) GameObject.DestroyImmediate(obj3Copy);
            obj3Copy = null;

            if (dontAddObj) GameObject.DestroyImmediate(dontAddObj);
            dontAddObj = null;

            returnedObjs = new List<GameObject>();
            returnedTrans = new List<Transform>();
            returnedObj = null;
        }

        private void AddObjects()
        {
            Assert.IsTrue(objPool.AddObject(obj));
            Assert.IsTrue(objPool.AddObject(obj2));
            Assert.IsTrue(objPool.AddObject(obj3));
            Assert.IsTrue(objPool.AddObject(objCopy));
            Assert.IsTrue(objPool.AddObject(obj2Copy));
            Assert.IsTrue(objPool.AddObject(obj3Copy));
        }
    }
}
