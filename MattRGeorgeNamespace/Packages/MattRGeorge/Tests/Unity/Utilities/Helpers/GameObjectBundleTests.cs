﻿using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using MattRGeorge.General.Utilities.Static;
using MattRGeorge.Unity.Utilities.Helpers;

namespace MattRGeorge.Tests.Unity.Utilities.Helpers
{
    public class GameObjectBundleTests
    {
        private GameObjectBundle objBundle = new GameObjectBundle();
        private List<GameObject> spawnedObjs = new List<GameObject>();
        private List<GameObject> testObjs = new List<GameObject>();

        [Test]
        public void Objs_Tests()
        {
            objBundle.Objs = testObjs;
            List<GameObject> returnObjs = (List<GameObject>)ReflectionUtility.GetInstanceField(typeof(GameObjectBundle), objBundle, "objs");
            Assert.IsNotNull(returnObjs);
            Assert.IsNotNull(objBundle.Objs);
            CollectionAssert.AreEqual(testObjs, returnObjs);
            CollectionAssert.AreEqual(testObjs, objBundle.Objs);

            GameObject newObj = new GameObject();
            spawnedObjs.Add(newObj);
            objBundle.Objs.Add(newObj);
            returnObjs = (List<GameObject>)ReflectionUtility.GetInstanceField(typeof(GameObjectBundle), objBundle, "objs");
            Assert.IsNotNull(returnObjs);
            Assert.IsNotNull(objBundle.Objs);
            Assert.AreEqual(testObjs.Count + 1, returnObjs.Count);
            Assert.AreEqual(testObjs.Count + 1, objBundle.Objs.Count);
            Assert.AreEqual(newObj, returnObjs[returnObjs.Count - 1]);
            Assert.AreEqual(newObj, objBundle.Objs[objBundle.Objs.Count - 1]);
        }
        [Test]
        public void Objs_InvalidInput_Tests()
        {
            objBundle.Objs = null;
            List<GameObject> returnObjs = (List<GameObject>)ReflectionUtility.GetInstanceField(typeof(GameObjectBundle), objBundle, "objs");
            Assert.IsNotNull(returnObjs);
            Assert.IsNotNull(objBundle.Objs);
            CollectionAssert.IsEmpty(returnObjs);
            CollectionAssert.IsEmpty(objBundle.Objs);

            testObjs[2] = null;
            objBundle.Objs = testObjs;
            returnObjs = (List<GameObject>)ReflectionUtility.GetInstanceField(typeof(GameObjectBundle), objBundle, "objs");
            Assert.IsNotNull(returnObjs);
            Assert.IsNotNull(objBundle.Objs);
            CollectionAssert.AllItemsAreNotNull(returnObjs);
            CollectionAssert.AllItemsAreNotNull(objBundle.Objs);
            Assert.AreEqual(testObjs.Count - 1, returnObjs.Count);
            Assert.AreEqual(testObjs.Count - 1, objBundle.Objs.Count);
        }

        [Test]
        public void CreateNewGameObject_Tests()
        {
            GameObject newObjA = objBundle.CreateNewGameObject("ObjA");
            spawnedObjs.Add(newObjA);
            Assert.IsTrue(newObjA);
            Assert.AreEqual("ObjA", newObjA.name);
            List<GameObject> returnObjs = objBundle.Objs;
            Assert.IsNotNull(returnObjs);
            Assert.AreEqual(1, returnObjs.Count);
            Assert.AreEqual(newObjA, returnObjs[0]);

            GameObject newObjB = objBundle.CreateNewGameObject("ObjB");
            spawnedObjs.Add(newObjA);
            Assert.IsTrue(newObjB);
            Assert.AreEqual("ObjB", newObjB.name);
            returnObjs = objBundle.Objs;
            Assert.IsNotNull(returnObjs);
            Assert.AreEqual(2, returnObjs.Count);
            Assert.AreEqual(newObjA, returnObjs[0]);
            Assert.AreEqual(newObjB, returnObjs[1]);
        }
        [Test]
        public void CreateNewGameObject_InvalidInput_Tests()
        {
            GameObject newObjA = objBundle.CreateNewGameObject(null);
            spawnedObjs.Add(newObjA);
            Assert.IsTrue(newObjA);
            List<GameObject> returnObjs = objBundle.Objs;
            Assert.IsNotNull(returnObjs);
            Assert.AreEqual(1, returnObjs.Count);
            Assert.AreEqual(newObjA, returnObjs[0]);

            GameObject newObjB = objBundle.CreateNewGameObject("");
            spawnedObjs.Add(newObjB);
            Assert.IsTrue(newObjB);
            returnObjs = objBundle.Objs;
            Assert.IsNotNull(returnObjs);
            Assert.AreEqual(2, returnObjs.Count);
            Assert.AreEqual(newObjA, returnObjs[0]);
            Assert.AreEqual(newObjB, returnObjs[1]);
        }

        [Test]
        public void DestroyObjs_Immediate_Tests()
        {
            objBundle.Objs = testObjs;
            objBundle.DestroyObjs(true);
            Assert.IsNotNull(objBundle.Objs);
            CollectionAssert.IsEmpty(objBundle.Objs);
            testObjs.ForEach(x => Assert.IsFalse(x));
            
            GameObject newObjA = objBundle.CreateNewGameObject("ObjA");
            spawnedObjs.Add(newObjA);
            objBundle.DestroyObjs(true);
            Assert.IsNotNull(objBundle.Objs);
            CollectionAssert.IsEmpty(objBundle.Objs);
            Assert.IsFalse(newObjA);
        }

        [SetUp]
        public void Setup()
        {
            for(int i = 0; i < 5; i++)
            {
                testObjs.Add(new GameObject($"Obj-{i + 1}"));
                spawnedObjs.Add(testObjs[i]);
            }
        }
        [TearDown]
        public void TearDown()
        {
            objBundle = new GameObjectBundle();

            foreach(Object obj in spawnedObjs)
            {
                if (obj) GameObject.DestroyImmediate(obj);
            }
            spawnedObjs = new List<GameObject>();
            testObjs = new List<GameObject>();
        }
    }
}
