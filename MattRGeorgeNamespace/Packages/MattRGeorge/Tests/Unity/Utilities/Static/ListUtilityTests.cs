﻿using MattRGeorge.General.Utilities.Enums;
using MattRGeorge.Unity.Utilities.Helpers;
using MattRGeorge.Unity.Utilities.Static;
using NUnit.Framework;
using System.Collections.Generic;
using UnityEngine;

namespace MattRGeorge.Tests.Unity.Utilities.Static
{
    public class ListUtilityTests
    {
        private string debugMSG = "";
        private List<GameObject> spawnedObjs = new List<GameObject>();

        [Test]
        public void RemoveNullEntries_Tests()
        {
            List<int> listNums = new List<int>()
            {
                0,
                1,
                2,
                3
            };
            listNums = ListUtility.RemoveNullEntries<int>(listNums);
            Assert.IsNotNull(listNums);
            Assert.AreEqual(4, listNums.Count);
            Assert.AreEqual(0, listNums[0]);
            Assert.AreEqual(1, listNums[1]);
            Assert.AreEqual(2, listNums[2]);
            Assert.AreEqual(3, listNums[3]);

            List<int?> listNullableNums = new List<int?>()
            {
                0,
                null,
                2,
                3
            };
            listNullableNums = ListUtility.RemoveNullEntries<int?>(listNullableNums);
            Assert.IsNotNull(listNullableNums);
            Assert.AreEqual(3, listNullableNums.Count);
            Assert.AreEqual(0, listNullableNums[0]);
            Assert.AreEqual(2, listNullableNums[1]);
            Assert.AreEqual(3, listNullableNums[2]);

            listNullableNums = new List<int?>()
            {
                null,
                1,
                null,
                3
            };
            listNullableNums = ListUtility.RemoveNullEntries<int?>(listNullableNums);
            Assert.IsNotNull(listNullableNums);
            Assert.AreEqual(2, listNullableNums.Count);
            Assert.AreEqual(1, listNullableNums[0]);
            Assert.AreEqual(3, listNullableNums[1]);

            listNullableNums = new List<int?>()
            {
                null,
                null,
                null,
                null
            };
            listNullableNums = ListUtility.RemoveNullEntries<int?>(listNullableNums);
            Assert.IsNotNull(listNullableNums);
            Assert.IsEmpty(listNullableNums);
        }
        [Test]
        public void RemoveNullEntries_GameObject_Tests()
        {
            List<GameObject> listObjs = new List<GameObject>()
            {
                new GameObject("ObjA"),
                new GameObject("ObjB"),
                new GameObject("ObjC")
            };
            listObjs.ForEach(x => spawnedObjs.Add(x));
            List<GameObject> returnedListObjs = ListUtility.RemoveNullEntries(listObjs);
            Assert.IsNotNull(returnedListObjs);
            Assert.AreEqual(3, returnedListObjs.Count);
            Assert.AreEqual(listObjs[0], returnedListObjs[0]);
            Assert.AreEqual(listObjs[1], returnedListObjs[1]);
            Assert.AreEqual(listObjs[2], returnedListObjs[2]);

            listObjs = new List<GameObject>()
            {
                new GameObject("ObjA"),
                null,
                new GameObject("ObjB"),
                new GameObject("ObjC")
            };
            listObjs.ForEach(x => spawnedObjs.Add(x));
            returnedListObjs = ListUtility.RemoveNullEntries(listObjs);
            Assert.IsNotNull(returnedListObjs);
            Assert.AreEqual(3, returnedListObjs.Count);
            Assert.AreEqual(listObjs[0], returnedListObjs[0]);
            Assert.AreEqual(listObjs[2], returnedListObjs[1]);
            Assert.AreEqual(listObjs[3], returnedListObjs[2]);

            listObjs = new List<GameObject>()
            {
                new GameObject("ObjA"),
                new GameObject("ObjB"), // Destroyed
                new GameObject("ObjC")
            };
            GameObject.DestroyImmediate(listObjs[1]);
            listObjs.ForEach(x => spawnedObjs.Add(x));
            returnedListObjs = ListUtility.RemoveNullEntries(listObjs);
            Assert.IsNotNull(returnedListObjs);
            Assert.AreEqual(2, returnedListObjs.Count);
            Assert.AreEqual(listObjs[0], returnedListObjs[0]);
            Assert.AreEqual(listObjs[2], returnedListObjs[1]);

            listObjs = new List<GameObject>()
            {
                new GameObject("ObjA"),
                null,
                new GameObject("ObjB"),
                new GameObject("ObjC") // Destroyed
            };
            GameObject.DestroyImmediate(listObjs[3]);
            listObjs.ForEach(x => spawnedObjs.Add(x));
            returnedListObjs = ListUtility.RemoveNullEntries(listObjs);
            Assert.IsNotNull(returnedListObjs);
            Assert.AreEqual(2, returnedListObjs.Count);
            Assert.AreEqual(listObjs[0], returnedListObjs[0]);
            Assert.AreEqual(listObjs[2], returnedListObjs[1]);

            listObjs = new List<GameObject>()
            {
                new GameObject("ObjA"), // Destroyed
                null,
                new GameObject("ObjB"), // Destroyed
                null
            };
            GameObject.DestroyImmediate(listObjs[0]);
            GameObject.DestroyImmediate(listObjs[2]);
            listObjs.ForEach(x => spawnedObjs.Add(x));
            returnedListObjs = ListUtility.RemoveNullEntries(listObjs);
            Assert.IsNotNull(returnedListObjs);
            Assert.IsEmpty(returnedListObjs);
        }
        [Test]
        public void RemoveNullEntries_InvalidInputs_Tests()
        {
            List<int?> nullList = null;
            List<int?> list = ListUtility.RemoveNullEntries<int?>(nullList);
            Assert.IsNull(list);

            list = new List<int?>();
            list = ListUtility.RemoveNullEntries<int?>(list);
            Assert.IsNotNull(list);
            Assert.IsEmpty(list);
        }
        [Test]
        public void RemoveNullEntries_Dictionary_Tests()
        {
            Dictionary<int?, int?> dict = new Dictionary<int?, int?>()
            {
                {0, 0 },
                {10, 5 },
                {20, 10 },
                {30, 15 },
                {40, 20 }
            };
            Dictionary<int?, int?> returnedDict = ListUtility.RemoveNullEntries(dict);
            Assert.IsNotNull(returnedDict);
            CollectionAssert.AreEqual(dict, returnedDict);

            dict[10] = null;
            returnedDict = ListUtility.RemoveNullEntries(dict);
            Assert.IsNotNull(returnedDict);
            Assert.AreEqual(4, returnedDict.Count);
            Assert.IsTrue(returnedDict.ContainsKey(0));
            Assert.IsFalse(returnedDict.ContainsKey(10));
            Assert.IsTrue(returnedDict.ContainsKey(20));
            Assert.IsTrue(returnedDict.ContainsKey(30));
            Assert.IsTrue(returnedDict.ContainsKey(40));
            Assert.AreEqual(dict[0], returnedDict[0]);
            Assert.AreEqual(dict[20], returnedDict[20]);
            Assert.AreEqual(dict[30], returnedDict[30]);
            Assert.AreEqual(dict[40], returnedDict[40]);

            dict[30] = null;
            returnedDict = ListUtility.RemoveNullEntries(dict);
            Assert.IsNotNull(returnedDict);
            Assert.AreEqual(3, returnedDict.Count);
            Assert.IsTrue(returnedDict.ContainsKey(0));
            Assert.IsFalse(returnedDict.ContainsKey(10));
            Assert.IsTrue(returnedDict.ContainsKey(20));
            Assert.IsFalse(returnedDict.ContainsKey(30));
            Assert.IsTrue(returnedDict.ContainsKey(40));
            Assert.AreEqual(dict[0], returnedDict[0]);
            Assert.AreEqual(dict[20], returnedDict[20]);
            Assert.AreEqual(dict[40], returnedDict[40]);

            dict[0] = null;
            dict[20] = null;
            dict[40] = null;
            returnedDict = ListUtility.RemoveNullEntries(dict);
            Assert.IsNotNull(returnedDict);
            Assert.IsEmpty(returnedDict);
        }
        [Test]
        public void RemoveNullEntries_Dictionary_GameObject_Tests()
        {
            Dictionary<int?, GameObject> dict = new Dictionary<int?, GameObject>()
            {
                {0, new GameObject("ObjA") },
                {10, new GameObject("ObjB") },
                {20, new GameObject("ObjC") },
                {30, new GameObject("ObjD") },
                {40, new GameObject("ObjE") }
            };
            foreach (KeyValuePair<int?, GameObject> pair in dict) spawnedObjs.Add(pair.Value);
            Dictionary<int?, GameObject> returnedDict = ListUtility.RemoveNullEntries(dict);
            Assert.IsNotNull(returnedDict);
            CollectionAssert.AreEqual(dict, returnedDict);

            dict[10] = null;
            returnedDict = ListUtility.RemoveNullEntries(dict);
            Assert.IsNotNull(returnedDict);
            Assert.AreEqual(4, returnedDict.Count);
            Assert.IsTrue(returnedDict.ContainsKey(0));
            Assert.IsFalse(returnedDict.ContainsKey(10));
            Assert.IsTrue(returnedDict.ContainsKey(20));
            Assert.IsTrue(returnedDict.ContainsKey(30));
            Assert.IsTrue(returnedDict.ContainsKey(40));
            Assert.AreEqual(dict[0], returnedDict[0]);
            Assert.AreEqual(dict[20], returnedDict[20]);
            Assert.AreEqual(dict[30], returnedDict[30]);
            Assert.AreEqual(dict[40], returnedDict[40]);

            GameObject.DestroyImmediate(dict[30]);
            returnedDict = ListUtility.RemoveNullEntries(dict);
            Assert.IsNotNull(returnedDict);
            Assert.AreEqual(3, returnedDict.Count);
            Assert.IsTrue(returnedDict.ContainsKey(0));
            Assert.IsFalse(returnedDict.ContainsKey(10));
            Assert.IsTrue(returnedDict.ContainsKey(20));
            Assert.IsFalse(returnedDict.ContainsKey(30));
            Assert.IsTrue(returnedDict.ContainsKey(40));
            Assert.AreEqual(dict[0], returnedDict[0]);
            Assert.AreEqual(dict[20], returnedDict[20]);
            Assert.AreEqual(dict[40], returnedDict[40]);

            GameObject.DestroyImmediate(dict[0]);
            dict[20] = null;
            GameObject.DestroyImmediate(dict[40]);
            returnedDict = ListUtility.RemoveNullEntries(dict);
            Assert.IsNotNull(returnedDict);
            Assert.IsEmpty(returnedDict);

            List<GameObject> keys = new List<GameObject>()
            {
                new GameObject("KeyA"),
                new GameObject("KeyB"),
                new GameObject("KeyC"),
                new GameObject("KeyD"),
                new GameObject("KeyE")
            };
            Dictionary<GameObject, GameObject> dictGO = new Dictionary<GameObject, GameObject>()
            {
                {keys[0], new GameObject("ObjA") },
                {keys[1], new GameObject("ObjB") },
                {keys[2], new GameObject("ObjC") },
                {keys[3], new GameObject("ObjD") },
                {keys[4], new GameObject("ObjE") }
            };
            keys.ForEach(x => spawnedObjs.Add(x));
            foreach (KeyValuePair<GameObject, GameObject> pair in dictGO)
            {
                spawnedObjs.Add(pair.Key);
                spawnedObjs.Add(pair.Value);
            }
            Dictionary<GameObject, GameObject> returnedDictGO = ListUtility.RemoveNullEntries(dictGO);
            Assert.IsNotNull(returnedDictGO);
            CollectionAssert.AreEqual(dictGO, returnedDictGO);

            dictGO[keys[1]] = null;
            returnedDictGO = ListUtility.RemoveNullEntries(dictGO);
            Assert.IsNotNull(returnedDictGO);
            Assert.AreEqual(4, returnedDictGO.Count);
            Assert.IsTrue(returnedDictGO.ContainsKey(keys[0]));
            Assert.IsFalse(returnedDictGO.ContainsKey(keys[1]));
            Assert.IsTrue(returnedDictGO.ContainsKey(keys[2]));
            Assert.IsTrue(returnedDictGO.ContainsKey(keys[3]));
            Assert.IsTrue(returnedDictGO.ContainsKey(keys[4]));
            Assert.AreEqual(dictGO[keys[0]], returnedDictGO[keys[0]]);
            Assert.AreEqual(dictGO[keys[2]], returnedDictGO[keys[2]]);
            Assert.AreEqual(dictGO[keys[3]], returnedDictGO[keys[3]]);
            Assert.AreEqual(dictGO[keys[4]], returnedDictGO[keys[4]]);

            GameObject.DestroyImmediate(dictGO[keys[3]]);
            returnedDictGO = ListUtility.RemoveNullEntries(dictGO);
            Assert.IsNotNull(returnedDictGO);
            Assert.AreEqual(3, returnedDictGO.Count);
            Assert.IsTrue(returnedDictGO.ContainsKey(keys[0]));
            Assert.IsFalse(returnedDictGO.ContainsKey(keys[1]));
            Assert.IsTrue(returnedDictGO.ContainsKey(keys[2]));
            Assert.IsFalse(returnedDictGO.ContainsKey(keys[3]));
            Assert.IsTrue(returnedDictGO.ContainsKey(keys[4]));
            Assert.AreEqual(dictGO[keys[0]], returnedDictGO[keys[0]]);
            Assert.AreEqual(dictGO[keys[2]], returnedDictGO[keys[2]]);
            Assert.AreEqual(dictGO[keys[4]], returnedDictGO[keys[4]]);

            GameObject.DestroyImmediate(dictGO[keys[0]]);
            dictGO[keys[2]] = null;
            GameObject.DestroyImmediate(dictGO[keys[4]]);
            returnedDictGO = ListUtility.RemoveNullEntries(dictGO);
            Assert.IsNotNull(returnedDictGO);
            Assert.IsEmpty(returnedDictGO);

            keys.Add(new GameObject("KeyF"));
            keys.Add(new GameObject("KeyG"));
            spawnedObjs.Add(new GameObject("ObjF"));
            spawnedObjs.Add(new GameObject("ObjG"));
            dictGO.Add(keys[5], spawnedObjs[spawnedObjs.Count - 2]);
            dictGO.Add(keys[6], spawnedObjs[spawnedObjs.Count - 1]);
            GameObject.DestroyImmediate(dictGO[keys[5]]);
            GameObject.DestroyImmediate(dictGO[keys[6]]);
            returnedDictGO = ListUtility.RemoveNullEntries(dictGO);
            Assert.IsNotNull(returnedDictGO);
            Assert.IsEmpty(returnedDictGO);
        }
        [Test]
        public void RemoveNullEntries_Dictionary_InvalidInput_Tests()
        {
            Dictionary<int?, int?> nullDict = null;
            Dictionary<int?, int?> dict = ListUtility.RemoveNullEntries(nullDict);
            Assert.IsNull(dict);

            dict = new Dictionary<int?, int?>();
            dict = ListUtility.RemoveNullEntries(dict);
            Assert.IsNotNull(dict);
            Assert.IsEmpty(dict);
        }

        [Test]
        public void RemoveNullEntriesFromDictionaryValue_Tests()
        {
            Dictionary<int?, List<int?>> dict = new Dictionary<int?, List<int?>>()
            {
                {0, new List<int?>(){ 0, 1, 2} },
                {10, new List<int?>(){ 3, 4} },
                {20, new List<int?>(){ 5, 6, 7} }
            };
            Dictionary<int?, List<int?>> returnedDict = ListUtility.RemoveNullEntriesFromDictionaryValue(dict);
            Assert.IsNotNull(returnedDict);
            CollectionAssert.AreEqual(dict, returnedDict);

            dict[0][0] = null;
            dict[10][1] = null;
            dict[20][0] = null;
            dict[20][1] = null;
            dict[20][2] = null;
            returnedDict = ListUtility.RemoveNullEntriesFromDictionaryValue(dict);
            Assert.IsNotNull(returnedDict);
            Assert.AreEqual(3, returnedDict.Count);
            Assert.IsTrue(returnedDict.ContainsKey(0));
            Assert.IsTrue(returnedDict.ContainsKey(10));
            Assert.IsTrue(returnedDict.ContainsKey(20));
            Assert.AreEqual(2, returnedDict[0].Count);
            Assert.AreEqual(1, returnedDict[10].Count);
            Assert.AreEqual(0, returnedDict[20].Count);
            Assert.AreEqual(1, returnedDict[0][0]);
            Assert.AreEqual(2, returnedDict[0][1]);
            Assert.AreEqual(3, returnedDict[10][0]);

            dict = new Dictionary<int?, List<int?>>()
            {
                {0, new List<int?>(){ 0, 1, 2} },
                {10, null },
                {20, new List<int?>(){ 5, 6, 7} }
            };
            returnedDict = ListUtility.RemoveNullEntriesFromDictionaryValue(dict);
            Assert.IsNotNull(returnedDict);
            Assert.AreEqual(2, returnedDict.Count);
            Assert.IsTrue(returnedDict.ContainsKey(0));
            Assert.IsFalse(returnedDict.ContainsKey(10));
            Assert.IsTrue(returnedDict.ContainsKey(20));
            CollectionAssert.AreEqual(dict[0], returnedDict[0]);
            CollectionAssert.AreEqual(dict[20], returnedDict[20]);

            dict[20] = new List<int?>();
            returnedDict = ListUtility.RemoveNullEntriesFromDictionaryValue(dict);
            Assert.IsNotNull(returnedDict);
            Assert.AreEqual(2, returnedDict.Count);
            Assert.IsTrue(returnedDict.ContainsKey(0));
            Assert.IsFalse(returnedDict.ContainsKey(10));
            Assert.IsTrue(returnedDict.ContainsKey(20));
            Assert.AreEqual(dict[0], returnedDict[0]);
            Assert.AreEqual(dict[20], returnedDict[20]);

            dict[0] = null;
            dict[20] = null;
            returnedDict = ListUtility.RemoveNullEntriesFromDictionaryValue(dict);
            Assert.IsNotNull(returnedDict);
            Assert.IsEmpty(returnedDict);
        }
        [Test]
        public void RemoveNullEntriesFromDictionaryValue_GameObject_Tests()
        {
            Dictionary<int?, List<GameObject>> dict = new Dictionary<int?, List<GameObject>>()
            {
                {0, new List<GameObject>(){ new GameObject("ObjAA"), new GameObject("ObjAB"), new GameObject("ObjAC") } },
                {10, new List<GameObject>(){ new GameObject("ObjBA"), new GameObject("ObjBB") } },
                {20, new List<GameObject>(){ new GameObject("ObjCA"), new GameObject("ObjCB"), new GameObject("ObjCC") } }
            };
            foreach(KeyValuePair<int?, List<GameObject>> pair in dict) pair.Value.ForEach(x => spawnedObjs.Add(x));
            Dictionary<int?, List<GameObject>> returnedDict = ListUtility.RemoveNullEntriesFromDictionaryValue(dict);
            Assert.IsNotNull(returnedDict);
            CollectionAssert.AreEqual(dict, returnedDict);

            dict[0][0] = null;
            GameObject.DestroyImmediate(dict[10][1]);
            dict[20][0] = null;
            GameObject.DestroyImmediate(dict[20][1]);
            dict[20][2] = null;
            returnedDict = ListUtility.RemoveNullEntriesFromDictionaryValue(dict);
            Assert.IsNotNull(returnedDict);
            Assert.AreEqual(3, returnedDict.Count);
            Assert.IsTrue(returnedDict.ContainsKey(0));
            Assert.IsTrue(returnedDict.ContainsKey(10));
            Assert.IsTrue(returnedDict.ContainsKey(20));
            Assert.AreEqual(2, returnedDict[0].Count);
            Assert.AreEqual(1, returnedDict[10].Count);
            Assert.AreEqual(0, returnedDict[20].Count);
            Assert.AreEqual(dict[0][1], returnedDict[0][0]);
            Assert.AreEqual(dict[0][2], returnedDict[0][1]);
            Assert.AreEqual(dict[10][0], returnedDict[10][0]);

            dict = new Dictionary<int?, List<GameObject>>()
            {
                {0, new List<GameObject>(){ new GameObject("ObjAA"), new GameObject("ObjAB"), new GameObject("ObjAC") } },
                {10, null },
                {20, new List<GameObject>(){ new GameObject("ObjCA"), new GameObject("ObjCB"), new GameObject("ObjCC") } }
            };
            foreach (KeyValuePair<int?, List<GameObject>> pair in dict)
            {
                if (pair.Value != null) pair.Value.ForEach(x => spawnedObjs.Add(x));
            }
            returnedDict = ListUtility.RemoveNullEntriesFromDictionaryValue(dict);
            Assert.IsNotNull(returnedDict);
            Assert.AreEqual(2, returnedDict.Count);
            Assert.IsTrue(returnedDict.ContainsKey(0));
            Assert.IsFalse(returnedDict.ContainsKey(10));
            Assert.IsTrue(returnedDict.ContainsKey(20));
            CollectionAssert.AreEqual(dict[0], returnedDict[0]);
            CollectionAssert.AreEqual(dict[20], returnedDict[20]);

            dict[20] = new List<GameObject>();
            returnedDict = ListUtility.RemoveNullEntriesFromDictionaryValue(dict);
            Assert.IsNotNull(returnedDict);
            Assert.AreEqual(2, returnedDict.Count);
            Assert.IsTrue(returnedDict.ContainsKey(0));
            Assert.IsFalse(returnedDict.ContainsKey(10));
            Assert.IsTrue(returnedDict.ContainsKey(20));
            Assert.AreEqual(dict[0], returnedDict[0]);
            Assert.AreEqual(dict[20], returnedDict[20]);

            dict[0] = null;
            dict[20] = null;
            returnedDict = ListUtility.RemoveNullEntriesFromDictionaryValue(dict);
            Assert.IsNotNull(returnedDict);
            Assert.IsEmpty(returnedDict);

            List<GameObject> keys = new List<GameObject>()
            {
                new GameObject("KeyA"),
                new GameObject("KeyB"),
                new GameObject("KeyC")
            };
            keys.ForEach(x => spawnedObjs.Add(x));
            Dictionary<GameObject, List<GameObject>> dictGO = new Dictionary<GameObject, List<GameObject>>()
            {
                {keys[0], new List<GameObject>(){ new GameObject("ObjAA"), new GameObject("ObjAB"), new GameObject("ObjAC") } },
                {keys[1], new List<GameObject>(){ new GameObject("ObjBA"), new GameObject("ObjBB") } },
                {keys[2], new List<GameObject>(){ new GameObject("ObjCA"), new GameObject("ObjCB"), new GameObject("ObjCC") } }
            };
            foreach (KeyValuePair<GameObject, List<GameObject>> pair in dictGO) pair.Value.ForEach(x => spawnedObjs.Add(x));
            Dictionary<GameObject, List<GameObject>> returnedDictGO = ListUtility.RemoveNullEntriesFromDictionaryValue(dictGO);
            Assert.IsNotNull(returnedDictGO);
            CollectionAssert.AreEqual(dictGO, returnedDictGO);

            GameObject.DestroyImmediate(keys[1]);
            GameObject.DestroyImmediate(dictGO[keys[0]][1]);
            dictGO[keys[0]][0] = null;
            GameObject.DestroyImmediate(dictGO[keys[1]][1]);
            GameObject.DestroyImmediate(dictGO[keys[2]][1]);
            dictGO[keys[2]][0] = null;
            returnedDictGO = ListUtility.RemoveNullEntriesFromDictionaryValue(dictGO);
            Assert.IsNotNull(returnedDictGO);
            Assert.AreEqual(2, returnedDictGO.Count);
            Assert.IsTrue(returnedDictGO.ContainsKey(keys[0]));
            Assert.IsFalse(returnedDictGO.ContainsKey(keys[1]));
            Assert.IsTrue(returnedDictGO.ContainsKey(keys[2]));
            Assert.AreEqual(1, returnedDictGO[keys[0]].Count);
            Assert.AreEqual(1, returnedDictGO[keys[2]].Count);
            Assert.AreEqual(dictGO[keys[0]][2], returnedDictGO[keys[0]][0]);
            Assert.AreEqual(dictGO[keys[2]][2], returnedDictGO[keys[2]][0]);

            GameObject.DestroyImmediate(keys[0]);
            GameObject.DestroyImmediate(keys[2]);
            returnedDictGO = ListUtility.RemoveNullEntriesFromDictionaryValue(dictGO);
            Assert.IsNotNull(returnedDictGO);
            CollectionAssert.IsEmpty(returnedDictGO);
        }
        [Test]
        public void RemoveNullEntriesFromDictionaryValue_InvalidInput_Tests()
        {
            Dictionary<int?, List<int?>> nullDict = null;
            Dictionary<int?, List<int?>> dict = ListUtility.RemoveNullEntriesFromDictionaryValue(nullDict);
            Assert.IsNull(dict);

            dict = new Dictionary<int?, List<int?>>();
            dict = ListUtility.RemoveNullEntriesFromDictionaryValue(dict);
            Assert.IsNotNull(dict);
            Assert.IsEmpty(dict);
        }

        [Test]
        public void GetRandomObjectByMode_RandomlyMode_Tests()
        {
            ListRandomChoiceMode mode = ListRandomChoiceMode.Randomly;
            List<int?> nums = new List<int?>()
            {
                0,
                1,
                2,
                3,
                4,
                5
            };
            HashSet<int> invalidIndexes = new HashSet<int>();

            debugMSG = "Chosen Numbers:\n";
            int? chosenNum = ListUtility.GetObjectByMode(mode, nums, ref invalidIndexes);
            Assert.IsNotNull(chosenNum);
            Assert.IsTrue(nums.Contains(chosenNum));
            debugMSG += $"{chosenNum}\n";

            chosenNum = ListUtility.GetObjectByMode(mode, nums, ref invalidIndexes);
            Assert.IsNotNull(chosenNum);
            Assert.IsTrue(nums.Contains(chosenNum));
            debugMSG += $"{chosenNum}\n";

            chosenNum = ListUtility.GetObjectByMode(mode, nums, ref invalidIndexes);
            Assert.IsNotNull(chosenNum);
            Assert.IsTrue(nums.Contains(chosenNum));
            debugMSG += $"{chosenNum}\n";

            Assert.Pass(debugMSG);
        }
        [Test]
        public void GetRandomObjectByMode_AllRandomlyMode_Tests()
        {
            ListRandomChoiceMode mode = ListRandomChoiceMode.AllRandomly;
            List<int?> nums = new List<int?>()
            {
                0,
                1,
                2,
                3
            };
            HashSet<int> invalidIndexes = new HashSet<int>();
            List<int?> pickedNums = new List<int?>();

            debugMSG = "Round 1 Chosen Numbers:\n";
            int? chosenNum = ListUtility.GetObjectByMode(mode, nums, ref invalidIndexes);
            pickedNums.Add(chosenNum);
            Assert.NotNull(pickedNums[pickedNums.Count - 1]);
            debugMSG += $"{chosenNum}\n";

            chosenNum = ListUtility.GetObjectByMode(mode, nums, ref invalidIndexes);
            Assert.IsTrue(!pickedNums.Contains(chosenNum));
            pickedNums.Add(chosenNum);
            Assert.NotNull(pickedNums[pickedNums.Count - 1]);
            debugMSG += $"{chosenNum}\n";

            chosenNum = ListUtility.GetObjectByMode(mode, nums, ref invalidIndexes);
            Assert.IsTrue(!pickedNums.Contains(chosenNum));
            pickedNums.Add(chosenNum);
            Assert.NotNull(pickedNums[pickedNums.Count - 1]);
            debugMSG += $"{chosenNum}\n";

            chosenNum = ListUtility.GetObjectByMode(mode, nums, ref invalidIndexes);
            Assert.IsTrue(!pickedNums.Contains(chosenNum));
            pickedNums.Add(chosenNum);
            Assert.NotNull(pickedNums[pickedNums.Count - 1]);
            debugMSG += $"{chosenNum}\n";

            debugMSG += "\nRound 2 Chosen Numbers:\n";
            pickedNums = new List<int?>();
            chosenNum = ListUtility.GetObjectByMode(mode, nums, ref invalidIndexes);
            Assert.IsTrue(!pickedNums.Contains(chosenNum));
            pickedNums.Add(chosenNum);
            Assert.NotNull(pickedNums[pickedNums.Count - 1]);
            debugMSG += $"{chosenNum}\n";

            chosenNum = ListUtility.GetObjectByMode(mode, nums, ref invalidIndexes);
            Assert.IsTrue(!pickedNums.Contains(chosenNum));
            pickedNums.Add(chosenNum);
            Assert.NotNull(pickedNums[pickedNums.Count - 1]);
            debugMSG += $"{chosenNum}\n";

            chosenNum = ListUtility.GetObjectByMode(mode, nums, ref invalidIndexes);
            Assert.IsTrue(!pickedNums.Contains(chosenNum));
            pickedNums.Add(chosenNum);
            Assert.NotNull(pickedNums[pickedNums.Count - 1]);
            debugMSG += $"{chosenNum}\n";

            chosenNum = ListUtility.GetObjectByMode(mode, nums, ref invalidIndexes);
            Assert.IsTrue(!pickedNums.Contains(chosenNum));
            pickedNums.Add(chosenNum);
            Assert.NotNull(pickedNums[pickedNums.Count - 1]);
            debugMSG += $"{chosenNum}\n";

            Assert.Pass(debugMSG);
        }
        [Test]
        public void GetRandomObjectByMode_AllSequentiallyMode_Tests()
        {
            ListRandomChoiceMode mode = ListRandomChoiceMode.AllSequentially;
            List<int?> nums = new List<int?>()
            {
                0,
                1,
                2,
                3
            };
            HashSet<int> invalidIndexes = new HashSet<int>();

            // Round 1
            int? chosenNum = ListUtility.GetObjectByMode(mode, nums, ref invalidIndexes);
            Assert.IsNotNull(chosenNum);
            Assert.AreEqual(0, chosenNum);

            chosenNum = ListUtility.GetObjectByMode(mode, nums, ref invalidIndexes);
            Assert.IsNotNull(chosenNum);
            Assert.AreEqual(1, chosenNum);

            chosenNum = ListUtility.GetObjectByMode(mode, nums, ref invalidIndexes);
            Assert.IsNotNull(chosenNum);
            Assert.AreEqual(2, chosenNum);

            chosenNum = ListUtility.GetObjectByMode(mode, nums, ref invalidIndexes);
            Assert.IsNotNull(chosenNum);
            Assert.AreEqual(3, chosenNum);

            // Round 2
            chosenNum = ListUtility.GetObjectByMode(mode, nums, ref invalidIndexes);
            Assert.IsNotNull(chosenNum);
            Assert.AreEqual(0, chosenNum);

            chosenNum = ListUtility.GetObjectByMode(mode, nums, ref invalidIndexes);
            Assert.IsNotNull(chosenNum);
            Assert.AreEqual(1, chosenNum);

            chosenNum = ListUtility.GetObjectByMode(mode, nums, ref invalidIndexes);
            Assert.IsNotNull(chosenNum);
            Assert.AreEqual(2, chosenNum);

            chosenNum = ListUtility.GetObjectByMode(mode, nums, ref invalidIndexes);
            Assert.IsNotNull(chosenNum);
            Assert.AreEqual(3, chosenNum);
        }

        [Test]
        public void GetRandomObjectByMode_RandomlyMode_InvalidInputs_Tests()
        {
            ListRandomChoiceMode mode = ListRandomChoiceMode.Randomly;
            HashSet<int> invalidIndexes = null;
            int? num = ListUtility.GetObjectByMode<int?>(mode, null, ref invalidIndexes);
            Assert.IsNull(num);

            num = ListUtility.GetObjectByMode(mode, new List<int?>(), ref invalidIndexes);
            Assert.IsNull(num);

            invalidIndexes = new HashSet<int>();
            num = ListUtility.GetObjectByMode(mode, new List<int?>(), ref invalidIndexes);
            Assert.IsNull(num);
        }
        [Test]
        public void GetRandomObjectByMode_AllRandomlyMode_InvalidInputs_Tests()
        {
            ListRandomChoiceMode mode = ListRandomChoiceMode.AllRandomly;
            HashSet<int> invalidIndexes = null;
            int? num = ListUtility.GetObjectByMode<int?>(mode, null, ref invalidIndexes);
            Assert.IsNull(num);

            num = ListUtility.GetObjectByMode(mode, new List<int?>(), ref invalidIndexes);
            Assert.IsNull(num);

            invalidIndexes = new HashSet<int>();
            num = ListUtility.GetObjectByMode(mode, new List<int?>(), ref invalidIndexes);
            Assert.IsNull(num);
        }
        [Test]
        public void GetRandomObjectByMode_AllSequentiallyMode_InvalidInputs_Tests()
        {
            ListRandomChoiceMode mode = ListRandomChoiceMode.AllSequentially;
            HashSet<int> invalidIndexes = null;
            int? num = ListUtility.GetObjectByMode<int?>(mode, null, ref invalidIndexes);
            Assert.IsNull(num);

            num = ListUtility.GetObjectByMode(mode, new List<int?>(), ref invalidIndexes);
            Assert.IsNull(num);

            invalidIndexes = new HashSet<int>();
            num = ListUtility.GetObjectByMode(mode, new List<int?>(), ref invalidIndexes);
            Assert.IsNull(num);
        }

        [Test]
        public void GetValidRandomObject_Tests()
        {
            List<int?> nums = new List<int?>()
            {
                0,
                1,
                2,
                3,
                4,
                5
            };
            HashSet<int> invalidIndexes = new HashSet<int>();
            List<int?> pickedNums = new List<int?>();

            debugMSG = "Chosen Numbers:\n";
            int? chosenNum = ListUtility.GetValidRandomObject(nums, ref invalidIndexes);
            pickedNums.Add(chosenNum);
            Assert.NotNull(pickedNums[pickedNums.Count - 1]);
            debugMSG += $"{chosenNum}\n";

            chosenNum = ListUtility.GetValidRandomObject(nums, ref invalidIndexes);
            Assert.IsTrue(!pickedNums.Contains(chosenNum));
            pickedNums.Add(chosenNum);
            Assert.NotNull(pickedNums[pickedNums.Count - 1]);
            debugMSG += $"{chosenNum}\n";

            chosenNum = ListUtility.GetValidRandomObject(nums, ref invalidIndexes);
            Assert.IsTrue(!pickedNums.Contains(chosenNum));
            pickedNums.Add(chosenNum);
            Assert.NotNull(pickedNums[pickedNums.Count - 1]);
            debugMSG += $"{chosenNum}\n";

            chosenNum = ListUtility.GetValidRandomObject(nums, ref invalidIndexes);
            Assert.IsTrue(!pickedNums.Contains(chosenNum));
            pickedNums.Add(chosenNum);
            Assert.NotNull(pickedNums[pickedNums.Count - 1]);
            debugMSG += $"{chosenNum}\n";

            chosenNum = ListUtility.GetValidRandomObject(nums, ref invalidIndexes);
            Assert.IsTrue(!pickedNums.Contains(chosenNum));
            pickedNums.Add(chosenNum);
            Assert.NotNull(pickedNums[pickedNums.Count - 1]);
            debugMSG += $"{chosenNum}\n";

            chosenNum = ListUtility.GetValidRandomObject(nums, ref invalidIndexes);
            Assert.IsTrue(!pickedNums.Contains(chosenNum));
            pickedNums.Add(chosenNum);
            Assert.NotNull(pickedNums[pickedNums.Count - 1]);
            debugMSG += $"{chosenNum}\n";

            chosenNum = ListUtility.GetValidRandomObject(nums, ref invalidIndexes);
            Assert.IsNull(chosenNum);

            Assert.Pass(debugMSG);
        }
        [Test]
        public void GetValidRandomObject_InvalidInputs_Tests()
        {
            HashSet<int> invalidIndexes = null;
            int? num = ListUtility.GetValidRandomObject<int?>(null, ref invalidIndexes);
            Assert.IsNull(num);

            num = ListUtility.GetValidRandomObject(new List<int?>(), ref invalidIndexes);
            Assert.IsNull(num);

            invalidIndexes = new HashSet<int>();
            num = ListUtility.GetValidRandomObject(new List<int?>(), ref invalidIndexes);
            Assert.IsNull(num);
        }

        [Test]
        public void GetRandomObjectWithChance_Tests()
        {
            List<DictionaryTObjectChance<int?>> nums = new List<DictionaryTObjectChance<int?>>()
            {
                new DictionaryTObjectChance<int?>()
                {
                    obj = 0,
                    chance = 1
                },
                new DictionaryTObjectChance<int?>()
                {
                    obj = 1,
                    chance = 3
                },
                new DictionaryTObjectChance<int?>()
                {
                    obj = 2,
                    chance = 6
                },
                new DictionaryTObjectChance<int?>()
                {
                    obj = 3,
                    chance = 0
                },
                new DictionaryTObjectChance<int?>()
                {
                    obj = 4,
                    chance = 2
                }
            };
            List<int?> pickedNums = new List<int?>();

            debugMSG = "Chosen Numbers (No Invalid Indexes):\n";
            int? chosenNum = ListUtility.GetRandomObjectWithChance(nums);
            pickedNums.Add(chosenNum);
            Assert.AreEqual(1, pickedNums.Count);
            Assert.IsFalse(pickedNums.Contains(3));
            Assert.NotNull(pickedNums[pickedNums.Count - 1]);
            debugMSG += $"{chosenNum}\n";

            chosenNum = ListUtility.GetRandomObjectWithChance(nums);
            pickedNums.Add(chosenNum);
            Assert.AreEqual(2, pickedNums.Count);
            Assert.IsFalse(pickedNums.Contains(3));
            Assert.NotNull(pickedNums[pickedNums.Count - 1]);
            debugMSG += $"{chosenNum}\n";

            chosenNum = ListUtility.GetRandomObjectWithChance(nums);
            pickedNums.Add(chosenNum);
            Assert.AreEqual(3, pickedNums.Count);
            Assert.IsFalse(pickedNums.Contains(3));
            Assert.NotNull(pickedNums[pickedNums.Count - 1]);
            debugMSG += $"{chosenNum}\n";

            chosenNum = ListUtility.GetRandomObjectWithChance(nums);
            pickedNums.Add(chosenNum);
            Assert.AreEqual(4, pickedNums.Count);
            Assert.IsFalse(pickedNums.Contains(3));
            Assert.NotNull(pickedNums[pickedNums.Count - 1]);
            debugMSG += $"{chosenNum}\n";

            chosenNum = ListUtility.GetRandomObjectWithChance(nums);
            pickedNums.Add(chosenNum);
            Assert.AreEqual(5, pickedNums.Count);
            Assert.IsFalse(pickedNums.Contains(3));
            Assert.NotNull(pickedNums[pickedNums.Count - 1]);
            debugMSG += $"{chosenNum}\n";

            debugMSG += "\nChosen Numbers (Invalid Indexes):\n";
            pickedNums = new List<int?>();
            HashSet<int> invalidIndexes = new HashSet<int>();
            chosenNum = ListUtility.GetRandomObjectWithChance(nums, ref invalidIndexes);
            Assert.NotNull(chosenNum);
            Assert.IsFalse(pickedNums.Contains(chosenNum));
            Assert.IsFalse(pickedNums.Contains(3));
            pickedNums.Add(chosenNum);
            debugMSG += $"{chosenNum}\n";

            chosenNum = ListUtility.GetRandomObjectWithChance(nums, ref invalidIndexes);
            Assert.NotNull(chosenNum);
            Assert.IsFalse(pickedNums.Contains(chosenNum));
            Assert.IsFalse(pickedNums.Contains(3));
            pickedNums.Add(chosenNum);
            debugMSG += $"{chosenNum}\n";

            chosenNum = ListUtility.GetRandomObjectWithChance(nums, ref invalidIndexes);
            Assert.NotNull(chosenNum);
            Assert.IsFalse(pickedNums.Contains(chosenNum));
            Assert.IsFalse(pickedNums.Contains(3));
            pickedNums.Add(chosenNum);
            debugMSG += $"{chosenNum}\n";

            chosenNum = ListUtility.GetRandomObjectWithChance(nums, ref invalidIndexes);
            Assert.NotNull(chosenNum);
            Assert.IsFalse(pickedNums.Contains(chosenNum));
            Assert.IsFalse(pickedNums.Contains(3));
            pickedNums.Add(chosenNum);
            debugMSG += $"{chosenNum}\n";

            chosenNum = ListUtility.GetRandomObjectWithChance(nums, ref invalidIndexes);
            Assert.IsNull(chosenNum);

            Assert.Pass(debugMSG);
        }
        [Test]
        public void GetRandomObjectWithChance_AllChancesZero_Tests()
        {
            List<DictionaryTObjectChance<int?>> nums = new List<DictionaryTObjectChance<int?>>()
            {
                new DictionaryTObjectChance<int?>()
                {
                    obj = 0,
                    chance = 0
                },
                new DictionaryTObjectChance<int?>()
                {
                    obj = 1,
                    chance = 0
                },
                new DictionaryTObjectChance<int?>()
                {
                    obj = 2,
                    chance = 0
                },
                new DictionaryTObjectChance<int?>()
                {
                    obj = 3,
                    chance = 0
                },
                new DictionaryTObjectChance<int?>()
                {
                    obj = 4,
                    chance = 0
                }
            };

            // Without invalid indexes
            int? chosenNum = ListUtility.GetRandomObjectWithChance(nums);
            Assert.IsNull(chosenNum);

            chosenNum = ListUtility.GetRandomObjectWithChance(nums);
            Assert.IsNull(chosenNum);

            chosenNum = ListUtility.GetRandomObjectWithChance(nums);
            Assert.IsNull(chosenNum);

            chosenNum = ListUtility.GetRandomObjectWithChance(nums);
            Assert.IsNull(chosenNum);

            chosenNum = ListUtility.GetRandomObjectWithChance(nums);
            Assert.IsNull(chosenNum);

            // With invalid indexes
            HashSet<int> invalidIndexes = new HashSet<int>();
            chosenNum = ListUtility.GetRandomObjectWithChance(nums, ref invalidIndexes);
            Assert.IsNull(chosenNum);

            chosenNum = ListUtility.GetRandomObjectWithChance(nums, ref invalidIndexes);
            Assert.IsNull(chosenNum);

            chosenNum = ListUtility.GetRandomObjectWithChance(nums, ref invalidIndexes);
            Assert.IsNull(chosenNum);

            chosenNum = ListUtility.GetRandomObjectWithChance(nums, ref invalidIndexes);
            Assert.IsNull(chosenNum);

            chosenNum = ListUtility.GetRandomObjectWithChance(nums, ref invalidIndexes);
            Assert.IsNull(chosenNum);
        }
        [Test]
        public void GetRandomObjectWithChance_InvalidInputs_Tests()
        {
            List<DictionaryTObjectChance<int?>> test = null;
            Assert.IsNull(ListUtility.GetRandomObjectWithChance(test));
            test = new List<DictionaryTObjectChance<int?>>();
            Assert.IsNull(ListUtility.GetRandomObjectWithChance(test));

            test = null;
            HashSet<int> usedIndexes = null;
            Assert.IsNull(ListUtility.GetRandomObjectWithChance(test, ref usedIndexes));
            test = new List<DictionaryTObjectChance<int?>>();
            usedIndexes = null;
            Assert.IsNull(ListUtility.GetRandomObjectWithChance(test, ref usedIndexes));
            test = null;
            usedIndexes = new HashSet<int>();
            Assert.IsNull(ListUtility.GetRandomObjectWithChance(test, ref usedIndexes));
            test = new List<DictionaryTObjectChance<int?>>();
            usedIndexes = new HashSet<int>();
            Assert.IsNull(ListUtility.GetRandomObjectWithChance(test, ref usedIndexes));
        }

        [Test]
        public void GetObjsWithChancesApplied_Tests()
        {
            List<DictionaryTObjectChance<float>> listWithChances = new List<DictionaryTObjectChance<float>>()
            {
                new DictionaryTObjectChance<float>()
                {
                    obj = 0,
                    chance = 1
                },
                new DictionaryTObjectChance<float>()
                {
                    obj = 1,
                    chance = 3
                },
                new DictionaryTObjectChance<float>()
                {
                    obj = 2,
                    chance = 6
                },
                new DictionaryTObjectChance<float>()
                {
                    obj = 3,
                    chance = 2
                }
            };

            List<float> listWithChancesApplied = ListUtility.GetObjsWithChancesApplied(listWithChances);
            Assert.IsNotNull(listWithChancesApplied);
            Assert.AreEqual(12, listWithChancesApplied.Count);

            Assert.AreEqual(0, listWithChancesApplied[0]);
            Assert.AreEqual(1, listWithChancesApplied[1]);
            Assert.AreEqual(1, listWithChancesApplied[3]);
            Assert.AreEqual(2, listWithChancesApplied[4]);
            Assert.AreEqual(2, listWithChancesApplied[9]);
            Assert.AreEqual(3, listWithChancesApplied[10]);
            Assert.AreEqual(3, listWithChancesApplied[11]);
        }
        [Test]
        public void GetObjsWithChancesApplied_InvalidInput_Tests()
        {
            List<DictionaryTObjectChance<float>> listWithChances = null;
            List<float> listWithChancesApplied = ListUtility.GetObjsWithChancesApplied(listWithChances);
            Assert.IsNotNull(listWithChancesApplied);

            listWithChances = new List<DictionaryTObjectChance<float>>();
            listWithChancesApplied = ListUtility.GetObjsWithChancesApplied(listWithChances);
            Assert.IsNotNull(listWithChancesApplied);
        }

        [Test]
        public void GetObjectWithChanceByChance_Tests()
        {
            List<DictionaryTObjectChance<int?>> nums = new List<DictionaryTObjectChance<int?>>()
            {
                new DictionaryTObjectChance<int?>()
                {
                    obj = 0,
                    chance = 1
                },
                new DictionaryTObjectChance<int?>()
                {
                    obj = 1,
                    chance = 3
                },
                new DictionaryTObjectChance<int?>()
                {
                    obj = 2,
                    chance = 6
                },
                new DictionaryTObjectChance<int?>()
                {
                    obj = 3,
                    chance = 0
                },
                new DictionaryTObjectChance<int?>()
                {
                    obj = 4,
                    chance = 2
                }
            };

            Assert.AreEqual(null, ListUtility.GetObjectWithChanceByChance<int?>(nums, -1));

            Assert.AreEqual(0, ListUtility.GetObjectWithChanceByChance<int?>(nums, 0));

            Assert.AreEqual(1, ListUtility.GetObjectWithChanceByChance<int?>(nums, 1));
            Assert.AreEqual(1, ListUtility.GetObjectWithChanceByChance<int?>(nums, 2));
            Assert.AreEqual(1, ListUtility.GetObjectWithChanceByChance<int?>(nums, 3));

            Assert.AreEqual(2, ListUtility.GetObjectWithChanceByChance<int?>(nums, 4));
            Assert.AreEqual(2, ListUtility.GetObjectWithChanceByChance<int?>(nums, 5));
            Assert.AreEqual(2, ListUtility.GetObjectWithChanceByChance<int?>(nums, 6));
            Assert.AreEqual(2, ListUtility.GetObjectWithChanceByChance<int?>(nums, 7));
            Assert.AreEqual(2, ListUtility.GetObjectWithChanceByChance<int?>(nums, 8));
            Assert.AreEqual(2, ListUtility.GetObjectWithChanceByChance<int?>(nums, 9));

            Assert.AreEqual(4, ListUtility.GetObjectWithChanceByChance<int?>(nums, 10));
            Assert.AreEqual(4, ListUtility.GetObjectWithChanceByChance<int?>(nums, 11));

            Assert.AreEqual(null, ListUtility.GetObjectWithChanceByChance<int?>(nums, 12));
        }
        [Test]
        public void GetObjectWithChanceByChance_InvalidInputs_Tests()
        {
            List<DictionaryTObjectChance<int?>> nums = null;
            Assert.IsNull(ListUtility.GetObjectWithChanceByChance<int?>(nums, 0));
            nums = new List<DictionaryTObjectChance<int?>>();
            Assert.IsNull(ListUtility.GetObjectWithChanceByChance<int?>(nums, 0));

            nums = new List<DictionaryTObjectChance<int?>>()
            {
                new DictionaryTObjectChance<int?>()
                {
                    obj = 0,
                    chance = 1
                },
                new DictionaryTObjectChance<int?>()
                {
                    obj = 1,
                    chance = 3
                },
                new DictionaryTObjectChance<int?>()
                {
                    obj = 2,
                    chance = 6
                },
                new DictionaryTObjectChance<int?>()
                {
                    obj = 3,
                    chance = 0
                },
                new DictionaryTObjectChance<int?>()
                {
                    obj = 4,
                    chance = 2
                }
            };
            Assert.IsNull(ListUtility.GetObjectWithChanceByChance<int?>(nums, -1));
            Assert.IsNull(ListUtility.GetObjectWithChanceByChance<int?>(nums, 12));
        }

        [TearDown]
        public void TearDown()
        {
            foreach (GameObject obj in spawnedObjs)
            {
                if (obj) GameObject.DestroyImmediate(obj);
            }
            spawnedObjs = new List<GameObject>();
        }
    }
}
