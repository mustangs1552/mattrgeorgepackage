﻿using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using MattRGeorge.General.Utilities;
using MattRGeorge.Unity.Utilities;
using MattRGeorge.Unity.Utilities.Static;

namespace MattRGeorge.Tests.Unity.Utilities.Static
{
    public class ParentChildUtilityTests
    {
        private Transform parent = null;
        private Transform parentA = null;
        private Transform childAA = null;
        private Transform childAB = null;
        private Transform childAC = null;
        private Transform childAAA = null;
        private Transform parentB = null;
        private Transform childBA = null;
        private Transform childBAA = null;
        private Transform childBAAA = null;
        private Transform childBAAAA = null;
        private bool value = false;
        private Bounds bounds = new Bounds();

        [Test]
        public void GetHighestParent_Tests()
        {
            parent = ParentChildUtility.GetHighestParent(parentA);
            Assert.IsNotNull(parent);
            Assert.AreEqual(parentA, parent);
            parent = ParentChildUtility.GetHighestParent(childAA);
            Assert.IsNotNull(parent);
            Assert.AreEqual(parentA, parent);
            parent = ParentChildUtility.GetHighestParent(childAB);
            Assert.IsNotNull(parent);
            Assert.AreEqual(parentA, parent);
            parent = ParentChildUtility.GetHighestParent(childAC);
            Assert.IsNotNull(parent);
            Assert.AreEqual(parentA, parent);
            parent = ParentChildUtility.GetHighestParent(childAAA);
            Assert.IsNotNull(parent);
            Assert.AreEqual(parentA, parent);

            parent = ParentChildUtility.GetHighestParent(parentB);
            Assert.IsNotNull(parent);
            Assert.AreEqual(parentB, parent);
            parent = ParentChildUtility.GetHighestParent(childBA);
            Assert.IsNotNull(parent);
            Assert.AreEqual(parentB, parent);
            parent = ParentChildUtility.GetHighestParent(childBAA);
            Assert.IsNotNull(parent);
            Assert.AreEqual(parentB, parent);
            parent = ParentChildUtility.GetHighestParent(childBAAA);
            Assert.IsNotNull(parent);
            Assert.AreEqual(parentB, parent);
            parent = ParentChildUtility.GetHighestParent(childBAAAA);
            Assert.IsNotNull(parent);
            Assert.AreEqual(parentB, parent);
        }
        [Test]
        public void GetHighestParent_InvalidInput_Tests()
        {
            parent = ParentChildUtility.GetHighestParent(null);
            Assert.IsNull(parent);
        }

        [Test]
        public void IsParent_Tests()
        {
            Assert.IsFalse(ParentChildUtility.IsParent(parentA, parentA));
            Assert.IsTrue(ParentChildUtility.IsParent(childAA, parentA));
            Assert.IsTrue(ParentChildUtility.IsParent(childAB, parentA));
            Assert.IsTrue(ParentChildUtility.IsParent(childAC, parentA));
            Assert.IsTrue(ParentChildUtility.IsParent(childAAA, parentA));
            Assert.IsTrue(ParentChildUtility.IsParent(childAAA, childAA));

            Assert.IsFalse(ParentChildUtility.IsParent(parentB, parentB));
            Assert.IsTrue(ParentChildUtility.IsParent(childBA, parentB));
            Assert.IsTrue(ParentChildUtility.IsParent(childBAA, parentB));
            Assert.IsTrue(ParentChildUtility.IsParent(childBAA, childBA));
            Assert.IsTrue(ParentChildUtility.IsParent(childBAAA, parentB));
            Assert.IsTrue(ParentChildUtility.IsParent(childBAAA, childBA));
            Assert.IsTrue(ParentChildUtility.IsParent(childBAAA, childBAA));
            Assert.IsTrue(ParentChildUtility.IsParent(childBAAAA, parentB));
            Assert.IsTrue(ParentChildUtility.IsParent(childBAAAA, childBA));
            Assert.IsTrue(ParentChildUtility.IsParent(childBAAAA, childBAA));
            Assert.IsTrue(ParentChildUtility.IsParent(childBAAAA, childBAAA));

            Assert.IsFalse(ParentChildUtility.IsParent(childAA, parentB));
            Assert.IsFalse(ParentChildUtility.IsParent(childAAA, parentB));
            Assert.IsFalse(ParentChildUtility.IsParent(childBA, parentA));
            Assert.IsFalse(ParentChildUtility.IsParent(childBAAA, parentA));
        }
        [Test]
        public void IsParent_InvalidInput_Tests()
        {
            value = ParentChildUtility.IsParent(null, null);
            Assert.IsFalse(value);

            value = ParentChildUtility.IsParent(childAA, null);
            Assert.IsFalse(value);

            value = ParentChildUtility.IsParent(null, parentA);
            Assert.IsFalse(value);
        }

        [Test]
        public void GetAllChildrenBounds_AllHaveRenderers_Tests()
        {
            bounds = ParentChildUtility.GetAllChildrenBounds(parentA);
            Assert.IsNotNull(bounds);
            Assert.AreEqual(new Vector3(0, .5f, 0), bounds.center);
            Assert.AreEqual(new Vector3(3, 2, 1), bounds.size);

            bounds = ParentChildUtility.GetAllChildrenBounds(childAA);
            Assert.IsNotNull(bounds);
            Assert.AreEqual(new Vector3(.5f, .5f, 0), bounds.center);
            Assert.AreEqual(new Vector3(2, 2, 1), bounds.size);

            bounds = ParentChildUtility.GetAllChildrenBounds(childAB);
            Assert.IsNotNull(bounds);
            Assert.AreEqual(new Vector3(0, 0, 0), bounds.center);
            Assert.AreEqual(new Vector3(1, 1, 1), bounds.size);

            bounds = ParentChildUtility.GetAllChildrenBounds(childAC);
            Assert.IsNotNull(bounds);
            Assert.AreEqual(new Vector3(-1, 0, 0), bounds.center);
            Assert.AreEqual(new Vector3(1, 1, 1), bounds.size);

            bounds = ParentChildUtility.GetAllChildrenBounds(childAAA);
            Assert.IsNotNull(bounds);
            Assert.AreEqual(new Vector3(1, 0, 0), bounds.center);
            Assert.AreEqual(new Vector3(1, 1, 1), bounds.size);
        }
        [Test]
        public void GetAllChildrenBounds_NotAllHaveRenderers_Tests()
        {
            bounds = ParentChildUtility.GetAllChildrenBounds(parentB);
            Assert.IsNotNull(bounds);
            Assert.AreEqual(new Vector3(.5f, .5f, 0), bounds.center);
            Assert.AreEqual(new Vector3(2, 2, 1), bounds.size);

            bounds = ParentChildUtility.GetAllChildrenBounds(childBAA);
            Assert.IsNotNull(bounds);
            Assert.AreEqual(new Vector3(.5f, .5f, 0), bounds.center);
            Assert.AreEqual(new Vector3(2, 2, 1), bounds.size);

            bounds = ParentChildUtility.GetAllChildrenBounds(childBAAA);
            Assert.IsNotNull(bounds);
            Assert.AreEqual(new Vector3(1, 0, 0), bounds.center);
            Assert.AreEqual(new Vector3(1, 1, 1), bounds.size);
        }
        [Test]
        public void GetAllChildrenBounds_InvalidInput_Tests()
        {
            bounds = ParentChildUtility.GetAllChildrenBounds(null);
            Assert.IsNotNull(bounds);

            // No renderers
            bounds = ParentChildUtility.GetAllChildrenBounds(childBAAAA);
            Assert.IsNotNull(bounds);
        }

        [Test]
        public void AddChildrenMeshColliders_FromParents_DontIncludeStart_Tests()
        {
            ParentChildUtility.AddChildrenMeshColliders(parentA);
            Assert.IsNull(parentA.GetComponent<MeshCollider>());
            Assert.IsNotNull(childAA.GetComponent<MeshCollider>());
            Assert.IsNotNull(childAB.GetComponent<MeshCollider>());
            Assert.IsNotNull(childAC.GetComponent<MeshCollider>());
            Assert.IsNotNull(childAAA.GetComponent<MeshCollider>());

            ParentChildUtility.AddChildrenMeshColliders(parentB);
            Assert.IsNull(parentB.GetComponent<MeshCollider>());
            Assert.IsNull(childBA.GetComponent<MeshCollider>());
            Assert.IsNotNull(childBAA.GetComponent<MeshCollider>());
            Assert.IsNotNull(childBAAA.GetComponent<MeshCollider>());
            Assert.IsNull(childBAAAA.GetComponent<MeshCollider>());
        }
        [Test]
        public void AddChildrenMeshColliders_FromParents_IncludeStart_Tests()
        {
            ParentChildUtility.AddChildrenMeshColliders(parentA, true);
            Assert.IsNotNull(parentA.GetComponent<MeshCollider>());
            Assert.IsNotNull(childAA.GetComponent<MeshCollider>());
            Assert.IsNotNull(childAB.GetComponent<MeshCollider>());
            Assert.IsNotNull(childAC.GetComponent<MeshCollider>());
            Assert.IsNotNull(childAAA.GetComponent<MeshCollider>());

            ParentChildUtility.AddChildrenMeshColliders(parentB, true);
            Assert.IsNull(parentB.GetComponent<MeshCollider>());
            Assert.IsNull(childBA.GetComponent<MeshCollider>());
            Assert.IsNotNull(childBAA.GetComponent<MeshCollider>());
            Assert.IsNotNull(childBAAA.GetComponent<MeshCollider>());
            Assert.IsNull(childBAAAA.GetComponent<MeshCollider>());
        }
        [Test]
        public void AddChildrenMeshColliders_FromChild_DontIncludeStart_Tests()
        {
            ParentChildUtility.AddChildrenMeshColliders(childAB);
            Assert.IsNull(parentA.GetComponent<MeshCollider>());
            Assert.IsNull(childAA.GetComponent<MeshCollider>());
            Assert.IsNull(childAB.GetComponent<MeshCollider>());
            Assert.IsNull(childAC.GetComponent<MeshCollider>());
            Assert.IsNull(childAAA.GetComponent<MeshCollider>());

            ParentChildUtility.AddChildrenMeshColliders(childBAA);
            Assert.IsNull(parentB.GetComponent<MeshCollider>());
            Assert.IsNull(childBA.GetComponent<MeshCollider>());
            Assert.IsNull(childBAA.GetComponent<MeshCollider>());
            Assert.IsNotNull(childBAAA.GetComponent<MeshCollider>());
            Assert.IsNull(childBAAAA.GetComponent<MeshCollider>());
        }
        [Test]
        public void AddChildrenMeshColliders_FromChild_IncludeStart_Tests()
        {
            ParentChildUtility.AddChildrenMeshColliders(childAB, true);
            Assert.IsNull(parentA.GetComponent<MeshCollider>());
            Assert.IsNull(childAA.GetComponent<MeshCollider>());
            Assert.IsNotNull(childAB.GetComponent<MeshCollider>());
            Assert.IsNull(childAC.GetComponent<MeshCollider>());
            Assert.IsNull(childAAA.GetComponent<MeshCollider>());

            ParentChildUtility.AddChildrenMeshColliders(childBAA, true);
            Assert.IsNull(parentB.GetComponent<MeshCollider>());
            Assert.IsNull(childBA.GetComponent<MeshCollider>());
            Assert.IsNotNull(childBAA.GetComponent<MeshCollider>());
            Assert.IsNotNull(childBAAA.GetComponent<MeshCollider>());
            Assert.IsNull(childBAAAA.GetComponent<MeshCollider>());
        }
        [Test]
        public void AddChildrenMeshColliders_InvalidInput_Tests()
        {
            ParentChildUtility.AddChildrenMeshColliders(null);
        }

        [SetUp]
        public void Setup()
        {
            #region ParentA
            /*ParentA (Renderer)
             * - ChildAA (Renderer)
             *   - ChildAAA (Renderer)
             * - ChildAB (Renderer)
             * - ChildAC (Renderer)
             */
            parentA = GameObject.CreatePrimitive(PrimitiveType.Cube).transform;
            parentA.name = "ParentA";
            childAA = GameObject.CreatePrimitive(PrimitiveType.Cube).transform;
            childAA.name = "ChildAA";
            childAA.Translate(Vector3.up * 1);
            childAA.parent = parentA;
            childAB = GameObject.CreatePrimitive(PrimitiveType.Cube).transform;
            childAB.name = "ChildAB";
            childAB.parent = parentA;
            childAC = GameObject.CreatePrimitive(PrimitiveType.Cube).transform;
            childAC.name = "ChildAC";
            childAC.Translate(Vector3.left * 1);
            childAC.parent = parentA;
            childAAA = GameObject.CreatePrimitive(PrimitiveType.Cube).transform;
            childAAA.name = "ChildAAA";
            childAAA.Translate(Vector3.right * 1);
            childAAA.parent = childAA;
            #endregion

            #region ParentB
            /*ParentB
             * - ChildBA
             *   - ChildBAA (Renderer)
             *     - ChildBAAA (Renderer)
             *       - ChildBAAAA
             */
            parentB = new GameObject("ParentB").transform;
            childBA = new GameObject("ChildBA").transform;
            childBA.parent = parentB;
            childBAA = GameObject.CreatePrimitive(PrimitiveType.Cube).transform;
            childBAA.name = "ChildBAA";
            childBAA.Translate(Vector3.up * 1);
            childBAA.parent = childBA;
            childBAAA = GameObject.CreatePrimitive(PrimitiveType.Cube).transform;
            childBAAA.name = "ChildBAAA";
            childBAAA.Translate(Vector3.right * 1);
            childBAAA.parent = childBAA;
            childBAAAA = new GameObject("ChildBAAAA").transform;
            childBAAAA.parent = childBAAA;
            #endregion
        }
        [TearDown]
        public void TearDown()
        {
            GameObject.DestroyImmediate(parentA.gameObject);
            parentA = null;
            childAA = null;
            childAB = null;
            childAC = null;
            childAAA = null;

            GameObject.DestroyImmediate(parentB.gameObject);
            parentB = null;
            childBA = null;
            childBAA = null;
            childBAAA = null;
            childBAA = null;
        }
    }
}
