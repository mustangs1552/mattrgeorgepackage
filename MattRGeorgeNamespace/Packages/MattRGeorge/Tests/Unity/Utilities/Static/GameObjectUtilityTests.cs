﻿using NUnit.Framework;
using UnityEngine;
using MattRGeorge.Unity.Utilities.Static;

namespace MattRGeorge.Tests.Unity.Utilities.Static
{
    public class GameObjectUtilityTests
    {
        private GameObject obj = null;
        private GameObject objClone = null;
        private GameObject objClone2 = null;
        private GameObject objCloneClone = null;
        string name = "";

        [Test]
        public void GetActualName_Tests()
        {
            name = GameObjectUtility.GetActualName(obj);
            Assert.IsNotNull(name);
            Assert.AreEqual("Obj", name);

            name = GameObjectUtility.GetActualName(objClone);
            Assert.IsNotNull(name);
            Assert.AreEqual("Obj", name);

            name = GameObjectUtility.GetActualName(objClone2);
            Assert.IsNotNull(name);
            Assert.AreEqual("Obj", name);

            name = GameObjectUtility.GetActualName(objCloneClone);
            Assert.IsNotNull(name);
            Assert.AreEqual("Obj", name);
        }
        [Test]
        public void GetActualName_InvalidInput_Tests()
        {
            name = GameObjectUtility.GetActualName(null);
            Assert.NotNull(name);
            Assert.IsEmpty(name);
        }

        [SetUp]
        public void Setup()
        {
            obj = new GameObject("Obj");
            objClone = GameObject.Instantiate(obj);
            objClone2 = GameObject.Instantiate(obj);
            objCloneClone = GameObject.Instantiate(objClone);
            name = "";
        }
        [TearDown]
        public void TearDown()
        {
            GameObject.DestroyImmediate(obj);
            obj = null;
            GameObject.DestroyImmediate(objClone);
            objClone = null;
            GameObject.DestroyImmediate(objClone2);
            objClone2 = null;
            GameObject.DestroyImmediate(objCloneClone);
            objCloneClone = null;
        }
    }
}
