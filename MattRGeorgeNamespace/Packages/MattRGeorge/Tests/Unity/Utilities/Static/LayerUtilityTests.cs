﻿using NUnit.Framework;
using UnityEngine;
using MattRGeorge.Unity.Utilities.Static;
using MattRGeorge.Unity.Utilities.Enums;

namespace MattRGeorge.Tests.Unity.Utilities.Static
{
    public class LayerUtilityTests
    {
        private LayerMask layerMask = (int)LayerOptions.Nothing;
        private GameObject defaultObj = null;
        private GameObject ignoreRaycastObj = null;

        [Test]
        public void InLayerMask_GameObject_Tests()
        {
            Assert.IsFalse(LayerUtility.InLayerMask((int)LayerOptions.Nothing, defaultObj));
            Assert.IsFalse(LayerUtility.InLayerMask((int)LayerOptions.Nothing, ignoreRaycastObj));

            Assert.IsTrue(LayerUtility.InLayerMask((int)LayerOptions.Everything, defaultObj));
            Assert.IsTrue(LayerUtility.InLayerMask((int)LayerOptions.Everything, ignoreRaycastObj));

            LayerUtility.AddLayerToLayerMask("Default", ref layerMask);
            Assert.IsTrue(LayerUtility.InLayerMask(layerMask, defaultObj));
            Assert.IsFalse(LayerUtility.InLayerMask(layerMask, ignoreRaycastObj));

            LayerUtility.AddLayerToLayerMask("Ignore Raycast", ref layerMask);
            Assert.IsTrue(LayerUtility.InLayerMask(layerMask, defaultObj));
            Assert.IsTrue(LayerUtility.InLayerMask(layerMask, ignoreRaycastObj));

            LayerUtility.RemoveLayerFromLayerMask("Default", ref layerMask);
            Assert.IsFalse(LayerUtility.InLayerMask(layerMask, defaultObj));
            Assert.IsTrue(LayerUtility.InLayerMask(layerMask, ignoreRaycastObj));
        }
        [Test]
        public void InLayerMask_GameObject_InvalidInput_Tests()
        {
            GameObject nullObj = null;

            Assert.IsFalse(LayerUtility.InLayerMask(0, nullObj));
            Assert.IsFalse(LayerUtility.InLayerMask(-10, nullObj));
            Assert.IsFalse(LayerUtility.InLayerMask(50, nullObj));
        }

        [Test]
        public void InLayerMask_String_Tests()
        {
            Assert.IsFalse(LayerUtility.InLayerMask((int)LayerOptions.Nothing, "Default"));
            Assert.IsFalse(LayerUtility.InLayerMask((int)LayerOptions.Nothing, "Ignore Raycast"));

            Assert.IsTrue(LayerUtility.InLayerMask((int)LayerOptions.Everything, "Default"));
            Assert.IsTrue(LayerUtility.InLayerMask((int)LayerOptions.Everything, "Ignore Raycast"));

            LayerUtility.AddLayerToLayerMask("Default", ref layerMask);
            Assert.IsTrue(LayerUtility.InLayerMask(layerMask, "Default"));
            Assert.IsFalse(LayerUtility.InLayerMask(layerMask, "Ignore Raycast"));

            LayerUtility.AddLayerToLayerMask("Ignore Raycast", ref layerMask);
            Assert.IsTrue(LayerUtility.InLayerMask(layerMask, "Default"));
            Assert.IsTrue(LayerUtility.InLayerMask(layerMask, "Ignore Raycast"));

            LayerUtility.RemoveLayerFromLayerMask("Default", ref layerMask);
            Assert.IsFalse(LayerUtility.InLayerMask(layerMask, "Default"));
            Assert.IsTrue(LayerUtility.InLayerMask(layerMask, "Ignore Raycast"));
        }
        [Test]
        public void InLayerMask_String_InvalidInput_Tests()
        {
            string nullStr = null;

            Assert.IsFalse(LayerUtility.InLayerMask(0, nullStr));
            Assert.IsFalse(LayerUtility.InLayerMask(-10, nullStr));
            Assert.IsFalse(LayerUtility.InLayerMask(50, nullStr));

            Assert.IsFalse(LayerUtility.InLayerMask(0, ""));
            Assert.IsFalse(LayerUtility.InLayerMask(-10, ""));
            Assert.IsFalse(LayerUtility.InLayerMask(50, ""));
        }

        [Test]
        public void AddLayerToLayerMask_Tests()
        {
            LayerUtility.AddLayerToLayerMask("Default", ref layerMask);
            Assert.IsTrue(LayerUtility.InLayerMask(layerMask, "Default"));
            Assert.IsFalse(LayerUtility.InLayerMask(layerMask, "Ignore Raycast"));

            LayerUtility.AddLayerToLayerMask("Ignore Raycast", ref layerMask);
            Assert.IsTrue(LayerUtility.InLayerMask(layerMask, "Default"));
            Assert.IsTrue(LayerUtility.InLayerMask(layerMask, "Ignore Raycast"));
        }
        [Test]
        public void AddLayerToLayerMask_InvalidInput_Tests()
        {
            LayerMask newLayerMask = (int)LayerOptions.Nothing;

            LayerUtility.AddLayerToLayerMask(null, ref newLayerMask);
            Assert.AreEqual(layerMask, newLayerMask);
            LayerUtility.AddLayerToLayerMask("", ref newLayerMask);
            Assert.AreEqual(layerMask, newLayerMask);
        }

        [Test]
        public void RemoveLayerFromLayerMask_Tests()
        {
            layerMask = (int)LayerOptions.Everything;

            LayerUtility.RemoveLayerFromLayerMask("Default", ref layerMask);
            Assert.IsFalse(LayerUtility.InLayerMask(layerMask, "Default"));
            Assert.IsTrue(LayerUtility.InLayerMask(layerMask, "Ignore Raycast"));

            LayerUtility.RemoveLayerFromLayerMask("Ignore Raycast", ref layerMask);
            Assert.IsFalse(LayerUtility.InLayerMask(layerMask, "Default"));
            Assert.IsFalse(LayerUtility.InLayerMask(layerMask, "Ignore Raycast"));
        }
        [Test]
        public void RemoveLayerFromLayerMask_InvalidInput_Tests()
        {
            layerMask = (int)LayerOptions.Everything;
            LayerMask newLayerMask = (int)LayerOptions.Everything;

            LayerUtility.RemoveLayerFromLayerMask(null, ref newLayerMask);
            Assert.AreEqual(layerMask, newLayerMask);
            LayerUtility.RemoveLayerFromLayerMask("", ref newLayerMask);
            Assert.AreEqual(layerMask, newLayerMask);
        }

        [SetUp]
        public void TestSetup()
        {
            layerMask = (int)LayerOptions.Nothing;

            defaultObj = new GameObject("DEFUALT");
            defaultObj.layer = LayerMask.NameToLayer("Default");

            ignoreRaycastObj = new GameObject("IGNORE-RAYCAST");
            ignoreRaycastObj.layer = LayerMask.NameToLayer("Ignore Raycast");
        }
    }
}
