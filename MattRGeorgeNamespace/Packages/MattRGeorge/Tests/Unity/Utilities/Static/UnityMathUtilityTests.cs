﻿using NUnit.Framework;
using UnityEngine;
using MattRGeorge.Unity.Utilities.Static;

namespace MattRGeorge.Tests.Unity.Utilities.Static
{
    public class UnityMathUtilityTests
    {
        [Test]
        public void FindAngleXZ_Tests()
        {
            Transform transA = new GameObject().transform;
            Transform transB = new GameObject().transform;
            float angle = UnityMathUtility.FindAngleXZ(transA, transB, Vector3.forward);
            Assert.AreEqual(0, (int)angle);

            transA.Rotate(Vector3.up * 90);
            angle = UnityMathUtility.FindAngleXZ(transA, transB, Vector3.forward);
            Assert.AreEqual(-90, (int)angle);

            transA.Rotate(Vector3.up * 90);
            angle = UnityMathUtility.FindAngleXZ(transA, transB, Vector3.forward);
            Assert.AreEqual(180, (int)angle);

            transA.Rotate(Vector3.up * 90);
            angle = UnityMathUtility.FindAngleXZ(transA, transB, Vector3.forward);
            Assert.AreEqual(90, (int)angle);

            transA.Rotate(Vector3.up * 90);
            angle = UnityMathUtility.FindAngleXZ(transA, transB, Vector3.forward);
            Assert.AreEqual(0, (int)angle);
        }
        [Test]
        public void FindAngleXZ_InvalidInput_Tests()
        {
            Transform transA = new GameObject().transform;
            Transform transB = null;
            float angle = UnityMathUtility.FindAngleXZ(transA, transB, Vector3.forward);
            Assert.AreEqual(0, angle);

            transA = null;
            transB = new GameObject().transform;
            angle = UnityMathUtility.FindAngleXZ(transA, transB, Vector3.forward);
            Assert.AreEqual(0, angle);

            transA = null;
            transB = null;
            angle = UnityMathUtility.FindAngleXZ(transA, transB, Vector3.forward);
            Assert.AreEqual(0, angle);
        }
    }
}
