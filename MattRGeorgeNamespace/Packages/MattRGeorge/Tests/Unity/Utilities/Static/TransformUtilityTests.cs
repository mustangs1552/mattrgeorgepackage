﻿using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using MattRGeorge.Unity.Utilities.Static;

namespace MattRGeorge.Tests.Unity.Utilities.Static
{
    public class TransformUtilityTests
    {
        private List<Transform> objTrans = new List<Transform>();
        private List<Collider> objColls = new List<Collider>();
        private Transform trans = null;

        [Test]
        public void GetClosest_Colliders_Tests()
        {
            objTrans[1].position = new Vector3(3, 5, 2);
            objTrans[2].position = new Vector3(3, 15, -12);

            trans = TransformUtility.GetClosest(Vector3.zero, objColls);
            Assert.IsNotNull(trans);
            Assert.AreEqual(objTrans[0], trans);

            trans = TransformUtility.GetClosest(Vector3.forward * 2, objColls);
            Assert.IsNotNull(trans);
            Assert.AreEqual(objTrans[0], trans);

            trans = TransformUtility.GetClosest(Vector3.up * 5, objColls);
            Assert.IsNotNull(trans);
            Assert.AreEqual(objTrans[0], trans);

            trans = TransformUtility.GetClosest(new Vector3(2, 4, 1), objColls);
            Assert.IsNotNull(trans);
            Assert.AreEqual(objTrans[1], trans);

            trans = TransformUtility.GetClosest(new Vector3(3, 5, 2), objColls);
            Assert.IsNotNull(trans);
            Assert.AreEqual(objTrans[1], trans);

            trans = TransformUtility.GetClosest(new Vector3(4, 6, 3), objColls);
            Assert.IsNotNull(trans);
            Assert.AreEqual(objTrans[1], trans);

            trans = TransformUtility.GetClosest(new Vector3(1, 12, -10), objColls);
            Assert.IsNotNull(trans);
            Assert.AreEqual(objTrans[2], trans);

            trans = TransformUtility.GetClosest(new Vector3(2, 15, -12), objColls);
            Assert.IsNotNull(trans);
            Assert.AreEqual(objTrans[2], trans);

            trans = TransformUtility.GetClosest(new Vector3(3, 18, -15), objColls);
            Assert.IsNotNull(trans);
            Assert.AreEqual(objTrans[2], trans);
        }
        [Test]
        public void GetClosest_Transforms_Tests()
        {
            objTrans[1].position = new Vector3(3, 5, 2);
            objTrans[2].position = new Vector3(3, 15, -12);

            trans = TransformUtility.GetClosest(Vector3.zero, objTrans);
            Assert.IsNotNull(trans);
            Assert.AreEqual(objTrans[0], trans);

            trans = TransformUtility.GetClosest(Vector3.forward * 2, objTrans);
            Assert.IsNotNull(trans);
            Assert.AreEqual(objTrans[0], trans);

            trans = TransformUtility.GetClosest(Vector3.up * 5, objTrans);
            Assert.IsNotNull(trans);
            Assert.AreEqual(objTrans[0], trans);

            trans = TransformUtility.GetClosest(new Vector3(2, 4, 1), objTrans);
            Assert.IsNotNull(trans);
            Assert.AreEqual(objTrans[1], trans);

            trans = TransformUtility.GetClosest(new Vector3(3, 5, 2), objTrans);
            Assert.IsNotNull(trans);
            Assert.AreEqual(objTrans[1], trans);

            trans = TransformUtility.GetClosest(new Vector3(4, 6, 3), objTrans);
            Assert.IsNotNull(trans);
            Assert.AreEqual(objTrans[1], trans);

            trans = TransformUtility.GetClosest(new Vector3(1, 12, -10), objTrans);
            Assert.IsNotNull(trans);
            Assert.AreEqual(objTrans[2], trans);

            trans = TransformUtility.GetClosest(new Vector3(2, 15, -12), objTrans);
            Assert.IsNotNull(trans);
            Assert.AreEqual(objTrans[2], trans);

            trans = TransformUtility.GetClosest(new Vector3(3, 18, -15), objTrans);
            Assert.IsNotNull(trans);
            Assert.AreEqual(objTrans[2], trans);
        }
        [Test]
        public void GetClosest_InvalidInput_Tests()
        {
            trans = TransformUtility.GetClosest(Vector3.zero, new List<Collider>());
            Assert.IsNull(trans);
            trans = TransformUtility.GetClosest(Vector3.zero, new List<Transform>());
            Assert.IsNull(trans);

            List<Transform> test = null;
            objColls = null;
            trans = TransformUtility.GetClosest(Vector3.zero, objColls);
            Assert.IsNull(trans);
            trans = TransformUtility.GetClosest(Vector3.zero, test);
            Assert.IsNull(trans);
        }

        [Test]
        public void CheckFOV_HorizontalFOV_Tests()
        {
            objTrans[0].Translate(Vector3.forward * 5);
            Assert.IsTrue(TransformUtility.CheckFOV(trans, objTrans[0], 0));
            Assert.IsTrue(TransformUtility.CheckFOV(trans, objTrans[0], 1));
            Assert.IsTrue(TransformUtility.CheckFOV(trans, objTrans[0], 0));

            objTrans[0].Translate(Vector3.left * 5);
            objTrans[1].Translate(new Vector3(5, 0, 5));
            Assert.IsFalse(TransformUtility.CheckFOV(trans, objTrans[0], 45));
            Assert.IsFalse(TransformUtility.CheckFOV(trans, objTrans[0], 89));
            Assert.IsTrue(TransformUtility.CheckFOV(trans, objTrans[0], 90));
            Assert.IsTrue(TransformUtility.CheckFOV(trans, objTrans[0], 91));
            Assert.IsFalse(TransformUtility.CheckFOV(trans, objTrans[1], 45));
            Assert.IsFalse(TransformUtility.CheckFOV(trans, objTrans[1], 89));
            Assert.IsTrue(TransformUtility.CheckFOV(trans, objTrans[1], 90));
            Assert.IsTrue(TransformUtility.CheckFOV(trans, objTrans[1], 91));

            objTrans[0].Translate(Vector3.back * 5);
            objTrans[1].Translate(Vector3.back * 5);
            Assert.IsFalse(TransformUtility.CheckFOV(trans, objTrans[0], 90));
            Assert.IsFalse(TransformUtility.CheckFOV(trans, objTrans[0], 179));
            Assert.IsTrue(TransformUtility.CheckFOV(trans, objTrans[0], 180));
            Assert.IsTrue(TransformUtility.CheckFOV(trans, objTrans[0], 181));
            Assert.IsFalse(TransformUtility.CheckFOV(trans, objTrans[1], 90));
            Assert.IsFalse(TransformUtility.CheckFOV(trans, objTrans[1], 179));
            Assert.IsTrue(TransformUtility.CheckFOV(trans, objTrans[1], 180));
            Assert.IsTrue(TransformUtility.CheckFOV(trans, objTrans[1], 181));

            objTrans[0].Translate(Vector3.back * 5);
            objTrans[1].Translate(Vector3.back * 5);
            Assert.IsFalse(TransformUtility.CheckFOV(trans, objTrans[0], 180));
            Assert.IsFalse(TransformUtility.CheckFOV(trans, objTrans[0], 269));
            Assert.IsTrue(TransformUtility.CheckFOV(trans, objTrans[0], 270));
            Assert.IsTrue(TransformUtility.CheckFOV(trans, objTrans[0], 271));
            Assert.IsFalse(TransformUtility.CheckFOV(trans, objTrans[1], 180));
            Assert.IsFalse(TransformUtility.CheckFOV(trans, objTrans[1], 269));
            Assert.IsTrue(TransformUtility.CheckFOV(trans, objTrans[1], 270));
            Assert.IsTrue(TransformUtility.CheckFOV(trans, objTrans[1], 271));

            objTrans[0].Translate(Vector3.right * 5);
            objTrans[1].Translate(Vector3.left * 5);
            Assert.IsFalse(TransformUtility.CheckFOV(trans, objTrans[0], 270));
            Assert.IsFalse(TransformUtility.CheckFOV(trans, objTrans[0], 359));
            Assert.IsTrue(TransformUtility.CheckFOV(trans, objTrans[0], 360));
            Assert.IsFalse(TransformUtility.CheckFOV(trans, objTrans[1], 270));
            Assert.IsFalse(TransformUtility.CheckFOV(trans, objTrans[1], 359));
            Assert.IsTrue(TransformUtility.CheckFOV(trans, objTrans[1], 360));
        }
        [Test]
        public void CheckFOV_VerticalFOV_Tests()
        {
            objTrans[0].Translate(Vector3.forward * 5);
            Assert.IsTrue(TransformUtility.CheckFOV(trans, objTrans[0], 0));
            Assert.IsTrue(TransformUtility.CheckFOV(trans, objTrans[0], 1));
            Assert.IsTrue(TransformUtility.CheckFOV(trans, objTrans[0], 0));

            objTrans[0].Translate(Vector3.up * 5);
            objTrans[1].Translate(new Vector3(0, -5, 5));
            Assert.IsFalse(TransformUtility.CheckFOV(trans, objTrans[0], 45));
            Assert.IsFalse(TransformUtility.CheckFOV(trans, objTrans[0], 89));
            Assert.IsTrue(TransformUtility.CheckFOV(trans, objTrans[0], 90));
            Assert.IsTrue(TransformUtility.CheckFOV(trans, objTrans[0], 91));
            Assert.IsFalse(TransformUtility.CheckFOV(trans, objTrans[1], 45));
            Assert.IsFalse(TransformUtility.CheckFOV(trans, objTrans[1], 89));
            Assert.IsTrue(TransformUtility.CheckFOV(trans, objTrans[1], 90));
            Assert.IsTrue(TransformUtility.CheckFOV(trans, objTrans[1], 91));

            objTrans[0].Translate(Vector3.back * 5);
            objTrans[1].Translate(Vector3.back * 5);
            Assert.IsFalse(TransformUtility.CheckFOV(trans, objTrans[0], 90));
            Assert.IsFalse(TransformUtility.CheckFOV(trans, objTrans[0], 179));
            Assert.IsTrue(TransformUtility.CheckFOV(trans, objTrans[0], 180));
            Assert.IsTrue(TransformUtility.CheckFOV(trans, objTrans[0], 181));
            Assert.IsFalse(TransformUtility.CheckFOV(trans, objTrans[1], 90));
            Assert.IsFalse(TransformUtility.CheckFOV(trans, objTrans[1], 179));
            Assert.IsTrue(TransformUtility.CheckFOV(trans, objTrans[1], 180));
            Assert.IsTrue(TransformUtility.CheckFOV(trans, objTrans[1], 181));

            objTrans[0].Translate(Vector3.back * 5);
            objTrans[1].Translate(Vector3.back * 5);
            Assert.IsFalse(TransformUtility.CheckFOV(trans, objTrans[0], 180));
            Assert.IsFalse(TransformUtility.CheckFOV(trans, objTrans[0], 269));
            Assert.IsTrue(TransformUtility.CheckFOV(trans, objTrans[0], 270));
            Assert.IsTrue(TransformUtility.CheckFOV(trans, objTrans[0], 271));
            Assert.IsFalse(TransformUtility.CheckFOV(trans, objTrans[1], 180));
            Assert.IsFalse(TransformUtility.CheckFOV(trans, objTrans[1], 269));
            Assert.IsTrue(TransformUtility.CheckFOV(trans, objTrans[1], 270));
            Assert.IsTrue(TransformUtility.CheckFOV(trans, objTrans[1], 271));

            objTrans[0].Translate(Vector3.down * 5);
            objTrans[1].Translate(Vector3.up * 5);
            Assert.IsFalse(TransformUtility.CheckFOV(trans, objTrans[0], 270));
            Assert.IsFalse(TransformUtility.CheckFOV(trans, objTrans[0], 359));
            Assert.IsTrue(TransformUtility.CheckFOV(trans, objTrans[0], 360));
            Assert.IsFalse(TransformUtility.CheckFOV(trans, objTrans[1], 270));
            Assert.IsFalse(TransformUtility.CheckFOV(trans, objTrans[1], 359));
            Assert.IsTrue(TransformUtility.CheckFOV(trans, objTrans[1], 360));
        }
        [Test]
        public void CheckFOV_Mixed_Tests()
        {
            objTrans[0].Translate(new Vector3(5, 5, 5));
            objTrans[1].Translate(new Vector3(5, -5, 5));
            Assert.IsFalse(TransformUtility.CheckFOV(trans, objTrans[0], 45));
            Assert.IsFalse(TransformUtility.CheckFOV(trans, objTrans[0], 109.4f));
            Assert.IsTrue(TransformUtility.CheckFOV(trans, objTrans[0], 109.5f));
            Assert.IsTrue(TransformUtility.CheckFOV(trans, objTrans[0], 110));
            Assert.IsFalse(TransformUtility.CheckFOV(trans, objTrans[1], 45));
            Assert.IsFalse(TransformUtility.CheckFOV(trans, objTrans[1], 109.4f));
            Assert.IsTrue(TransformUtility.CheckFOV(trans, objTrans[1], 109.5f));
            Assert.IsTrue(TransformUtility.CheckFOV(trans, objTrans[1], 110));

            objTrans[0].Translate(Vector3.back * 5);
            objTrans[1].Translate(Vector3.back * 5);
            Assert.IsFalse(TransformUtility.CheckFOV(trans, objTrans[0], 90));
            Assert.IsFalse(TransformUtility.CheckFOV(trans, objTrans[0], 179));
            Assert.IsTrue(TransformUtility.CheckFOV(trans, objTrans[0], 180));
            Assert.IsTrue(TransformUtility.CheckFOV(trans, objTrans[0], 181));
            Assert.IsFalse(TransformUtility.CheckFOV(trans, objTrans[1], 90));
            Assert.IsFalse(TransformUtility.CheckFOV(trans, objTrans[1], 179));
            Assert.IsTrue(TransformUtility.CheckFOV(trans, objTrans[1], 180));
            Assert.IsTrue(TransformUtility.CheckFOV(trans, objTrans[1], 181));

            objTrans[0].Translate(Vector3.back * 5);
            objTrans[1].Translate(Vector3.back * 5);
            Assert.IsFalse(TransformUtility.CheckFOV(trans, objTrans[0], 180));
            Assert.IsFalse(TransformUtility.CheckFOV(trans, objTrans[0], 250.5f));
            Assert.IsTrue(TransformUtility.CheckFOV(trans, objTrans[0], 250.6f));
            Assert.IsTrue(TransformUtility.CheckFOV(trans, objTrans[0], 251));
            Assert.IsFalse(TransformUtility.CheckFOV(trans, objTrans[1], 180));
            Assert.IsFalse(TransformUtility.CheckFOV(trans, objTrans[1], 250.5f));
            Assert.IsTrue(TransformUtility.CheckFOV(trans, objTrans[1], 250.6f));
            Assert.IsTrue(TransformUtility.CheckFOV(trans, objTrans[1], 251));

            objTrans[0].Translate(Vector3.left * 10);
            objTrans[1].Translate(Vector3.left * 10);
            Assert.IsFalse(TransformUtility.CheckFOV(trans, objTrans[0], 180));
            Assert.IsFalse(TransformUtility.CheckFOV(trans, objTrans[0], 250.5f));
            Assert.IsTrue(TransformUtility.CheckFOV(trans, objTrans[0], 250.6f));
            Assert.IsTrue(TransformUtility.CheckFOV(trans, objTrans[0], 251));
            Assert.IsFalse(TransformUtility.CheckFOV(trans, objTrans[1], 180));
            Assert.IsFalse(TransformUtility.CheckFOV(trans, objTrans[1], 250.5f));
            Assert.IsTrue(TransformUtility.CheckFOV(trans, objTrans[1], 250.6f));
            Assert.IsTrue(TransformUtility.CheckFOV(trans, objTrans[1], 251));

            objTrans[0].Translate(Vector3.forward * 5);
            objTrans[1].Translate(Vector3.forward * 5);
            Assert.IsFalse(TransformUtility.CheckFOV(trans, objTrans[0], 90));
            Assert.IsFalse(TransformUtility.CheckFOV(trans, objTrans[0], 179));
            Assert.IsTrue(TransformUtility.CheckFOV(trans, objTrans[0], 180));
            Assert.IsTrue(TransformUtility.CheckFOV(trans, objTrans[0], 181));
            Assert.IsFalse(TransformUtility.CheckFOV(trans, objTrans[1], 90));
            Assert.IsFalse(TransformUtility.CheckFOV(trans, objTrans[1], 179));
            Assert.IsTrue(TransformUtility.CheckFOV(trans, objTrans[1], 180));
            Assert.IsTrue(TransformUtility.CheckFOV(trans, objTrans[1], 181));

            objTrans[0].Translate(Vector3.forward * 5);
            objTrans[1].Translate(Vector3.forward * 5);
            Assert.IsFalse(TransformUtility.CheckFOV(trans, objTrans[0], 45));
            Assert.IsFalse(TransformUtility.CheckFOV(trans, objTrans[0], 109.4f));
            Assert.IsTrue(TransformUtility.CheckFOV(trans, objTrans[0], 109.5f));
            Assert.IsTrue(TransformUtility.CheckFOV(trans, objTrans[0], 110));
            Assert.IsFalse(TransformUtility.CheckFOV(trans, objTrans[1], 45));
            Assert.IsFalse(TransformUtility.CheckFOV(trans, objTrans[1], 109.4f));
            Assert.IsTrue(TransformUtility.CheckFOV(trans, objTrans[1], 109.5f));
            Assert.IsTrue(TransformUtility.CheckFOV(trans, objTrans[1], 110));
        }
        [Test]
        public void CheckFOV_InvalidInput_Tests()
        {
            #region FOV Min
            Assert.IsFalse(TransformUtility.CheckFOV(null, null, -1));

            Assert.IsFalse(TransformUtility.CheckFOV(trans, null, -1));
            Assert.IsFalse(TransformUtility.CheckFOV(null, objTrans[0], -1));
            Assert.IsFalse(TransformUtility.CheckFOV(null, null, 0));

            Assert.IsFalse(TransformUtility.CheckFOV(trans, objTrans[0], -1));
            Assert.IsFalse(TransformUtility.CheckFOV(trans, null, 0));

            Assert.IsFalse(TransformUtility.CheckFOV(null, objTrans[0], 0));
            #endregion

            #region FOV Min
            Assert.IsFalse(TransformUtility.CheckFOV(null, null, 361));

            Assert.IsFalse(TransformUtility.CheckFOV(trans, null, 361));
            Assert.IsFalse(TransformUtility.CheckFOV(null, objTrans[0], 361));
            Assert.IsFalse(TransformUtility.CheckFOV(null, null, 360));

            Assert.IsFalse(TransformUtility.CheckFOV(trans, objTrans[0], 361));
            Assert.IsFalse(TransformUtility.CheckFOV(trans, null, 360));

            Assert.IsFalse(TransformUtility.CheckFOV(null, objTrans[0], 360));
            #endregion
        }

        [SetUp]
        public void Setup()
        {
            trans = new GameObject("ObjA").transform;

            objTrans.Add(new GameObject("ObjB").transform);
            objTrans.Add(new GameObject("ObjC").transform);
            objTrans.Add(new GameObject("ObjD").transform);

            foreach (Transform obj in objTrans) objColls.Add(obj.gameObject.AddComponent<BoxCollider>());
        }
        [TearDown]
        public void TearDown()
        {
            if (trans) GameObject.DestroyImmediate(trans.gameObject);
            trans = null;

            foreach (Transform obj in objTrans)
            {
                if (obj) GameObject.DestroyImmediate(obj.gameObject);
            }
            objTrans = new List<Transform>();
            objColls = new List<Collider>();
        }
    }
}
