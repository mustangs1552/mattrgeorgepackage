﻿using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using MattRGeorge.General.Utilities.Static;
using MattRGeorge.Unity.Utilities.Components;

namespace MattRGeorge.Tests.Unity.Utilities.Components
{
    public class TranslateObjectViaNodesTests
    {
        private TranslateObjectViaNodes obj = null;
        private List<Transform> nodes = new List<Transform>();
        private List<Transform> nodesTwo = new List<Transform>();
        private List<Transform> returnedList = new List<Transform>();

        [Test]
        public void Nodes_Tests()
        {
            obj.Nodes = nodes;
            returnedList = obj.Nodes;
            Assert.IsNotNull(returnedList);
            CollectionAssert.AllItemsAreNotNull(returnedList);
            CollectionAssert.AreEqual(nodes, returnedList);
            Assert.AreEqual(ReflectionUtility.GetInstanceField(typeof(TranslateObjectViaNodes), obj, "nodes"), returnedList);

            returnedList.Add(nodes[0]);
            obj.Nodes = returnedList;
            returnedList = obj.Nodes;
            Assert.IsNotNull(returnedList);
            CollectionAssert.AllItemsAreNotNull(returnedList);
            Assert.AreEqual(nodes.Count + 1, returnedList.Count);
            Assert.AreEqual(nodes[0], returnedList[returnedList.Count - 1]);
            Assert.AreEqual(ReflectionUtility.GetInstanceField(typeof(TranslateObjectViaNodes), obj, "nodes"), returnedList);

            returnedList.Add(nodes[0]);
            Assert.AreNotEqual(ReflectionUtility.GetInstanceField(typeof(TranslateObjectViaNodes), obj, "nodes"), returnedList);
        }
        [Test]
        public void Nodes_InvalidInput_Tests()
        {
            obj.Nodes = null;
            returnedList = obj.Nodes;
            Assert.IsNotNull(returnedList);
            CollectionAssert.AllItemsAreNotNull(returnedList);
            Assert.AreEqual(0, returnedList.Count);
            Assert.AreEqual(ReflectionUtility.GetInstanceField(typeof(TranslateObjectViaNodes), obj, "nodes"), returnedList);

            nodesTwo.Add(new GameObject("TestNode-1").transform);
            nodesTwo.Add(null);
            nodesTwo.Add(new GameObject("TestNode-3").transform);
            obj.Nodes = nodesTwo;
            returnedList = obj.Nodes;
            Assert.IsNotNull(returnedList);
            CollectionAssert.AllItemsAreNotNull(returnedList);
            Assert.AreEqual(2, returnedList.Count);
            Assert.AreEqual(ReflectionUtility.GetInstanceField(typeof(TranslateObjectViaNodes), obj, "nodes"), returnedList);
        }

        [SetUp]
        public void Setup()
        {
            obj = new GameObject("Obj").AddComponent<TranslateObjectViaNodes>();

            for(int i = 0; i <= 5; i++) nodes.Add(new GameObject($"Node-{i + 1}").transform);
        }
        [TearDown]
        public void TearDown()
        {
            if (obj) GameObject.DestroyImmediate(obj.gameObject);
            obj = null;

            foreach(Transform node in nodes)
            {
                if (node) GameObject.DestroyImmediate(node.gameObject);
            }
            nodes = new List<Transform>();

            foreach (Transform node in nodesTwo)
            {
                if (node) GameObject.DestroyImmediate(node.gameObject);
            }
            nodesTwo = new List<Transform>();

            returnedList = new List<Transform>();
        }
    }
}
