﻿using NUnit.Framework;
using UnityEngine;
using MattRGeorge.General.Utilities.Static;
using MattRGeorge.Unity.Utilities.Helpers;
using MattRGeorge.Unity.Utilities.Components;

namespace MattRGeorge.Tests.Unity.Utilities.Components
{
    public class SpawnObjectNodeTests
    {
        private GameObjectBundle objBundle = new GameObjectBundle();
        private SpawnObjectNode spawnObj = null;
        private float returnedFloat = 0;

        [Test]
        public void SpawnDelay_Tests()
        {
            spawnObj.SpawnDelay = 5;
            returnedFloat = (float)ReflectionUtility.GetInstanceField(typeof(SpawnObjectNode), spawnObj, "spawnDelay");
            Assert.AreEqual(5, returnedFloat);
            Assert.AreEqual(returnedFloat, spawnObj.SpawnDelay);

            spawnObj.SpawnDelay = 10;
            returnedFloat = (float)ReflectionUtility.GetInstanceField(typeof(SpawnObjectNode), spawnObj, "spawnDelay");
            Assert.AreEqual(10, returnedFloat);
            Assert.AreEqual(returnedFloat, spawnObj.SpawnDelay);

            spawnObj.SpawnDelay = 1000;
            returnedFloat = (float)ReflectionUtility.GetInstanceField(typeof(SpawnObjectNode), spawnObj, "spawnDelay");
            Assert.AreEqual(1000, returnedFloat);
            Assert.AreEqual(returnedFloat, spawnObj.SpawnDelay);
        }
        [Test]
        public void SpawnDelay_InvalidInput_Tests()
        {
            spawnObj.SpawnDelay = -5;
            returnedFloat = (float)ReflectionUtility.GetInstanceField(typeof(SpawnObjectNode), spawnObj, "spawnDelay");
            Assert.AreEqual(0, returnedFloat);
            Assert.AreEqual(returnedFloat, spawnObj.SpawnDelay);

            spawnObj.SpawnDelay = -10;
            returnedFloat = (float)ReflectionUtility.GetInstanceField(typeof(SpawnObjectNode), spawnObj, "spawnDelay");
            Assert.AreEqual(0, returnedFloat);
            Assert.AreEqual(returnedFloat, spawnObj.SpawnDelay);

            spawnObj.SpawnDelay = 5;
            returnedFloat = (float)ReflectionUtility.GetInstanceField(typeof(SpawnObjectNode), spawnObj, "spawnDelay");
            Assert.AreEqual(5, returnedFloat);
            Assert.AreEqual(returnedFloat, spawnObj.SpawnDelay);

            spawnObj.SpawnDelay = -1000;
            returnedFloat = (float)ReflectionUtility.GetInstanceField(typeof(SpawnObjectNode), spawnObj, "spawnDelay");
            Assert.AreEqual(0, returnedFloat);
            Assert.AreEqual(returnedFloat, spawnObj.SpawnDelay);
        }

        [Test]
        public void PostSpawnDelay_Tests()
        {
            spawnObj.PostSpawnDelay = 5;
            returnedFloat = (float)ReflectionUtility.GetInstanceField(typeof(SpawnObjectNode), spawnObj, "postSpawnDelay");
            Assert.AreEqual(5, returnedFloat);
            Assert.AreEqual(returnedFloat, spawnObj.PostSpawnDelay);

            spawnObj.PostSpawnDelay = 10;
            returnedFloat = (float)ReflectionUtility.GetInstanceField(typeof(SpawnObjectNode), spawnObj, "postSpawnDelay");
            Assert.AreEqual(10, returnedFloat);
            Assert.AreEqual(returnedFloat, spawnObj.PostSpawnDelay);

            spawnObj.PostSpawnDelay = 1000;
            returnedFloat = (float)ReflectionUtility.GetInstanceField(typeof(SpawnObjectNode), spawnObj, "postSpawnDelay");
            Assert.AreEqual(1000, returnedFloat);
            Assert.AreEqual(returnedFloat, spawnObj.PostSpawnDelay);
        }
        [Test]
        public void PostSpawnDelay_InvalidInput_Tests()
        {
            spawnObj.PostSpawnDelay = -5;
            returnedFloat = (float)ReflectionUtility.GetInstanceField(typeof(SpawnObjectNode), spawnObj, "postSpawnDelay");
            Assert.AreEqual(0, returnedFloat);
            Assert.AreEqual(returnedFloat, spawnObj.PostSpawnDelay);

            spawnObj.PostSpawnDelay = -10;
            returnedFloat = (float)ReflectionUtility.GetInstanceField(typeof(SpawnObjectNode), spawnObj, "postSpawnDelay");
            Assert.AreEqual(0, returnedFloat);
            Assert.AreEqual(returnedFloat, spawnObj.PostSpawnDelay);

            spawnObj.PostSpawnDelay = 5;
            returnedFloat = (float)ReflectionUtility.GetInstanceField(typeof(SpawnObjectNode), spawnObj, "postSpawnDelay");
            Assert.AreEqual(5, returnedFloat);
            Assert.AreEqual(returnedFloat, spawnObj.PostSpawnDelay);

            spawnObj.PostSpawnDelay = -1000;
            returnedFloat = (float)ReflectionUtility.GetInstanceField(typeof(SpawnObjectNode), spawnObj, "postSpawnDelay");
            Assert.AreEqual(0, returnedFloat);
            Assert.AreEqual(returnedFloat, spawnObj.PostSpawnDelay);
        }

        [Test]
        public void IsSpawnable_Tests()
        {
            Assert.IsTrue(spawnObj.IsSpawnable(spawnObj.gameObject));
            Assert.IsFalse(spawnObj.IsSpawnable(null));

            GameObject spawnObjGO = spawnObj.gameObject;
            GameObject.DestroyImmediate(spawnObj.gameObject);
            Assert.IsFalse(spawnObj.IsSpawnable(spawnObjGO));
        }

        [SetUp]
        public void Setup()
        {
            spawnObj = objBundle.CreateNewGameObject("SpawnObject").AddComponent<SpawnObjectNode>();
        }
        [TearDown]
        public void TearDown()
        {
            objBundle.DestroyObjs(true);

            returnedFloat = 0;
        }
    }
}
