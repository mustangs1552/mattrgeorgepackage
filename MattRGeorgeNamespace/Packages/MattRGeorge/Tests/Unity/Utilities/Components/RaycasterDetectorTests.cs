﻿using NUnit.Framework;
using UnityEngine;
using MattRGeorge.General.Utilities.Static;
using MattRGeorge.Unity.Utilities.Components;

namespace MattRGeorge.Tests.Unity.Utilities.Components
{
    public class RaycasterDetectorTests
    {
        private RaycasterDetector raycaster = null;
        float testFloat = 0;
        bool testBool = false;

        [Test]
        public void Range_Tests()
        {
            raycaster.Range = 10;
            testFloat = raycaster.Range;
            Assert.AreEqual(10, testFloat);
            Assert.AreEqual(ReflectionUtility.GetInstanceField(typeof(RaycasterDetector), raycaster, "range"), testFloat);
            
            raycaster.Range = 100;
            testFloat = raycaster.Range;
            Assert.AreEqual(100, testFloat);
            Assert.AreEqual(ReflectionUtility.GetInstanceField(typeof(RaycasterDetector), raycaster, "range"), testFloat);
            
            raycaster.Range = 1;
            testFloat = raycaster.Range;
            Assert.AreEqual(1, testFloat);
            Assert.AreEqual(ReflectionUtility.GetInstanceField(typeof(RaycasterDetector), raycaster, "range"), testFloat);

            raycaster.Range = 0;
            testFloat = raycaster.Range;
            Assert.AreEqual(0, testFloat);
            Assert.AreEqual(ReflectionUtility.GetInstanceField(typeof(RaycasterDetector), raycaster, "range"), testFloat);
        }
        [Test]
        public void Range_InvalidInput_Tests()
        {
            raycaster.Range = -10;
            testFloat = raycaster.Range;
            Assert.AreEqual(0, testFloat);
            Assert.AreEqual(ReflectionUtility.GetInstanceField(typeof(RaycasterDetector), raycaster, "range"), testFloat);

            raycaster.Range = -100;
            testFloat = raycaster.Range;
            Assert.AreEqual(0, testFloat);
            Assert.AreEqual(ReflectionUtility.GetInstanceField(typeof(RaycasterDetector), raycaster, "range"), testFloat);

            raycaster.Range = -1;
            testFloat = raycaster.Range;
            Assert.AreEqual(0, testFloat);
            Assert.AreEqual(ReflectionUtility.GetInstanceField(typeof(RaycasterDetector), raycaster, "range"), testFloat);
        }

        [Test]
        public void Auto_Tests()
        {
            raycaster.Auto = true;
            testBool = raycaster.Auto;
            Assert.IsTrue(testBool);
            Assert.AreEqual(ReflectionUtility.GetInstanceField(typeof(RaycasterDetector), raycaster, "auto"), testBool);

            raycaster.Auto = false;
            testBool = raycaster.Auto;
            Assert.IsFalse(testBool);
            Assert.AreEqual(ReflectionUtility.GetInstanceField(typeof(RaycasterDetector), raycaster, "auto"), testBool);

            raycaster.Auto = true;
            testBool = raycaster.Auto;
            Assert.IsTrue(testBool);
            Assert.AreEqual(ReflectionUtility.GetInstanceField(typeof(RaycasterDetector), raycaster, "auto"), testBool);
        }

        [Test]
        public void Frequency_Tests()
        {
            raycaster.Frequency = 10;
            testFloat = raycaster.Frequency;
            Assert.AreEqual(10, testFloat);
            Assert.AreEqual(ReflectionUtility.GetInstanceField(typeof(RaycasterDetector), raycaster, "frequency"), testFloat);

            raycaster.Frequency = 100;
            testFloat = raycaster.Frequency;
            Assert.AreEqual(100, testFloat);
            Assert.AreEqual(ReflectionUtility.GetInstanceField(typeof(RaycasterDetector), raycaster, "frequency"), testFloat);

            raycaster.Frequency = 1;
            testFloat = raycaster.Frequency;
            Assert.AreEqual(1, testFloat);
            Assert.AreEqual(ReflectionUtility.GetInstanceField(typeof(RaycasterDetector), raycaster, "frequency"), testFloat);

            raycaster.Frequency = 0;
            testFloat = raycaster.Frequency;
            Assert.AreEqual(0, testFloat);
            Assert.AreEqual(ReflectionUtility.GetInstanceField(typeof(RaycasterDetector), raycaster, "frequency"), testFloat);
        }
        [Test]
        public void Frequency_InvalidInput_Tests()
        {
            raycaster.Frequency = -10;
            testFloat = raycaster.Frequency;
            Assert.AreEqual(0, testFloat);
            Assert.AreEqual(ReflectionUtility.GetInstanceField(typeof(RaycasterDetector), raycaster, "frequency"), testFloat);

            raycaster.Frequency = -100;
            testFloat = raycaster.Frequency;
            Assert.AreEqual(0, testFloat);
            Assert.AreEqual(ReflectionUtility.GetInstanceField(typeof(RaycasterDetector), raycaster, "frequency"), testFloat);

            raycaster.Frequency = -1;
            testFloat = raycaster.Frequency;
            Assert.AreEqual(0, testFloat);
            Assert.AreEqual(ReflectionUtility.GetInstanceField(typeof(RaycasterDetector), raycaster, "frequency"), testFloat);
        }

        [SetUp]
        public void Setup()
        {
            raycaster = new GameObject("RaycasterDetector").AddComponent<RaycasterDetector>();
        }
        [TearDown]
        public void TearDown()
        {
            if (raycaster) GameObject.DestroyImmediate(raycaster.gameObject);
            raycaster = null;
        }
    }
}
