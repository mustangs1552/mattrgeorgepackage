﻿using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using MattRGeorge.General.Utilities.Static;
using MattRGeorge.Unity.Utilities.Helpers;
using MattRGeorge.Unity.Utilities.Components;

namespace MattRGeorge.Tests.Unity.Utilities.Components
{
    public class AssetLibraryWithChancesTests
    {
        private AssetLibraryWithChances assetLib = null;
        private List<DictionaryNameListDictionaryGameObjectChance> objChanceLists = new List<DictionaryNameListDictionaryGameObjectChance>();
        private List<DictionaryNameListDictionaryGameObjectChance> objChanceListsTwo = new List<DictionaryNameListDictionaryGameObjectChance>();
        private List<List<GameObject>> objLists = new List<List<GameObject>>();
        private GameObject returnedObj = null;
        private List<GameObject> returnedList = new List<GameObject>();
        private HashSet<int> indexes = null;
        private List<string> stringList = new List<string>();
        private List<string> stringList2 = new List<string>();
        private string outString = "";
        private List<DictionaryNameListDictionaryGameObjectChance> setObjs = new List<DictionaryNameListDictionaryGameObjectChance>();

        [Test]
        public void ObjsWithChance_Tests()
        {
            assetLib.ObjsWithChance = objChanceLists;
            setObjs = assetLib.ObjsWithChance;
            Assert.IsNotNull(setObjs);
            CollectionAssert.AllItemsAreNotNull(setObjs);
            foreach (DictionaryNameListDictionaryGameObjectChance list in setObjs)
            {
                Assert.IsNotNull(list.name);
                Assert.IsNotNull(list.objs);
                CollectionAssert.AllItemsAreNotNull(list.objs);
            }
            CollectionAssert.AreEqual(objChanceLists, setObjs);
            Assert.AreEqual(ReflectionUtility.GetInstanceField(typeof(AssetLibraryWithChances), assetLib, "objsWithChance"), setObjs);

            setObjs.Add(objChanceLists[0]);
            assetLib.ObjsWithChance = setObjs;
            setObjs = assetLib.ObjsWithChance;
            Assert.IsNotNull(setObjs);
            CollectionAssert.AllItemsAreNotNull(setObjs);
            foreach (DictionaryNameListDictionaryGameObjectChance list in setObjs)
            {
                Assert.IsNotNull(list.name);
                Assert.IsNotNull(list.objs);
                CollectionAssert.AllItemsAreNotNull(list.objs);
            }
            Assert.AreEqual(objChanceLists.Count + 1, setObjs.Count);
            Assert.AreEqual(objChanceLists[0], setObjs[setObjs.Count - 1]);
            Assert.AreEqual(ReflectionUtility.GetInstanceField(typeof(AssetLibraryWithChances), assetLib, "objsWithChance"), setObjs);

            setObjs.Add(objChanceLists[0]);
            Assert.AreNotEqual(ReflectionUtility.GetInstanceField(typeof(AssetLibraryWithChances), assetLib, "objsWithChance"), setObjs);
        }
        [Test]
        public void ObjsWithChance_InvalidInput_Tests()
        {
            SetList();

            assetLib.ObjsWithChance = null;
            List<DictionaryNameListDictionaryGameObjectChance> setObjs = assetLib.ObjsWithChance;
            Assert.IsNotNull(setObjs);
            CollectionAssert.AllItemsAreNotNull(setObjs);
            Assert.AreEqual(0, setObjs.Count);
            Assert.AreEqual(ReflectionUtility.GetInstanceField(typeof(AssetLibraryWithChances), assetLib, "objsWithChance"), setObjs);

            objChanceListsTwo = new List<DictionaryNameListDictionaryGameObjectChance>()
            {
                new DictionaryNameListDictionaryGameObjectChance(),
                null,
                new DictionaryNameListDictionaryGameObjectChance()
            };
            assetLib.ObjsWithChance = objChanceListsTwo;
            setObjs = assetLib.ObjsWithChance;
            Assert.IsNotNull(setObjs);
            CollectionAssert.AllItemsAreNotNull(setObjs);
            Assert.AreEqual(2, setObjs.Count);
            Assert.AreEqual(ReflectionUtility.GetInstanceField(typeof(AssetLibraryWithChances), assetLib, "objsWithChance"), setObjs);
        }

        [Test]
        public void GetList_Tests()
        {
            SetList();

            returnedList = assetLib.GetList("List1");
            Assert.IsNotNull(returnedList);
            CollectionAssert.IsNotEmpty(returnedList);
            CollectionAssert.AreEqual(objLists[0], returnedList);

            returnedList = assetLib.GetList("List2");
            Assert.IsNotNull(returnedList);
            CollectionAssert.IsNotEmpty(returnedList);
            CollectionAssert.AreEqual(objLists[1], returnedList);

            returnedList = assetLib.GetList("List3");
            Assert.IsNotNull(returnedList);
            CollectionAssert.IsNotEmpty(returnedList);
            CollectionAssert.AreEqual(objLists[2], returnedList);
        }
        [Test]
        public void GetList_InvalidInputs_Tests()
        {
            returnedList = assetLib.GetList(null);
            Assert.IsNotNull(returnedList);
            CollectionAssert.IsEmpty(returnedList);
            returnedList = assetLib.GetList("");
            Assert.IsNotNull(returnedList);
            CollectionAssert.IsEmpty(returnedList);
        }

        [Test]
        public void GetRandomObject_Tests()
        {
            SetList();

            for (int i = 0; i < 10; i++)
            {
                returnedObj = assetLib.GetRandomObjectWithChance("List1");
                Assert.IsNotNull(returnedObj);
                CheckObjInList(0, returnedObj);
            }

            for (int i = 0; i < 10; i++)
            {
                returnedObj = assetLib.GetRandomObjectWithChance("List2");
                Assert.IsNotNull(returnedObj);
                CheckObjInList(1, returnedObj);
            }

            for (int i = 0; i < 10; i++)
            {
                returnedObj = assetLib.GetRandomObjectWithChance("List3");
                Assert.IsNotNull(returnedObj);
                CheckObjInList(2, returnedObj);
            }
        }
        [Test]
        public void GetRandomObject_WithInvalidIndexes_Tests()
        {
            SetList();

            returnedObj = assetLib.GetRandomObjectWithChance("List1", ref indexes);
            Assert.IsNotNull(returnedObj);
            Assert.AreEqual(1, indexes.Count);
            returnedObj = assetLib.GetRandomObjectWithChance("List1", ref indexes);
            Assert.IsNotNull(returnedObj);
            Assert.AreEqual(2, indexes.Count);
            returnedObj = assetLib.GetRandomObjectWithChance("List1", ref indexes);
            Assert.IsNotNull(returnedObj);
            Assert.AreEqual(3, indexes.Count);
            returnedObj = assetLib.GetRandomObjectWithChance("List1", ref indexes);
            Assert.IsNotNull(returnedObj);
            Assert.AreEqual(4, indexes.Count);
            returnedObj = assetLib.GetRandomObjectWithChance("List1", ref indexes);
            Assert.IsNotNull(returnedObj);
            Assert.AreEqual(5, indexes.Count);
            foreach (int index in indexes)
            {
                Assert.GreaterOrEqual(index, 0);
                Assert.Less(index, 5);
            }

            returnedObj = assetLib.GetRandomObjectWithChance("List1", ref indexes);
            Assert.IsNull(returnedObj);
            Assert.AreEqual(5, indexes.Count);

            indexes = new HashSet<int>();
            returnedObj = assetLib.GetRandomObjectWithChance("List1", ref indexes);
            Assert.IsNotNull(returnedObj);
            Assert.AreEqual(1, indexes.Count);
            returnedObj = assetLib.GetRandomObjectWithChance("List1", ref indexes);
            Assert.IsNotNull(returnedObj);
            Assert.AreEqual(2, indexes.Count);
            returnedObj = assetLib.GetRandomObjectWithChance("List1", ref indexes);
            Assert.IsNotNull(returnedObj);
            Assert.AreEqual(3, indexes.Count);
            returnedObj = assetLib.GetRandomObjectWithChance("List1", ref indexes);
            Assert.IsNotNull(returnedObj);
            Assert.AreEqual(4, indexes.Count);
            returnedObj = assetLib.GetRandomObjectWithChance("List1", ref indexes);
            Assert.IsNotNull(returnedObj);
            Assert.AreEqual(5, indexes.Count);
            foreach (int index in indexes)
            {
                Assert.GreaterOrEqual(index, 0);
                Assert.Less(index, 5);
            }

            returnedObj = assetLib.GetRandomObjectWithChance("List1", ref indexes);
            Assert.IsNull(returnedObj);
            Assert.AreEqual(5, indexes.Count);
        }
        [Test]
        public void GetRandomObject_MultipleLists_Tests()
        {
            SetList();

            stringList = new List<string>() { "List1" };
            stringList2 = new List<string>() { "List1", "List2", "List3" };
            for (int i = 0; i < 30; i++)
            {
                returnedObj = assetLib.GetRandomObjectWithChance((i % 2 == 0) ? stringList : stringList2);
                Assert.IsNotNull(returnedObj);
                CheckObjInAllLists(returnedObj);
            }
        }
        [Test]
        public void GetRandomObject_MultipleLists_WithInvalidIndexes_Tests()
        {
            SetList();

            stringList = new List<string>() { "List1" };
            stringList2 = new List<string>() { "List1", "List2", "List3" };
            for (int i = 0; i < 30; i++)
            {
                returnedObj = assetLib.GetRandomObjectWithChance((i % 2 == 0) ? stringList : stringList2, out outString);
                Assert.IsNotNull(returnedObj);
                Assert.IsNotNull(outString);
                Assert.IsTrue(outString == "List1" || outString == "List2" || outString == "List3");
                switch(outString)
                {
                    case "List1":
                        CheckObjInList(0, returnedObj);
                        break;
                    case "List2":
                        CheckObjInList(1, returnedObj);
                        break;
                    case "List3":
                        CheckObjInList(2, returnedObj);
                        break;
                }
            }
        }
        [Test]
        public void GetRandomObject_InvalidInputs_Tests()
        {
            returnedObj = assetLib.GetRandomObjectWithChance("");
            Assert.IsNull(returnedObj);
            returnedObj = assetLib.GetRandomObjectWithChance("", ref indexes);
            Assert.IsNull(returnedObj);

            stringList = null;
            returnedObj = assetLib.GetRandomObjectWithChance(stringList);
            Assert.IsNull(returnedObj);
            stringList = new List<string>();
            returnedObj = assetLib.GetRandomObjectWithChance(stringList);
            Assert.IsNull(returnedObj);
        }

        [Test]
        public void ClearGeneratedLists_Tests()
        {
            SetList();
            assetLib.GetList("List1");
            Assert.IsNotNull(ReflectionUtility.GetInstanceField(typeof(AssetLibraryWithChances), assetLib, "objsObjsWithChance"));

            assetLib.ClearGeneratedLists();
            Assert.IsNull(ReflectionUtility.GetInstanceField(typeof(AssetLibraryWithChances), assetLib, "objsObjsWithChance"));
        }

        [SetUp]
        public void Setup()
        {
            assetLib = new GameObject("AssetLibrary").AddComponent<AssetLibraryWithChances>();

            objLists = new List<List<GameObject>>();
            objLists.Add(new List<GameObject>());
            List<DictionaryGameObjectChance> tempObjs = new List<DictionaryGameObjectChance>();
            DictionaryGameObjectChance currObjChance = new DictionaryGameObjectChance();
            for (int i = 1; i <= 5; i++)
            {
                currObjChance = new DictionaryGameObjectChance();
                currObjChance.chance = i * 2;
                currObjChance.obj = new GameObject($"Obj{i}");
                objLists[0].Add(currObjChance.obj);
                tempObjs.Add(currObjChance);
            }
            DictionaryNameListDictionaryGameObjectChance objList = new DictionaryNameListDictionaryGameObjectChance();
            objList.name = "List1";
            objList.objs = tempObjs;
            objChanceLists.Add(objList);
            objLists.Add(new List<GameObject>());
            tempObjs = new List<DictionaryGameObjectChance>();
            for (int i = 6; i <= 10; i++)
            {
                currObjChance = new DictionaryGameObjectChance();
                currObjChance.chance = i * 2;
                currObjChance.obj = new GameObject($"Obj{i}");
                objLists[1].Add(currObjChance.obj);
                tempObjs.Add(currObjChance);
            }
            objList = new DictionaryNameListDictionaryGameObjectChance();
            objList.name = "List2";
            objList.objs = tempObjs;
            objChanceLists.Add(objList);
            objLists.Add(new List<GameObject>());
            tempObjs = new List<DictionaryGameObjectChance>();
            for (int i = 11; i <= 15; i++)
            {
                currObjChance = new DictionaryGameObjectChance();
                currObjChance.chance = i * 2;
                currObjChance.obj = new GameObject($"Obj{i}");
                objLists[2].Add(currObjChance.obj);
                tempObjs.Add(currObjChance);
            }
            objList = new DictionaryNameListDictionaryGameObjectChance();
            objList.name = "List3";
            objList.objs = tempObjs;
            objChanceLists.Add(objList);

            indexes = new HashSet<int>();
            stringList = new List<string>();
            stringList2 = new List<string>();
        }
        [TearDown]
        public void TearDown()
        {
            if (assetLib) GameObject.DestroyImmediate(assetLib.gameObject);
            assetLib = null;

            foreach(DictionaryNameListDictionaryGameObjectChance nLGO in objChanceLists)
            {
                foreach (DictionaryGameObjectChance goChance in nLGO.objs)
                {
                    if (goChance.obj) GameObject.DestroyImmediate(goChance.obj);
                }
            }
            foreach (DictionaryNameListDictionaryGameObjectChance nLGO in objChanceListsTwo)
            {
                if (nLGO == null || nLGO.objs == null) continue;
                foreach (DictionaryGameObjectChance goChance in nLGO.objs)
                {
                    if (goChance.obj) GameObject.DestroyImmediate(goChance.obj);
                }
            }
            objChanceLists = new List<DictionaryNameListDictionaryGameObjectChance>();
            objChanceListsTwo = new List<DictionaryNameListDictionaryGameObjectChance>();
            objLists = new List<List<GameObject>>();

            setObjs = new List<DictionaryNameListDictionaryGameObjectChance>();
        }

        private void SetList()
        {
            ReflectionUtility.SetInstanceField(typeof(AssetLibraryWithChances), assetLib, "objsWithChance", objChanceLists);
        }

        private void CheckObjInList(int listI, GameObject obj)
        {
            Assert.IsTrue(
                obj == objLists[listI][0] ||
                obj == objLists[listI][1] ||
                obj == objLists[listI][2] ||
                obj == objLists[listI][3] ||
                obj == objLists[listI][4]);
        }
        private void CheckObjInAllLists(GameObject obj)
        {
            Assert.IsTrue(
                obj == objLists[0][0] ||
                obj == objLists[0][1] ||
                obj == objLists[0][2] ||
                obj == objLists[0][3] ||
                obj == objLists[0][4] ||
                obj == objLists[1][0] ||
                obj == objLists[1][1] ||
                obj == objLists[1][2] ||
                obj == objLists[1][3] ||
                obj == objLists[1][4] ||
                obj == objLists[2][0] ||
                obj == objLists[2][1] ||
                obj == objLists[2][2] ||
                obj == objLists[2][3] ||
                obj == objLists[2][4]);
        }
    }
}
