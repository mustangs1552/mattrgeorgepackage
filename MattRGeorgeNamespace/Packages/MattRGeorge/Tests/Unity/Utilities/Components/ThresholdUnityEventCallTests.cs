﻿using UnityEngine;
using NUnit.Framework;
using MattRGeorge.General.Utilities.Static;
using MattRGeorge.Unity.Utilities.Components;

namespace MattRGeorge.Tests.Unity.Utilities.Components
{
    public class ThresholdUnityEventCallTests
    {
        private ThresholdUnityEventCall obj = null;
        private bool onAmountIncreasedRan = false;
        private bool onAmountDecreasedRan = false;
        private bool onThresholdReachedRan = false;

        [Test]
        public void Threshold_Tests()
        {
            obj.Threshold = 3;
            Assert.AreEqual(3, ReflectionUtility.GetInstanceField(typeof(ThresholdUnityEventCall), obj, "threshold"));
            Assert.AreEqual(3, obj.Threshold);
            
            obj.Threshold = 10;
            Assert.AreEqual(10, ReflectionUtility.GetInstanceField(typeof(ThresholdUnityEventCall), obj, "threshold"));
            Assert.AreEqual(10, obj.Threshold);

            obj.Threshold = 0;
            Assert.AreEqual(1, ReflectionUtility.GetInstanceField(typeof(ThresholdUnityEventCall), obj, "threshold"));
            Assert.AreEqual(1, obj.Threshold);

            obj.Threshold = -3;
            Assert.AreEqual(1, ReflectionUtility.GetInstanceField(typeof(ThresholdUnityEventCall), obj, "threshold"));
            Assert.AreEqual(1, obj.Threshold);
        }
        [Test]
        public void StartAmount_Tests()
        {
            obj.StartAmount = 1;
            Assert.AreEqual(1, ReflectionUtility.GetInstanceField(typeof(ThresholdUnityEventCall), obj, "startAmount"));
            Assert.AreEqual(1, obj.StartAmount);

            obj.StartAmount = 2;
            Assert.AreEqual(2, ReflectionUtility.GetInstanceField(typeof(ThresholdUnityEventCall), obj, "startAmount"));
            Assert.AreEqual(2, obj.StartAmount);

            obj.StartAmount = 10;
            Assert.AreEqual(3, ReflectionUtility.GetInstanceField(typeof(ThresholdUnityEventCall), obj, "startAmount"));
            Assert.AreEqual(3, obj.StartAmount);

            obj.StartAmount = 0;
            Assert.AreEqual(0, ReflectionUtility.GetInstanceField(typeof(ThresholdUnityEventCall), obj, "startAmount"));
            Assert.AreEqual(0, obj.StartAmount);

            obj.StartAmount = -3;
            Assert.AreEqual(0, ReflectionUtility.GetInstanceField(typeof(ThresholdUnityEventCall), obj, "startAmount"));
            Assert.AreEqual(0, obj.StartAmount);
        }
        [Test]
        public void Called_Tests()
        {
            Assert.AreEqual(ReflectionUtility.GetInstanceField(typeof(ThresholdUnityEventCall), obj, "called"), obj.Called);
            ReflectionUtility.SetInstanceField(typeof(ThresholdUnityEventCall), obj, "called", true);
            Assert.AreEqual(ReflectionUtility.GetInstanceField(typeof(ThresholdUnityEventCall), obj, "called"), obj.Called);
        }
        [Test]
        public void ThresholdProgress_Tests()
        {
            obj.Threshold = 10;
            obj.autoResetAmountOnCalled = false;

            Assert.AreEqual(0, obj.ThresholdProgress);
            obj.IncreaseAmount();
            Assert.AreEqual(.1f, obj.ThresholdProgress);
            obj.IncreaseAmount(4);
            Assert.AreEqual(.5f, obj.ThresholdProgress);
            obj.IncreaseAmount(3);
            Assert.AreEqual(.8f, obj.ThresholdProgress);
            obj.IncreaseAmount();
            Assert.AreEqual(.9f, obj.ThresholdProgress);
            obj.IncreaseAmount();
            Assert.AreEqual(1, obj.ThresholdProgress);
            obj.IncreaseAmount();
            Assert.AreEqual(1, obj.ThresholdProgress);
        }
        [Test]
        public void CurrAmount_Tests()
        {
            Assert.AreEqual(ReflectionUtility.GetInstanceField(typeof(ThresholdUnityEventCall), obj, "currAmount"), obj.CurrAmount);
            obj.IncreaseAmount();
            Assert.AreEqual(1, obj.CurrAmount);
            Assert.AreEqual(ReflectionUtility.GetInstanceField(typeof(ThresholdUnityEventCall), obj, "currAmount"), obj.CurrAmount);
        }

        [Test]
        public void IncreaseAmount_Tests()
        {
            obj.autoResetAmountOnCalled = false;
            Assert.IsFalse(onAmountDecreasedRan);
            Assert.IsFalse(onAmountIncreasedRan);
            Assert.IsFalse(onThresholdReachedRan);
            Assert.AreEqual(0, obj.CurrAmount);

            obj.IncreaseAmount(2);
            Assert.IsFalse(onAmountDecreasedRan);
            Assert.IsTrue(onAmountIncreasedRan);
            Assert.IsFalse(onThresholdReachedRan);
            Assert.AreEqual(2, obj.CurrAmount);

            ResetFlags();
            obj.IncreaseAmount();
            Assert.IsFalse(onAmountDecreasedRan);
            Assert.IsTrue(onAmountIncreasedRan);
            Assert.IsTrue(onThresholdReachedRan);
            Assert.AreEqual(3, obj.CurrAmount);

            ResetFlags();
            obj.IncreaseAmount();
            Assert.IsFalse(onAmountDecreasedRan);
            Assert.IsFalse(onAmountIncreasedRan);
            Assert.IsFalse(onThresholdReachedRan);
            Assert.AreEqual(3, obj.CurrAmount);

            obj.ResetAmount();
            obj.IncreaseAmount(4);
            Assert.IsFalse(onAmountDecreasedRan);
            Assert.IsTrue(onAmountIncreasedRan);
            Assert.IsTrue(onThresholdReachedRan);
            Assert.AreEqual(3, obj.CurrAmount);
        }
        [Test]
        public void IncreaseAmount_AutoResetOnReached_Tests()
        {
            Assert.IsFalse(onAmountDecreasedRan);
            Assert.IsFalse(onAmountIncreasedRan);
            Assert.IsFalse(onThresholdReachedRan);
            Assert.AreEqual(0, obj.CurrAmount);

            obj.IncreaseAmount(2);
            Assert.IsFalse(onAmountDecreasedRan);
            Assert.IsTrue(onAmountIncreasedRan);
            Assert.IsFalse(onThresholdReachedRan);
            Assert.AreEqual(2, obj.CurrAmount);

            ResetFlags();
            obj.IncreaseAmount();
            Assert.IsFalse(onAmountDecreasedRan);
            Assert.IsTrue(onAmountIncreasedRan);
            Assert.IsTrue(onThresholdReachedRan);
            Assert.AreEqual(0, obj.CurrAmount);

            ResetFlags();
            obj.IncreaseAmount();
            Assert.IsFalse(onAmountDecreasedRan);
            Assert.IsTrue(onAmountIncreasedRan);
            Assert.IsFalse(onThresholdReachedRan);
            Assert.AreEqual(1, obj.CurrAmount);

            obj.ResetAmount();
            obj.IncreaseAmount(4);
            Assert.IsFalse(onAmountDecreasedRan);
            Assert.IsTrue(onAmountIncreasedRan);
            Assert.IsTrue(onThresholdReachedRan);
            Assert.AreEqual(1, obj.CurrAmount);
        }
        [Test]
        public void IncreaseAmount_InvalidInput_Tests()
        {
            obj.IncreaseAmount(0);
            Assert.AreEqual(0, obj.CurrAmount);

            obj.IncreaseAmount(-1);
            Assert.AreEqual(0, obj.CurrAmount);
        }

        [Test]
        public void DecreaseAmount_Tests()
        {
            obj.StartAmount = 2;
            obj.ResetAmount(true);
            Assert.IsFalse(onAmountDecreasedRan);
            Assert.IsFalse(onAmountIncreasedRan);
            Assert.IsFalse(onThresholdReachedRan);
            Assert.AreEqual(2, obj.CurrAmount);

            obj.DecreaseAmount();
            Assert.AreEqual(1, obj.CurrAmount);
            Assert.IsTrue(onAmountDecreasedRan);
            Assert.IsFalse(onAmountIncreasedRan);
            Assert.IsFalse(onThresholdReachedRan);
            ResetFlags();
            obj.DecreaseAmount();
            Assert.AreEqual(0, obj.CurrAmount);
            Assert.IsTrue(onAmountDecreasedRan);
            Assert.IsFalse(onAmountIncreasedRan);
            Assert.IsFalse(onThresholdReachedRan);
            ResetFlags();
            obj.DecreaseAmount();
            Assert.AreEqual(0, obj.CurrAmount);
            Assert.IsFalse(onAmountDecreasedRan);
            Assert.IsFalse(onAmountIncreasedRan);
            Assert.IsFalse(onThresholdReachedRan);
            ResetFlags();

            obj.ResetAmount(true);
            Assert.IsFalse(onAmountDecreasedRan);
            Assert.IsFalse(onAmountIncreasedRan);
            Assert.IsFalse(onThresholdReachedRan);
            Assert.AreEqual(2, obj.CurrAmount);

            obj.DecreaseAmount(2);
            Assert.AreEqual(0, obj.CurrAmount);
            Assert.IsTrue(onAmountDecreasedRan);
            Assert.IsFalse(onAmountIncreasedRan);
            Assert.IsFalse(onThresholdReachedRan);
            ResetFlags();
            obj.DecreaseAmount(2);
            Assert.AreEqual(0, obj.CurrAmount);
            Assert.IsFalse(onAmountDecreasedRan);
            Assert.IsFalse(onAmountIncreasedRan);
            Assert.IsFalse(onThresholdReachedRan);
            ResetFlags();

            obj.ResetAmount(true);
            Assert.IsFalse(onAmountDecreasedRan);
            Assert.IsFalse(onAmountIncreasedRan);
            Assert.IsFalse(onThresholdReachedRan);
            Assert.AreEqual(2, obj.CurrAmount);

            obj.DecreaseAmount();
            Assert.AreEqual(1, obj.CurrAmount);
            Assert.IsTrue(onAmountDecreasedRan);
            Assert.IsFalse(onAmountIncreasedRan);
            Assert.IsFalse(onThresholdReachedRan);
            ResetFlags();
            obj.DecreaseAmount(2);
            Assert.AreEqual(0, obj.CurrAmount);
            Assert.IsTrue(onAmountDecreasedRan);
            Assert.IsFalse(onAmountIncreasedRan);
            Assert.IsFalse(onThresholdReachedRan);
            ResetFlags();
        }
        [Test]
        public void DecreaseAmount_InvalidInput_Tests()
        {
            obj.IncreaseAmount();
            Assert.AreEqual(1, obj.CurrAmount);

            obj.DecreaseAmount(0);
            Assert.AreEqual(1, obj.CurrAmount);

            obj.DecreaseAmount(-1);
            Assert.AreEqual(1, obj.CurrAmount);
        }

        [Test]
        public void ResetAmount_Tests()
        {
            obj.IncreaseAmount();
            Assert.AreEqual(1, obj.CurrAmount);
            obj.ResetAmount();
            Assert.AreEqual(0, obj.CurrAmount);

            obj.IncreaseAmount(2);
            Assert.AreEqual(2, obj.CurrAmount);
            obj.ResetAmount();
            Assert.AreEqual(0, obj.CurrAmount);

            obj.IncreaseAmount(2);
            Assert.AreEqual(2, obj.CurrAmount);
            obj.StartAmount = 1;
            obj.ResetAmount(true);
            Assert.AreEqual(1, obj.CurrAmount);
            obj.ResetAmount();
            Assert.AreEqual(0, obj.CurrAmount);
        }

        [SetUp]
        public void Setup()
        {
            obj = new GameObject("Object").AddComponent<ThresholdUnityEventCall>();
            obj.OnAmountIncreased.AddListener(OnAmountIncreasedAmount);
            obj.OnAmountDecreased.AddListener(OnAmountDecreasedAmount);
            obj.OnThresholdReached.AddListener(OnThresholdReached);
        }
        [TearDown]
        public void Teardown()
        {
            if (obj) GameObject.DestroyImmediate(obj.gameObject);
            obj = null;

            ResetFlags();
        }

        private void OnAmountIncreasedAmount()
        {
            onAmountIncreasedRan = true;
        }
        private void OnAmountDecreasedAmount()
        {
            onAmountDecreasedRan = true;
        }
        private void OnThresholdReached()
        {
            onThresholdReachedRan = true;
        }
        private void ResetFlags()
        {
            onAmountIncreasedRan = false;
            onAmountDecreasedRan = false;
            onThresholdReachedRan = false;
        }
    }
}
