﻿using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using MattRGeorge.General.Utilities.Static;
using MattRGeorge.Unity.Utilities.Helpers;
using MattRGeorge.Unity.Utilities.Components;

namespace MattRGeorge.Tests.Unity.Utilities.Components
{
    public class ObjectSwitcherTests
    {
        private ObjectSwitcher switcher = null;
        private List<DictionaryNameGameObject> objs = new List<DictionaryNameGameObject>();
        private List<DictionaryNameGameObject> objsTwo = new List<DictionaryNameGameObject>();
        private List<DictionaryNameGameObject> setObjs = new List<DictionaryNameGameObject>();

        [Test]
        public void Objs_Tests()
        {
            switcher.Objs = objs;
            setObjs = switcher.Objs;
            Assert.IsNotNull(setObjs);
            CollectionAssert.AllItemsAreNotNull(setObjs);
            CollectionAssert.AreEqual(objs, setObjs);
            Assert.AreEqual(ReflectionUtility.GetInstanceField(typeof(ObjectSwitcher), switcher, "objs"), setObjs);

            setObjs.Add(objs[0]);
            switcher.Objs = setObjs;
            setObjs = switcher.Objs;
            Assert.IsNotNull(setObjs);
            CollectionAssert.AllItemsAreNotNull(setObjs);
            Assert.AreEqual(objs.Count + 1, setObjs.Count);
            Assert.AreEqual(objs[0], setObjs[setObjs.Count - 1]);
            Assert.AreEqual(ReflectionUtility.GetInstanceField(typeof(ObjectSwitcher), switcher, "objs"), setObjs);

            setObjs.Add(objs[0]);
            Assert.AreNotEqual(ReflectionUtility.GetInstanceField(typeof(ObjectSwitcher), switcher, "objs"), setObjs);
        }
        [Test]
        public void Objs_InvalidInput_Tests()
        {
            switcher.Objs = null;
            setObjs = switcher.Objs;
            Assert.IsNotNull(setObjs);
            CollectionAssert.AllItemsAreNotNull(setObjs);
            Assert.AreEqual(0, setObjs.Count);
            Assert.AreEqual(ReflectionUtility.GetInstanceField(typeof(ObjectSwitcher), switcher, "objs"), setObjs);

            objsTwo = new List<DictionaryNameGameObject>()
            {
                new DictionaryNameGameObject(),
                null,
                new DictionaryNameGameObject()
            };
            switcher.Objs = objsTwo;
            setObjs = switcher.Objs;
            Assert.IsNotNull(setObjs);
            CollectionAssert.AllItemsAreNotNull(setObjs);
            Assert.AreEqual(2, setObjs.Count);
            Assert.AreEqual(ReflectionUtility.GetInstanceField(typeof(ObjectSwitcher), switcher, "objs"), setObjs);
        }

        [Test]
        public void SwitchToFirstAvailableObject_Tests()
        {
            ReflectionUtility.SetInstanceField(typeof(ObjectSwitcher), switcher, "objs", objs);
            switcher.SwitchToFirstAvailableObject();
            CheckVisibleObj(objs[0]);

            switcher.SwitchToFirstAvailableObject();
            CheckVisibleObj(objs[0]);

            switcher.SwitchToFirstAvailableObject();
            CheckVisibleObj(objs[0]);
        }

        [Test]
        public void SwitchObjects_Tests()
        {
            ReflectionUtility.SetInstanceField(typeof(ObjectSwitcher), switcher, "objs", objs);
            switcher.SwitchObjects("Object-1");
            CheckVisibleObj(objs[0]);

            switcher.SwitchObjects("Object-3");
            CheckVisibleObj(objs[2]);

            switcher.SwitchObjects("Object-2");
            CheckVisibleObj(objs[1]);

            switcher.SwitchObjects("Object-1");
            CheckVisibleObj(objs[0]);
        }
        [Test]
        public void SwitchObjects_InvalidInputs_Tests()
        {
            ReflectionUtility.SetInstanceField(typeof(ObjectSwitcher), switcher, "objs", objs);
            switcher.SwitchObjects(null);
            CheckVisibleObj(null, true);

            switcher.SwitchObjects("");
            CheckVisibleObj(null, true);

            switcher.SwitchObjects("Object-4");
            CheckVisibleObj(objs[0]);
        }

        [SetUp]
        public void Setup()
        {
            switcher = new GameObject("Switcher").AddComponent<ObjectSwitcher>();

            for (int i = 0; i < 3; i++)
            {
                objs.Add(new DictionaryNameGameObject());
                objs[i].name = $"Object-{i + 1}";
                objs[i].obj = new GameObject($"Object-{i + 1}");
            }
        }
        [TearDown]
        public void TearDown()
        {
            if (switcher) GameObject.DestroyImmediate(switcher.gameObject);
            switcher = null;

            foreach(DictionaryNameGameObject obj in objs)
            {
                if (obj.obj) GameObject.DestroyImmediate(obj.obj);
            }
            objs = new List<DictionaryNameGameObject>();

            foreach (DictionaryNameGameObject obj in objsTwo)
            {
                if (obj != null && obj.obj) GameObject.DestroyImmediate(obj.obj);
            }
            objsTwo = new List<DictionaryNameGameObject>();

            setObjs = new List<DictionaryNameGameObject>();
        }

        private void CheckVisibleObj(DictionaryNameGameObject activeObj, bool allVisible = false)
        {
            foreach(DictionaryNameGameObject obj in objs)
            {
                if (allVisible || (activeObj != null && obj.name == activeObj.name)) Assert.IsTrue(obj.obj.activeInHierarchy);
                else Assert.IsFalse(obj.obj.activeInHierarchy);
            }
        }
    }
}
