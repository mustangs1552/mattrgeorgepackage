﻿using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using MattRGeorge.General.Utilities.Static;
using MattRGeorge.Unity.Utilities.Helpers;
using MattRGeorge.Unity.Utilities.Components;

namespace MattRGeorge.Tests.Unity.Utilities.Components
{
    public class AssetLibraryTests
    {
        private AssetLibrary assetLib = null;
        private List<DictionaryNameGameObject> objs = new List<DictionaryNameGameObject>();
        private List<DictionaryNameListGameObject> objLists = new List<DictionaryNameListGameObject>();
        private GameObject returnedObj = null;
        private List<GameObject> returnedList = new List<GameObject>();
        private List<DictionaryNameGameObject> setObjs = new List<DictionaryNameGameObject>();
        private List<DictionaryNameListGameObject> setObjLists = new List<DictionaryNameListGameObject>();

        [Test]
        public void Objs_Tests()
        {
            assetLib.Objs = objs;
            setObjs = assetLib.Objs;
            Assert.IsNotNull(setObjs);
            CollectionAssert.AllItemsAreNotNull(setObjs);
            CollectionAssert.AreEqual(objs, setObjs);
            Assert.AreEqual(ReflectionUtility.GetInstanceField(typeof(AssetLibrary), assetLib, "objs"), setObjs);

            setObjs.Add(objs[0]);
            assetLib.Objs = setObjs;
            setObjs = assetLib.Objs;
            Assert.IsNotNull(setObjs);
            CollectionAssert.AllItemsAreNotNull(setObjs);
            Assert.AreEqual(objs.Count + 1, setObjs.Count);
            Assert.AreEqual(objs[0], setObjs[setObjs.Count - 1]);
            Assert.AreEqual(ReflectionUtility.GetInstanceField(typeof(AssetLibrary), assetLib, "objs"), setObjs);

            setObjs.Add(objs[0]);
            Assert.AreNotEqual(ReflectionUtility.GetInstanceField(typeof(AssetLibrary), assetLib, "objs"), setObjs);
        }
        [Test]
        public void Objs_InvalidInput_Tests()
        {
            SetLists();

            assetLib.Objs = null;
            List<DictionaryNameGameObject> setObjs = assetLib.Objs;
            Assert.IsNotNull(setObjs);
            CollectionAssert.AllItemsAreNotNull(setObjs);
            Assert.AreEqual(0, setObjs.Count);
            Assert.AreEqual(ReflectionUtility.GetInstanceField(typeof(AssetLibrary), assetLib, "objs"), setObjs);

            List<DictionaryNameGameObject> testObjs = new List<DictionaryNameGameObject>()
            {
                new DictionaryNameGameObject(),
                null,
                new DictionaryNameGameObject()
            };
            assetLib.Objs = testObjs;
            setObjs = assetLib.Objs;
            Assert.IsNotNull(setObjs);
            CollectionAssert.AllItemsAreNotNull(setObjs);
            Assert.AreEqual(2, setObjs.Count);
            Assert.AreEqual(ReflectionUtility.GetInstanceField(typeof(AssetLibrary), assetLib, "objs"), setObjs);
        }

        [Test]
        public void ObjLists_Tests()
        {
            assetLib.ObjLists = objLists;
            setObjLists = assetLib.ObjLists;
            Assert.IsNotNull(setObjLists);
            CollectionAssert.AllItemsAreNotNull(setObjLists);
            CollectionAssert.AreEqual(objLists, setObjLists);
            Assert.AreEqual(ReflectionUtility.GetInstanceField(typeof(AssetLibrary), assetLib, "objLists"), setObjLists);

            setObjLists.Add(objLists[0]);
            assetLib.ObjLists = setObjLists;
            setObjLists = assetLib.ObjLists;
            Assert.IsNotNull(setObjLists);
            CollectionAssert.AllItemsAreNotNull(setObjLists);
            Assert.AreEqual(objLists.Count + 1, setObjLists.Count);
            Assert.AreEqual(objLists[0], setObjLists[setObjLists.Count - 1]);
            Assert.AreEqual(ReflectionUtility.GetInstanceField(typeof(AssetLibrary), assetLib, "objLists"), setObjLists);

            setObjLists.Add(objLists[0]);
            Assert.AreNotEqual(ReflectionUtility.GetInstanceField(typeof(AssetLibrary), assetLib, "objLists"), setObjLists);
        }
        [Test]
        public void ObjLists_InvalidInput_Tests()
        {
            SetLists();

            assetLib.ObjLists = null;
            List<DictionaryNameListGameObject> setObjs = assetLib.ObjLists;
            Assert.IsNotNull(setObjs);
            CollectionAssert.AllItemsAreNotNull(setObjs);
            Assert.AreEqual(0, setObjs.Count);
            Assert.AreEqual(ReflectionUtility.GetInstanceField(typeof(AssetLibrary), assetLib, "objLists"), setObjs);

            List<DictionaryNameListGameObject> testObjs = new List<DictionaryNameListGameObject>()
            {
                new DictionaryNameListGameObject(),
                null,
                new DictionaryNameListGameObject()
            };
            assetLib.ObjLists = testObjs;
            setObjs = assetLib.ObjLists;
            Assert.IsNotNull(setObjs);
            CollectionAssert.AllItemsAreNotNull(setObjs);
            Assert.AreEqual(2, setObjs.Count);
            Assert.AreEqual(ReflectionUtility.GetInstanceField(typeof(AssetLibrary), assetLib, "objLists"), setObjs);
        }

        [Test]
        public void GetObject_Tests()
        {
            SetLists();

            returnedObj = assetLib.GetObject("Obj1");
            Assert.IsNotNull(returnedObj);
            Assert.AreEqual(objs[0].obj, returnedObj);

            returnedObj = assetLib.GetObject("Obj3");
            Assert.IsNotNull(returnedObj);
            Assert.AreEqual(objs[2].obj, returnedObj);

            returnedObj = assetLib.GetObject("Obj5");
            Assert.IsNotNull(returnedObj);
            Assert.AreEqual(objs[4].obj, returnedObj);
        }
        [Test]
        public void GetObject_InvalidInputs_Tests()
        {
            Assert.IsNull(assetLib.GetObject("Obj1"));
            SetLists();
            Assert.IsNull(assetLib.GetObject(""));
            Assert.IsNull(assetLib.GetObject(null));
        }

        [Test]
        public void GetRandomObject_Tests()
        {
            SetLists();

            for (int i = 0; i < 10; i++)
            {
                returnedObj = assetLib.GetRandomObject();
                Assert.IsNotNull(returnedObj);
                CheckObjInObjsList(returnedObj);
            }
        }
        [Test]
        public void GetRandomObject_InvalidIndexes_Tests()
        {
            SetLists();

            HashSet<int> invalidIndexes = new HashSet<int>();
            List<GameObject> chosenObjs = new List<GameObject>();
            for (int i = 0; i < 10; i++)
            {
                returnedObj = assetLib.GetRandomObject(ref invalidIndexes);
                Assert.IsNotNull(returnedObj, $"Loop index {i}");
                CheckObjInObjsList(returnedObj);
                Assert.IsFalse(chosenObjs.Contains(returnedObj), $"Loop index {i}");
                chosenObjs.Add(returnedObj);

                if (invalidIndexes.Count == objs.Count)
                {
                    invalidIndexes = new HashSet<int>();
                    chosenObjs = new List<GameObject>();
                }
            }
        }

        [Test]
        public void GetRandomObjectFromObjectLists_Tests()
        {
            SetLists();

            for (int i = 0; i < 10; i++)
            {
                returnedObj = assetLib.GetRandomObjectFromObjectLists(new List<string>() { "List1", "List2", "List3"});
                Assert.IsNotNull(returnedObj);
                CheckObjInAllLists(returnedObj);
            }

            for (int i = 0; i < 10; i++)
            {
                returnedObj = assetLib.GetRandomObjectFromObjectLists(new List<string>() { "List1", "List3" });
                Assert.IsNotNull(returnedObj);
                CheckObjInAllLists(returnedObj);
            }

            for (int i = 0; i < 10; i++)
            {
                returnedObj = assetLib.GetRandomObjectFromObjectLists(new List<string>() { "List2" });
                Assert.IsNotNull(returnedObj);
                CheckObjInAllLists(returnedObj);
            }
        }
        [Test]
        public void GetRandomObjectFromObjectLists_ChosenList_Tests()
        {
            SetLists();

            string chosenList = "";
            for (int i = 0; i < 10; i++)
            {
                returnedObj = assetLib.GetRandomObjectFromObjectLists(new List<string>() { "List1", "List2", "List3" }, out chosenList);
                Assert.IsNotNull(returnedObj);
                Assert.IsFalse(string.IsNullOrEmpty(chosenList));
                if (chosenList[4] == '1') CheckObjInList(0, returnedObj);
                else if (chosenList[4] == '2') CheckObjInList(1, returnedObj);
                else if (chosenList[4] == '3') CheckObjInList(2, returnedObj);
            }

            chosenList = "";
            for (int i = 0; i < 10; i++)
            {
                returnedObj = assetLib.GetRandomObjectFromObjectLists(new List<string>() { "List1", "List3" }, out chosenList);
                Assert.IsNotNull(returnedObj);
                Assert.IsFalse(string.IsNullOrEmpty(chosenList));
                Assert.AreNotEqual("List2", chosenList);
                if (chosenList[4] == '1') CheckObjInList(0, returnedObj);
                else if (chosenList[4] == '3') CheckObjInList(2, returnedObj);
            }

            chosenList = "";
            for (int i = 0; i < 10; i++)
            {
                returnedObj = assetLib.GetRandomObjectFromObjectLists(new List<string>() { "List2" }, out chosenList);
                Assert.IsNotNull(returnedObj);
                Assert.IsFalse(string.IsNullOrEmpty(chosenList));
                Assert.AreNotEqual("List1", chosenList);
                Assert.AreNotEqual("List3", chosenList);
                if (chosenList[4] == '2') CheckObjInList(1, returnedObj);
            }
        }
        [Test]
        public void GetRandomObjectFromObjectLists_InvalidInput_Tests()
        {
            SetLists();

            returnedObj = assetLib.GetRandomObjectFromObjectLists(null);
            Assert.IsNull(returnedObj);

            returnedObj = assetLib.GetRandomObjectFromObjectLists(new List<string>());
            Assert.IsNull(returnedObj);

            returnedObj = assetLib.GetRandomObjectFromObjectLists(new List<string>() { null });
            Assert.IsNull(returnedObj);

            returnedObj = assetLib.GetRandomObjectFromObjectLists(new List<string>() { "" });
            Assert.IsNull(returnedObj);

            returnedObj = assetLib.GetRandomObjectFromObjectLists(new List<string>() { null, "" });
            Assert.IsNull(returnedObj);
        }
        [Test]
        public void GetRandomObjectFromObjectLists_ChosenList_InvalidInput_Tests()
        {
            SetLists();

            string chosenList = "";
            returnedObj = assetLib.GetRandomObjectFromObjectLists(null, out chosenList);
            Assert.IsNull(returnedObj);
            Assert.IsNotNull(chosenList);
            Assert.AreEqual("", chosenList);

            chosenList = "";
            returnedObj = assetLib.GetRandomObjectFromObjectLists(new List<string>(), out chosenList);
            Assert.IsNull(returnedObj);
            Assert.IsNotNull(chosenList);
            Assert.AreEqual("", chosenList);

            chosenList = "";
            returnedObj = assetLib.GetRandomObjectFromObjectLists(new List<string>() { null }, out chosenList);
            Assert.IsNull(returnedObj);
            Assert.IsNotNull(chosenList);
            Assert.AreEqual("", chosenList);

            chosenList = "";
            returnedObj = assetLib.GetRandomObjectFromObjectLists(new List<string>() { "" }, out chosenList);
            Assert.IsNull(returnedObj);
            Assert.IsNotNull(chosenList);
            Assert.AreEqual("", chosenList);

            chosenList = "";
            returnedObj = assetLib.GetRandomObjectFromObjectLists(new List<string>() { null, "" }, out chosenList);
            Assert.IsNull(returnedObj);
            Assert.IsNotNull(chosenList);
            Assert.AreEqual("", chosenList);
        }

        [Test]
        public void GetObjectLists_Tests()
        {
            SetLists();

            returnedList = assetLib.GetObjectLists("List1");
            Assert.IsNotNull(returnedList);
            CollectionAssert.IsNotEmpty(returnedList);
            CollectionAssert.AreEqual(objLists[0].objs, returnedList);

            returnedList = assetLib.GetObjectLists("List2");
            Assert.IsNotNull(returnedList);
            CollectionAssert.IsNotEmpty(returnedList);
            CollectionAssert.AreEqual(objLists[1].objs, returnedList);

            returnedList = assetLib.GetObjectLists("List3");
            Assert.IsNotNull(returnedList);
            CollectionAssert.IsNotEmpty(returnedList);
            CollectionAssert.AreEqual(objLists[2].objs, returnedList);
        }
        [Test]
        public void GetObjectLists_InvalidInputs_Tests()
        {
            Assert.IsNull(assetLib.GetObjectLists("List1"));
            SetLists();
            Assert.IsNull(assetLib.GetObjectLists(""));
            Assert.IsNull(assetLib.GetObjectLists(null));
        }

        [Test]
        public void GetRandomObjectLists_Tests()
        {
            SetLists();

            for (int i = 0; i < 10; i++)
            {
                returnedList = assetLib.GetRandomObjectLists();
                Assert.IsNotNull(returnedList);
                CollectionAssert.IsNotEmpty(returnedList);
                Assert.AreEqual(5, returnedList.Count);
            }
        }
        [Test]
        public void GetRandomObjectLists_InvalidIndexes_Tests()
        {
            SetLists();

            HashSet<int> invalidIndexes = new HashSet<int>();
            List<List<GameObject>> chosenLists = new List<List<GameObject>>();
            for (int i = 0; i < 10; i++)
            {
                returnedList = assetLib.GetRandomObjectLists(ref invalidIndexes);
                Assert.IsNotNull(returnedList, $"Loop index {i}");
                CollectionAssert.IsNotEmpty(returnedList, $"Loop index {i}");
                Assert.AreEqual(5, returnedList.Count, $"Loop index {i}");
                Assert.IsFalse(chosenLists.Contains(returnedList), $"Loop index {i}");
                chosenLists.Add(returnedList);

                if (invalidIndexes.Count == objLists.Count)
                {
                    invalidIndexes = new HashSet<int>();
                    chosenLists = new List<List<GameObject>>();
                }
            }
        }

        [Test]
        public void ClearGeneratedLists_Tests()
        {
            SetLists();
            assetLib.GetObject("Obj1");
            assetLib.GetObjectLists("List1");
            Assert.IsNotNull(ReflectionUtility.GetInstanceField(typeof(AssetLibrary), assetLib, "objsDict"));
            Assert.IsNotNull(ReflectionUtility.GetInstanceField(typeof(AssetLibrary), assetLib, "objListsDict"));

            assetLib.ClearGeneratedLists();
            Assert.IsNull(ReflectionUtility.GetInstanceField(typeof(AssetLibrary), assetLib, "objsDict"));
            Assert.IsNull(ReflectionUtility.GetInstanceField(typeof(AssetLibrary), assetLib, "objListsDict"));
        }

        [SetUp]
        public void Setup()
        {
            assetLib = new GameObject("AssetLibrary").AddComponent<AssetLibrary>();

            DictionaryNameGameObject obj = null;
            for (int i = 1; i <= 5; i++)
            {
                obj = new DictionaryNameGameObject();
                obj.name = $"Obj{i}";
                obj.obj = new GameObject($"Obj{i}");
                objs.Add(obj);
            }

            List<GameObject> tempObjs = new List<GameObject>();
            for (int i = 6; i <= 10; i++) tempObjs.Add(new GameObject($"Obj{i}"));
            DictionaryNameListGameObject objList = new DictionaryNameListGameObject();
            objList.name = "List1";
            objList.objs = tempObjs;
            objLists.Add(objList);
            tempObjs = new List<GameObject>();
            for (int i = 11; i <= 15; i++) tempObjs.Add(new GameObject($"Obj{i}"));
            objList = new DictionaryNameListGameObject();
            objList.name = "List2";
            objList.objs = tempObjs;
            objLists.Add(objList);
            tempObjs = new List<GameObject>();
            for (int i = 16; i <= 20; i++) tempObjs.Add(new GameObject($"Obj{i}"));
            objList = new DictionaryNameListGameObject();
            objList.name = "List3";
            objList.objs = tempObjs;
            objLists.Add(objList);
        }
        [TearDown]
        public void TearDown()
        {
            if (assetLib) GameObject.DestroyImmediate(assetLib.gameObject);
            assetLib = null;

            foreach (DictionaryNameGameObject nGO in objs)
            {
                if (nGO.obj) GameObject.DestroyImmediate(nGO.obj);
            }
            objs = new List<DictionaryNameGameObject>();

            foreach(DictionaryNameListGameObject nLGO in objLists)
            {
                foreach (GameObject go in nLGO.objs)
                {
                    if (go) GameObject.DestroyImmediate(go);
                }
            }
            objLists = new List<DictionaryNameListGameObject>();

            setObjs = new List<DictionaryNameGameObject>();
            setObjLists = new List<DictionaryNameListGameObject>();
        }

        private void SetLists()
        {
            ReflectionUtility.SetInstanceField(typeof(AssetLibrary), assetLib, "objs", objs);
            ReflectionUtility.SetInstanceField(typeof(AssetLibrary), assetLib, "objLists", objLists);
        }

        private void CheckObjInObjsList(GameObject obj)
        {
            Assert.IsTrue(
                obj == objs[0].obj ||
                obj == objs[1].obj ||
                obj == objs[2].obj ||
                obj == objs[3].obj ||
                obj == objs[4].obj);
        }
        private void CheckObjInList(int listI, GameObject obj)
        {
            Assert.IsTrue(
                obj == objLists[listI].objs[0] ||
                obj == objLists[listI].objs[1] ||
                obj == objLists[listI].objs[2] ||
                obj == objLists[listI].objs[3] ||
                obj == objLists[listI].objs[4]);
        }
        private void CheckObjInAllLists(GameObject obj)
        {
            Assert.IsTrue(
                obj == objLists[0].objs[0] ||
                obj == objLists[0].objs[1] ||
                obj == objLists[0].objs[2] ||
                obj == objLists[0].objs[3] ||
                obj == objLists[0].objs[4] ||
                obj == objLists[1].objs[0] ||
                obj == objLists[1].objs[1] ||
                obj == objLists[1].objs[2] ||
                obj == objLists[1].objs[3] ||
                obj == objLists[1].objs[4] ||
                obj == objLists[2].objs[0] ||
                obj == objLists[2].objs[1] ||
                obj == objLists[2].objs[2] ||
                obj == objLists[2].objs[3] ||
                obj == objLists[2].objs[4]);
        }
    }
}
