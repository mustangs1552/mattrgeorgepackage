﻿using System.Collections.Generic;
using NUnit.Framework;
using MattRGeorge.General.Utilities.Static;
using MattRGeorge.Unity.Utilities.Helpers;
using MattRGeorge.Unity.Utilities.Components;

namespace MattRGeorge.Tests.Unity.Utilities.Components
{
    public class SpawnObjectAdvancedNodeTests
    {
        private GameObjectBundle objBundle = new GameObjectBundle();
        private SpawnObjectAdvancedNode spawnObj = null;

        [Test]
        public void MaxCount_Tests()
        {
            spawnObj.MaxCount = 5;
            int returnedFloat = (int)ReflectionUtility.GetInstanceField(typeof(SpawnObjectAdvancedNode), spawnObj, "maxCount");
            Assert.AreEqual(5, returnedFloat);
            Assert.AreEqual(returnedFloat, spawnObj.MaxCount);

            spawnObj.MaxCount = 10;
            returnedFloat = (int)ReflectionUtility.GetInstanceField(typeof(SpawnObjectAdvancedNode), spawnObj, "maxCount");
            Assert.AreEqual(10, returnedFloat);
            Assert.AreEqual(returnedFloat, spawnObj.MaxCount);

            spawnObj.MaxCount = 1000;
            returnedFloat = (int)ReflectionUtility.GetInstanceField(typeof(SpawnObjectAdvancedNode), spawnObj, "maxCount");
            Assert.AreEqual(1000, returnedFloat);
            Assert.AreEqual(returnedFloat, spawnObj.MaxCount);

            spawnObj.MaxCount = 0;
            returnedFloat = (int)ReflectionUtility.GetInstanceField(typeof(SpawnObjectAdvancedNode), spawnObj, "maxCount");
            Assert.AreEqual(0, returnedFloat);
            Assert.AreEqual(returnedFloat, spawnObj.MaxCount);

            spawnObj.MaxCount = -10;
            returnedFloat = (int)ReflectionUtility.GetInstanceField(typeof(SpawnObjectAdvancedNode), spawnObj, "maxCount");
            Assert.AreEqual(-10, returnedFloat);
            Assert.AreEqual(returnedFloat, spawnObj.MaxCount);
        }

        [Test]
        public void ObjsWithMaxCounts_Tests()
        {
            List<DictionaryGameObjectCount> objMaxCounts = new List<DictionaryGameObjectCount>()
            {
                new DictionaryGameObjectCount()
                {
                    obj = objBundle.CreateNewGameObject("ObjA"),
                    count = 5
                },
                new DictionaryGameObjectCount()
                {
                    obj = objBundle.CreateNewGameObject("ObjB"),
                    count = 2
                },
                new DictionaryGameObjectCount()
                {
                    obj = objBundle.CreateNewGameObject("ObjC"),
                    count = 0
                },
                new DictionaryGameObjectCount()
                {
                    obj = objBundle.CreateNewGameObject("ObjD"),
                    count = 50
                }
            };
            spawnObj.ObjsWithMaxCounts = objMaxCounts;
            List<DictionaryGameObjectCount> returnedObjMaxCounts = (List<DictionaryGameObjectCount>)ReflectionUtility.GetInstanceField(typeof(SpawnObjectAdvancedNode), spawnObj, "objsWithMaxCounts");
            CollectionAssert.AreEqual(objMaxCounts, returnedObjMaxCounts);
            CollectionAssert.AreEqual(returnedObjMaxCounts, spawnObj.ObjsWithMaxCounts);
        }
        [Test]
        public void ObjsWithMaxCounts_InvalidInput_Tests()
        {
            List<DictionaryGameObjectCount> objMaxCounts = new List<DictionaryGameObjectCount>()
            {
                new DictionaryGameObjectCount()
                {
                    obj = objBundle.CreateNewGameObject("ObjA"),
                    count = 5
                },
                new DictionaryGameObjectCount()
                {
                    obj = objBundle.CreateNewGameObject("ObjB"),
                    count = 2
                },
                null,
                new DictionaryGameObjectCount()
                {
                    obj = objBundle.CreateNewGameObject("ObjD"),
                    count = 50
                }
            };
            spawnObj.ObjsWithMaxCounts = objMaxCounts;
            List<DictionaryGameObjectCount> returnedObjMaxCounts = (List<DictionaryGameObjectCount>)ReflectionUtility.GetInstanceField(typeof(SpawnObjectAdvancedNode), spawnObj, "objsWithMaxCounts");
            Assert.AreEqual(objMaxCounts.Count - 1, returnedObjMaxCounts.Count);
            Assert.AreEqual(objMaxCounts[0], returnedObjMaxCounts[0]);
            Assert.AreEqual(objMaxCounts[1], returnedObjMaxCounts[1]);
            Assert.AreEqual(objMaxCounts[3], returnedObjMaxCounts[2]);
            returnedObjMaxCounts = spawnObj.ObjsWithMaxCounts;
            Assert.AreEqual(objMaxCounts[0], returnedObjMaxCounts[0]);
            Assert.AreEqual(objMaxCounts[1], returnedObjMaxCounts[1]);
            Assert.AreEqual(objMaxCounts[3], returnedObjMaxCounts[2]);

            spawnObj.ObjsWithMaxCounts = null;
            returnedObjMaxCounts = (List<DictionaryGameObjectCount>)ReflectionUtility.GetInstanceField(typeof(SpawnObjectAdvancedNode), spawnObj, "objsWithMaxCounts");
            CollectionAssert.IsEmpty(returnedObjMaxCounts);
            CollectionAssert.IsEmpty(spawnObj.ObjsWithMaxCounts);

            spawnObj.ObjsWithMaxCounts = new List<DictionaryGameObjectCount>();
            returnedObjMaxCounts = (List<DictionaryGameObjectCount>)ReflectionUtility.GetInstanceField(typeof(SpawnObjectAdvancedNode), spawnObj, "objsWithMaxCounts");
            CollectionAssert.IsEmpty(returnedObjMaxCounts);
            CollectionAssert.IsEmpty(spawnObj.ObjsWithMaxCounts);
        }

        [SetUp]
        public void Setup()
        {
            spawnObj = objBundle.CreateNewGameObject("SpawnObject").AddComponent<SpawnObjectAdvancedNode>();
        }
        [TearDown]
        public void TearDown()
        {
            objBundle.DestroyObjs(true);
        }
    }
}
