﻿using NUnit.Framework;
using UnityEngine;
using MattRGeorge.General.Utilities.Static;
using MattRGeorge.Unity.Utilities.Components;

namespace MattRGeorge.Tests.Unity.Utilities.Components
{
    public class TimerTests
    {
        private Timer timer = null;
        float testFloat = 0;

        [Test]
        public void CountdownTimer_Tests()
        {
            timer.CountdownTimer = 10;
            testFloat = timer.CountdownTimer;
            Assert.AreEqual(10, testFloat);
            Assert.AreEqual(ReflectionUtility.GetInstanceField(typeof(Timer), timer, "countdownTimer"), testFloat);
            
            timer.CountdownTimer = 100;
            testFloat = timer.CountdownTimer;
            Assert.AreEqual(100, testFloat);
            Assert.AreEqual(ReflectionUtility.GetInstanceField(typeof(Timer), timer, "countdownTimer"), testFloat);
            
            timer.CountdownTimer = 1;
            testFloat = timer.CountdownTimer;
            Assert.AreEqual(1, testFloat);
            Assert.AreEqual(ReflectionUtility.GetInstanceField(typeof(Timer), timer, "countdownTimer"), testFloat);

            timer.CountdownTimer = 0;
            testFloat = timer.CountdownTimer;
            Assert.AreEqual(0, testFloat);
            Assert.AreEqual(ReflectionUtility.GetInstanceField(typeof(Timer), timer, "countdownTimer"), testFloat);
        }
        [Test]
        public void CountdownTimer_InvalidInput_Tests()
        {
            timer.CountdownTimer = -10;
            testFloat = timer.CountdownTimer;
            Assert.AreEqual(0, testFloat);
            Assert.AreEqual(ReflectionUtility.GetInstanceField(typeof(Timer), timer, "countdownTimer"), testFloat);

            timer.CountdownTimer = -100;
            testFloat = timer.CountdownTimer;
            Assert.AreEqual(0, testFloat);
            Assert.AreEqual(ReflectionUtility.GetInstanceField(typeof(Timer), timer, "countdownTimer"), testFloat);

            timer.CountdownTimer = -1;
            testFloat = timer.CountdownTimer;
            Assert.AreEqual(0, testFloat);
            Assert.AreEqual(ReflectionUtility.GetInstanceField(typeof(Timer), timer, "countdownTimer"), testFloat);
        }

        [SetUp]
        public void Setup()
        {
            timer = new GameObject("Timer").AddComponent<Timer>();
        }
        [TearDown]
        public void TearDown()
        {
            if (timer) GameObject.DestroyImmediate(timer.gameObject);
            timer = null;
        }
    }
}
