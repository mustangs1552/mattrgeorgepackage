﻿using MattRGeorge.Unity.Utilities.Components;
using NUnit.Framework;
using System.Collections.Generic;
using UnityEngine;

namespace MattRGeorge.Tests.Unity.Utilities.Components
{
    public class LineRendererCasterTests
    {
        private LineRendererCaster caster = null;
        private List<Transform> points = new List<Transform>();
        private List<Transform> extraPoints = new List<Transform>();
        private List<Transform> returnList = new List<Transform>();

        [Test]
        public void Points_Tests()
        {
            returnList = caster.Points;
            Assert.IsNotNull(returnList);
            CollectionAssert.IsEmpty(returnList);

            caster.Points = points;
            returnList = caster.Points;
            Assert.IsNotNull(returnList);
            CollectionAssert.AreEqual(points, returnList);

            caster.Points = new List<Transform>();
            returnList = caster.Points;
            Assert.IsNotNull(returnList);
            CollectionAssert.IsEmpty(returnList);
        }
        [Test]
        public void Points_InvalidInput_Tests()
        {
            caster.Points = null;
            returnList = caster.Points;
            Assert.IsNotNull(returnList);
            CollectionAssert.IsEmpty(returnList);

            caster.Points = new List<Transform>()
            {
                null,
                points[1],
                null
            };
            returnList = caster.Points;
            Assert.IsNotNull(returnList);
            Assert.AreEqual(1, returnList.Count);
            Assert.AreEqual(points[1], returnList[0]);
        }

        [Test]
        public void AddPoints_Tests()
        {
            caster.AddPoints(points);
            returnList = caster.Points;
            Assert.IsNotNull(returnList);
            CollectionAssert.AreEqual(points, returnList);

            caster.AddPoints(points);
            returnList = caster.Points;
            Assert.IsNotNull(returnList);
            Assert.AreEqual(6, returnList.Count);

        }
        [Test]
        public void AddPoints_InvalidInput_Tests()
        {
            caster.AddPoints(null);
            returnList = caster.Points;
            Assert.IsNotNull(returnList);
            CollectionAssert.IsEmpty(returnList);
            
            caster.AddPoints(new List<Transform>());
            returnList = caster.Points;
            Assert.IsNotNull(returnList);
            CollectionAssert.IsEmpty(returnList);

            caster.AddPoints(new List<Transform>() { null, points[1], null});
            returnList = caster.Points;
            Assert.IsNotNull(returnList);
            Assert.AreEqual(1, returnList.Count);
            Assert.AreEqual(points[1], returnList[0]);
        }

        [Test]
        public void AddPoint_Tests()
        {
            caster.AddPoint(points[0]);
            returnList = caster.Points;
            Assert.IsNotNull(returnList);
            Assert.AreEqual(1, returnList.Count);
            Assert.AreEqual(points[0], returnList[0]);

            caster.AddPoint(points[2]);
            returnList = caster.Points;
            Assert.IsNotNull(returnList);
            Assert.AreEqual(2, returnList.Count);
            Assert.AreEqual(points[0], returnList[0]);
            Assert.AreEqual(points[2], returnList[1]);

            caster.AddPoint(points[1]);
            returnList = caster.Points;
            Assert.IsNotNull(returnList);
            Assert.AreEqual(3, returnList.Count);
            Assert.AreEqual(points[0], returnList[0]);
            Assert.AreEqual(points[2], returnList[1]);
            Assert.AreEqual(points[1], returnList[2]);

            caster.AddPoint(points[0]);
            returnList = caster.Points;
            Assert.IsNotNull(returnList);
            Assert.AreEqual(4, returnList.Count);
            Assert.AreEqual(points[0], returnList[0]);
            Assert.AreEqual(points[2], returnList[1]);
            Assert.AreEqual(points[1], returnList[2]);
            Assert.AreEqual(points[0], returnList[3]);
        }
        [Test]
        public void AddPoint_InvalidInput_Tests()
        {
            caster.AddPoint(null);
            returnList = caster.Points;
            Assert.IsNotNull(returnList);
            CollectionAssert.IsEmpty(returnList);
        }

        [SetUp]
        public void Setup()
        {
            caster = new GameObject("Caster").AddComponent<LineRendererCaster>();

            for (int i = 0; i < 3; i++) points.Add(new GameObject($"Point-{i + 1}").transform);
            for (int i = 3; i < 6; i++) extraPoints.Add(new GameObject($"ExtraPoint-{i + 1}").transform);
        }
        [TearDown]
        public void TearDown()
        {
            if (caster) GameObject.DestroyImmediate(caster.gameObject);
            caster = null;

            foreach (Transform trans in points)
            {
                if (trans) GameObject.DestroyImmediate(trans.gameObject);
            }
            points = new List<Transform>();

            foreach (Transform trans in extraPoints)
            {
                if (trans) GameObject.DestroyImmediate(trans.gameObject);
            }
            extraPoints = new List<Transform>();
        }
    }
}
