﻿using NUnit.Framework;
using UnityEngine;
using MattRGeorge.General.Utilities.Static;
using MattRGeorge.Unity.Utilities.Components;

namespace MattRGeorge.Tests.Unity.Utilities.Components
{
    public class TranslateObjectTests
    {
        private TranslateObject obj = null;
        float testFloat = 0;

        [Test]
        public void Range_Tests()
        {
            obj.Life = 10;
            testFloat = obj.Life;
            Assert.AreEqual(10, testFloat);
            Assert.AreEqual(ReflectionUtility.GetInstanceField(typeof(TranslateObject), obj, "life"), testFloat);
            
            obj.Life = 100;
            testFloat = obj.Life;
            Assert.AreEqual(100, testFloat);
            Assert.AreEqual(ReflectionUtility.GetInstanceField(typeof(TranslateObject), obj, "life"), testFloat);
            
            obj.Life = 1;
            testFloat = obj.Life;
            Assert.AreEqual(1, testFloat);
            Assert.AreEqual(ReflectionUtility.GetInstanceField(typeof(TranslateObject), obj, "life"), testFloat);

            obj.Life = 0;
            testFloat = obj.Life;
            Assert.AreEqual(0, testFloat);
            Assert.AreEqual(ReflectionUtility.GetInstanceField(typeof(TranslateObject), obj, "life"), testFloat);
        }
        [Test]
        public void Range_InvalidInput_Tests()
        {
            obj.Life = -10;
            testFloat = obj.Life;
            Assert.AreEqual(0, testFloat);
            Assert.AreEqual(ReflectionUtility.GetInstanceField(typeof(TranslateObject), obj, "life"), testFloat);

            obj.Life = -100;
            testFloat = obj.Life;
            Assert.AreEqual(0, testFloat);
            Assert.AreEqual(ReflectionUtility.GetInstanceField(typeof(TranslateObject), obj, "life"), testFloat);

            obj.Life = -1;
            testFloat = obj.Life;
            Assert.AreEqual(0, testFloat);
            Assert.AreEqual(ReflectionUtility.GetInstanceField(typeof(TranslateObject), obj, "life"), testFloat);
        }

        [SetUp]
        public void Setup()
        {
            obj = new GameObject("Obj").AddComponent<TranslateObject>();
        }
        [TearDown]
        public void TearDown()
        {
            if (obj) GameObject.DestroyImmediate(obj.gameObject);
            obj = null;
        }
    }
}
