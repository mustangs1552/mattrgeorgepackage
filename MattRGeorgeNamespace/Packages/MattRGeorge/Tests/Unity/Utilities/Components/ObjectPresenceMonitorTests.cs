﻿using System.Collections.Generic;
using NUnit.Framework;
using UnityEngine;
using MattRGeorge.General.Utilities.Static;
using MattRGeorge.Unity.Utilities.Components;

namespace MattRGeorge.Tests.Unity.Utilities.Components
{
    public class ObjectPresenceMonitorTests
    {
        private ObjectPresenceMonitor monitor = null;
        private List<GameObject> objs = new List<GameObject>();

        [Test]
        public void CheckObject_InOut_Tests()
        {
            ReflectionUtility.SetInstanceField(typeof(ObjectPresenceMonitor), monitor, "objsToMonitor", objs);

            Assert.AreEqual(0, monitor.ObjCount);

            // Check In
            monitor.CheckObjectIn(null, objs[0].GetComponent<BoxCollider>());
            Assert.AreEqual(1, monitor.ObjCount);

            monitor.CheckObjectIn(null, objs[0].GetComponent<BoxCollider>());
            Assert.AreEqual(1, monitor.ObjCount);

            monitor.CheckObjectIn(null, objs[2].GetComponent<BoxCollider>());
            Assert.AreEqual(2, monitor.ObjCount);

            monitor.CheckObjectIn(null, objs[1].GetComponent<BoxCollider>());
            Assert.AreEqual(3, monitor.ObjCount);

            // Check Out
            monitor.CheckObjectOut(null, objs[0].GetComponent<BoxCollider>());
            Assert.AreEqual(2, monitor.ObjCount);

            monitor.CheckObjectOut(null, objs[0].GetComponent<BoxCollider>());
            Assert.AreEqual(2, monitor.ObjCount);

            monitor.CheckObjectOut(null, objs[2].GetComponent<BoxCollider>());
            Assert.AreEqual(1, monitor.ObjCount);

            monitor.CheckObjectOut(null, objs[1].GetComponent<BoxCollider>());
            Assert.AreEqual(0, monitor.ObjCount);
        }

        [Test]
        public void CheckObjectIn_InvalidInputs_Tests()
        {
            monitor.CheckObjectIn(null, null);
            monitor.CheckObjectIn(null, objs[0].GetComponent<BoxCollider>());
            monitor.CheckObjectIn(objs[0].gameObject, null);
        }
        [Test]
        public void CheckObjectOut_InvalidInputs_Tests()
        {
            monitor.CheckObjectOut(null, null);
            monitor.CheckObjectOut(null, objs[0].GetComponent<BoxCollider>());
            monitor.CheckObjectOut(objs[0].gameObject, null);
        }

        [SetUp]
        public void Setup()
        {
            monitor = new GameObject("Monitor").AddComponent<ObjectPresenceMonitor>();
            for (int i = 0; i < 3; i++)
            {
                objs.Add(new GameObject($"Obj-{i + 1}"));
                objs[i].gameObject.AddComponent<BoxCollider>();
            }
        }
        [TearDown]
        public void TearDown()
        {
            if (monitor) GameObject.DestroyImmediate(monitor.gameObject);
            monitor = null;

            foreach(GameObject obj in objs)
            {
                if (obj) GameObject.DestroyImmediate(obj);
            }
            objs = new List<GameObject>();
        }
    }
}
