﻿using UnityEngine;
using NUnit.Framework;
using MattRGeorge.Unity.Utilities.Components;

namespace MattRGeorge.Tests.Unity.Utilities.Components
{
    public class DelayedUnityEventCallTests
    {
        private DelayedUnityEventCall obj = null;

        [Test]
        public void Delay_Tests()
        {
            obj.Delay = 10;
            Assert.AreEqual(10, obj.Delay);
            obj.Delay = 100;
            Assert.AreEqual(100, obj.Delay);
            obj.Delay = 0;
            Assert.AreEqual(0, obj.Delay);
            obj.Delay = .5f;
            Assert.AreEqual(.5f, obj.Delay);

            obj.Delay = -5;
            Assert.AreEqual(0, obj.Delay);
        }

        [SetUp]
        public void Setup()
        {
            obj = new GameObject("Obj").AddComponent<DelayedUnityEventCall>();
        }
        [TearDown]
        public void Teardown()
        {
            if (obj.gameObject) GameObject.DestroyImmediate(obj.gameObject);
        }
    }
}
