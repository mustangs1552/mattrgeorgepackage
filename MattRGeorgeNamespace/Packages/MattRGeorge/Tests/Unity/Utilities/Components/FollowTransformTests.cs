﻿using MattRGeorge.General.Utilities.Static;
using MattRGeorge.Unity.Utilities.Components;
using NUnit.Framework;
using UnityEngine;

namespace MattRGeorge.Tests.Unity.Utilities.Components
{
    public class FollowTransformTests
    {
        private FollowTransform followTrans = null;
        private Transform target = null;

        [Test]
        public void UpdatePosition_X_Tests()
        {
            ReflectionUtility.SetInstanceField(typeof(FollowTransform), followTrans, "followPositionY", false);
            ReflectionUtility.SetInstanceField(typeof(FollowTransform), followTrans, "followPositionZ", false);

            Assert.AreEqual(target.position, followTrans.transform.position);

            target.position += Vector3.right * 5;
            followTrans.UpdatePosition();
            Assert.AreEqual(target.position, followTrans.transform.position);

            target.position += Vector3.left * 10;
            followTrans.UpdatePosition();
            Assert.AreEqual(target.position, followTrans.transform.position);
        }
        [Test]
        public void UpdatePosition_Y_Tests()
        {
            ReflectionUtility.SetInstanceField(typeof(FollowTransform), followTrans, "followPositionX", false);
            ReflectionUtility.SetInstanceField(typeof(FollowTransform), followTrans, "followPositionZ", false);

            Assert.AreEqual(target.position, followTrans.transform.position);

            target.position += Vector3.up * 5;
            followTrans.UpdatePosition();
            Assert.AreEqual(target.position, followTrans.transform.position);

            target.position += Vector3.down * 10;
            followTrans.UpdatePosition();
            Assert.AreEqual(target.position, followTrans.transform.position);
        }
        [Test]
        public void UpdatePosition_Z_Tests()
        {
            ReflectionUtility.SetInstanceField(typeof(FollowTransform), followTrans, "followPositionX", false);
            ReflectionUtility.SetInstanceField(typeof(FollowTransform), followTrans, "followPositionY", false);

            Assert.AreEqual(target.position, followTrans.transform.position);

            target.position += Vector3.forward * 5;
            followTrans.UpdatePosition();
            Assert.AreEqual(target.position, followTrans.transform.position);

            target.position += Vector3.back * 10;
            followTrans.UpdatePosition();
            Assert.AreEqual(target.position, followTrans.transform.position);
        }
        [Test]
        public void UpdatePosition_XYZ_Tests()
        {
            Assert.AreEqual(target.position, followTrans.transform.position);

            target.position += new Vector3(5, 5, 5);
            followTrans.UpdatePosition();
            Assert.AreEqual(target.position, followTrans.transform.position);

            target.position -= new Vector3(10, 10, 10);
            followTrans.UpdatePosition();
            Assert.AreEqual(target.position, followTrans.transform.position);
        }

        [Test]
        public void UpdateRotation_X_Tests()
        {
            ReflectionUtility.SetInstanceField(typeof(FollowTransform), followTrans, "followRotationY", false);
            ReflectionUtility.SetInstanceField(typeof(FollowTransform), followTrans, "followRotationZ", false);

            CheckRotation();

            target.Rotate(new Vector3(5, 0, 0));
            followTrans.UpdateRotation();
            CheckRotation();

            target.Rotate(new Vector3(-10, 0, 0));
            followTrans.UpdateRotation();
            CheckRotation();
        }
        [Test]
        public void UpdateRotation_Y_Tests()
        {
            ReflectionUtility.SetInstanceField(typeof(FollowTransform), followTrans, "followRotationX", false);
            ReflectionUtility.SetInstanceField(typeof(FollowTransform), followTrans, "followRotationZ", false);

            CheckRotation();

            target.Rotate(new Vector3(0, 5, 0));
            followTrans.UpdateRotation();
            CheckRotation();

            target.Rotate(new Vector3(0, -10, 0));
            followTrans.UpdateRotation();
            CheckRotation();
        }
        [Test]
        public void UpdateRotation_Z_Tests()
        {
            ReflectionUtility.SetInstanceField(typeof(FollowTransform), followTrans, "followRotationX", false);
            ReflectionUtility.SetInstanceField(typeof(FollowTransform), followTrans, "followRotationY", false);

            CheckRotation();

            target.Rotate(new Vector3(0, 0, 5));
            followTrans.UpdateRotation();
            CheckRotation();

            target.Rotate(new Vector3(0, 0, -10));
            followTrans.UpdateRotation();
            CheckRotation();
        }
        [Test]
        public void UpdateRotation_XYZ_Tests()
        {
            CheckRotation();

            target.Rotate(new Vector3(5, 5, 5));
            followTrans.UpdateRotation();
            CheckRotation();

            target.Rotate(new Vector3(-10, -10, -10));
            followTrans.UpdateRotation();
            CheckRotation();
        }

        [Test]
        public void UpdateTransform_X_Tests()
        {
            ReflectionUtility.SetInstanceField(typeof(FollowTransform), followTrans, "followPositionY", false);
            ReflectionUtility.SetInstanceField(typeof(FollowTransform), followTrans, "followPositionZ", false);
            ReflectionUtility.SetInstanceField(typeof(FollowTransform), followTrans, "followRotationY", false);
            ReflectionUtility.SetInstanceField(typeof(FollowTransform), followTrans, "followRotationZ", false);

            Assert.AreEqual(target.position, followTrans.transform.position);
            CheckRotation();

            target.position += Vector3.right * 5;
            target.Rotate(new Vector3(5, 0, 0));
            followTrans.UpdateTransform();
            Assert.AreEqual(target.position, followTrans.transform.position);
            CheckRotation();

            target.position += Vector3.left * 10;
            target.Rotate(new Vector3(-10, 0, 0));
            followTrans.UpdateTransform();
            Assert.AreEqual(target.position, followTrans.transform.position);
            CheckRotation();
        }
        [Test]
        public void UpdateTransform_Y_Tests()
        {
            ReflectionUtility.SetInstanceField(typeof(FollowTransform), followTrans, "followPositionX", false);
            ReflectionUtility.SetInstanceField(typeof(FollowTransform), followTrans, "followPositionZ", false);
            ReflectionUtility.SetInstanceField(typeof(FollowTransform), followTrans, "followRotationX", false);
            ReflectionUtility.SetInstanceField(typeof(FollowTransform), followTrans, "followRotationZ", false);

            Assert.AreEqual(target.position, followTrans.transform.position);
            CheckRotation();

            target.position += Vector3.up * 5;
            target.Rotate(new Vector3(0, 5, 0));
            followTrans.UpdateTransform();
            Assert.AreEqual(target.position, followTrans.transform.position);
            CheckRotation();

            target.position += Vector3.down * 10;
            target.Rotate(new Vector3(0, -10, 0));
            followTrans.UpdateTransform();
            Assert.AreEqual(target.position, followTrans.transform.position);
            CheckRotation();
        }
        [Test]
        public void UpdateTransform_Z_Tests()
        {
            ReflectionUtility.SetInstanceField(typeof(FollowTransform), followTrans, "followPositionX", false);
            ReflectionUtility.SetInstanceField(typeof(FollowTransform), followTrans, "followPositionY", false);
            ReflectionUtility.SetInstanceField(typeof(FollowTransform), followTrans, "followRotationX", false);
            ReflectionUtility.SetInstanceField(typeof(FollowTransform), followTrans, "followRotationY", false);

            Assert.AreEqual(target.position, followTrans.transform.position);
            CheckRotation();

            target.position += Vector3.forward * 5;
            target.Rotate(new Vector3(0, 0, 5));
            followTrans.UpdateTransform();
            Assert.AreEqual(target.position, followTrans.transform.position);
            CheckRotation();

            target.position += Vector3.back * 10;
            target.Rotate(new Vector3(0, 0, -10));
            followTrans.UpdateTransform();
            Assert.AreEqual(target.position, followTrans.transform.position);
            CheckRotation();
        }
        [Test]
        public void UpdateTransform_XYZ_Tests()
        {
            Assert.AreEqual(target.position, followTrans.transform.position);
            CheckRotation();

            target.position += new Vector3(5, 5, 5);
            target.Rotate(new Vector3(5, 5, 5));
            followTrans.UpdateTransform();
            Assert.AreEqual(target.position, followTrans.transform.position);
            CheckRotation();

            target.position += new Vector3(-10, -10, -10);
            target.Rotate(new Vector3(-10, -10, -10));
            followTrans.UpdateTransform();
            Assert.AreEqual(target.position, followTrans.transform.position);
            CheckRotation();
        }

        [SetUp]
        public void Setup()
        {
            target = new GameObject("Target").transform;
            GameObject temp = new GameObject("FollowTrans");
            followTrans = temp.AddComponent<FollowTransform>();
            followTrans.target = target;
        }
        [TearDown]
        public void Teardown()
        {
            if (target) GameObject.DestroyImmediate(target.gameObject);
            target = null;
            if (followTrans) GameObject.DestroyImmediate(followTrans.gameObject);
            followTrans = null;
        }

        private void CheckRotation()
        {
            Assert.IsTrue(MathUtility.AreAlmostEqual(followTrans.transform.eulerAngles.x, target.eulerAngles.x, .05f));
            Assert.IsTrue(MathUtility.AreAlmostEqual(followTrans.transform.eulerAngles.y, target.eulerAngles.y, .05f));
            Assert.IsTrue(MathUtility.AreAlmostEqual(followTrans.transform.eulerAngles.z, target.eulerAngles.z, .05f));
        }
    }
}
