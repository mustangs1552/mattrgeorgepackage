﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using MattRGeorge.General.Utilities;
using MattRGeorge.Unity.Utilities.Static;

namespace MattRGeorge.Unity.Utilities.Components
{
    public class ObjectSpawner : ObjectSpawnerBase
    {
        [Tooltip("How should the objects being spawned be chosen?")]
        [SerializeField] protected ListChoiceMode objChoiceMode = ListChoiceMode.Randomly;
        [Tooltip("The maximum amount of objects that can be spawned. Set to -1 for unlimited.")]
        [SerializeField] protected int maxCount = -1;
        [Tooltip("Allow spawning at a node that already has a spawned object?")]
        [SerializeField] protected bool allowSameNodeSpawn = true;
        [Tooltip("The maximum amount of objects that can be spawned by object.")]
        [SerializeField] protected List<GameObjectWithCount> objsWithMaxCounts = new List<GameObjectWithCount>();
        public UnityEvent OnFinishedSpawning = new UnityEvent();

        /// <summary>
        /// The max amount of objects that this spawner can spawn.
        /// </summary>
        public int MaxCount => maxCount;

        private List<SpawnObject> nullessNodes = new List<SpawnObject>();
        private List<GameObject> nullessObjs = new List<GameObject>();
        private HashSet<int> invalidNodeIndexes = new HashSet<int>();
        private HashSet<int> invalidObjIndexes = new HashSet<int>();
        private SpawnObject chosenNode = null;
        private GameObject chosenObj = null;
        private int remainingCount = 0;
        private int currCount = 0;
        private Dictionary<GameObject, int> objsWithMaxCountsDict = new Dictionary<GameObject, int>();
        private Dictionary<GameObject, int> currObjsWithMaxCountsDict = new Dictionary<GameObject, int>();

        #region Methods
        /// <summary>
        /// Spawns the given amount of objects from the given list at the given nodes using the set spawn modes.
        /// </summary>
        /// <param name="nodes">The nodes to spawn objects at.</param>
        /// <param name="objs">The objects to spawn.</param>
        /// <param name="amount">The amount of objects to spawn. Can be set to -1 to spawn up to max count or if 'allowSameNodeSpawn' is false will spawn 'til all nodes have an object.</param>
        /// <returns>The amount that was not able to be spawned.</returns>
        public virtual int SpawnObjects(List<SpawnObject> nodes, List<GameObject> objs, int amount)
        {
            if (nodes == null || nodes.Count == 0 || objs == null || objs.Count == 0 || amount == 0 || (maxCount != -1 && currCount >= maxCount)) return amount;
            if (amount < 0)
            {
                if (maxCount > 0) amount = maxCount;
                else if (!allowSameNodeSpawn) amount = nodes.Count;
                else return amount;
            }
            nullessNodes = ListUtility.RemoveNullEntries(nodes);
            nullessObjs = ListUtility.RemoveNullEntries(objs);
            if (nullessNodes == null || nullessNodes.Count == 0 || nullessObjs == null || nullessObjs.Count == 0) return amount;

            invalidNodeIndexes = new HashSet<int>();
            invalidObjIndexes = new HashSet<int>();
            chosenNode = null;
            chosenObj = null;
            remainingCount = 0;
            for (int i = 0; i < amount; i++)
            {
                while (chosenNode == null) chosenNode = ListUtility.GetObjectByMode(nodeChoiceMode, nullessNodes, ref invalidNodeIndexes);
                while (chosenObj == null)
                {
                    chosenObj = ListUtility.GetObjectByMode(objChoiceMode, nullessObjs, ref invalidObjIndexes);
                    if (!CheckIfValidObject(chosenObj)) chosenObj = null;
                }

                if (SpawnObject(chosenNode, chosenObj))
                {
                    remainingCount--;
                    if (!allowSameNodeSpawn)
                    {
                        nullessNodes.Remove(chosenNode);
                        if (nullessNodes.Count == 0) break;
                    }
                }
                chosenNode = null;
                chosenObj = null;
            }

            OnFinishedSpawning?.Invoke();
            return remainingCount;
        }
        /// <summary>
        /// Spawns the given amount of objects at the given nodes using the set spawn modes.
        /// </summary>
        /// <param name="nodes">The nodes to spawn objects at.</param>
        /// <param name="obj">The object to spawn.</param>
        /// <param name="amount">The amount of objects to spawn. Can be set to -1 to spawn up to the max count or if 'allowSameNodeSpawn' is false will spawn 'til all nodes have an object.</param>
        /// <returns>The amount that was not able to be spawned.</returns>
        public virtual int SpawnObjects(List<SpawnObject> nodes, GameObject obj, int amount)
        {
            if (nodes == null || nodes.Count == 0 || obj == null || amount == 0 || (maxCount != -1 && currCount >= maxCount)) return amount;
            if (amount < 0)
            {
                if (maxCount > 0) amount = maxCount;
                else if (!allowSameNodeSpawn) amount = nodes.Count;
                else return amount;
            }
            nullessNodes = ListUtility.RemoveNullEntries(nodes);
            if (nullessNodes == null || nullessNodes.Count == 0) return amount;

            invalidNodeIndexes = new HashSet<int>();
            chosenNode = null;
            remainingCount = 0;
            for (int i = 0; i < amount; i++)
            {
                while (chosenNode == null) chosenNode = ListUtility.GetObjectByMode(nodeChoiceMode, nullessNodes, ref invalidNodeIndexes);

                if (SpawnObject(chosenNode, obj))
                {
                    remainingCount--;
                    if (!allowSameNodeSpawn)
                    {
                        nullessNodes.Remove(chosenNode);
                        if (nullessNodes.Count == 0) break;
                    }
                }
                chosenNode = null;
            }

            OnFinishedSpawning?.Invoke();
            return remainingCount;
        }
        /// <summary>
        /// Spawns the given object at the given node.
        /// </summary>
        /// <param name="node">The node to spawn at.</param>
        /// <param name="obj">The object to spawn.</param>
        /// <returns>True if successful.</returns>
        public virtual bool SpawnObject(SpawnObject node, GameObject obj)
        {
            if (node == null || obj == null || (!allowSameNodeSpawn && node.HasObj) || !CheckIfValidObject(obj) || (maxCount != -1 && currCount >= maxCount)) return false;

            bool successful = node.Spawn(obj);
            if (successful)
            {
                currCount++;
                if (!currObjsWithMaxCountsDict.ContainsKey(obj)) currObjsWithMaxCountsDict.Add(obj, 0);
                currObjsWithMaxCountsDict[obj]++;
                node.OnSpawn.RemoveListener(CallObjectSpawnedEvent);
                node.OnSpawn.AddListener(CallObjectSpawnedEvent);
            }
            return successful;
        }

        /// <summary>
        /// Reset the stored object counts.
        /// </summary>
        public void ResetCounts()
        {
            currCount = 0;
            objsWithMaxCountsDict = GameObjectWithCount.ToDictionary(objsWithMaxCounts);
            currObjsWithMaxCountsDict = new Dictionary<GameObject, int>();
        }

        /// <summary>
        /// Check if the given object is able to be spawned.
        /// </summary>
        /// <param name="obj">The object to check.</param>
        /// <returns>True if valid.</returns>
        protected bool CheckIfValidObject(GameObject obj)
        {
            if (obj == null) return false;

            if (!currObjsWithMaxCountsDict.ContainsKey(obj)) currObjsWithMaxCountsDict.Add(obj, 0);
            if (objsWithMaxCountsDict.ContainsKey(obj))
            {
                if (currObjsWithMaxCountsDict[obj] >= objsWithMaxCountsDict[obj]) return false;
            }

            return true;
        }
        #endregion

        protected virtual void OnValidate()
        {
            if (maxCount < -1) maxCount = -1;
        }

        protected virtual void Awake()
        {
            ResetCounts();
        }
    }
}
