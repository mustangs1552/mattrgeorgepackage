﻿using System.Collections.Generic;
using UnityEngine;

namespace MattRGeorge.Unity.Utilities.Components
{
    public class SpawnObjectNodeViaAssetLibrary : SpawnObject
    {
        [Tooltip("The names of the lists of objects to use from the assigned asset library.")]
        [SerializeField] protected List<string> listNames = new List<string>();

        /// <summary>
        /// The list names allowed by this object.
        /// </summary>
        public List<string> ListNames
        {
            get
            {
                return listNames;
            }
        }

        private GameObject chosenObj = null;
        private string temp = "";
        private List<string> validNames = new List<string>();

        /// <summary>
        /// Spawn an object from the given asset library.
        /// </summary>
        /// <param name="assetLib">The AssetLibrary to use to select an object.</param>
        /// <param name="validListNames">The list of names that are allowed to be spawned (optional).</param>
        /// <returns>True if successful.</returns>
        public virtual bool SpawnObject(AssetLibraryWithChances assetLib, List<string> validListNames = null)
        {
            return SpawnObject(assetLib, out temp, validListNames);
        }
        /// <summary>
        /// Spawn an object from the given asset library.
        /// </summary>
        /// <param name="assetLib">The AssetLibrary to use to select an object.</param>
        /// <param name="spawnedListName">The list name that the spawned object was from.</param>
        /// <param name="validListNames">The list of names that are allowed to be spawned (optional).</param>
        /// <returns>True if successful.</returns>
        public virtual bool SpawnObject(AssetLibraryWithChances assetLib, out string spawnedListName, List<string> validListNames = null)
        {
            spawnedListName = "";
            if (assetLib == null || listNames == null || listNames.Count == 0) return false;

            validNames = new List<string>();
            if (validListNames != null)
            {
                foreach (string listName in listNames)
                {
                    if (validListNames.Contains(listName)) validNames.Add(listName);
                }
                if (validNames.Count == 0) return false;
            }
            else validNames = listNames;

            chosenObj = assetLib.GetRandomObjectWithChance(validNames, out spawnedListName);
            if (chosenObj == null) return false;

            StartCoroutine(base.InternalSpawn(chosenObj));
            return true;
        }
    }
}
