﻿using System.Collections.Generic;
using UnityEngine;
using MattRGeorge.General.Utilities;
using MattRGeorge.Unity.Utilities.Static;

namespace MattRGeorge.Unity.Utilities.Components
{
    public class SpawnObjectsAuto : SpawnObjects
    {
        [Tooltip("The amount of spawns on awake or when spawning a batch.")]
        [SerializeField] protected int batchSpawnCount = 1;
        [Tooltip("The list of objects to choose from.")]
        [SerializeField] private List<GameObject> objs = new List<GameObject>();
        [Tooltip("How should the objects be chosen?")]
        [SerializeField] private ListChoiceMode spawnMode = ListChoiceMode.Randomly;
        [Tooltip("Spawn on awake/start?")]
        [SerializeField] private UnityStartMethods spawnOn = UnityStartMethods.Awake;

        /// <summary>
        /// The max amount of objects that this spawner can spawn.
        /// </summary>
        public int MaxCount => maxCount;
        /// <summary>
        /// The prefabs that are being spawned.
        /// </summary>
        public List<GameObject> Objs
        {
            get
            {
                return objs;
            }
        }

        private HashSet<int> invalidIndexes = new HashSet<int>();
        private int remainingCount = 0;

        #region Methods
        /// <summary>
        /// Spawn a batch of objects using the batch count setting.
        /// </summary>
        /// <returns>The amount that was not able to be spawned.</returns>
        public virtual int BatchSpawnObjects()
        {
            if (batchSpawnCount <= 0) return batchSpawnCount;

            return SpawnObjects(batchSpawnCount);
        }
        /// <summary>
        /// Spawn the given amount of objects.
        /// </summary>
        /// <param name="amount">The amount of objects to spawn. Can be set to -1 to spawn up to the max count.</param>
        /// <returns>The amount that was not able to be spawned.</returns>
        public virtual int SpawnObjects(int amount)
        {
            if (amount == 0) return amount;
            if (amount < 0)
            {
                if (maxCount > 0) amount = maxCount;
                else return amount;
            }

            remainingCount = amount;
            for (int i = 0; i < amount; i++)
            {
                if(SpawnObject(ListUtility.GetObjectByMode(spawnMode, objs, ref invalidIndexes))) remainingCount--;
            }

            return remainingCount;
        }
        #endregion

        protected override void OnValidate()
        {
            base.OnValidate();
            if (batchSpawnCount < -1) batchSpawnCount = -1;
        }

        protected override void Awake()
        {
            base.Awake();
            
            objs = ListUtility.RemoveNullEntries(objs);
            if (spawnOn == UnityStartMethods.Awake) BatchSpawnObjects();
        }
        protected virtual void Start()
        {
            if (spawnOn == UnityStartMethods.Start) BatchSpawnObjects();
        }
    }
}
