﻿using System.Collections;
using UnityEngine;
using MattRGeorge.Unity.Tools.ObjectPooling;

namespace MattRGeorge.Unity.Utilities.Components
{
    public class SpawnObject : MonoBehaviour
    {
        [Tooltip("Should the object be active when spawned?")]
        public bool spawnAsActive = true;
        [Tooltip("Should the object be managed by the Object Pool system?")]
        public bool isObjectPoolManaged = true;
        [Tooltip("The time to wait before spawning the object. This and 'postSpawnDelay' determines spawn times between objects.")]
        [SerializeField] protected float spawnDelay = 0;
        [Tooltip("Time to wait before calling post spawn actions. This and 'spawnDelay' determines spawn times between objects.")]
        [SerializeField] protected float postSpawnDelay = 0;
        public GameObjectUnityEvent OnSpawn = new GameObjectUnityEvent();
        public GameObjectUnityEvent OnPostSpawn = new GameObjectUnityEvent();

        /// <summary>
        /// Delay before spawning an object.
        /// </summary>
        public float SpawnDelay
        {
            get => spawnDelay;
            set
            {
                if (value < 0) return;
                spawnDelay = value;
            }
        }
        /// <summary>
        /// Delay after the object is spawned before the post spawn event is called.
        /// </summary>
        public float PostSpawnDelay
        {
            get => postSpawnDelay;
            set
            {
                if (value < 0) return;
                postSpawnDelay = value;
            }
        }

        /// <summary>
        /// Does this object have a spawned object or one pending spawn?
        /// </summary>
        public bool HasObj
        {
            get
            {
                return lastSpawnedObj || HasPendingObjs;
            }
        }
        /// <summary>
        /// The last object that was spawned.
        /// </summary>
        public GameObject LastSpawnedObj => lastSpawnedObj;
        /// <summary>
        /// Is there objects pending spawn?
        /// </summary>
        public bool HasPendingObjs
        {
            get
            {
                return pendingObjsCount > 0;
            }
        }

        protected GameObject lastSpawnedObj = null;
        private int pendingObjsCount = 0;

        #region Methods
        /// <summary>
        /// Spawn the given object.
        /// </summary>
        /// <param name="obj">The object to spawn.</param>
        public virtual bool Spawn(GameObject obj)
        {
            if (!obj) return false;

            StartCoroutine(InternalSpawn(obj));
            return true;
        }

        /// <summary>
        /// Destroys the last spawnd object.
        /// </summary>
        public virtual void DestroyLastSpawnedObject()
        {
            if (lastSpawnedObj != null) return;

            ObjectPoolManager.SINGLETON.Destroy(lastSpawnedObj);
            pendingObjsCount = 0;
            lastSpawnedObj = null;
        }

        /// <summary>
        /// Spawn the given object using the set settings.
        /// </summary>
        /// <param name="obj">The given object.</param>
        protected virtual IEnumerator InternalSpawn(GameObject obj)
        {
            if (!obj)
            {
                OnSpawnFailed(obj, "Provided object was null!");
                yield break;
            }
            pendingObjsCount++;
            yield return new WaitForSeconds(SpawnDelay);

            if(pendingObjsCount <= 0)
            {
                OnSpawnFailed(obj, "Pending spawns canceled!");
                yield break;
            }
            lastSpawnedObj = ObjectPoolManager.SINGLETON.Instantiate(obj, transform.position, transform.rotation, spawnAsActive, isObjectPoolManaged);
            OnSpawn?.Invoke(lastSpawnedObj);
            pendingObjsCount--;
            yield return new WaitForSeconds(PostSpawnDelay);

            OnPostSpawn?.Invoke(lastSpawnedObj);
        }

        /// <summary>
        /// Called when the spawn coroutine fails to spawn the object.
        /// Reasons:
        /// - "Provided object was null!".
        /// - "Pending spawns canceled!".
        /// </summary>
        /// <param name="obj">The object that was attempted to be spawned.</param>
        protected virtual void OnSpawnFailed(GameObject obj, string reason)
        {
            Debug.LogWarning($"Failed to spawn '{((obj) ? obj.name : "null")}'. Reason: {reason}");
        }
        #endregion

        protected virtual void OnValidate()
        {
            SpawnDelay = spawnDelay;
            PostSpawnDelay = postSpawnDelay;
        }
    }
}
