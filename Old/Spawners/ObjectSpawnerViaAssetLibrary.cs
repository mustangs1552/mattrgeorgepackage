﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using MattRGeorge.Unity.Utilities.Static;

namespace MattRGeorge.Unity.Utilities.Components
{
    public class ObjectSpawnerViaAssetLibrary : ObjectSpawnerBase
    {
        [Tooltip("The asset library to choose objects from.")]
        [SerializeField] protected AssetLibraryWithChances assetLib = null;
        [Tooltip("The maximum amount of objects that can be spawned. Set to -1 for unlimited.")]
        [SerializeField] protected int maxCount = -1;
        [Tooltip("Allow spawning at a node that already has a spawned object?")]
        [SerializeField] protected bool allowSameNodeSpawn = true;
        [Tooltip("Object list names that should have a limit for how many are spawned through this spawner.")]
        [SerializeField] private List<DictionaryStringInt> objListNameCountLimits = new List<DictionaryStringInt>();
        public UnityEvent OnFinishedSpawning = new UnityEvent();

        /// <summary>
        /// The max amount of objects that this spawner can spawn.
        /// </summary>
        public int MaxCount => maxCount;

        private List<SpawnObjectNodeViaAssetLibrary> nullessNodes = new List<SpawnObjectNodeViaAssetLibrary>();
        private HashSet<int> invalidNodeIndexes = new HashSet<int>();
        private SpawnObjectNodeViaAssetLibrary chosenNode = null;
        private int remainingCount = 0;
        private int currCount = 0;
        private Dictionary<string, int> objListNameMaxCounts = new Dictionary<string, int>();
        private Dictionary<string, int> currObjListNameCounts = new Dictionary<string, int>();

        #region Methods
        /// <summary>
        /// Spawns the given amount of objects at the given nodes using the set spawn mode.
        /// </summary>
        /// <param name="nodes">The nodes to spawn objects at.</param>
        /// <param name="amount">The amount of objects to spawn. Can be set to -1 to spawn up to the max count or if 'allowSameNodeSpawn' is false will spawn 'til all nodes have an object.</param>
        /// <returns>The amount that was not able to be spawned.</returns>
        public virtual int SpawnObjects(List<SpawnObjectNodeViaAssetLibrary> nodes, int amount)
        {
            if (nodes == null || nodes.Count == 0 || amount == 0 || (maxCount != -1 && currCount >= maxCount)) return amount;
            if (amount < 0)
            {
                if (maxCount > 0) amount = maxCount;
                else if (!allowSameNodeSpawn) amount = nodes.Count;
                else return amount;
            }
            nullessNodes = ListUtility.RemoveNullEntries(nodes);
            if (nullessNodes == null || nullessNodes.Count == 0) return amount;

            invalidNodeIndexes = new HashSet<int>();
            chosenNode = null;
            remainingCount = 0;
            for (int i = 0; i < amount; i++)
            {
                while (chosenNode == null) chosenNode = ListUtility.GetObjectByMode(nodeChoiceMode, nullessNodes, ref invalidNodeIndexes);

                if (SpawnObject(chosenNode))
                {
                    remainingCount--;
                    if (!allowSameNodeSpawn)
                    {
                        nullessNodes.Remove(chosenNode);
                        if (nullessNodes.Count == 0) break;
                    }
                }
                chosenNode = null;
            }

            OnFinishedSpawning?.Invoke();
            return remainingCount;
        }

        /// <summary>
        /// Spawns an object at the given node.
        /// </summary>
        /// <param name="node">The node to spawn at.</param>
        /// <param name="validListNames">The list of names that are allowed to be spawned (optional).</param>
        /// <returns>True if successful.</returns>
        public virtual bool SpawnObject(SpawnObjectNodeViaAssetLibrary node, List<string> validListNames = null)
        {
            string temp = "";
            return SpawnObject(node, out temp, validListNames);
        }
        /// <summary>
        /// Spawns an object at the given node.
        /// </summary>
        /// <param name="node">The node to spawn at.</param>
        /// <param name="spawnedObjListName">The list name that the spawned object was from.</param>
        /// <param name="validListNames">The list of names that are allowed to be spawned (optional).</param>
        /// <returns>True if successful.</returns>
        public virtual bool SpawnObject(SpawnObjectNodeViaAssetLibrary node, out string spawnedObjListName, List<string> validListNames = null)
        {
            spawnedObjListName = "";
            if (assetLib == null || node == null || (!allowSameNodeSpawn && node.HasObj) || (maxCount != -1 && currCount >= maxCount)) return false;

            if (validListNames == null)
            {
                validListNames = new List<string>();
                foreach (string listName in node.ListNames)
                {
                    if (!validListNames.Contains(listName) && (!objListNameMaxCounts.ContainsKey(listName) || (currObjListNameCounts.ContainsKey(listName) && currObjListNameCounts[listName] < objListNameMaxCounts[listName]))) validListNames.Add(listName);
                }
            }

            bool successful = node.SpawnObject(assetLib, out spawnedObjListName, validListNames);
            if (successful)
            {
                currCount++;
                if (!string.IsNullOrEmpty(spawnedObjListName) && currObjListNameCounts.ContainsKey(spawnedObjListName)) currObjListNameCounts[spawnedObjListName]++;
                node.OnSpawn.RemoveListener(CallObjectSpawnedEvent);
                node.OnSpawn.AddListener(CallObjectSpawnedEvent);
            }
            return successful;
        }

        /// <summary>
        /// Reset the stored object counts.
        /// </summary>
        public void ResetCounts()
        {
            currCount = 0;
            ResetDictionaries();
        }

        /// <summary>
        /// Reset the dictionaries for the object spawn limits.
        /// </summary>
        protected void ResetDictionaries()
        {
            objListNameMaxCounts = new Dictionary<string, int>();
            currObjListNameCounts = new Dictionary<string, int>();
            foreach (DictionaryStringInt obj in objListNameCountLimits)
            {
                if (!currObjListNameCounts.ContainsKey(obj.name) && !objListNameMaxCounts.ContainsKey(obj.name))
                {
                    objListNameMaxCounts.Add(obj.name, obj.maxCount);
                    currObjListNameCounts.Add(obj.name, 0);
                }
                else Debug.LogError($"'{obj.name}' is listed in 'objListNameCountLimits' twice and should only be listed once!");
            }
        }
        #endregion

        protected virtual void OnValidate()
        {
            if (maxCount < -1) maxCount = -1;
        }

        protected virtual void Awake()
        {
            ResetCounts();
        }
    }
}
