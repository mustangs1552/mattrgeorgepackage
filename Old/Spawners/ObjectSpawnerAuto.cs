﻿using System.Collections.Generic;
using UnityEngine;

namespace MattRGeorge.Unity.Utilities.Components
{
    public class ObjectSpawnerAuto : ObjectSpawner
    {
        [Tooltip("The amount of spawns on awake or when spawning a batch.")]
        [SerializeField] private int batchSpawnCount = 1;
        [Tooltip("The nodes to spawn objects at.")]
        [SerializeField] private List<SpawnObject> nodes = new List<SpawnObject>();
        [Tooltip("The list of objects to choose from.")]
        [SerializeField] private List<GameObject> objs = new List<GameObject>();
        [Tooltip("Spawn on awake/start?")]
        [SerializeField] private UnityStartMethods spawnOn = UnityStartMethods.Awake;

        /// <summary>
        /// All the objects spawned at the saved nodes.
        /// </summary>
        public List<GameObject> SpawnedObjs
        {
            get
            {
                List<GameObject> spawnedObjs = new List<GameObject>();
                foreach(SpawnObject node in nodes)
                {
                    if (node.LastSpawnedObj != null) spawnedObjs.Add(node.LastSpawnedObj);
                }
                return spawnedObjs;
            }
        }

        #region Methods
        /// <summary>
        /// Spawn a batch of objects using the batch count setting.
        /// </summary>
        /// <returns>The amount that was not able to be spawned.</returns>
        public virtual int BatchSpawnObjects()
        {
            if (nodes == null || nodes.Count == 0 || objs == null || objs.Count == 0) return batchSpawnCount;

            return SpawnObjects(batchSpawnCount);
        }
        /// <summary>
        /// Spawn the given amount of objects.
        /// </summary>
        /// <param name="amount">The amount to spawn.</param>
        /// <returns>The amount that was not able to be spawned.</returns>
        public virtual int SpawnObjects(int amount)
        {
            if (nodes == null || nodes.Count == 0 || objs == null || objs.Count == 0) return amount;

            return SpawnObjects(nodes, objs, amount);
        }
        /// <summary>
        /// Spawn the given object at the given node.
        /// The node and object must exist in the assigned lists.
        /// </summary>
        /// <param name="node">The node in the saved list to spawn at.</param>
        /// <param name="obj">The object in the saved list to spawn.</param>
        /// <returns>True if successful.</returns>
        public override bool SpawnObject(SpawnObject node, GameObject obj)
        {
            if (node == null || obj == null || !nodes.Contains(node) || !objs.Contains(obj)) return false;

            return base.SpawnObject(node, obj);
        }

        /// <summary>
        /// Destroy the objects that were spawned at the given object nodes and reset the object spawn limits.
        /// </summary>
        public void DestroyObjects()
        {
            nodes.ForEach(x => x.DestroyLastSpawnedObject());
            ResetCounts();
        }
        #endregion

        protected override void OnValidate()
        {
            base.OnValidate();
            if (batchSpawnCount < -1) batchSpawnCount = -1;
        }

        protected override void Awake()
        {
            base.Awake();
            if (spawnOn == UnityStartMethods.Awake) BatchSpawnObjects();
        }
        protected virtual void Start()
        {
            if (spawnOn == UnityStartMethods.Start) BatchSpawnObjects();
        }
    }
}
