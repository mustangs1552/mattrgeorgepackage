﻿using System.Collections.Generic;
using UnityEngine;
using MattRGeorge.Unity.Tools.ObjectPooling;

namespace MattRGeorge.Unity.Utilities.Components
{
    public class SpawnObjects : SpawnObject
    {
        [Tooltip("The maximum amount of objects that can be spawned. Set to -1 for unlimited.")]
        public int maxCount = -1;
        [Tooltip("The maximum amount of objects that can be spawned by object.")]
        [SerializeField] protected List<GameObjectWithCount> objsWithMaxCounts = new List<GameObjectWithCount>();

        /// <summary>
        /// Max counts by object.
        /// </summary>
        public List<GameObjectWithCount> ObjsWithMaxCounts
        {
            get => objsWithMaxCounts;
            set
            {
                if (value == null) return;
                objsWithMaxCounts = value;
            }
        }

        /// <summary>
        /// The object that has been spawned.
        /// </summary>
        public List<GameObject> SpawnedObjs => spawnedObjs;

        private List<GameObject> spawnedObjs = new List<GameObject>();
        private int currCount = 0;
        private Dictionary<GameObject, int> objsWithMaxCountsDict = new Dictionary<GameObject, int>();
        private Dictionary<GameObject, int> currObjsWithMaxCountsDict = new Dictionary<GameObject, int>();

        #region Methods
        /// <summary>
        /// Spawn the given object.
        /// </summary>
        /// <param name="obj">An object that may not be in the saved list to spawn.</param>
        /// <returns>True if successful.</returns>
        public bool SpawnObject(GameObject obj)
        {
            if (obj == null || (maxCount > -1 && currCount >= maxCount)) return false;

            if (!currObjsWithMaxCountsDict.ContainsKey(obj)) currObjsWithMaxCountsDict.Add(obj, 0);
            if (objsWithMaxCountsDict.ContainsKey(obj))
            {
                if (currObjsWithMaxCountsDict[obj] >= objsWithMaxCountsDict[obj]) return false;
            }

            if (Spawn(obj))
            {
                currObjsWithMaxCountsDict[obj]++;
                currCount++;
                return true;
            }

            return false;
        }

        /// <summary>
        /// Destroys all the objects that this object spawned.
        /// </summary>
        public virtual void DestroySpawnedObjects()
        {
            DestroyLastSpawnedObject();
            spawnedObjs.ForEach(x => ObjectPoolManager.SINGLETON.Destroy(x));
            spawnedObjs = new List<GameObject>();
            ResetDictionaries();
            currCount = 0;
        }

        /// <summary>
        /// Add the spawned object to the list of spawned objects.
        /// </summary>
        /// <param name="obj">The object spawned.</param>
        protected virtual void AddObject(GameObject obj)
        {
            if (!obj) return;

            spawnedObjs.Add(obj);
        }

        /// <summary>
        /// Reset the count dictionaries.
        /// </summary>
        protected void ResetDictionaries()
        {
            objsWithMaxCountsDict = GameObjectWithCount.ToDictionary(objsWithMaxCounts);
            currObjsWithMaxCountsDict = new Dictionary<GameObject, int>();
        }
        #endregion

        protected override void OnValidate()
        {
            base.OnValidate();
        }

        protected virtual void Awake()
        {
            OnSpawn.AddListener(AddObject);
            ResetDictionaries();
        }
    }
}
