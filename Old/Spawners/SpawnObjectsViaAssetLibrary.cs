﻿using System.Collections.Generic;
using UnityEngine;

namespace MattRGeorge.Unity.Utilities.Components
{
    public class SpawnObjectsViaAssetLibrary : SpawnObjectsNodeViaAssetLibrary
    {
        [Tooltip("The asset library to choose objects from.")]
        [SerializeField] protected AssetLibraryWithChances assetLib = null;

        /// <summary>
        /// Spawn an object from the assigned asset library.
        /// </summary>
        /// <param name="assetLib">Ignored, uses 'assetLib' assigned in inspector.</param>
        /// <param name="validListNames">The list of names that are allowed to be spawned (optional).</param>
        /// <returns>True if successful.</returns>
        public override bool SpawnObject(AssetLibraryWithChances assetLib = null, List<string> validListNames = null)
        {
            if (this.assetLib == null) return false;

            return base.SpawnObject(this.assetLib, validListNames);
        }
        /// <summary>
        /// Spawn an object from the assigned asset library.
        /// </summary>
        /// <param name="assetLib">Ignored, uses 'assetLib' assigned in inspector.</param>
        /// <param name="spawnedListName">The list name that the spawned object was from.</param>
        /// <param name="validListNames">The list of names that are allowed to be spawned (optional).</param>
        /// <returns>True if successful.</returns>
        public override bool SpawnObject(AssetLibraryWithChances assetLib, out string spawnedListName, List<string> validListNames = null)
        {
            spawnedListName = "";
            if (this.assetLib == null) return false;

            return base.SpawnObject(this.assetLib, out spawnedListName, validListNames);
        }
    }
}
