﻿using UnityEngine;
using MattRGeorge.General.Utilities;

namespace MattRGeorge.Unity.Utilities.Components
{
    public abstract class ObjectSpawnerBase : MonoBehaviour
    {
        [Tooltip("How should the nodes being spawned be chosen?")]
        [SerializeField] protected ListChoiceMode nodeChoiceMode = ListChoiceMode.Randomly;
        public GameObjectUnityEvent OnObjectSpawned = new GameObjectUnityEvent();

        /// <summary>
        /// Called when an object was spawned, calls this spawner's OnObjectSpawned event.
        /// </summary>
        /// <param name="spawnedObj">The spawned object.</param>
        protected void CallObjectSpawnedEvent(GameObject spawnedObj)
        {
            OnObjectSpawned?.Invoke(spawnedObj);
        }
    }
}
