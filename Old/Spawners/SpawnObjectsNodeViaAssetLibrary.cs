﻿using System.Collections.Generic;
using UnityEngine;

namespace MattRGeorge.Unity.Utilities.Components
{
    public class SpawnObjectsNodeViaAssetLibrary : SpawnObjects
    {
        [Tooltip("The names of the lists of objects to use from the assigned asset library.")]
        [SerializeField] protected List<string> listNames = new List<string>();
        [Tooltip("Object list names that should have a limit for how many are spawned through this spawner.")]
        [SerializeField] private List<DictionaryStringInt> objListNameCountLimits = new List<DictionaryStringInt>();

        private GameObject chosenObj = null;
        private string temp = "";
        private List<string> validNames = new List<string>();
        private Dictionary<string, int> objListNameMaxCounts = new Dictionary<string, int>();
        private Dictionary<string, int> currObjListNameCounts = new Dictionary<string, int>();

        /// <summary>
        /// Spawn an object from the given assigned asset library.
        /// </summary>
        /// <param name="assetLib">The asset library to use to pick an object.</param>
        /// <param name="validListNames">The list of names that are allowed to be spawned (optional).</param>
        /// <returns>True if successful.</returns>
        public virtual bool SpawnObject(AssetLibraryWithChances assetLib, List<string> validListNames = null)
        {
            return SpawnObject(assetLib, out temp, validListNames);
        }
        /// <summary>
        /// Spawn an object from the given asset library.
        /// </summary>
        /// <param name="assetLib">The AssetLibrary to use to select an object.</param>
        /// <param name="spawnedListName">The list name that the spawned object was from.</param>
        /// <param name="validListNames">The list of names that are allowed to be spawned (optional).</param>
        /// <returns>True if successful.</returns>
        public virtual bool SpawnObject(AssetLibraryWithChances assetLib, out string spawnedListName, List<string> validListNames = null)
        {
            spawnedListName = "";
            if (assetLib == null || listNames == null || listNames.Count == 0) return false;

            validNames = new List<string>();
            if (validListNames != null)
            {
                foreach (string listName in listNames)
                {
                    if (validListNames.Contains(listName)) validNames.Add(listName);
                }
                if (validNames.Count == 0) return false;
            }
            else validNames = listNames;

            chosenObj = assetLib.GetRandomObjectWithChance(validNames, out spawnedListName);
            if (chosenObj == null) return false;
            if (!currObjListNameCounts.ContainsKey(spawnedListName)) currObjListNameCounts.Add(spawnedListName, 0);
            if(objListNameMaxCounts.ContainsKey(spawnedListName))
            {
                if (currObjListNameCounts[spawnedListName] >= objListNameMaxCounts[spawnedListName]) return false;
            }

            bool successful = SpawnObject(chosenObj);
            if(successful) currObjListNameCounts[spawnedListName]++;
            return successful;
        }

        /// <summary>
        /// Destroys all objects that this object spawned.
        /// </summary>
        public override void DestroySpawnedObjects()
        {
            base.DestroySpawnedObjects();
            ResetDictionaries();
        }

        /// <summary>
        /// Reset the dictionaries for the object spawn limits.
        /// </summary>
        protected void ResetListNameDictionaries()
        {
            objListNameMaxCounts = new Dictionary<string, int>();
            currObjListNameCounts = new Dictionary<string, int>();
            foreach (DictionaryStringInt obj in objListNameCountLimits)
            {
                if (!currObjListNameCounts.ContainsKey(obj.name) && !objListNameMaxCounts.ContainsKey(obj.name))
                {
                    objListNameMaxCounts.Add(obj.name, obj.maxCount);
                    currObjListNameCounts.Add(obj.name, 0);
                }
                else Debug.LogError($"'{obj.name}' is listed in 'objListNameCountLimits' twice and should only be listed once!");
            }
        }

        protected override void Awake()
        {
            base.Awake();
            ResetListNameDictionaries();
        }
    }
}
