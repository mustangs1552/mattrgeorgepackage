﻿using System;
using UnityEngine;
using MattRGeorge.Unity.Utilities.Helpers;

namespace MattRGeorge.Unity.Tools.Stats
{
    /// <summary>
    /// A stat with a single value that can be changed.
    /// </summary>
    [Serializable]
    public class Stat
    {
        [Tooltip("The starting value for this stat.")]
        [SerializeField] protected float startingAmount = 1;

        /// <summary>
        /// The current amount of this stat.
        /// </summary>
        public virtual float CurrAmount
        {
            get
            {
                return currAmount;
            }
            set
            {
                currAmount = value;
                OnChanged?.Invoke(CurrAmount);
            }
        }

        public FloatUnityEvent OnChanged = new FloatUnityEvent();

        protected float currAmount = 0;

        /// <summary>
        /// Create a new stat.
        /// </summary>
        /// <param name="startingAmount">The starting amount.</param>
        public Stat(float startingAmount)
        {
            this.startingAmount = startingAmount;
            StatReset();
        }

        /// <summary>
        /// Reset all the current values to their starting values.
        /// </summary>
        public virtual void StatReset()
        {
            CurrAmount = startingAmount;
        }
    }

    /// <summary>
    /// A simple stat with starting and active values.
    /// Uses a max value value and a minnimum value of 0,.
    /// Best used for gauges such as health or energy.
    /// Does nothing on its own.
    /// </summary>
    [Serializable]
    public class StatGauge : Stat
    {
        [Tooltip("The starting max amount allowed.")]
        [SerializeField] protected float startingMaxAmount = 100;
        [Tooltip("Update the current amount to keep the same percentage of full when the max amount is changed?")]
        [SerializeField] protected bool updateWithMaxAmount = true;

        public override float CurrAmount
        {
            get
            {
                return currAmount;
            }
            set
            {
                if (value <= 0)
                {
                    if (!reachedMinLimit)
                    {
                        reachedMinLimit = true;
                        reachedMaxLimit = false;
                        currAmount = 0;
                        OnMinReached?.Invoke(CurrAmount);
                        OnChanged?.Invoke(CurrAmount);
                    }
                }
                else if (value >= CurrMaxAmount)
                {
                    if (!reachedMaxLimit)
                    {
                        reachedMinLimit = false;
                        reachedMaxLimit = true;
                        currAmount = CurrMaxAmount;
                        OnMaxReached?.Invoke(CurrAmount);
                        OnChanged?.Invoke(CurrAmount);
                    }
                }
                else if (value != currAmount)
                {
                    reachedMinLimit = false;
                    reachedMaxLimit = false;
                    currAmount = value;
                    OnChanged?.Invoke(CurrAmount);
                }
            }
        }
        /// <summary>
        /// The current max amount allowed.
        /// </summary>
        public virtual float CurrMaxAmount
        {
            get
            {
                return currMaxAmount;
            }
            set
            {
                if (value > 0)
                {
                    float amountPerc = -1;
                    if (updateWithMaxAmount) amountPerc = CurrAmountPerc;

                    currMaxAmount = value;

                    reachedMaxLimit = false;
                    CurrAmount = (amountPerc != -1) ? currMaxAmount * amountPerc : CurrAmount;
                }
            }
        }
        /// <summary>
        /// The precentage of current amount out of the current max amount.
        /// </summary>
        public virtual float CurrAmountPerc
        {
            get
            {
                return CurrAmount / currMaxAmount;
            }
        }

        public FloatUnityEvent OnMinReached = new FloatUnityEvent();
        public FloatUnityEvent OnMaxReached = new FloatUnityEvent();

        protected float currMaxAmount = 0;
        protected bool reachedMinLimit = false;
        protected bool reachedMaxLimit = false;

        /// <summary>
        /// Create a new stat gauge.
        /// </summary>
        /// <param name="startingAmount">The starting amount.</param>
        /// <param name="updateWithMaxAmount">Update current amount with max amount?</param>
        public StatGauge(float startingAmount, float startingMaxAmount, bool updateWithMaxAmount) : base(startingAmount)
        {
            this.startingMaxAmount = startingMaxAmount;
            this.updateWithMaxAmount = updateWithMaxAmount;
            StatReset();
        }

        /// <summary>
        /// Set the current amount via a percentage of the range.
        /// </summary>
        /// <param name="percentage">A value between 0 and 1.</param>
        public virtual void SetAmount(float percentage)
        {
            if (percentage < 0 || percentage > 1) return;
            CurrAmount = percentage * CurrMaxAmount;
        }

        public override void StatReset()
        {
            CurrMaxAmount = startingMaxAmount;
            CurrAmount = startingAmount;
            reachedMinLimit = false;
            reachedMaxLimit = false;
        }
    }
    /// <summary>
    /// An extension of a simple gauge stat, this adds the stat's ability to decrease/increase via an update.
    /// The update method must be called by another class, does nothing on its own.
    /// </summary>
    [Serializable]
    public class DynamicStatGauge : StatGauge
    {
        [Tooltip("The starting amount adjusted for each stat update.")]
        [SerializeField] protected float startingUpdateAmount = -1;

        /// <summary>
        /// The current amount adjusted for each stat update.
        /// </summary>
        public virtual float CurrUpdateAmount
        {
            get
            {
                return currUpdateAmount;
            }
            set
            {
                currUpdateAmount = value;
            }
        }

        protected float currUpdateAmount = 0;

        /// <summary>
        /// Create a new dynamic stat gauge.
        /// </summary>
        /// <param name="startingAmount">The starting amount.</param>
        /// <param name="updateWithMaxAmount">Update current amount with max amount?</param>
        /// <param name="startingUpdateAmount">The starting update amount.</param>
        public DynamicStatGauge(float startingAmount, float startingMaxAmount, bool updateWithMaxAmount, float startingUpdateAmount) : base(startingAmount, startingMaxAmount, updateWithMaxAmount)
        {
            this.startingUpdateAmount = startingUpdateAmount;
            StatReset();
        }

        /// <summary>
        /// Update this stat via the current update amount.
        /// </summary>
        public virtual void StatUpdate()
        {
            CurrAmount += CurrUpdateAmount;
        }
        public override void StatReset()
        {
            base.StatReset();
            CurrUpdateAmount = startingUpdateAmount;
        }
    }

    /// <summary>
    /// A stat with a minimum and maximum amount.
    /// Does nothing on its own.
    /// </summary>
    [Serializable]
    public class RangeStat : StatGauge
    {
        [Tooltip("The starting minimum amount.")]
        [SerializeField] protected float startingMinAmount = 0;

        /// <summary>
        /// The current minimum amount.
        /// </summary>
        public virtual float CurrMinAmount
        {
            get
            {
                return currMinAmount;
            }
            set
            {
                if (value < ((initialValuesSet) ? CurrMaxAmount : startingMaxAmount))
                {
                    float amountPerc = -1;
                    if (updateWithMaxAmount) amountPerc = CurrAmountPerc;

                    currMinAmount = value;

                    reachedMinLimit = false;
                    CurrAmount = (amountPerc != -1) ? (CurrMaxAmount - currMinAmount) * amountPerc + currMinAmount : CurrAmount;
                }
            }
        }
        /// <summary>
        /// The current amount.
        /// </summary>
        public override float CurrAmount
        {
            get
            {
                return currAmount;
            }
            set
            {
                if (value <= CurrMinAmount)
                {
                    if (!reachedMinLimit)
                    {
                        reachedMinLimit = true;
                        reachedMaxLimit = false;
                        currAmount = CurrMinAmount;
                        OnMinReached?.Invoke(CurrAmount);
                        OnChanged?.Invoke(CurrAmount);
                    }
                }
                else if (value >= CurrMaxAmount)
                {
                    if (!reachedMaxLimit)
                    {
                        reachedMinLimit = false;
                        reachedMaxLimit = true;
                        currAmount = CurrMaxAmount;
                        OnMaxReached?.Invoke(CurrAmount);
                        OnChanged?.Invoke(CurrAmount);
                    }
                }
                else if (value != currAmount)
                {
                    reachedMinLimit = false;
                    reachedMaxLimit = false;
                    currAmount = value;
                    OnChanged?.Invoke(CurrAmount);
                }
            }
        }
        /// <summary>
        /// The current amount as a percentage (0-1).
        /// </summary>
        public override float CurrAmountPerc
        {
            get
            {
                return CurrAmount / (CurrMaxAmount - CurrMinAmount);
            }
        }

        protected float currMinAmount = 0;
        protected bool initialValuesSet = false;

        /// <summary>
        /// Create a new range stat and reset values.
        /// </summary>
        /// <param name="startingAmount">The starting amount.</param>
        /// <param name="updateWithMaxAmount">Update current amount with max amount?</param>
        /// <param name="sma">The starting minimum amount.</param>
        public RangeStat(float startingAmount, float startingMaxAmount, bool updateWithMaxAmount, float startingMinAmount) : base(startingAmount, startingMaxAmount, updateWithMaxAmount)
        {
            this.startingMinAmount = startingMinAmount;
            StatReset();
        }
        
        /// <summary>
         /// Set the current amount via a percentage of the range.
         /// </summary>
         /// <param name="percentage">A value between 0 and 1.</param>
        public override void SetAmount(float percentage)
        {
            if (percentage < 0 || percentage > 1) return;
            CurrAmount = percentage * (CurrMaxAmount - CurrMinAmount) + CurrMinAmount;
        }

        /// <summary>
        /// Reset all the current values to their starting values.
        /// </summary>
        public override void StatReset()
        {
            base.StatReset();

            initialValuesSet = false;

            CurrMinAmount = startingMinAmount;
            reachedMinLimit = false;
            CurrAmount = startingAmount;
            reachedMinLimit = false;
            initialValuesSet = true;
        }
    }
}