﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace MattRGeorge.Unity.Utilities
{
    [Serializable]
    public class NameInt
    {
        public string name = "";
        public int count = 0;
    }

    [Serializable]
    public class DictionaryStringInt
    {
        public string name = "";
        public int maxCount = 0;
    }

    [Serializable]
    public class NameGameObject
    {
        public string name = "";
        public GameObject obj = null;

        public static Dictionary<string, GameObject> ConvertToDictionary(List<NameGameObject> objs)
        {
            if (objs == null || objs.Count == 0) return new Dictionary<string, GameObject>();
            Dictionary<string, GameObject> objsDict = new Dictionary<string, GameObject>();

            foreach(NameGameObject obj in objs)
            {
                if (!objsDict.ContainsKey(obj.name) && !string.IsNullOrEmpty(obj.name)) objsDict.Add(obj.name, obj.obj);
                else Debug.LogError($"'{obj.name}' is listed in 'objs' twice and should only be listed once!");
            }

            return objsDict;
        }
    }
    [Serializable]
    public class NameListGameObject
    {
        public string name = "";
        public List<GameObject> objs = new List<GameObject>();

        public static Dictionary<string, List<GameObject>> ConvertToDictionary(List<NameListGameObject> objLists)
        {
            if (objLists == null || objLists.Count == 0) return new Dictionary<string, List<GameObject>>();
            Dictionary<string, List<GameObject>> objListsDict = new Dictionary<string, List<GameObject>>();

            foreach (NameListGameObject obj in objLists)
            {
                if (!objListsDict.ContainsKey(obj.name) && !string.IsNullOrEmpty(obj.name)) objListsDict.Add(obj.name, obj.objs);
                else Debug.LogError($"'{obj.name}' is listed in 'objLists' twice and should only be listed once!");
            }

            return objListsDict;
        }
    }

    [Serializable]
    public class GameObjectWithCount
    {
        public GameObject obj = null;
        public int count = 0;

        public static Dictionary<GameObject, int> ToDictionary(List<GameObjectWithCount> objsWithCounts)
        {
            Dictionary<GameObject, int> dict = new Dictionary<GameObject, int>();
            foreach(GameObjectWithCount objWithCount in objsWithCounts)
            {
                if (!dict.ContainsKey(objWithCount.obj)) dict.Add(objWithCount.obj, objWithCount.count);
                else Debug.LogError($"'{objWithCount.obj.name}' is listed in 'objsWithCounts' twice and should only be listed once!");
            }
            return dict;
        }
    }

    public class DictionaryTObjectChance<T>
    {
        public T obj = default;
        public int chance = 1;
    }

    [Serializable]
    public class DictionaryGameObjectChance
    {
        public GameObject obj = null;
        public int chance = 1;

        /// <summary>
        /// Converts to ObjectWithChance<GameObject>.
        /// </summary>
        /// <returns>The converted object.</returns>
        public DictionaryTObjectChance<GameObject> ConvertToObjectWithChance()
        {
            return new DictionaryTObjectChance<GameObject>()
            {
                obj = this.obj,
                chance = this.chance
            };
        }
    }
    [Serializable]
    public class DictionaryNameListDictionaryGameObjectChance
    {
        public string name = "";
        public List<DictionaryGameObjectChance> objs = new List<DictionaryGameObjectChance>();

        /// <summary>
        /// Converts the List<GameObjectWithChance> to List<ObjectWithChance<GameObject>>.
        /// </summary>
        /// <returns>The converted list.</returns>
        public List<DictionaryTObjectChance<GameObject>> ConvertToObjectsWithChance()
        {
            if (objs == null || objs.Count == 0) return new List<DictionaryTObjectChance<GameObject>>();

            List<DictionaryTObjectChance<GameObject>> resutlingList = new List<DictionaryTObjectChance<GameObject>>();
            foreach (DictionaryGameObjectChance gameObjectChance in objs) resutlingList.Add(gameObjectChance.ConvertToObjectWithChance());

            return resutlingList;
        }
        /// <summary>
        /// Converts a List<GameObjectWithChance> to List<ObjectWithChance<GameObject>>.
        /// </summary>
        /// <param name="gameobjectsWithChance">The list to convert.</param>
        /// <returns>The converted list.</returns>
        public static List<DictionaryTObjectChance<GameObject>> ConvertToObjectsWithChance(List<DictionaryGameObjectChance> gameobjectsWithChance)
        {
            if (gameobjectsWithChance == null || gameobjectsWithChance.Count == 0) return new List<DictionaryTObjectChance<GameObject>>();

            List<DictionaryTObjectChance<GameObject>> resutlingList = new List<DictionaryTObjectChance<GameObject>>();
            foreach (DictionaryGameObjectChance gameObjectChance in gameobjectsWithChance) resutlingList.Add(gameObjectChance.ConvertToObjectWithChance());

            return resutlingList;
        }

        /// <summary>
        /// Converts a List<GameObjectList> to a Dictionary<string, List<ObjectWithChance<GameObject>>>.
        /// </summary>
        /// <param name="gameObjectLists">The list to be converted.</param>
        /// <returns>The converted list.</returns>
        public static Dictionary<string, List<DictionaryTObjectChance<GameObject>>> ConvertToDictionaryObjectsWithChance(List<DictionaryNameListDictionaryGameObjectChance> gameObjectLists)
        {
            if (gameObjectLists == null || gameObjectLists.Count == 0) return new Dictionary<string, List<DictionaryTObjectChance<GameObject>>>();

            Dictionary<string, List<DictionaryTObjectChance<GameObject>>> resutlingDict = new Dictionary<string, List<DictionaryTObjectChance<GameObject>>>();
            foreach (DictionaryNameListDictionaryGameObjectChance gameObject in gameObjectLists)
            {
                if (string.IsNullOrEmpty(gameObject.name) || gameObject.objs == null || gameObject.objs.Count == 0) continue;

                try
                {
                    resutlingDict.Add(gameObject.name, gameObject.ConvertToObjectsWithChance());
                }
                catch (ArgumentException e)
                {
                    Debug.LogError($"'{gameObject.name}' already exists in the given list of 'GameObjectList'!\n\n{e}");
                }
            }

            return resutlingDict;
        }
    }
}
